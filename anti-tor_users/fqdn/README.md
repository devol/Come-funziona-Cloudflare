# Kontraŭ-Tor uzantoj FQDN-listo [ℹ](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/ss/pct_at.php)


[//]: # (do not edit me; start)

## *150,382* FQDN

[//]: # (do not edit me; end)


- Ne ĉiuj uzantoj de Tor estas malbonaj. Ne punu ĉiujn.
  - Kiel vi sentas, se iu blokas vin sen kialo?
  - Uzi Tor ne estas krimo.
  - [Fakuloj diras, ke gruppuno estas senutila, kontraŭproduktiva, mallaborema kaj neetika](https://mypointexactly.wordpress.com/2009/07/21/group-punishment-ineffective-unethical/).
- Blokado de Tor ne estas solvo. Estas VPNj, retprogramoj kaj prokuroj.


![](../../image/anonexist.jpg)


- Bonvolu vidi [INSTRUCTION.md](../../INSTRUCTION.md) por dosiera celo kaj formato specifoj.
- [Malamikeco kontraŭ Tor](../../not_cloudflare/domains/README.md)


-----

# Anti-Tor users FQDN list

- Not all Tor users are bad. Do not punish everyone.
  - How do you feel if someone block you for no reason?
  - Using Tor is not a crime.
  - Experts say that group punishment is ineffective, counterproductive, lazy and unethical.
- Blocking Tor is not a solution. There are VPNs, network program and proxies.
- See [INSTRUCTION.md](../../INSTRUCTION.md) for file purpose and format specifications.
- [Hostility against Tor](../../not_cloudflare/domains/README.md)
