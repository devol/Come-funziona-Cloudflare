### Block Cloudflare MITM Attack

`Take action against Cloudflare`

![](../image/goodorbad.jpg)


```

This add-on will block, notify, or redirect your request if the target website is using Cloudflare.
Submit to global surveillance or resist. The choice is yours.
 
This add-on never send any data.
Your cloudflare-domain collection is yours.

```


Download add-on
- From Crimeflare (_Recommend_): [Firefox ESR / Chromium](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/)
- From Gitea (Delay Sync): [FirefoxESR](https://codeberg.org/crimeflare/cloudflare-tor/raw/branch/master/addons/releases/bcma.xpi) / [Chromium](https://codeberg.org/crimeflare/cloudflare-tor/raw/branch/master/addons/releases/bcma.crx)
