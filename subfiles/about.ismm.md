### Are links vulnerable to MITM attack?

`Scan FQDN using API`

![](../image/ismmpreview.jpg)


```

You′ve found something on the internet.
Are these links or images vulnerable to MITM attack or not?
 
This add-on is using[1] Crimeflare′s Public[4] API to scan[3] FQDN.
	e.g. https://ekzemplo.com/page.html → "ekzemplo.com"

This add-on never send other information.



[1] How to use offline database
	1. Open add-on's option page and select "Use Offline Local Database".
	2. Click "Database" link.
	3. Create a new text file[2] (or download text file from cloudflare-tor)
	4. Click "Import Database" and select your text file.
	5. Wait until the message appear.

[2] Text file example
	(FQDN/Domain)
	--------------------
	www.cloudflare.com
	domain.com
	example.org
	--------------------

[3] "Observe and Learn" mode
	If you don't want to use online public API, or don't want to maintain offline database
	this option is for you.
	You'll have to visit cloudflared website first because this add-on never make a
	request to websites.
	To activate this option, go to "Database" section and select "Use offline local database".
	To purge local database, go to "Database" section and click "Database", "Clear all data".

[4] Public API services
	You can select which API service you want to use.

```
 

Download add-on
- From Crimeflare (_Recommend_): [Firefox ESR / Chromium](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/)
- From Gitea (Delay Sync): [FirefoxESR](https://codeberg.org/crimeflare/cloudflare-tor/raw/branch/master/addons/releases/ismm.xpi) / [Chromium](https://codeberg.org/crimeflare/cloudflare-tor/raw/branch/master/addons/releases/ismm.crx)
