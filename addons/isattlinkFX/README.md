[//]: # (do not edit me; start)

Versio: _1.0.2.6_

[//]: # (do not edit me; end)


- [Pli informo / Elŝuto](../../subfiles/about.isat.md)
- Malgarantio: Ĉi tio NE estas spegulo de suprenflua deponejo. Sinkronigado eble prokrastas.

-----

- [More information / Download](../../subfiles/about.isat.md)
- Disclaimer: This is NOT a mirror of upstream repository. Synchronization may be delayed.
