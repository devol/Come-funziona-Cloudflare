# The Cloudwall Agung


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Lirén Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Great Cloudwall" nyaéta Cloudflare Inc., perusahaan A.S.Nyayogikeun jasa CDN (jaringan pangiriman eusi), mitigasi DDoS, kaamanan Internét, sareng jasa DNS (domain name server).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare mangrupikeun proxy MITM panglegana (proxy sabalikna) di dunya.Cloudflare gaduh langkung ti 80% pangsa pasar CDN sareng jumlah pangguna awan mérek ngembang unggal dinten.Aranjeunna parantos ngalegaan jaringanna ka langkung ti 100 nagara.Cloudflare ngagaduhan lalulintas wéb anu langkung saé tibatan Twitter, Amazon, Apple, Instagram, Bing & Wikipedia.Cloudflare nawiskeun rencana gratis sareng seueur jalma anu ngagunakeunana gaganti ngonfigurasi serverna leres.Aranjeunna padagang privasi tina langkung genah.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare calik antara anjeun sareng asal wéb, polah siga agén ronda wates.Anjeun teu tiasa nyambung ka tujuan anu anjeun dipilih.Anjeun nyambungkeun ka Cloudflare sareng sadaya inpormasi anjeun anu dikarang sareng diserahkeun ku laleur. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Pentadbir wéb anu asli ngamungkinkeun agén - Cloudflare - mutuskeun saha anu tiasa ngaksés kana "harta wéb" aranjeunna sareng ngartikeun "kawasan larangan".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Tingali kana gambar anu leres.Anjeun bakal mikir blok Cloudflare ngan ukur jalma jahat.Anjeun bakal mikir Cloudflare sok online (henteu kantos turun).Anjeun bakal mikir bot legit sareng panjelajah tiasa indéks situs wéb anjeun.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Nanging henteu leres-leres.Cloudflare ngahalang jalma anu polos kalayan henteu aya alesan.Cloudflare tiasa turun.Cloudflare blok blok bot.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Sama sapertos jasa palayanan anu mana waé, Cloudflare henteu sampurna.Anjeun tiasa ningali layar ieu sanaos upami server asal anu saé damel.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Naha anjeun nyangka Cloudflare ngagaduhan waktos 100%?Anjeun henteu kéngingkeun sabaraha kali Cloudflare turun.Upami Cloudflare turun para nasabah anjeun henteu tiasa ngaksés halaman wéb anjeun. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Disebutkeun ieu salaku rujukan ka Great Firewall of China anu ngalaksanakeun padamelan anu nyaring seueur manusa pikeun ningali eusi wéb (nyaéta sadayana di daratan Cina sareng jalma luar).Nalika dina waktos anu sami anu henteu kapangaruhan ningali wéb anu béda sacara dramatis, wéb anu gratis tina censorship sapertos gambar "tank man" sareng sejarah "protés Tiananmen Square". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare gaduh kakuatan anu hébat.Dina rasa, aranjeunna ngatur naon pamustunganana anu ditingali ku pamaké akhir.Anjeun dihalang tina ngotéktak wéb kusabab Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare tiasa dianggo kanggo ngawasan. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Anjeun teu tiasa ningali situs web cloudflared upami anjeun ngagunakeun browser anu alit anu Cloudflare panginten éta bot (sabab henteu seueur jalma anu nganggo). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Anjeun teu tiasa ngalebetkeun "cek panyungsi ieu" henteu ngamungkinkeun Javascript.Ieu mangrupikeun miceunan lima (atanapi langkung) detik tina kahirupan anjeun anu berharga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare ogé sacara otomatis meungpeuk legit legit / crawler sapertos para palanggan Google, Yandex, Yacy, sareng API.Cloudflare aktip ngawaskeun "bypass cloudflare" komunitas kalayan niat pikeun mutuskeun bot panalungtikan sah. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare sakaligus nyegah seueur jalma anu gaduh konektipitas internét anu goréng tina aksés kana situs wéb anu aya di tukangeunana (contona, aranjeunna tiasa tinggaleun 7+ lapisan NAT atanapi ngabagi IP anu sami, contona umum Wifi) kecuali aranjeunna ngabéréskeun sababaraha gambar CAPTCHA.Dina sababaraha kasus, ieu bakal nyandak 10 30 menit pikeun nyugemakeun Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Dina taun 2020 Cloudflare beralih ti Recaptcha Google ka hCaptcha sakumaha Google badé ngecas pikeun panggunaan na.Cloudflare nyarios yén anjeun paduli ka privasi anjeun ("éta ngabantosan masalah anu kabebilkeun dina kabijakan privasi") tapi ieu écés bohong.Éta sadayana ngeunaan artos."HCaptcha ngamungkinkeun situs web ngadamel artos ngaladenan paménta ieu bari ngotongkeun bot sareng bentuk nyiksa" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Tina sudut pandang pangguna, ieu henteu seueur robih. Anjeun kapaksa ngabéréskeun éta. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Seueur manusa sareng parangkat lunak anu diblokir ku Cloudflare unggal dinten. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare ngaganggu jalma-jalma di sakumna dunya.Tingali kana daptar sareng pikir naha nganut Cloudflare dina situs anjeun saé pikeun pangalaman pangguna. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Naon tujuanna internet lamun teu tiasa ngalakukeun naon anu anjeun pikahoyong?Kaseueuran jalma anu nganjang halaman wéb anjeun ngan ukur milarian halaman sanés upami henteu tiasa muka halaman wéb.Anjeun meureun moal aktip ngalangi sémah mana waé, tapi firewall standar Cloudflare ketat pikeun meungpeuk jalma réa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Teu aya deui jalan pikeun ngajawab captcha tanpa ngamungkinkeun Javascript sareng Cookies.Cloudflare ngagunakeun aranjeunna pikeun ngadamel tandatangan browser pikeun ngidentipikasi anjeun.Cloudflare kedah terang idéntitas anjeun pikeun mutuskeun naha anjeun layak pikeun neraskeun browsing situs éta. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Pamaké Tor sareng pangguna VPN ogé janten korban Cloudflare.Kadua solusi anu dianggo ku seueur jalma anu teu tiasa nampi internét anu teu kabayar kusabab kawijakan nagara / korporasi / jaringan na atanapi anu hoyong nambihan lapisan tambahan pikeun ngajagaan kabijakanana.Cloudflare teu isin teu nyerang jalma éta, maksa maranéhna pikeun mareuman solusi proksi na. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Upami anjeun henteu nyobaan Tor dugi waktos ieu, kami ajak anjeun undeuran Tor Browser sareng kunjungan halaman wéb favorit anjeun.Kami nyarankeun anjeun henteu login ka website bank anjeun atanapi halaman wéb pamaréntah atanapi aranjeunna bakal nyetak akun anjeun. Anggo VPN pikeun situs wéb éta. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Anjeun meureun hoyong saurna "Tor haram! Pangguna Tor sacara kriminal! Tor goréng! ". Teu.Anjeun panginten diajar ngeunaan Tor tina tivi, nyarios Tor tiasa dianggo pikeun ngotéktak blacknet sareng bedil dagang, obat-ubatan atanapi chid porno.Nalika pernyataan di luhur leres bilih aya seueur halaman wéb pasar dimana anjeun tiasa mésér barang sapertos ieu, situs-situs éta sering muncul dina clearnet ogé.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor dikembangkeun ku Angkatan Darat AS, tapi ayeuna Tor dikembangkeun ku proyék Tor.Aya seueur jalma sareng organisasi anu nganggo Tor kalebet babaturan anjeun ka hareup.Janten, upami anjeun nganggo Cloudflare dina halaman wéb anjeun nuju nyekat manusa nyata.Anjeun bakal leungit poténsi silaturahim sareng urusan bisnis. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Sareng jasa DNS na, 1.1.1.1, ogé nyaring pangguna tina nganjang kana situs web ku balik alamat IP palsu anu dipimilik ku Cloudflare, localhost IP sapertos "127.0.0.x", atanapi ngan ukur mulang. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS ogé ngarobih perangkat lunak online tina aplikasi smartphone kana kaulinan komputer kusabab jawaban DNS palsu maranéhanana.Cloudflare DNS teu tiasa naroskeun sababaraha situs wéb. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Sareng dieu anjeun panginten,<br>Kuring henteu nganggo Tor atanapi VPN, naha kuring kedah paduli?<br>Kuring percanten pamasaran Cloudflare, naha kuring kudu paduli<br>Situs wéb abdi nyaéta https naha kuring kudu paduli | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Upami anjeun ngadatangan situs wéb anu nganggo Cloudflare, anjeun milarian inpormasi henteu ngan ukur ka nu gaduh situs wéb tapi ogé Cloudflare.Ieu kumaha jalan proxy sabalikna dianggo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Mustahil pikeun nganalisa tanpa nyeken lalulintas TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare terang sadaya data anjeun sapertos sandi atah. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed tiasa lumangsung iraha waé. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https Cloudflare moal pernah tungtung-tungtung. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Naha anjeun hoyong bagikeun data anjeun sareng Cloudflare, sareng ogé agénsi 3-hurup? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Propil online Internét pangguna mangrupikeun "produk" anu dituju ku pamaréntah sareng perusahaan téknologi gedé. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Departemen Kaamanan Dalam Negeri A.S.:<br><br>Naha anjeun gaduh ide kumaha pentingna data anu anjeun? Naha aya cara anu anjeun bakal ngajual data ka urang?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare ogé nawiskeun jasa VPN FREE anu katelah "Cloudflare Warp".Upami anjeun ngagunakeun, sambungan sadayana smartphone (atanapi komputer) dikirim ka server Cloudflare.Cloudflare tiasa terang halaman mana anu anjeun parantos maca, mairan naon anu anjeun kirimkeun, anu anjeun parantos dicarioskeun, jsb.Anjeun sacara sukarela masihan sadaya inpormasi anjeun ka Cloudflare.Upami anjeun pikir "Naha anjeun guyonan? Cloudflare aman. ” maka anjeun kedah diajar kumaha VPN jalan. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare nyarios jasa VPN maranéhanana ngajantenkeun internét anjeun gancang.Tapi VPN ngajantenkeun sambungan internét anjeun langkung laun ti sambungan anu parantos aya. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Anjeun panginten bakal terang ngeunaan skandal PRISM.Memang leres anu AT&T ngamungkinkeun NSA nyalin sadaya data internét pikeun panjagaan. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Hayu urang tiasa damel di NSA, sareng anjeun hoyong profil internét unggal warga.Anjeun terang kalolobaanana dipercaya percanten Cloudflare sareng nganggo éta - ngan aya hiji gateway terpusat - pikeun proxy konéksi server perusahaanna (SSH / RDP), situs wéb pribadi, situs wéb, website wéb, website bank, website asuransi, mesin pencari, anggota rahasia situs web, situs lélang, balanja, situs wéb, situs wéb NSFW, sareng situs web anu haram.Anjeun ogé terang yén aranjeunna nganggo jasa DNS Cloudflare ("1.1.1.1") sareng jasa VPN ("Cloudflare Warp") pikeun "Aman! Leuwih gancang! Hadé! ” pangalaman internét.Kombinasikeun sareng alamat IP pangguna, sidik browser, cookies sareng RAY-ID bakal aya gunana pikeun ngawangun profil online target. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Anjeun hoyong data na. Naon anu anjeun bakal lakukeun? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare mangrupikeun honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Madu gratis kanggo sadayana. Sababaraha senar anu dipasang.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Entong nganggo Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Hatur internét.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Mangga neruskeun halaman hareup:  "[Etika Cloudflare](su.ethics.md)"

---

<details>
<summary>_malik kuring_

## Data sareng Inpormasi Langkung lengkep
</summary>


Repositori ieu mangrupikeun daptar situs wéb anu aya di tukangeun "The Great Cloudwall", ngahalang pangguna Tor sareng CDNs anu sanés.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Pangguna Cloudflare](../cloudflare_users/)
* [Cloudflare Domain](../cloudflare_users/domains/)
* [Pamaké CDN Non-Cloudflare](../not_cloudflare/)
* [Pamaké anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Langkung Inpormasi**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Unduh: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * EBook asli (ePUB) dihapus ku BookRix GmbH kusabab ngalanggar hak cipta bahan CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Tikét éta diruksak pisan sababaraha kali.
  * [Dipiceun ku Projek Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Tingali tikét 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Tikét tukang arsip 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_malik kuring_

## Naon nu bisa kumaneh di pigawe?
</summary>

* [Baca daptar tindakan urang anu dianjurkeun sareng bagikeun babaturan anjeun.](../ACTION.md)

* [Maca sora pangguna sanés sareng tuliskeun pikiran anjeun.](../PEOPLE.md)

* Milarian hiji hal: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Ngapdét daptar domain: [Daptar paréntah](../INSTRUCTION.md).

* [Tambihkeun Cloudflare atanapi acara anu aya hubunganana sareng sajarah.](../HISTORY.md)

* [Coba & nulis Alat / Tulisan anyar.](../tool/)

* [Ieu sababaraha PDF / ePUB pikeun dibaca.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Ngeunaan akun palsu

Crimeflare terang ngeunaan ayana akun palsu anu nyababkeun saluran resmi kami, nya éta Twitter, Facebook, Patreon, OpenCollective, Kalayan jsb.
**Kami henteu pernah naroskeun ka email anjeun.
Kami henteu kantos naroskeun nami anjeun.
Kami moal naroskeun idéntitas anjeun.
Kami henteu kantos naroskeun lokasi anjeun.
Kami henteu kantos naroskeun ka anjeun.
Kami henteu kantos naroskeun ulasan anjeun.
Kami moal pernah nanyakeun anjeun ngiringan dina média sosial.
Kami moal pernah nanyakeun média sosial anjeun.**

# TANPA PERCAKAPAN AKAUN AKAN.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)