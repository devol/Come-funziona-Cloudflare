# De grote Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Stop Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Great Cloudwall" is Cloudflare Inc., het Amerikaanse bedrijf.Het biedt CDN-services (Content Delivery Network), DDoS-beperking, internetbeveiliging en gedistribueerde DNS-services (Domain Name Server).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare is 's werelds grootste MITM-proxy (reverse proxy).Cloudflare bezit meer dan 80% van het CDN-marktaandeel en het aantal cloudflare-gebruikers groeit elke dag.Ze hebben hun netwerk uitgebreid naar meer dan 100 landen.Cloudflare bedient meer webverkeer dan Twitter, Amazon, Apple, Instagram, Bing en Wikipedia samen.Cloudflare biedt een gratis abonnement en veel mensen gebruiken het in plaats van hun servers correct te configureren.Ze ruilden privacy in plaats van gemak.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare zit tussen jou en de oorspronkelijke webserver in en gedraagt ​​zich als een grensbewakingsagent.U kunt geen verbinding maken met de door u gekozen bestemming.U maakt verbinding met Cloudflare en al uw informatie wordt on-the-fly gedecodeerd en overgedragen. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  De oorspronkelijke webserverbeheerder stond de agent - Cloudflare - toe om te beslissen wie toegang heeft tot hun "webproperty" en om "beperkt gebied" te definiëren.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Bekijk de juiste afbeelding.Je zult denken dat Cloudflare alleen slechteriken blokkeert.Je zult denken dat Cloudflare altijd online is (nooit down).U zult denken dat legitieme bots en crawlers uw website kunnen indexeren.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Die zijn echter helemaal niet waar.Cloudflare blokkeert onschuldige mensen zonder reden.Cloudflare kan dalen.Cloudflare blokkeert legitieme bots.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Net als elke andere hostingservice is Cloudflare niet perfect.U zult dit scherm zelfs zien als de oorspronkelijke server goed werkt.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Denk je echt dat Cloudflare 100% uptime heeft?Je hebt geen idee hoe vaak Cloudflare uitvalt.Als Cloudflare uitvalt, heeft uw klant geen toegang tot uw website. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Het wordt dit genoemd met verwijzing naar de Grote Firewall van China, die een vergelijkbare taak heeft om veel mensen te filteren op het zien van webinhoud (dwz iedereen op het vasteland van China en mensen daarbuiten).Terwijl degenen die er niet bij betrokken zijn, tegelijkertijd een drastisch ander web zien, een web zonder censuur zoals een afbeelding van 'tankman' en de geschiedenis van 'protesten op het Tiananmenplein'. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare heeft een grote kracht.In zekere zin bepalen ze wat de eindgebruiker uiteindelijk ziet.U kunt door Cloudflare niet browsen op de website. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare kan worden gebruikt voor censuur. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  U kunt de cloudflared-website niet bekijken als u een kleine browser gebruikt waarvan Cloudflare denkt dat het een bot is (omdat niet veel mensen deze gebruiken). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  U kunt deze invasieve "browsercontrole" niet doorstaan ​​zonder Javascript in te schakelen.Dit is een verspilling van vijf (of meer) seconden van uw waardevolle leven. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare blokkeert ook automatisch legitieme robots / crawlers zoals Google-, Yandex-, Yacy- en API-clients.Cloudflare houdt actief toezicht op de "bypass cloudflare" -gemeenschap met de bedoeling om legitieme onderzoeksrobots te breken. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare voorkomt op dezelfde manier dat veel mensen met een slechte internetverbinding toegang krijgen tot de websites erachter (ze kunnen bijvoorbeeld achter 7+ lagen NAT zitten of hetzelfde IP-adres delen, bijvoorbeeld openbare wifi), tenzij ze CAPTCHA's met meerdere afbeeldingen oplossen.In sommige gevallen duurt dit 10 tot 30 minuten om Google tevreden te stellen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  In het jaar 2020 schakelde Cloudflare over van Google’s Recaptcha naar hCaptcha, aangezien Google van plan is kosten in rekening te brengen voor het gebruik ervan.Cloudflare heeft je verteld dat ze om je privacy geven ("het helpt bij het aanpakken van een privacyprobleem") maar dit is duidelijk een leugen.Het draait allemaal om geld."Met hCaptcha kunnen websites geld verdienen aan deze vraag terwijl bots en andere vormen van misbruik worden geblokkeerd" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Vanuit het perspectief van de gebruiker verandert dit niet veel. Je wordt gedwongen het op te lossen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Veel mensen en software worden elke dag geblokkeerd door Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare ergert veel mensen over de hele wereld.Bekijk de lijst en bedenk of het gebruik van Cloudflare op uw site goed is voor de gebruikerservaring. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Wat is het doel van internet als u niet kunt doen wat u wilt?De meeste mensen die uw website bezoeken, zoeken alleen naar andere pagina's als ze een webpagina niet kunnen laden.U blokkeert misschien niet actief bezoekers, maar de standaardfirewall van Cloudflare is streng genoeg om veel mensen te blokkeren. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Er is geen manier om de captcha op te lossen zonder Javascript en cookies in te schakelen.Cloudflare gebruikt ze om een ​​browserhandtekening te maken om u te identificeren.Cloudflare heeft uw identiteit nodig om te beslissen of u in aanmerking komt om door te gaan met browsen op de site. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor-gebruikers en VPN-gebruikers zijn ook het slachtoffer van Cloudflare.Beide oplossingen worden gebruikt door veel mensen die zich geen ongecensureerd internet kunnen veroorloven vanwege hun land / bedrijf / netwerkbeleid of die een extra laag willen toevoegen om hun privacy te beschermen.Cloudflare valt die mensen schaamteloos aan en dwingt hen hun proxy-oplossing uit te schakelen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Als je Tor nog niet hebt geprobeerd, raden we je aan Tor Browser te downloaden en je favoriete websites te bezoeken.We raden u aan om niet in te loggen op de website van uw bank of de webpagina van de overheid, anders wordt uw account gemarkeerd. Gebruik VPN voor die websites. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Je zou kunnen zeggen “Tor is illegaal! Tor-gebruikers zijn crimineel! Tor is slecht! ". Nee.Je hebt misschien van de televisie over Tor gehoord en zegt dat Tor kan worden gebruikt om door darknet te bladeren en wapens, drugs of kinderporno te verhandelen.Hoewel bovenstaande bewering waar is dat er veel marktwebsites zijn waar u dergelijke items kunt kopen, worden die sites vaak ook op clearnet weergegeven.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor is ontwikkeld door het Amerikaanse leger, maar de huidige Tor is ontwikkeld door het Tor-project.Er zijn veel mensen en organisaties die Tor gebruiken, inclusief je toekomstige vrienden.Dus als u Cloudflare op uw website gebruikt, blokkeert u echte mensen.U verliest potentiële vriendschap en zakelijke deal. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  En hun DNS-service, 1.1.1.1, filtert ook gebruikers uit het bezoeken van de website door een nep IP-adres van Cloudflare, localhost IP zoals "127.0.0.x" te retourneren of gewoon niets terug te sturen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS breekt ook online software van smartphone-app naar computerspel vanwege hun nep DNS-antwoord.Cloudflare DNS kan sommige bankwebsites niet opvragen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  En hier denk je misschien<br>Ik gebruik geen Tor of VPN, waarom zou het mij iets kunnen schelen?<br>Ik vertrouw Cloudflare-marketing, waarom zou het mij iets kunnen schelen<br>Mijn website is https waarom zou het mij iets kunnen schelen | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Als u een website bezoekt die Cloudflare gebruikt, deelt u uw informatie niet alleen met de eigenaar van de website, maar ook met Cloudflare.Dit is hoe de omgekeerde proxy werkt. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Het is onmogelijk om te analyseren zonder TLS-verkeer te decoderen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare kent al uw gegevens zoals onbewerkt wachtwoord. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed kan op elk moment gebeuren. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  De https van Cloudflare is nooit end-to-end. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Wil je jouw data echt delen met Cloudflare, en ook met 3-letter agency? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Het online profiel van de internetgebruiker is een 'product' dat de overheid en grote technologiebedrijven willen kopen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Zei het Amerikaanse ministerie van Binnenlandse Veiligheid:<br><br>Heeft u enig idee hoe waardevol de gegevens die u heeft, zijn? Is er een manier waarop u ons die gegevens zou verkopen?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare biedt ook GRATIS VPN-service genaamd "Cloudflare Warp".Als u het gebruikt, worden al uw smartphone (of uw computer) verbindingen naar Cloudflare-servers gestuurd.Cloudflare kan weten welke website u heeft gelezen, welke opmerking u heeft geplaatst, met wie u heeft gesproken, enz.U geeft vrijwillig al uw informatie aan Cloudflare.Als je denkt "Maak je een grapje? Cloudflare is veilig. " dan moet je leren hoe VPN werkt. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare zei dat hun VPN-service uw internet snel maakt.Maar VPN maakt uw internetverbinding langzamer dan uw bestaande verbinding. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Misschien weet u al van het PRISM-schandaal.Het is waar dat AT&T de NSA toestaat alle internetgegevens te kopiëren voor bewaking. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Stel dat u bij de NSA werkt en dat u het internetprofiel van elke burger wilt.U weet dat de meesten van hen Cloudflare blindelings vertrouwen en het gebruiken - slechts één gecentraliseerde gateway - om hun bedrijfsserververbinding (SSH / RDP), persoonlijke website, chatwebsite, forumwebsite, bankwebsite, verzekeringswebsite, zoekmachine, geheim lid te proxy -alleen website, veilingwebsite, winkel-, videowebsite, NSFW-website en illegale website.U weet ook dat ze de DNS-service ("1.1.1.1") en VPN-service ("Cloudflare Warp") van Cloudflare gebruiken voor "Secure! Sneller! Beter!" internetervaring.Het combineren met het IP-adres van de gebruiker, de vingerafdruk van de browser, cookies en RAY-ID zal nuttig zijn om het online profiel van het doelwit op te bouwen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  U wilt hun gegevens. Wat ga je doen? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare is een honingpot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Gratis honing voor iedereen. Enkele touwtjes eraan.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Gebruik Cloudflare niet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Decentraliseer het internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Ga door naar de volgende pagina:  "[Cloudflare Ethiek](nl.ethics.md)"

---

<details>
<summary>_Klik hier_

## Gegevens en meer informatie
</summary>


Deze repository is een lijst met websites die achter "The Great Cloudwall" staan ​​en Tor-gebruikers en andere CDN's blokkeren.


**Gegevens**
* [Van Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare-gebruikers](../cloudflare_users/)
* [Cloudflare-domeinen](../cloudflare_users/domains/)
* [Niet-Cloudflare CDN-gebruikers](../not_cloudflare/)
* [Anti-Tor-gebruikers](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Meer informatie**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Downloaden: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Het originele eBook (ePUB) is verwijderd door BookRix GmbH vanwege copyright-inbreuk op CC0-materiaal
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Het kaartje is zo vaak vernield.
  * [Verwijderd door het Tor-project.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Zie ticket 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Laatste archiefticket 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_Klik hier_

## Wat kan je doen?
</summary>

* [Lees onze lijst met aanbevolen acties en deel deze met je vrienden.](../ACTION.md)

* [Lees de stem van een andere gebruiker en schrijf uw gedachten op.](../PEOPLE.md)

* Zoek iets: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Werk de domeinlijst bij: [Lijst instructies](../INSTRUCTION.md).

* [Voeg Cloudflare of projectgerelateerde gebeurtenis toe aan de geschiedenis.](../HISTORY.md)

* [Probeer en schrijf een nieuwe tool / script.](../tool/)

* [Hier is wat PDF / ePUB om te lezen.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Over nepaccounts

Crimeflare is op de hoogte van het bestaan ​​van nepaccounts die zich voordoen als onze officiële kanalen, of het nu Twitter, Facebook, Patreon, OpenCollective, Villages etc. is.
**We vragen nooit uw e-mail.
We vragen nooit uw naam.
We vragen nooit uw identiteit.
We vragen nooit uw locatie.
We vragen nooit uw donatie.
We vragen nooit uw beoordeling.
We vragen je nooit om te volgen op sociale media.
We vragen nooit uw sociale media.**

# VERTROUW GEEN VALSE ACCOUNTS.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)