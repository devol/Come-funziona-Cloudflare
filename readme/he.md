# הענן הגדול


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## עצור את הענן


|  🖹  |  🖼 |
| --- | --- |
|  "ה- Cloudwall הגדול" הוא Cloudflare Inc., החברה האמריקאית.היא מספקת שירותי CDN (רשת משלוח תוכן), הפחתת DDoS, אבטחת אינטרנט ושירותי DNS (שרת שמות דומיין) מבוזרים.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare הוא ה- MITM Proxy הגדול בעולם (פרוקסי הפוך).Cloudflare מחזיקה ביותר מ- 80% מנתח השוק של CDN ומספר משתמשי ה- Cloudflare גדל מדי יום.הם הרחיבו את הרשת שלהם ליותר ממאה מדינות.Cloudflare משרת יותר תנועה באינטרנט מאשר טוויטר, אמזון, אפל, אינסטגרם, בינג וויקיפדיה יחד.Cloudflare מציע תוכנית בחינם ואנשים רבים משתמשים בה במקום להגדיר את השרתים שלהם כראוי.הם סחרו בפרטיות בגלל נוחות.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare יושב בינך לבין שרת האינטרנט המקורי, מתנהג כמו סוכן סיור גבול.אינך מצליח להתחבר ליעד שבחרת.אתה מתחבר ל- Cloudflare וכל המידע שלך מפוענח ונמסר תוך כדי תנועה. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  מנהל שרת האינטרנט המקורי איפשר לסוכן - Cloudflare - להחליט מי יכול לגשת ל"רכוש האינטרנט "שלהם ולהגדיר" אזור מוגבל ".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  התבונן בתמונה הנכונה.אתם תחשבו ש- Cloudflare חוסם רק רעים.אתם תחשבו ש- Cloudflare תמיד מקוון (לעולם אל תרדו).אתם תחשבו שבוטים וסורקים חוקיים יכולים להוסיף אינדקס לאתר שלכם.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  עם זאת אלה אינם נכונים כלל.Cloudflare חוסם אנשים חפים מפשע ללא סיבה.הענן יכול לרדת.Cloudflare חוסם בוטים לגיטימיים.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  ממש כמו כל שירות אירוח, Cloudflare אינו מושלם.תראה מסך זה גם אם שרת המקור עובד טוב.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  האם אתה באמת חושב ש- Cloudflare יש זמן של 100%?אין לך מושג כמה פעמים Cloudflare יורד.אם Cloudflare יורד הלקוח שלך לא יכול לגשת לאתר שלך. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  זה נקרא כך בהתייחסות לחומת האש הגדולה של סין שעושה עבודה דומה לסינון בני אדם רבים מלראות תוכן באינטרנט (כלומר, כולם בסין היבשתית ואנשים בחוץ).בעוד שבאותה עת אלה שלא הושפעו רואים רשת שונה באופן דרטי, רשת נטולת צנזורה כמו דימוי של "איש טנק" והיסטוריה של "מחאות כיכר טיינאנמן". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare הוא בעל כוח רב.במובן מסוים הם שולטים במה שמשתמש הקצה רואה בסופו של דבר.מנועה ממך לגלוש באתר בגלל Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  ניתן להשתמש בענן הענן לצנזורה. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  אינך יכול להציג אתר Cloudflared אם אתה משתמש בדפדפן מינורי אשר Cloudflare עשוי לחשוב שהוא בוט (מכיוון שלא רבים משתמשים בו). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  אינך יכול לעבור "בדיקת דפדפן" פולשנית זו מבלי להפעיל את Javascript.זה בזבוז של חמש שניות (או יותר) מחייך היקרים. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare גם חוסם אוטומטית רובוטים / סורקים לגיטימיים כמו לקוחות Google, Yandex, Yacy ו- API.Cloudflare עוקב באופן פעיל אחר קהילת "עקיפה של עננים פרטיים" מתוך כוונה לשבור בוטים של מחקר חוקי. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare באופן דומה מונע מאנשים רבים שיש להם קישוריות אינטרנט לקויה לגשת לאתרי האינטרנט שמאחוריה (לדוגמה, הם עשויים להיות מאחורי 7+ שכבות NAT או לשתף אותה IP, למשל Wifi ציבורי) אלא אם כן הם פותרים CAPTCHAs תמונה מרובים.בחלק מהמקרים זה ייקח 10 עד 30 דקות כדי לספק את גוגל. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  בשנת 2020 Cloudflare עבר מ- Recaptcha של גוגל ל- hCaptcha מכיוון שגוגל מתכוונת לגבות תשלום עבור השימוש בו.Cloudflare אמר לך שאכפת להם מהפרטיות שלך ("זה עוזר לטפל בדאגה לפרטיות") אך ברור שזה שקר.הכל קשור לכסף."HCaptcha מאפשר לאתרים להרוויח כסף המשרת את הדרישה הזו תוך חסימת בוטים וצורות התעללות אחרות" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  מבחינת המשתמש זה לא משתנה הרבה. אתה נאלץ לפתור את זה. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  אנשים ותוכנות רבים נחסמים על ידי Cloudflare כל יום. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare מרגיז אנשים רבים ברחבי העולם.התבונן ברשימה וחשוב אם אימוץ Cloudflare באתר שלך טוב לחוויית המשתמש. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  מה מטרת האינטרנט אם אינך יכול לעשות מה שאתה רוצה?רוב האנשים שפוקדים את האתר שלך יחפשו דפים אחרים אם הם לא יכולים לטעון דף אינטרנט.ייתכן שאתה לא חוסם מבקרים באופן פעיל, אך חומת האש של ברירת המחדל של Cloudflare מחמירה מספיק כדי לחסום אנשים רבים. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  אין דרך לפתור את הקאפטצ'ה מבלי לאפשר Javascript ועוגיות.Cloudflare משתמש בהם כדי ליצור חתימת דפדפן כדי לזהות אותך.Cloudflare צריך לדעת את זהותך כדי להחליט אם אתה זכאי להמשיך לגלוש באתר. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  משתמשי Tor ומשתמשי VPN הם גם קורבן של Cloudflare.שני הפתרונות משמשים אנשים רבים שאינם יכולים להרשות לעצמם אינטרנט לא מצונזר בגלל מדיניות המדינה / התאגיד / הרשת שלהם או שרוצים להוסיף רובד נוסף לשמירה על פרטיותם.Cloudflare תוקף ללא בושה את האנשים האלה, מכריח אותם לכבות את פיתרון ה- Proxy שלהם. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  אם לא ניסית את Tor עד לרגע זה, אנו ממליצים לך להוריד את Tor Browser ולבקר באתרי האינטרנט המועדפים עליך.אנו ממליצים לך לא להיכנס לאתר הבנק שלך או לדף האינטרנט הממשלתי, אחרת הם יסמנו את חשבונך. השתמש ב- VPN עבור אותם אתרים. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  אולי תרצו לומר "טור הוא לא חוקי! משתמשי Tor הם פליליים! טור הוא רע! ". לא.אולי למדת על טור מהטלוויזיה, ואמר שאתה יכול להשתמש בטור כדי לגלוש ברשת כהה ולסחר בתותחים, סמים או פורנו לרשת.אמנם ההצהרה שלמעלה נכונה שישנם אתרי אינטרנט רבים בשוק בהם תוכלו לקנות פריטים כאלה, אך אתרים אלה מופיעים לרוב גם ב- clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  טור פותח על ידי צבא ארה"ב, אך טור הנוכחי מפותח על ידי פרויקט טור.ישנם אנשים וארגונים רבים שמשתמשים בתור כולל חברים שלך לעתיד.לכן, אם אתה משתמש ב- Cloudflare באתר האינטרנט שלך אתה חוסם בני אדם אמיתיים.תאבד חברות פוטנציאלית ועסקה עסקית. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  ושירות ה- DNS שלהם, 1.1.1.1, מסנן גם משתמשים מביקור באתר באמצעות החזרת כתובת IP מזויפת בבעלות Cloudflare, IP של localhost כמו "127.0.0.x", או פשוט לא מחזיר דבר. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  ה- Cloudflare DNS גם מפרק תוכנה מקוונת מאפליקציית סמארטפון למשחק מחשב בגלל תשובת ה- DNS המזויפת שלהם.DNS Cloudflare אינו יכול לבצע שאילתות בכמה אתרי בנק. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  וכאן אתה עשוי לחשוב,<br>אני לא משתמש ב- Tor או ב- VPN, מדוע אכפת לי?<br>אני סומך על שיווק Cloudflare, מדוע אכפת לי<br>האתר שלי הוא https מדוע אכפת לי | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  אם אתה מבקר באתר המשתמש ב- Cloudflare, אתה משתף את המידע שלך לא רק לבעלי האתר אלא גם Cloudflare.כך עובד ה- proxy ההפוך. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  אי אפשר לנתח בלי לפענח את תעבורת TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare מכיר את כל הנתונים שלך, כגון סיסמא גולמית. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  עננים יכולים לקרות בכל עת. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https של Cloudflare לעולם אינו מקצה לקצה. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  האם אתה באמת רוצה לשתף את הנתונים שלך עם Cloudflare, וגם סוכנות בת 3 אותיות? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  הפרופיל המקוון של משתמש האינטרנט הוא "מוצר" שהממשלה וחברות הטכנולוגיה הגדולות מעוניינות לקנות. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  כך נמסר מהמחלקה האמריקאית לביטחון פנים:<br><br>האם יש לך מושג כמה הנתונים שלך יקרי ערך? האם יש דרך שתמכור לנו את הנתונים האלה?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare מציעים גם שירות VPN בחינם הנקרא "Cloudflare Warp".אם אתה משתמש בו, כל חיבורי הטלפון החכם (או המחשב שלך) נשלחים לשרתי Cloudflare.Cloudflare יכול לדעת איזה אתר קראת, איזו תגובה פרסמת, עם מי דיברת וכו '.אתה מרצון למסור את כל המידע שלך ל- Cloudflare.אם אתה חושב "אתה מתבדח? Cloudflare מאובטח. " אז אתה צריך ללמוד כיצד VPN עובד. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare אמר כי שירות ה- VPN שלהם הופך את האינטרנט שלך למהיר.אבל VPN הופך את חיבור האינטרנט לאיטי יותר מהקשר שלך הקיים. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  אולי כבר ידעת על שערוריית ה- PRISM.נכון AT&T מאפשר ל- NSA להעתיק את כל נתוני האינטרנט למעקב. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  נניח שאתה עובד ב- NSA, ואתה מעוניין בפרופיל האינטרנט של כל אזרח.אתה יודע שרובם סומכים באופן עיוור על Cloudflare ומשתמשים בו - רק שער מרכזי אחד - כדי לפרוק את חיבור שרת החברה שלהם (SSH / RDP), אתר אישי, אתר צ'אט, אתר פורום, אתר בנק, אתר ביטוח, מנוע חיפוש, חבר סודי אתר אינטרנט בלבד, מכירה פומבית, קניות, אתר וידאו, אתר NSFW ואתר לא חוקי.אתה גם יודע שהם משתמשים בשירות ה- DNS של Cloudflare ("1.1.1.1") ובשירות VPN ("עיוות Cloudflare") לצורך "מאובטח! מהר יותר! טוב יותר!" חווית אינטרנט.שילובם עם כתובת ה- IP של המשתמש, טביעת אצבע של הדפדפן, קובצי Cookie ומזהה RAY יעיל לבניית פרופיל המקוון של היעד. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  אתה רוצה את הנתונים שלהם. מה תעשה? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare הוא דבש.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **דבש חינם לכולם. כמה מיתרים צמודים.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **אל תשתמש ב- Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **ביזר את האינטרנט.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    המשך לדף הבא:  "[אתיקה של פרח ענן](he.ethics.md)"

---

<details>
<summary>_לחץ עלי_

## נתונים ומידע נוסף
</summary>


מאגר זה הוא רשימה של אתרים העומדים מאחורי "The Cloudwall הגדול", וחוסמים את משתמשי Tor ו- CDNs אחרים.


**נתונים**
* [Cloudflare בע"מ](../cloudflare_inc/)
* [משתמשי Cloudflare](../cloudflare_users/)
* [תחומי ענן](../cloudflare_users/domains/)
* [משתמשי CDN שאינם עננים](../not_cloudflare/)
* [משתמשי אנטי טור](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**עוד מידע**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * הורד: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * הספר האלקטרוני המקורי (ePUB) נמחק על ידי BookRix GmbH עקב הפרת זכויות יוצרים של חומר CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * הכרטיס הושחת כל כך הרבה פעמים.
  * [נמחק על ידי פרויקט טור.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [ראו כרטיס 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [כרטיס ארכיב אחרון 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_לחץ עלי_

## מה אתה יכול לעשות?
</summary>

* [קרא את רשימת הפעולות המומלצות שלנו ושתף אותה עם חבריך.](../ACTION.md)

* [קרא את הקול של משתמש אחר וכתוב את מחשבותיך.](../PEOPLE.md)

* חפש משהו: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* עדכן את רשימת הדומיינים: [רשימת הוראות](../INSTRUCTION.md).

* [הוסף היסטוריית Cloudflare או אירוע הקשור לפרויקט.](../HISTORY.md)

* [נסה וכתוב כלי / סקריפט חדש.](../tool/)

* [להלן PDF / ePUB לקריאה.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### על חשבונות מזויפים

Crimeflare יודע על קיומם של חשבונות מזויפים המתחזים לערוצים הרשמיים שלנו, יהיה זה טוויטר, פייסבוק, פטרון, OpenCollective, כפרים וכו '.
**לעולם לא נשאל את הדוא"ל שלך.
אנחנו אף פעם לא שואלים את שמך.
אנחנו אף פעם לא שואלים את זהותך.
אנחנו אף פעם לא שואלים את המיקום שלך.
אנחנו אף פעם לא מבקשים את התרומה שלך.
לעולם לא נשאל את הביקורת שלך.
אנחנו אף פעם לא מבקשים מכם לעקוב במדיה החברתית.
לעולם איננו שואלים את המדיה החברתית שלכם.**

# אל תבטח בחשבונות מזויפים.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)