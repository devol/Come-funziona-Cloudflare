# Cloudwall Hebat


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Hentikan Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Great Cloudwall" adalah Cloudflare Inc., syarikat A.S.Ia menyediakan perkhidmatan CDN (rangkaian penghantaran kandungan), mitigasi DDoS, keamanan Internet, dan perkhidmatan DNS (domain name server) yang diedarkan.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare adalah proksi MITM terbesar (proksi terbalik) di dunia.Cloudflare memiliki lebih daripada 80% bahagian pasaran CDN dan jumlah pengguna cloudflare bertambah setiap hari.Mereka telah mengembangkan rangkaian mereka ke lebih dari 100 negara.Cloudflare melayani lebih banyak lalu lintas web daripada gabungan Twitter, Amazon, Apple, Instagram, Bing & Wikipedia.Cloudflare menawarkan rancangan percuma dan banyak orang menggunakannya dan bukannya mengkonfigurasi pelayan mereka dengan betul.Mereka menukar privasi atas kemudahan.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare berada di antara anda dan pelayan laman web asal, bertindak seperti ejen peronda sempadan.Anda tidak dapat menyambung ke destinasi pilihan anda.Anda menyambung ke Cloudflare dan semua maklumat anda sedang didekripsi dan diserahkan dengan cepat. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Pentadbir pelayan web asal membenarkan ejen - Cloudflare - untuk memutuskan siapa yang dapat mengakses ke "harta web" mereka dan menentukan "kawasan larangan".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Lihat gambar yang betul.Anda akan menganggap Cloudflare menyekat hanya orang jahat.Anda akan menganggap Cloudflare sentiasa dalam talian (jangan turun).Anda akan berfikir bot sah dan perayap dapat mengindeks laman web anda.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Walau bagaimanapun, semua itu tidak benar.Cloudflare menyekat orang yang tidak bersalah tanpa alasan.Cloudflare boleh turun.Cloudflare menyekat bot yang sah.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Sama seperti perkhidmatan hosting, Cloudflare tidak sempurna.Anda akan melihat skrin ini walaupun pelayan asal berfungsi dengan baik.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Adakah anda benar-benar berpendapat bahawa Cloudflare mempunyai masa operasi 100%?Anda tidak tahu berapa kali Cloudflare turun.Sekiranya Cloudflare turun, pelanggan anda tidak dapat mengakses laman web anda. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Ini disebut sebagai merujuk kepada Great Firewall of China yang melakukan pekerjaan setanding dengan menyaring banyak manusia daripada melihat kandungan web (iaitu semua orang di daratan China dan orang di luar).Sementara pada saat yang sama mereka yang tidak terpengaruh untuk melihat web yang berbeda secara dramatis, web yang bebas daripada penapisan seperti gambar "tank man" dan sejarah "tunjuk perasaan Tiananmen Square". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare mempunyai kekuatan yang hebat.Dari satu segi, mereka mengawal apa yang akhirnya dilihat oleh pengguna akhir.Anda dilarang melayari laman web kerana Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare boleh digunakan untuk penapisan. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Anda tidak dapat melihat laman web cloudflared jika anda menggunakan penyemak imbas kecil yang mungkin dianggap Cloudflare sebagai bot (kerana tidak banyak orang menggunakannya). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Anda tidak boleh lulus "pemeriksaan penyemak imbas" invasif ini tanpa mengaktifkan Javascript.Ini membuang masa lima (atau lebih) saat berharga anda. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare juga secara automatik menyekat robot / crawler yang sah seperti klien Google, Yandex, Yacy, dan API.Cloudflare secara aktif memantau komuniti "bypass cloudflare" dengan niat untuk mematahkan bot penyelidikan yang sah. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare juga menghalang banyak orang yang mempunyai kesambungan internet yang lemah dari mengakses laman web di belakangnya (contohnya, mereka mungkin berada di belakang 7+ lapisan NAT atau berkongsi IP yang sama, misalnya Wifi awam) kecuali mereka menyelesaikan banyak CAPTCHA gambar.Dalam beberapa kes, ini memerlukan masa 10 hingga 30 minit untuk memuaskan Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Pada tahun 2020 Cloudflare beralih dari Recaptcha Google ke hCaptcha kerana Google berhasrat untuk mengenakan bayaran untuk penggunaannya.Cloudflare memberitahu anda bahawa mereka menjaga privasi anda ("ini membantu mengatasi masalah privasi") tetapi ini jelas bohong.Ini semua mengenai wang."HCaptcha membolehkan laman web menghasilkan wang yang memenuhi permintaan ini sambil menyekat bot dan bentuk penyalahgunaan lain" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Dari perspektif pengguna, ini tidak banyak berubah. Anda terpaksa menyelesaikannya. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Banyak manusia dan perisian disekat oleh Cloudflare setiap hari. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare mengganggu banyak orang di seluruh dunia.Lihatlah senarai dan fikirkan sama ada penggunaan Cloudflare di laman web anda sesuai untuk pengalaman pengguna. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Apa tujuan internet jika anda tidak dapat melakukan apa yang anda mahukan?Sebilangan besar orang yang melayari laman web anda hanya akan mencari halaman lain sekiranya mereka tidak dapat memuatkan laman web.Anda mungkin tidak menyekat pengunjung secara aktif, tetapi firewall lalai Cloudflare cukup ketat untuk menyekat banyak orang. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Tidak ada cara untuk menyelesaikan captcha tanpa mengaktifkan Javascript dan Cookies.Cloudflare menggunakannya untuk membuat tandatangan penyemak imbas untuk mengenal pasti anda.Cloudflare perlu mengetahui identiti anda untuk memutuskan sama ada anda layak untuk terus melayari laman web ini. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Pengguna Tor dan pengguna VPN juga menjadi mangsa Cloudflare.Kedua-dua penyelesaian itu digunakan oleh banyak orang yang tidak mampu menggunakan internet tanpa sensor kerana dasar negara / syarikat / rangkaian mereka atau yang ingin menambahkan lapisan tambahan untuk melindungi privasi mereka.Cloudflare tanpa malu-malu menyerang orang-orang itu, memaksa mereka mematikan penyelesaian proksi mereka. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Sekiranya anda tidak mencuba Tor hingga saat ini, kami menggalakkan anda memuat turun Tor Browser dan melayari laman web kegemaran anda.Kami mencadangkan anda untuk tidak masuk ke laman web bank atau laman web kerajaan anda atau mereka akan menandakan akaun anda. Gunakan VPN untuk laman web tersebut. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Anda mungkin ingin mengatakan "Tor tidak sah! Pengguna Tor adalah penjenayah! Tor buruk! ". Tidak.Anda mungkin mengetahui mengenai Tor dari televisyen, mengatakan Tor boleh digunakan untuk melayari darknet dan menukar senjata, dadah atau pornografi kanak-kanak.Walaupun pernyataan di atas adalah benar bahawa terdapat banyak laman web pasaran di mana anda boleh membeli barang-barang tersebut, laman web tersebut juga sering muncul di clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor dikembangkan oleh US Army, tetapi Tor semasa dikembangkan oleh projek Tor.Terdapat banyak orang dan organisasi yang menggunakan Tor termasuk rakan masa depan anda.Oleh itu, jika anda menggunakan Cloudflare di laman web anda, anda menyekat manusia sebenar.Anda akan kehilangan hubungan persahabatan dan perniagaan yang berpotensi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Dan perkhidmatan DNS mereka, 1.1.1.1, juga menyaring pengguna dari mengunjungi laman web dengan mengembalikan alamat IP palsu yang dimiliki oleh Cloudflare, IP localhost seperti "127.0.0.x", atau tidak mengembalikan apa-apa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS juga memutuskan perisian dalam talian dari aplikasi telefon pintar ke permainan komputer kerana jawapan DNS palsu mereka.Cloudflare DNS tidak dapat meminta beberapa laman web bank. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Dan di sini anda mungkin berfikir,<br>Saya tidak menggunakan Tor atau VPN, mengapa saya harus peduli?<br>Saya mempercayai pemasaran Cloudflare, mengapa saya harus peduli<br>Laman web saya https mengapa saya mesti peduli | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Sekiranya anda melayari laman web yang menggunakan Cloudflare, anda akan berkongsi maklumat anda bukan sahaja kepada pemilik laman web tetapi juga Cloudflare.Ini adalah cara proksi terbalik berfungsi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Adalah mustahil untuk dianalisis tanpa mendekripsi trafik TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare mengetahui semua data anda seperti kata laluan mentah. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed boleh berlaku bila-bila masa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https Cloudflare tidak pernah habis-habis. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Adakah anda benar-benar mahu berkongsi data anda dengan Cloudflare, dan juga agensi 3 huruf? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Profil dalam talian pengguna internet adalah "produk" yang ingin dibeli oleh kerajaan dan syarikat teknologi besar. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Kata Jabatan Keselamatan Dalam Negeri A.S.:<br><br>Adakah anda tahu betapa berharganya data yang anda miliki? Adakah anda boleh menjual data tersebut kepada kami?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare juga menawarkan perkhidmatan VPN PERCUMA yang disebut "Cloudflare Warp".Sekiranya anda menggunakannya, semua sambungan telefon pintar (atau komputer anda) dihantar ke pelayan Cloudflare.Cloudflare dapat mengetahui laman web mana yang telah anda baca, komen apa yang telah anda siarkan, dengan siapa anda telah berbicara, dll.Anda secara sukarela memberikan semua maklumat anda kepada Cloudflare.Sekiranya anda berfikir “Adakah anda bergurau? Cloudflare selamat. " maka anda perlu belajar bagaimana VPN berfungsi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare mengatakan bahawa perkhidmatan VPN mereka menjadikan internet anda pantas.Tetapi VPN menjadikan sambungan internet anda lebih perlahan daripada sambungan yang ada. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Anda mungkin sudah tahu mengenai skandal PRISM.Memang benar bahawa AT&T membolehkan NSA menyalin semua data internet untuk pengawasan. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Katakan anda bekerja di NSA, dan anda mahukan profil internet setiap warganegara.Anda tahu kebanyakan mereka mempercayai Cloudflare secara membabi buta dan menggunakannya - hanya satu pintu masuk terpusat - untuk memproksi sambungan pelayan syarikat mereka (SSH / RDP), laman web peribadi, laman web sembang, laman web forum, laman web bank, laman web insurans, enjin carian, ahli rahsia -hanya laman web, laman web lelong, belanja, laman web video, laman web NSFW, dan laman web haram.Anda juga tahu bahawa mereka menggunakan perkhidmatan DNS Cloudflare ("1.1.1.1") dan perkhidmatan VPN ("Cloudflare Warp") untuk "Selamat! Lebih pantas! Lebih baik! " pengalaman internet.Menggabungkannya dengan alamat IP pengguna, cap jari penyemak imbas, kuki dan RAY-ID akan berguna untuk membina profil dalam talian sasaran. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Anda mahukan data mereka. Apa yang akan kamu lakukan? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare adalah honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Madu percuma untuk semua orang. Beberapa tali dilampirkan.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Jangan gunakan Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Desentralisasi internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Sila terus ke halaman seterusnya:  "[Etika Cloudflare](ms.ethics.md)"

---

<details>
<summary>_klik saya_

## Data dan Maklumat Lanjut
</summary>


Repositori ini adalah senarai laman web yang berada di belakang "The Great Cloudwall", menyekat pengguna Tor dan CDN lain.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Pengguna Cloudflare](../cloudflare_users/)
* [Domain Cloudflare](../cloudflare_users/domains/)
* [Pengguna CDN Bukan Cloudflare](../not_cloudflare/)
* [Pengguna Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Maklumat lanjut**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Muat turun: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * EBook asal (ePUB) telah dihapus oleh BookRix GmbH kerana pelanggaran hak cipta terhadap bahan CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Tiket itu dirampas berkali-kali.
  * [Dipadamkan oleh Projek Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Lihat tiket 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Tiket arkib terakhir 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_klik saya_

## Apa yang kau boleh buat?
</summary>

* [Baca senarai tindakan yang disyorkan dan kongsikan kepada rakan anda.](../ACTION.md)

* [Baca suara pengguna lain dan tuliskan pendapat anda.](../PEOPLE.md)

* Cari sesuatu: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Kemas kini senarai domain: [Senaraikan arahan](../INSTRUCTION.md).

* [Tambahkan Cloudflare atau acara yang berkaitan dengan projek ke sejarah.](../HISTORY.md)

* [Cuba & tulis Alat / Skrip baru.](../tool/)

* [Berikut adalah beberapa PDF / ePUB untuk dibaca.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Mengenai akaun palsu

Crimeflare mengetahui tentang adanya akaun palsu yang menyamar di saluran rasmi kami, sama ada Twitter, Facebook, Patreon, OpenCollective, Villages dll.
**Kami tidak pernah meminta e-mel anda.
Kami tidak pernah meminta nama anda.
Kami tidak pernah meminta identiti anda.
Kami tidak pernah bertanya lokasi anda.
Kami tidak pernah meminta sumbangan anda.
Kami tidak pernah meminta ulasan anda.
Kami tidak pernah meminta anda mengikuti di media sosial.
Kami tidak pernah meminta media sosial anda.**

# JANGAN AMANAH AKAUN FAKE.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)