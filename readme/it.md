# Il Great Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Ferma Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Great Cloudwall" è Cloudflare Inc., la società statunitense.Fornisce servizi CDN (content delivery network), mitigazione DDoS, sicurezza Internet e servizi DNS distribuiti (domain name server).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare è il più grande proxy MITM al mondo (proxy inverso).Cloudflare possiede oltre l'80% della quota di mercato CDN e il numero di utenti cloudflare cresce ogni giorno.Hanno ampliato la loro rete a più di 100 paesi.Cloudflare serve più traffico web rispetto a Twitter, Amazon, Apple, Instagram, Bing e Wikipedia messi insieme.Cloudflare offre un piano gratuito e molte persone lo utilizzano invece di configurare correttamente i propri server.Hanno scambiato la privacy con la comodità.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare si trova tra te e il server web di origine, agendo come un agente di pattuglia di confine.Non sei in grado di connetterti alla destinazione scelta.Ti stai connettendo a Cloudflare e tutte le tue informazioni vengono decrittografate e consegnate al volo. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  L'amministratore del server web di origine ha permesso all'agente - Cloudflare - di decidere chi può accedere alla loro "proprietà web" e definire "area riservata".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Dai un'occhiata all'immagine giusta.Penserai che Cloudflare blocchi solo i cattivi.Penserai che Cloudflare sia sempre online (mai interrotto).Penserai che bot e crawler legittimi possano indicizzare il tuo sito web.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Tuttavia quelli non sono affatto veri.Cloudflare sta bloccando persone innocenti senza motivo.Cloudflare può andare giù.Cloudflare blocca i bot legittimi.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Proprio come qualsiasi servizio di hosting, Cloudflare non è perfetto.Vedrai questa schermata anche se il server di origine funziona bene.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Pensi davvero che Cloudflare abbia il 100% di uptime?Non hai idea di quante volte Cloudflare si interrompa.Se Cloudflare non funziona, il tuo cliente non può accedere al tuo sito web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Si chiama così in riferimento al Great Firewall of China che fa un lavoro analogo nel filtrare molti esseri umani dal vedere i contenuti web (cioè tutti nella Cina continentale e le persone al di fuori).Mentre allo stesso tempo coloro che non sono interessati a vedere una rete drasticamente diversa, una rete priva di censura come un'immagine di "tank man" e la storia delle "proteste di piazza Tienanmen". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare possiede un grande potere.In un certo senso, controllano ciò che l'utente finale vede alla fine.Ti viene impedito di navigare nel sito Web a causa di Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare può essere utilizzato per la censura. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Non è possibile visualizzare il sito Web cloudflared se si utilizza un browser minore che Cloudflare potrebbe pensare che sia un bot (perché non molte persone lo usano). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Non è possibile superare questo "controllo del browser" invasivo senza abilitare Javascript.Questa è una perdita di cinque (o più) secondi della tua preziosa vita. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare blocca automaticamente anche robot / crawler legittimi come Google, Yandex, Yacy e client API.Cloudflare sta monitorando attivamente la comunità "bypass cloudflare" con l'intento di rompere i robot di ricerca legittimi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Allo stesso modo, Cloudflare impedisce a molte persone con scarsa connettività Internet di accedere ai siti Web dietro di esso (ad esempio, potrebbero trovarsi dietro 7+ livelli di NAT o condividere lo stesso IP, ad esempio il Wifi pubblico) a meno che non risolvano più CAPTCHA di immagini.In alcuni casi, ci vorranno dai 10 ai 30 minuti per soddisfare Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Nell'anno 2020 Cloudflare è passato da Recaptcha di Google a hCaptcha poiché Google intende far pagare per il suo utilizzo.Cloudflare ti ha detto che ha a cuore la tua privacy ("aiuta a risolvere un problema di privacy") ma questa è ovviamente una bugia.È tutta una questione di soldi."HCaptcha consente ai siti web di fare soldi servendo questa domanda bloccando bot e altre forme di abuso" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Dal punto di vista dell'utente, questo non cambia molto. Sei costretto a risolverlo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Molte persone e molti software vengono bloccati da Cloudflare ogni giorno. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare infastidisce molte persone in tutto il mondo.Dai un'occhiata all'elenco e pensa se l'adozione di Cloudflare sul tuo sito è positivo per l'esperienza utente. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Qual è lo scopo di Internet se non puoi fare quello che vuoi?La maggior parte delle persone che visitano il tuo sito web cerca semplicemente altre pagine se non riesce a caricare una pagina web.Potresti non bloccare attivamente alcun visitatore, ma il firewall predefinito di Cloudflare è abbastanza rigido da bloccare molte persone. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Non è possibile risolvere il captcha senza abilitare Javascript e cookie.Cloudflare li utilizza per creare una firma del browser per identificarti.Cloudflare deve conoscere la tua identità per decidere se sei idoneo a continuare a navigare nel sito. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Anche gli utenti Tor e gli utenti VPN sono vittima di Cloudflare.Entrambe le soluzioni vengono utilizzate da molte persone che non possono permettersi Internet senza censure a causa della politica del proprio paese / azienda / rete o che desiderano aggiungere un ulteriore livello per proteggere la propria privacy.Cloudflare sta attaccando senza vergogna quelle persone, costringendole a disattivare la loro soluzione proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Se non hai provato Tor fino a questo momento, ti invitiamo a scaricare Tor Browser e visitare i tuoi siti Web preferiti.Ti suggeriamo di non accedere al sito web della tua banca o alla pagina web del governo o contrassegneranno il tuo account. Usa VPN per quei siti web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Potresti dire "Tor è illegale! Gli utenti Tor sono criminali! Tor è cattivo! ". No.Potresti aver imparato a conoscere Tor dalla televisione, dicendo che Tor può essere usato per navigare nella darknet e scambiare armi, droghe o pornografia.Sebbene l'affermazione di cui sopra sia vera che ci sono molti siti Web di mercato in cui è possibile acquistare tali articoli, questi siti vengono spesso visualizzati anche su Clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor è stato sviluppato dall'esercito degli Stati Uniti, ma l'attuale Tor è sviluppato dal progetto Tor.Ci sono molte persone e organizzazioni che usano Tor, compresi i tuoi futuri amici.Quindi, se stai usando Cloudflare sul tuo sito web, stai bloccando i veri esseri umani.Perderai potenziale amicizia e affare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  E il loro servizio DNS, 1.1.1.1, sta anche filtrando gli utenti dal visitare il sito web restituendo un falso indirizzo IP di proprietà di Cloudflare, un IP localhost come "127.0.0.x", o semplicemente non restituendo nulla. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS interrompe anche il software online dall'app per smartphone al gioco per computer a causa della loro falsa risposta DNS.Il DNS di Cloudflare non può interrogare alcuni siti web di banche. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  E qui potresti pensare,<br>Non sto usando Tor o VPN, perché dovrei preoccuparmi?<br>Mi fido del marketing di Cloudflare, perché dovrei preoccuparmene<br>Il mio sito web è https perché dovrei preoccuparmi | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Se visiti un sito Web che utilizza Cloudflare, condividi le tue informazioni non solo con il proprietario del sito Web, ma anche con Cloudflare.Ecco come funziona il proxy inverso. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  È impossibile analizzare senza decrittografare il traffico TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare conosce tutti i tuoi dati come la password non elaborata. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed può accadere in qualsiasi momento. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  L'https di Cloudflare non è mai end-to-end. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Vuoi davvero condividere i tuoi dati con Cloudflare e anche con un'agenzia di 3 lettere? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Il profilo online dell'utente di Internet è un "prodotto" che il governo e le grandi aziende tecnologiche vogliono acquistare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Ha detto il Dipartimento della sicurezza interna degli Stati Uniti:<br><br>Hai idea di quanto siano preziosi i dati che hai? C'è un modo per venderci quei dati?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare offre anche un servizio VPN GRATUITO chiamato "Cloudflare Warp".Se lo usi, tutte le connessioni del tuo smartphone (o del tuo computer) vengono inviate ai server Cloudflare.Cloudflare può sapere quale sito web hai letto, quale commento hai pubblicato, con chi hai parlato, ecc.Stai fornendo volontariamente tutte le tue informazioni a Cloudflare.Se pensi “Stai scherzando? Cloudflare è sicuro. " allora devi imparare come funziona la VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare ha affermato che il suo servizio VPN rende veloce la tua connessione Internet.Ma la VPN rende la tua connessione Internet più lenta della tua connessione esistente. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Potresti già sapere dello scandalo PRISM.È vero che AT&T consente alla NSA di copiare tutti i dati Internet per la sorveglianza. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Supponiamo che tu stia lavorando alla NSA e che desideri il profilo Internet di ogni cittadino.Sai che la maggior parte di loro si fida ciecamente di Cloudflare e lo utilizza - un solo gateway centralizzato - per eseguire il proxy della connessione al server aziendale (SSH / RDP), sito Web personale, sito Web di chat, sito Web del forum, sito Web della banca, sito Web assicurativo, motore di ricerca, membro segreto -solo sito web, sito web di aste, shopping, sito web di video, sito web NSFW e sito web illegale.Sai anche che usano il servizio DNS di Cloudflare ("1.1.1.1") e il servizio VPN ("Cloudflare Warp") per "Sicuro! Più veloce! Meglio!" esperienza di Internet.Combinarli con l'indirizzo IP dell'utente, l'impronta digitale del browser, i cookie e RAY-ID sarà utile per creare il profilo online del target. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Vuoi i loro dati. Cosa farai? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare è un honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Miele gratis per tutti. Alcune stringhe allegate.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Non utilizzare Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Decentralizza Internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Si prega di passare alla pagina successiva:  "[Etica di Cloudflare](it.ethics.md)"

---

<details>
<summary>_cliccami_

## Dati e ulteriori informazioni
</summary>


Questo repository è un elenco di siti Web che si trovano dietro "The Great Cloudwall", che blocca gli utenti Tor e altri CDN.


**Dati**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Utenti di Cloudflare](../cloudflare_users/)
* [Domini Cloudflare](../cloudflare_users/domains/)
* [Utenti CDN non Cloudflare](../not_cloudflare/)
* [Utenti Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Maggiori informazioni**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Scarica: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * L'eBook originale (ePUB) è stato eliminato da BookRix GmbH a causa della violazione del copyright del materiale CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Il biglietto è stato vandalizzato così tante volte.
  * [Eliminato dal progetto Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Vedi ticket 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Ultimo biglietto archivio 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_cliccami_

## Cosa sai fare?
</summary>

* [Leggi il nostro elenco di azioni consigliate e condividilo con i tuoi amici.](../ACTION.md)

* [Leggi la voce di altri utenti e scrivi i tuoi pensieri.](../PEOPLE.md)

* Cerca qualcosa: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Aggiorna l'elenco dei domini: [Elenca le istruzioni](../INSTRUCTION.md).

* [Aggiungi Cloudflare o un evento relativo al progetto alla cronologia.](../HISTORY.md)

* [Prova e scrivi un nuovo strumento / script.](../tool/)

* [Ecco alcuni PDF / ePUB da leggere.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### A proposito di account falsi

Crimeflare è a conoscenza dell'esistenza di account falsi che impersonano i nostri canali ufficiali, siano essi Twitter, Facebook, Patreon, OpenCollective, Villages ecc.
**Non chiediamo mai la tua email.
Non chiediamo mai il tuo nome.
Non chiediamo mai la tua identità.
Non chiediamo mai la tua posizione.
Non chiediamo mai la tua donazione.
Non chiediamo mai la tua recensione.
Non ti chiediamo mai di seguirci sui social media.
Non chiediamo mai ai tuoi social media.**

# NON FIDATEVI DI ACCOUNT FALSI.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)