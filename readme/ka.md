# დიდი ღრუბელი


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## შეაჩერე Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  ”დიდი Cloudwall” არის ამერიკული კომპანია Cloudflare Inc..იგი უზრუნველყოფს CDN (შინაარსის მიწოდების ქსელის) მომსახურებას, DDoS შერბილებას, ინტერნეტის უსაფრთხოებას და განაწილებულ DNS (დომენის სერვერის) სერვისებს.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare არის მსოფლიოში უდიდესი MITM მარიონეტული (საპირისპირო მარიონეტული).Cloudflare ფლობს CDN ბაზრის 80% -ზე მეტ წილს და cloudflare მომხმარებლების რაოდენობა ყოველდღიურად იზრდება.მათ გააფართოვეს ქსელი ასზე მეტ ქვეყანაში.Cloudflare უფრო მეტ ვებ – ტრაფიკს ემსახურება ვიდრე Twitter, Amazon, Apple, Instagram, Bing და Wikipedia– ის კომბინირებული.Cloudflare გთავაზობთ უფასო გეგმას და ბევრი ადამიანი იყენებს მის სერვერების სწორად კონფიგურაციის ნაცვლად.ისინი კონფიდენციალურობაზე ვაჭრობდნენ კონფიდენციალურობას.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare ზის თქვენსა და წარმოშობის ვებ სერვერს შორის, მოქმედებს სასაზღვრო საპატრულო აგენტის მსგავსად.თქვენ ვერ შეძლებთ თქვენს არჩეულ დანიშნულების ადგილთან დაკავშირებას.თქვენ უკავშირდებით Cloudflare- ს და თქვენი ყველა ინფორმაციის გაშიფვრა ხდება და გადასცემენ ფრენას. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  წარმოშობის ვებ – გვერდის ადმინისტრატორმა აგენტს - Cloudflare– ს მისცა უფლება, გადაწყვიტოს, ვის შეუძლია შესვლა მათ „ვებსაიტს“ და განსაზღვროს „შეზღუდული ტერიტორია“.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  გადახედეთ სწორ სურათს.თქვენ იფიქრებთ Cloudflare ბლოკავს მხოლოდ ცუდი ბიჭების.თქვენ იფიქრებთ, რომ Cloudflare ყოველთვის ონლაინშია (არასდროს დაელოდოთ).თქვენ იფიქრებთ ლეგიტიმურ ბოტებს და მცოცავებს შეუძლიათ თქვენი ვებგვერდის ინდექსირება.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  თუმცა ეს საერთოდ არ შეესაბამება სინამდვილეს.Cloudflare უშეცდომოდ აკრძალავს უდანაშაულო ადამიანებს.Cloudflare შეიძლება ჩავიდეს.Cloudflare ბლოკავს ლეგენდულ ბოტებს.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  ისევე, როგორც ნებისმიერი ჰოსტინგის სერვისი, Cloudflare არ არის სრულყოფილი.თქვენ ნახავთ ამ ეკრანს მაშინაც კი, თუ წარმოშობის სერვერი კარგად მუშაობს.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  მართლა ფიქრობთ, რომ Cloudflare- ს აქვს 100% და მეტი დრო?თქვენ წარმოდგენა არ გაქვთ რამდენჯერ დაიშვება Cloudflare.თუ Cloudflare იშლება, თქვენი მომხმარებელი ვერ შედის თქვენს ვებსაიტზე. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  ამას უწოდებენ ჩინეთის დიდ Firewall- ს, რომელიც მრავალი ადამიანის ფილტრაციას ახდენს ვებ – შინაარსის დანახვის გზით (ე.ი. ყველას, ჩინეთში და მის გარეთ მყოფი ხალხი).ამავე დროს, დაზარალებულები არ არიან დრამატულად განსხვავებული ვებ – გვერდის სანახავად, ქსელისგან თავისუფალი ცენზურისგან, როგორიცაა "სატანკო ადამიანის" სურათი და "ტიანანმენის მოედანზე საპროტესტო აქციების" ისტორია. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare ფლობს დიდ ძალას.გარკვეული თვალსაზრისით, ისინი აკონტროლებენ რას ხედავს საბოლოო მომხმარებელი.Cloudflare– ის გამო, ვებგვერდზე ათვალიერებთ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare შეიძლება გამოყენებულ იქნას ცენზურისთვის. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  თქვენ არ შეგიძლიათ ნახოთ cloudflared ვებ – გვერდი, თუ იყენებთ მცირე ბრაუზერს, რომელსაც Cloudflare ფიქრობს, რომ ეს არის bot (რადგან არცერთი ადამიანი იყენებს მას). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  თქვენ არ შეგიძლიათ გადაიტანოთ ეს ინვაზიური "ბრაუზერის შემოწმება" Javascript ჩართვის გარეშე.ეს არის თქვენი ძვირფასი ცხოვრების ხუთი (ან მეტი) წამის დაკარგვა. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare ასევე ავტომატურად ბლოკავს ლეგიტიმურ რობოტებს / მცოცავებს, როგორიცაა Google, Yandex, Yacy და API კლიენტები.Cloudflare აქტიურად აკონტროლებს "შემოვლითი ღრუბლის" თემს, რომლის მიზანია დაარღვიოს კანონიერი კვლევა. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare ანალოგიურად ხელს უშლის ბევრ ადამიანს, ვისაც ინტერნეტთან ცუდი კავშირი აქვს მის უკან მის ვებგვერდებზე წვდომის გარეშე (მაგალითად, ისინი შეიძლება იყვნენ NAT + 7 ფენის მიღმა ან იგივე IP, მაგალითად საჯარო Wifi) გაზიარება, თუ ისინი არ გადაჭრით მრავალჯერადი გამოსახულების CAPTCHA.ზოგიერთ შემთხვევაში, ამ Google- ს დაკმაყოფილებისთვის 10-დან 30 წუთი დასჭირდება. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020 წელს Cloudflare გადავიდა Google– ს Recaptcha– დან hCaptcha– ში, რადგან Google აპირებს დააკისროს მისი გამოყენება.Cloudflare გითხრათ, რომ ისინი ზრუნავენ თქვენს კონფიდენციალურობაზე (”ეს ხელს უწყობს კონფიდენციალურობის საკითხის მოგვარებას”), მაგრამ ეს აშკარად სიცრუეა.ეს ყველაფერი ფულზეა.”HCaptcha ვებსაიტებს საშუალებას აძლევს, ფულის გაკეთება და ბოროტად გამოყენების სხვა ფორმების დაბლოკვის დროს, ამ მოთხოვნას ემსახურებოდეს.” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  მომხმარებლის თვალსაზრისით, ეს მნიშვნელოვნად არ იცვლება. თქვენ იძულებული ხართ, რომ გადაწყვიტოთ ეს. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare- ს მიერ ყოველდღიურად უამრავი ადამიანი და პროგრამა ბლოკავს. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare აღიზიანებს ბევრ ადამიანს მთელს მსოფლიოში.გადახედეთ ჩამონათვალს და იფიქრეთ თუ არა თქვენს საიტზე Cloudflare– ის მიღება კარგია მომხმარებლის გამოცდილებისთვის. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  რა არის ინტერნეტი, თუ ვერ გააკეთებ იმას, რაც გინდა?უმეტესობა, ვინც თქვენს ვებ – გვერდს სტუმრობს, მხოლოდ სხვა გვერდს ეძებს, თუ მათ არ შეუძლიათ ვებგვერდის ჩატვირთვა.თქვენ შეიძლება არ აქტიურად დაბლოკოს ნებისმიერი ვიზიტორი, მაგრამ Cloudflare- ის ნაგულისხმევი ფრაგმენტი საკმარისად მკაცრია, რომ დაბლოკოს მრავალი ადამიანი. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Captcha- ს ამოხსნის საშუალება არ არის Javascript- ისა და Cookies- ის გამოყენების გარეშე.Cloudflare იყენებს მათ, ბრაუზერის ხელმოწერის გასაკეთებლად, რათა დაადგინონ თქვენ.Cloudflare- ს უნდა იცოდეს თქვენი პირადობა, რომ გადაწყვიტოს, ხართ თუ არა, რომ განაგრძოთ საიტის დათვალიერება. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor მომხმარებლები და VPN მომხმარებლები ასევე Cloudflare- ის მსხვერპლი არიან.ორივე გადაწყვეტას იყენებენ მრავალი ადამიანი, რომელსაც არ აქვს შესაძლებლობა გაუწიოს არცენზირებული ინტერნეტი საკუთარი ქვეყნის / კორპორაციის / ქსელის პოლიტიკის გამო, ან ვისაც სურს კონფიდენციალურობის დასაცავად დამატებითი ფენის დამატება.Cloudflare ურცხვად დაესხმის თავს იმ ხალხს, აიძულებს მათ გამორთონ თავიანთი მარიონეტული ხსნარი. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  თუ თქვენ არ სცადეთ Tor ამ მომენტამდე, ჩვენ გირჩევთ ჩამოტვირთოთ Tor ბროუზერი და მოინახულოთ თქვენი საყვარელი ვებსაიტები.ჩვენ გირჩევთ არ შეხვიდეთ თქვენი ბანკის ვებგვერდზე ან მთავრობის ვებგვერდზე, ან ისინი თქვენს ანგარიშს დროშას გახდის. გამოიყენეთ VPN ამ ვებსაიტებზე. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  შეიძლება გითხრათ, ”ტორ უკანონოა! ტორ მომხმარებლები კრიმინალები არიან! ტორ ცუდია! ”. არა.თქვენ შეიძლება ტორის შესახებ გაეცნოთ ტელევიზიიდან და თქვა, რომ ტორს შეგიძლიათ გამოიყენოთ darknetnet- სა და ვაჭრობის იარაღი, ნარკოტიკი ან ჩიდო პორნო.მიუხედავად იმისა, რომ ზემოთ ნათქვამია, მართალია, რომ არსებობს უამრავი ბაზრის ვებგვერდი, სადაც შეგიძლიათ შეიძინოთ ასეთი ნივთები, ის საიტები ხშირად ჩანს კლარნეტში.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  ტორმა შეიმუშავა აშშ-ს არმია, მაგრამ ამჟამინდელი ტორი განვითარებულია ტორის პროექტით.უამრავი ადამიანი და ორგანიზაცია არსებობს, რომლებიც Tor- ს იყენებენ თქვენი მომავალი მეგობრების ჩათვლით.ასე რომ, თუ თქვენს ვებ – გვერდზე Cloudflare იყენებთ, თქვენ ბლოკავს ნამდვილ ადამიანებს.თქვენ დაკარგავთ პოტენციურ მეგობრობას და საქმიანი გარიგებას. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  მათი DNS სერვისი, 1.1.1.1, ასევე ახდენს მომხმარებლების გაფილტვრას ვებსაიტიდან დაბრუნების გზით, Cloudflare- ის საკუთრებაში არსებულ ყალბი IP მისამართის დაბრუნებით, localhost IP– ით, როგორიცაა “127.0.0.x”, ან უბრალოდ არაფერს უბრუნებს. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS ასევე არღვევს ონლაინ პროგრამებს სმარტფონის აპიდან კომპიუტერულ თამაშში, ყალბი DNS პასუხის გამო.Cloudflare DNS ვერ ითხოვს ბანკის ზოგიერთ ვებსაიტს. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  და აქ შეიძლება იფიქროთ,<br>მე არ ვიყენებ Tor ან VPN, რატომ უნდა ვიზრუნო?<br>Cloudflare– ს მარკეტინგს ვენდობი, რატომ უნდა ვიზრუნო<br>ჩემი ვებ-გვერდია https, რატომ უნდა ვიზრუნო | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  თუ თქვენ ეწვიეთ ვებგვერდს, რომელიც იყენებს Cloudflare- ს, თქვენს ინფორმაციას უზიარებთ არამარტო ვებსაიტის მფლობელს, არამედ Cloudflare- ს.ასე მოქმედებს საპირისპირო პროქსი. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  შეუძლებელია ანალიზი TLS ტრაფიკის გაშიფვრის გარეშე. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare იცის თქვენი ყველა მონაცემი, როგორიცაა უხეში პაროლი. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed შეიძლება მოხდეს ნებისმიერ დროს. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare- ის https არასოდეს დასრულდა. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  გსურთ თქვენი მონაცემების გაზიარება Cloudflare და ასევე 3-წერილიანი სააგენტოს საშუალებით? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  ინტერნეტ მომხმარებელთა ონლაინ პროფილი არის "პროდუქტი", რომელსაც მთავრობა და დიდი ტექნიკური კომპანიები ყიდულობენ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  ამის შესახებ აშშ-ს საშინაო უსაფრთხოების დეპარტამენტმა განაცხადა:<br><br>თქვენ იცით, რამდენად ღირებული გაქვთ თქვენი მონაცემები? არსებობს რაიმე გზა, რომლითაც ჩვენ მოგვყიდით ამ მონაცემებს?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare ასევე გთავაზობთ უფასო VPN მომსახურებას, სახელწოდებით "Cloudflare Warp".თუ მას იყენებთ, თქვენი სმარტფონის (ან თქვენი კომპიუტერის) კავშირი იგზავნება Cloudflare სერვერებზე.Cloudflare- ს შეუძლია იცოდეს რომელ ვებსაიტზე გაქვთ წაკითხული, რა კომენტარი გამოაქვეყნეთ, ვისთან გაქვთ საუბარი და ა.შ.ნებაყოფლობით აძლევთ ყველა თქვენს ინფორმაციას Cloudflare- ს.თუ ფიქრობთ „ხუმრობთ? Cloudflare უსაფრთხოა. ” შემდეგ უნდა გაიგოთ როგორ მუშაობს VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare– მა თქვა, რომ მათი VPN სერვისი თქვენს ინტერნეტს სწრაფად ავითარებს.მაგრამ VPN გახდის თქვენს ინტერნეტ კავშირს უფრო ნელა, ვიდრე არსებულ კავშირზე. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  თქვენ შეიძლება უკვე იცოდეთ PRISM სკანდალის შესახებ.მართალია, AT&T საშუალებას აძლევს NSA- ს კოპირება განახორციელოს ინტერნეტში ყველა მონაცემის მეთვალყურეობისთვის. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  ვთქვათ, რომ თქვენ მუშაობთ NSA- ში და გინდათ ყველა მოქალაქის ინტერნეტი.თქვენ იცით, რომ მათი უმრავლესობა ბრმად ენდობა Cloudflare- ს და იყენებს მას - მხოლოდ ერთი ცენტრალიზებული კარიბჭე - მათი კომპანიის სერვერის კავშირის (SSH / RDP) მარიონეტულობის, პირად ვებსაიტზე, ჩატის ვებსაიტზე, ფორუმის ვებსაიტზე, ბანკის ვებსაიტზე, სადაზღვევო ვებსაიტზე, საძიებო სისტემაზე, საიდუმლო წევრზე. ერთ ვებსაიტზე, აუქციონის ვებსაიტზე, საყიდლებზე, ვიდეო ვებსაიტზე, NSFW ვებსაიტზე და არალეგალურ ვებსაიტზე.თქვენ ასევე იცით, რომ ისინი იყენებენ Cloudflare- ს DNS მომსახურებას ("1.1.1.1") და VPN მომსახურებას ("Cloudflare Warp") "უსაფრთხო! უფრო სწრაფად! Უკეთესი!" ინტერნეტის გამოცდილება.მათი კომბინაცია მომხმარებლის IP მისამართთან, ბრაუზერის თითის ანაბეჭდთან, ქუქი – ფაილებთან და RAY-ID– ით სასარგებლო იქნება სამიზნე ონლაინ პროფილის შესაქმნელად. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  გსურთ მათი მონაცემები. Რას იზავ? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare არის honpot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **ყველასთვის უფასო თაფლი. რამდენიმე სტრიქონი ერთვის.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **არ გამოიყენოთ Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **ინტერნეტის დეცენტრალიზაცია.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    გთხოვთ გააგრძელოთ შემდეგ გვერდზე:  "[Cloudflare ეთიკა](ka.ethics.md)"

---

<details>
<summary>_დამაკლიკე_

## მონაცემები და მეტი ინფორმაცია
</summary>


ეს საცავი არის ვებსაიტების სია, რომლებიც ჩამორჩებიან "დიდ ღრუბელს", რომელიც ბლოკავს Tor მომხმარებლებს და სხვა CDN- ს.


**მონაცემები**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare მომხმარებლები](../cloudflare_users/)
* [Cloudflare დომენები](../cloudflare_users/domains/)
* [Cloudflare CDN მომხმარებლები](../not_cloudflare/)
* [ანტი-ტორის მომხმარებლები](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Მეტი ინფორმაცია**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * გადმოწერა: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * წიგნის ორიგინალი eBook (ePUB) წაიშალა BookRix GmbH– ის მიერ CC0 მასალის საავტორო უფლებების დარღვევის გამო.
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * ბილეთი ბევრჯერ იქნა ვანდალიზებული.
  * [ამოღებულია Tor Project– ით.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [იხილეთ ბილეთი 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [ბოლო საარქივო ბილეთი 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_დამაკლიკე_

## Რა შეგიძლია?
</summary>

* [წაიკითხეთ ჩვენი რეკომენდებული მოქმედებების სია და გაუზიარეთ თქვენს მეგობრებს.](../ACTION.md)

* [წაიკითხეთ სხვა მომხმარებლის ხმა და დაწერეთ თქვენი აზრები.](../PEOPLE.md)

* მოძებნეთ რამე: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* განაახლეთ დომენის სია: [ჩამოთვალეთ ინსტრუქციები](../INSTRUCTION.md).

* [Cloudflare ან პროექტთან დაკავშირებული ღონისძიების დამატება ისტორიაში.](../HISTORY.md)

* [სცადეთ და დაწერეთ ახალი ხელსაწყო / სკრიპტი.](../tool/)

* [აქ წაიკითხეთ PDF / ePUB.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### ყალბი ანგარიშების შესახებ

Crimeflare იცოდეთ ყალბი ანგარიშების არსებობის შესახებ, რომლებიც ახდენენ ჩვენს ოფიციალურ არხებს, იქნება ეს Twitter, Facebook, Patreon, OpenCollective, Village და ა.შ.
**ჩვენ არასდროს ვეკითხებით თქვენს ელ.ფოსტს.
ჩვენ არასდროს ვეკითხებით თქვენს სახელს.
ჩვენ არასდროს ვეკითხებით თქვენს პირადობას.
ჩვენ არასდროს ვეკითხებით თქვენს ადგილს.
ჩვენ არასდროს ვთხოვთ თქვენს შემოწირულობას.
ჩვენ არასდროს ვთხოვთ თქვენს მიმოხილვას.
ჩვენ არასდროს მოგთხოვთ, რომ დაიცვას სოციალური მედია.
ჩვენ არასოდეს ვკითხავთ თქვენს სოციალურ მედიას.**

# ნუ ენდობით ფალანგებს.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)