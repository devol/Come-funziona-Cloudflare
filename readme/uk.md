# Велика Хмара


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Зупиніть Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Great Cloudwall" - це компанія Cloudflare Inc., США.Він надає послуги CDN (мережа доставки вмісту), пом'якшення DDoS, захист Інтернету та розподілені DNS (сервер доменних імен).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare - це найбільший у світі проксі-сервер MITM (зворотний проксі).Cloudflare володіє більше 80% частки ринку CDN, а кількість користувачів cloudflare зростає з кожним днем.Вони розширили свою мережу до понад 100 країн.Cloudflare обслуговує більше веб-трафіку, ніж у Twitter, Amazon, Apple, Instagram, Bing та Wikipedia.Cloudflare пропонує безкоштовний план, і багато людей використовують його замість того, щоб правильно налаштувати свої сервери.Вони торгували приватністю заради зручності.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare сидить між вами та початковим веб-сервером, діючи як агент прикордонного патрулювання.Ви не можете підключитися до обраного пункту призначення.Ви під’єднуєтесь до Cloudflare, і вся ваша інформація розшифровується та передається на льоту. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Адміністратор веб-сервера походження дозволив агенту - Cloudflare - вирішити, хто може отримати доступ до своєї "веб-власності" та визначити "область з обмеженим доступом".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Погляньте на правильне зображення.Ви подумаєте, що Cloudflare блокує лише поганих хлопців.Ви думаєте, що Cloudflare завжди в Інтернеті (ніколи не спускайтеся).Ви вважаєте, що законні боти та сканери можуть індексувати ваш веб-сайт.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Однак це зовсім не вірно.Cloudflare без причини блокує невинних людей.Cloudflare може знизитися.Cloudflare блокує законних ботів.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Як і будь-який хостинг-сервіс, Cloudflare не є ідеальним.Цей екран ви побачите, навіть якщо сервер-джерело працює добре.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Ви дійсно думаєте, що Cloudflare має 100% часу роботи?Ви поняття не маєте, скільки разів спадає Cloudflare.Якщо Cloudflare знижується, ваш клієнт не може отримати доступ до вашого веб-сайту. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Це називається посиланням на Великий Брандмауер Китаю, який виконує порівняну роботу з фільтрації багатьох людей від перегляду веб-контенту (тобто всіх у Китаї та людей поза межами).У той же час тих, хто не зачіпає бачити докорінно різну павутину, Інтернет, що не містить цензури, наприклад, образ "танкіста" та історія "протестів на площі Тяньаньмен". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare володіє великою силою.У певному сенсі вони контролюють те, що в кінцевому підсумку бачить кінцевий користувач.Вам не дозволяють переглядати веб-сайт через Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare можна використовувати для цензури. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Ви не можете переглядати хмарний веб-сайт, якщо ви використовуєте незначний веб-переглядач, який Cloudflare може вважати ботом (тому що не багато людей ним користуються). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Ви не можете пройти цю інвазивну "перевірку браузера" без включення Javascript.Це марна трата п'яти (і більше) секунд вашого цінного життя. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare також автоматично блокує законних роботів / сканерів, таких як Google, Yandex, Yacy та API-клієнти.Cloudflare активно відслідковує спільноту «обхід хмарних областей» з наміром зламати законних дослідницьких ботів. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare аналогічно перешкоджає багатьом людям, які мають поганий підключення до Інтернету, доступ до веб-сайтів, що стоять за ним (наприклад, вони можуть бути поза 7+ шарами NAT або спільним IP-адресом, наприклад, загальнодоступним Wi-Fi), якщо вони не вирішать кілька CAPTCHA зображень.У деяких випадках для задоволення Google потрібно буде від 10 до 30 хвилин. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  У 2020 році Cloudflare перейшов з Recaptcha Google на hCaptcha, оскільки Google має намір стягувати плату за його використання.Cloudflare сказав вам, що вони піклуються про вашу конфіденційність ("це допомагає вирішити питання про конфіденційність"), але це, очевидно, брехня.Вся справа в грошах."HCaptcha дозволяє веб-сайтам заробляти гроші, обслуговуючи цей попит, блокуючи ботів та інші форми зловживань" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  З точки зору користувача, це не сильно зміниться. Вас змушують вирішити. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare щодня блокує багатьох людей та програмне забезпечення. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare дратує багатьох людей по всьому світу.Погляньте на список і подумайте, чи корисне використання Cloudflare на вашому веб-сайті. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Яка мета Інтернету, якщо ви не можете робити те, що хочете?Більшість людей, які відвідують ваш веб-сайт, просто шукають інші сторінки, якщо вони не можуть завантажити веб-сторінку.Можливо, ви не блокуєте активних відвідувачів, але брандмауер за замовчуванням Cloudflare досить суворий, щоб блокувати багатьох людей. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Немає способу вирішити капчу, не ввімкнувши Javascript та Cookies.Cloudflare використовує їх для створення підпису браузера, щоб ідентифікувати вас.Cloudflare повинен знати вашу особу, щоб вирішити, чи можете ви продовжувати перегляд сайту. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Користувачі Tor та VPN також є жертвою Cloudflare.Обидва рішення використовуються багатьма людьми, які не можуть дозволити собі безцензурний Інтернет через свою країну / корпорацію / мережеву політику або хочуть додати додатковий рівень для захисту своєї конфіденційності.Cloudflare безсоромно атакує цих людей, змушуючи їх вимкнути рішення проксі. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Якщо ви до цього моменту не спробували Tor, радимо завантажити браузер Tor і відвідати ваші улюблені веб-сайти.Ми пропонуємо вам не входити на веб-сайт вашого банку чи веб-сторінку уряду, інакше вони позначать ваш рахунок. Використовуйте VPN для цих веб-сайтів. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Ви можете сказати: «Тор незаконно! Користувачі Tor - злочинні! Тор поганий! ". Ні.Ви можете дізнатися про Тор з телебачення, кажучи, що Тор можна використовувати для перегляду темних мереж і торгівлі зброєю, наркотиками або порно-чидом.Хоча вищевикладене твердження вірно, що існує багато ринкових веб-сайтів, де ви можете придбати подібні товари, ці сайти часто з’являються і на clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Тор був розроблений армією США, але нинішній Тор розробляється за проектом "Тор".Є багато людей та організацій, які використовують Tor, включаючи майбутніх друзів.Отже, якщо ви використовуєте Cloudflare на своєму веб-сайті, ви блокуєте справжніх людей.Ви втратите потенційну дружбу та ділову угоду. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  І їх служба DNS, 1.1.1.1, також фільтрує користувачів від відвідування веб-сайту, повертаючи підроблені IP-адреси, що належать Cloudflare, локальному IP-адресу, наприклад "127.0.0.x", або просто нічого не повертають. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS також перемикає програмне забезпечення в Інтернеті від програми для смартфонів до комп'ютерної гри через свою підроблену відповідь DNS.Cloudflare DNS не може запитувати деякі веб-сайти банку. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  І тут ви можете подумати,<br>Я не використовую Tor чи VPN, чому я повинен дбати?<br>Я довіряю маркетингу Cloudflare, чому я повинен дбати<br>Мій веб-сайт https, чому я повинен піклуватися | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Якщо ви відвідуєте веб-сайт, який використовує Cloudflare, ви ділитесь своєю інформацією не лише власнику веб-сайту, але й Cloudflare.Так працює зворотний проксі. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Неможливо проаналізувати без дешифрування трафіку TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare знає всі ваші дані, такі як необроблений пароль. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed може статися будь-коли. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https Cloudflare ніколи не закінчується. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Ви дійсно хочете поділитися своїми даними з Cloudflare, а також з 3-листним агентством? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Інтернет-профіль користувача Інтернету - це «продукт», який хочуть придбати уряд та великі технологічні компанії. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Про це повідомили в департаменті внутрішньої безпеки США:<br><br>Чи маєте ви уявлення, наскільки цінні ваші дані? Чи є спосіб, щоб ви продали нам ці дані?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare також пропонує безкоштовну послугу VPN під назвою "Cloudflare Warp".Якщо ви користуєтесь цим, усі ваші смартфонні зв'язки (або ваш комп'ютер) надсилаються на сервери Cloudflare.Cloudflare може знати, який веб-сайт ви читали, який коментар ви опублікували, з ким ви спілкувались тощо.Ви добровільно надаєте всю свою інформацію Cloudflare.Якщо ви думаєте: «Ви жартуєте? Cloudflare захищений. " тоді вам потрібно дізнатися, як працює VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare сказав, що їх сервіс VPN робить ваш Інтернет швидким.Але VPN роблять ваше інтернет-з'єднання повільніше, ніж наявне. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Ви вже можете знати про скандал ПРИЗМ.Це правда, що AT&T дозволяє NSA копіювати всі дані Інтернету для спостереження. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Скажімо, ви працюєте в АНБ, і вам потрібен Інтернет-профіль кожного громадянина.Ви знаєте, що більшість з них сліпо довіряють Cloudflare і використовують його - лише один централізований шлюз - для проксі підключення до їх сервера компанії (SSH / RDP), персонального веб-сайту, веб-сайту чату, веб-сайту форуму, веб-сайту банку, страхового веб-сайту, пошукової системи, секретного члена -тільки веб-сайт, веб-сайт аукціону, торговий, веб-сайт, веб-сайт NSFW та нелегальний веб-сайт.Ви також знаєте, що вони використовують службу DNS Cloudflare ("1.1.1.1") та VPN-сервіс ("Cloudflare Warp") для "Безпечного! Швидше! Краще! » Інтернет-досвід.Поєднання їх з IP-адресою користувача, відбитком пальців браузера, файлами cookie та RAY-ідентифікатором буде корисно для створення онлайн-профілю цілі. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Ви хочете їх даних. Що ти робитимеш? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare - це медонос.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Безкоштовний мед для всіх. Деякі рядки додаються.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Не використовуйте Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Децентралізуйте Інтернет.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Перейдіть на наступну сторінку:  "[Етика хмарної флори](uk.ethics.md)"

---

<details>
<summary>_натисніть на мене_

## Дані та додаткова інформація
</summary>


Цей репозиторій - це список веб-сайтів, які стоять за "Великим хмаром", блокуючи користувачів Tor та інших CDN.


**Дані**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Користувачі Cloudflare](../cloudflare_users/)
* [Cloudflare доменів](../cloudflare_users/domains/)
* [Користувачі CDN, які не є Cloudflare](../not_cloudflare/)
* [Користувачі Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Більше інформації**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Завантажити: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Оригінальну електронну книгу (ePUB) було видалено компанією BookRix GmbH через порушення авторських прав на матеріали CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Квиток стільки разів вандалізували.
  * [Видалено проектом Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Дивіться квиток 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Останній архівний квиток 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_натисніть на мене_

## Що ти можеш зробити?
</summary>

* [Прочитайте наш список рекомендованих дій та поділіться ним із друзями.](../ACTION.md)

* [Прочитайте голос іншого користувача та запишіть свої думки.](../PEOPLE.md)

* Шукайте щось: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Оновіть список доменів: [Перелічіть інструкції](../INSTRUCTION.md).

* [Додайте до історії події Cloudflare або події.](../HISTORY.md)

* [Спробуйте і напишіть новий інструмент / сценарій.](../tool/)

* [Ось деякі PDF / ePUB для читання.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Про підроблені акаунти

Crimeflare знає про існування підроблених акаунтів, що представляють себе офіційними каналами, будь то Twitter, Facebook, Patreon, OpenCollective, Villageges тощо.
**Ми ніколи не запитуємо вашу електронну пошту.
Ми ніколи не запитуємо ваше ім’я.
Ми ніколи не запитуємо вашу особу.
Ми ніколи не запитуємо ваше місцезнаходження.
Ми ніколи не просимо вашої пожертви.
Ми ніколи не просимо вашого огляду.
Ми ніколи не просимо вас слідкувати в соціальних мережах.
Ми ніколи не запитуємо ваші соціальні медіа.**

# НЕ ВІДПОВІДАЙТЕ ФАЙКОВИХ РАХУНОК.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)