# Veliki oblačni zid


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Ustavi Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "Veliki oblačni zid" je Cloudflare Inc., ameriško podjetje.Ponuja storitve CDN (omrežje za dostavo vsebine), blaženje DDoS, internetno varnost in distribuirane storitve DNS (strežnik domenskih imen).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare je največji MITM proxy na svetu (reverse proxy).Cloudflare je lastnik več kot 80% tržnega deleža CDN, število uporabnikov oblakov pa raste vsak dan.Svojo mrežo so razširili na več kot 100 držav.Cloudflare služi več spletnega prometa kot Twitter, Amazon, Apple, Instagram, Bing in Wikipedia.Cloudflare ponuja brezplačen načrt in veliko ljudi ga uporablja, namesto da pravilno konfigurira strežnike.Trgovali so zasebnost nad udobjem.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare sedi med vami in spletnim strežnikom, ki deluje kot agent mejne patrulje.Ne morete se povezati z izbranim ciljem.Povezujete se z Cloudflare in vsi vaši podatki se dešifrirajo in predajo v hipu. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Skrbnik izvornega spletnega strežnika je agentu Cloudflare dovolil, da se sam odloči, kdo lahko dostopa do njihove "spletne znamke" in določi "območje z omejitvami".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Oglejte si pravo sliko.Mislili boste, da Cloudflare blokira samo slabe fante.Mislili boste, da je Cloudflare vedno na spletu (nikoli ne spuščajte).Mislili boste, da lahko zakoniti boti in pajki indeksirajo vaše spletno mesto.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Vendar to sploh ne drži.Cloudflare brez razloga blokira nedolžne ljudi.Cloudflare se lahko spusti.Cloudflare blokira zakonite bote.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Tako kot vsaka storitev gostovanja tudi Cloudflare ni popoln.Ta zaslon boste videli, tudi če izvorni strežnik dobro deluje.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Ali res mislite, da ima Cloudflare 100-odstotno podaljšanje časa?Nimate pojma, kolikokrat se Cloudflare spusti.Če se oblak Cloudflare spusti, vaša stranka ne more dostopati do vašega spletnega mesta. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Tako se imenuje v zvezi s kitajskim požarnim zidom, ki opravi primerljivo delo filtriranja mnogih ljudi, da bi videli spletno vsebino (tj. Vsi na celinskem Kitajskem in ljudje zunaj).Medtem ko tisti, ki niso prizadeti, vidijo drazno drugačen splet, splet brez cenzure, kot je slika "tankovca" in zgodovina protestov na trgu Tiananmen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare ima veliko moč.V nekem smislu nadzorujejo, kaj končni uporabnik na koncu vidi.Zaradi Cloudflare-ja ne morete brskati po spletni strani. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare se lahko uporablja za cenzuro. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Ne morete si ogledati spletnega mesta z oblakom, če uporabljate manjši brskalnik, za katerega Cloudflare morda misli, da je bot (ker ga ne uporablja veliko ljudi). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Te invazivne preveritve brskalnika ne morete prenesti, ne da bi omogočili Javascript.To je izguba petih (ali več) sekund vašega dragocenega življenja. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare tudi samodejno blokira zakonite robote / pajke, kot so Google, Yandex, Yacy in API odjemalci.Cloudflare aktivno spremlja skupnost "bypass cloudflare" z namenom prebiti zakonite raziskovalne bote. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare podobno preprečuje, da bi mnogi ljudje, ki imajo slabo internetno povezavo, dostopali do spletnih strani za njim (na primer, lahko bi zaostali za 7+ sloji NAT ali delili isti IP, na primer javni Wifi), razen če ne rešijo več slikovnih CAPTCHA.V nekaterih primerih bo to trajalo od 10 do 30 minut, da Google zadovolji. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Leta 2020 je Cloudflare prešel iz Googlove Recaptcha v hCaptcha, saj namerava Google zaračunati njegovo uporabo.Cloudflare vam je povedal, da skrbi za vašo zasebnost ("pomaga reševati vprašanje zasebnosti"), vendar je to očitno laž.Vse gre za denar."HCaptcha omogoča spletnim mestom, da zaslužijo denar za to povpraševanje, hkrati pa blokira bote in druge oblike zlorabe." | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Z vidika uporabnika se to ne spremeni veliko. Prisiljen si ga rešiti. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare vsak dan blokira veliko ljudi in programske opreme. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare moti veliko ljudi po vsem svetu.Oglejte si seznam in pomislite, ali je sprejemanje Cloudflare na vašem spletnem mestu dobro za uporabniško izkušnjo. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Kakšen je namen interneta, če ne morete početi, kar želite?Večina ljudi, ki obišče vaše spletno mesto, bo samo poiskala druge strani, če ne morejo naložiti spletne strani.Morda ne blokirate nobenih obiskovalcev, vendar je privzeti požarni zid Cloudflare dovolj strog, da blokira veliko ljudi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Ni mogoče rešiti captcha, ne da bi omogočili Javascript in Cookies.Cloudflare jih uporablja za podpis brskalnika, da vas prepozna.Cloudflare mora poznati svojo identiteto, da se odloči, ali lahko še naprej brskate po spletnem mestu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Uporabniki Tor in uporabniki VPN so tudi žrtev Cloudflare-ja.Obe rešitvi uporablja veliko ljudi, ki si zaradi svoje države / korporacije / omrežne politike ne morejo privoščiti necenzuriranega interneta ali želijo dodati dodaten sloj za zaščito svoje zasebnosti.Cloudflare sramotno napada te ljudi in jih prisili, da izklopijo svojo proxy rešitev. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Če do tega trenutka niste preizkusili Tor, vam priporočamo, da naložite Tor Browser in obiščete priljubljena spletna mesta.Predlagamo, da se ne prijavite na svoje bančno spletno mesto ali vladno spletno stran ali pa bodo označili vaš račun. Za ta spletna mesta uporabite VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Morda boste želeli reči »Tor je nezakonit! Uporabniki Tor so krivi! Tor je slab! ". Ne.Morda ste se o Toru naučili s televizije, češ da se Tor lahko uporablja za brskanje po darknetu in trgovanje s puškami, drogami ali drobnimi pornografijami.Čeprav zgornja trditev drži, da obstaja veliko tržnih spletnih strani, na katerih lahko kupite takšne predmete, se ta spletna mesta pogosto pojavljajo tudi na clearnetu.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor je razvila ameriška vojska, trenutni Tor pa razvija projekt Tor.Veliko ljudi in organizacij uporablja Tor, vključno z vašimi bodočimi prijatelji.Če na svojem spletnem mestu uporabljate Cloudflare, blokirate prave ljudi.Izgubili boste morebitno prijateljstvo in poslovno pogodbo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Njihova storitev DNS, 1.1.1.1, prav tako odstrani uporabnike, da obiščejo spletno stran, tako da vrnejo ponarejeni IP naslov v lasti Cloudflare, localhost IP, kot je "127.0.0.x", ali pa preprosto ne vrnejo ničesar. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS prav tako prebija spletno programsko opremo od pametne aplikacije do računalniške igre zaradi ponarejenega odgovora DNS.Cloudflare DNS ne more poizvedovati nekaterih bančnih spletnih mest. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  In tukaj si lahko mislite,<br>Ne uporabljam Tor ali VPN, zakaj bi jaz skrbel?<br>Zaupam marketingu Cloudflare, zakaj bi me bilo vseeno<br>Na mojem spletnem mestu je https, zakaj bi mi bilo mar | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Če obiščete spletno mesto, ki uporablja Cloudflare, svoje podatke delite ne le lastniku spletnega mesta, ampak tudi Cloudflare.Tako deluje obratni proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Nemogoče je analizirati brez dešifriranja TLS prometa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare pozna vse vaše podatke, na primer surovo geslo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed se lahko zgodi kadar koli. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https Cloudflare-jev ni nikoli do konca. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Ali res želite deliti svoje podatke z Cloudflare in tudi s 3-pisno agencijo? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Spletni profil internetnih uporabnikov je "izdelek", ki ga želijo kupiti vlada in velika tehnološka podjetja. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Ameriško ministrstvo za notranjo varnost je sporočilo:<br><br>Ali imate kakšno idejo, kako dragoceni so vaši podatki? Ali obstaja kakšen način, da bi nam prodali te podatke?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare ponuja tudi BREZPLAČNO VPN storitev, imenovano "Cloudflare Warp".Če ga uporabljate, se vse povezave vašega pametnega telefona (ali računalnika) pošljejo strežnikom Cloudflare.Cloudflare lahko ve, katero spletno mesto ste prebrali, kakšen komentar ste objavili, s kom ste govorili itd.Vse svoje podatke prostovoljno pošljete Cloudflareu.Če mislite, "se šalite? Cloudflare je varen. " potem se morate naučiti, kako deluje VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare je dejal, da njihova storitev VPN omogoča hiter internet.Toda prek omrežja VPN je vaša internetna povezava počasnejša od obstoječe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Morda že veste za škandal z PRISM.Res je, da AT&T dovoljuje agenciji NSA, da kopira vse internetne podatke za nadzor. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Recimo, da delate pri NSA in želite imeti internetni profil vsakega državljana.Veste, da večina njih slepo zaupa Cloudflare-ju in uporablja le - en sam centraliziran prehod - za posredovanje povezave s svojim podjetniškim strežnikom (SSH / RDP), osebnim spletnim mestom, spletnim mestom za klepete, spletnim mestom na forumu, bančnim spletnim mestom, zavarovalnim mestom, iskalnikom, tajnim članom samo spletna stran, spletna stran za dražbe, nakupovanje, video spletna stran, spletna stran NSFW in nezakonita spletna stran.Prav tako veste, da uporabljajo storitev DNS Cloudflare ("1.1.1.1)" in storitev VPN ("Cloudflare Warp") za "Varno! Hitreje! Bolje! " internetna izkušnja.Če jih sestavite z uporabnikovim IP-naslovom, prstnim odtisom brskalnika, piškotki in ID-jem RAY-ja, bo koristno za izdelavo spletnega profila cilja. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Želiš njihove podatke. Kaj boš naredil? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare je satje.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Brezplačen med za vse. Nekaj ​​strun je pripetih.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Ne uporabljajte Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Decentralizirajte internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Prosimo, nadaljujte na naslednjo stran:  "[Cloudflare etika](sl.ethics.md)"

---

<details>
<summary>_klikni me_

## Podatki in več informacij
</summary>


To skladišče je seznam spletnih mest, ki stojijo za "Great Cloudwall" in blokirajo Tor-ove uporabnike in druge CDN-je.


**Podatki**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Uporabniki Cloudflare-ja](../cloudflare_users/)
* [Oblake v oblaku](../cloudflare_users/domains/)
* [Uporabniki CDN, ki niso v oblaku](../not_cloudflare/)
* [Uporabniki Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Več informacij**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Prenesi: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Originalno e-knjigo (ePUB) je BookRix GmbH izbrisal zaradi kršitve avtorskih pravic CC0 materiala
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Vstopnica je bila tolikokrat vandalizirana.
  * [Izbrisal projekt Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Glej vozovnico 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Zadnja arhivska vozovnica 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_klikni me_

## Kaj lahko narediš?
</summary>

* [Preberite naš seznam priporočenih dejanj in jih delite s prijatelji.](../ACTION.md)

* [Preberite drug uporabnikov glas in napišite svoje misli.](../PEOPLE.md)

* Nekaj ​​poiščite: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Posodobite seznam domen: [Seznam navodil](../INSTRUCTION.md).

* [V zgodovino dodajte Cloudflare ali dogodek, povezan s projektom.](../HISTORY.md)

* [Poskusite in napišite novo orodje / skript.](../tool/)

* [Tukaj je prebrati nekaj PDF / ePUB.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### O ponarejenih računih

Crimeflare ve za obstoj lažnih računov, ki predstavljajo naše uradne kanale, pa naj bodo to Twitter, Facebook, Patreon, OpenCollective, Villageges itd.
**Nikoli ne prosimo za vašo e-pošto.
Nikoli ne vprašamo tvojega imena.
Nikoli ne vprašamo vaše identitete.
Nikoli ne vprašamo vaše lokacije.
Nikoli ne prosimo za vašo donacijo.
Nikoli ne prosimo za vašo recenzijo.
Nikoli vas ne prosimo, da spremljate na družbenih medijih.
Nikoli ne vprašamo vaših socialnih medijev.**

# NE ZAUPAJTE FAKSE.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)