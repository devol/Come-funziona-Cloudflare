# Големиот Облак


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Престанете со Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  „The Great Cloudwall“ е компанија Cloudflare Inc., американска компанија.Обезбедува услуги на CDN (мрежна достава на содржина), ублажување на ДДОС, Интернет безбедност и дистрибуирани услуги на DNS (име на домен).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Облак Clareflare е најголемиот светски прокси MITM во светот (обратен прокси).Cloudflare поседува повеќе од 80% од пазарниот удел на CDN, а бројот на корисници на cloudflare се зголемува секој ден.Тие ја проширија својата мрежа во повеќе од 100 земји.Cloudflare служи повеќе веб сообраќај отколку комбинирани Twitter, Amazon, Apple, Instagram, Bing & Wikipedia.Cloudflare нуди бесплатен план и многу луѓе го користат наместо правилно да ги конфигурираат своите сервери.Тие тргувале со приватност преку погодност.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare седи помеѓу вас и веб-страната за потекло, дејствувајќи како агент за гранична патрола.Не сте во можност да се поврзете со одбраната дестинација.Вие се поврзувате со Cloudflare и сите ваши информации се дешифрираат и предаваат на летот. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Администраторот на веб-страницата со потекло му дозволи на агентот - Cloudflare - да одлучи кој може да има пристап до нивната „веб-сопственост“ и да дефинира „ограничена област“.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Погледнете ја вистинската слика.Thinkе помислите дека Cloudflare ги блокира само лошите момци.Willе помислите дека Cloudflare е секогаш на Интернет (никогаш не слегувајте).Willе помислите дека ботови и роботите можат да ја индексираат вашата веб-страница.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Сепак, тие воопшто не се вистинити.Облак Clare блокира невини луѓе без причина.Облак може да се спушти.Cloudflare блокира логички ботови.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Исто како и секоја услуга за хостирање, Cloudflare не е совршен.Screenе го видите овој екран дури и ако серверот за потекло работи добро.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Дали навистина мислите дека Cloudflare има 100% uptime?Немате претстава колку пати Cloudflare се спушта.Ако Cloudflare се спушти, вашиот клиент не може да пристапи до вашата веб-страница. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Се нарекува ова во однос на Големиот огнен ид на Кина што прави споредлива работа за филтрирање на многу луѓе од гледање на веб-содржини (т.е. сите во копното на Кина и луѓе надвор).Додека во исто време, оние кои не се засегнати да гледаат драматично поинаква мрежа, веб без цензура, како што е слика на „човек на тенк“ и историја на протести на „Плоштад Тијанамен“. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Облак Clare поседува голема моќ.Во извесна смисла, тие контролираат што конечно гледа крајниот корисник.Вие сте спречени да ја прелистувате веб-страницата заради Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare може да се користи за цензура. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Не можете да ја гледате веб-страницата со облак, ако користите помал прелистувач за кој Cloudflare може да мисли дека е бот (затоа што не го користат многу луѓе). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Не можете да ја поминете оваа инвазивна „проверка на прелистувачот“ без да го вклучите Javascript.Ова е губење на пет (или повеќе) секунди од вашиот вреден живот. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare исто така автоматски блокира легитимни роботи / роботи како што се клиенти Google, Yandex, Yacy и API.Cloudflare активно ја следи заедницата „заобиколувачки cloudflare“ со цел да ги разбие блогите за истражување. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare на сличен начин им спречува на многу луѓе кои имаат лоша поврзаност на Интернет да пристапат на веб-страниците зад неа (на пример, тие би можеле да бидат зад 7+ слоеви на NAT или да споделат иста IP, на пример јавни Wifi) освен ако не решат повеќекратна слика CAPTCHA.Во некои случаи, ова ќе потрае од 10 до 30 минути за да се задоволи Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Во 2020 година, Cloudflare се префрли од Recaptcha на Google во hCaptcha бидејќи Google има намера да наплати за неговата употреба.Cloudflare ви кажа дека се грижат за вашата приватност („помага во решавање на приватноста“), но очигледно е лага.Се работи за пари.„HCaptcha им овозможува на веб-страниците да заработат пари за да ја задоволат оваа побарувачка, додека блокираат ботови и други форми на злоупотреба“ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Од гледна точка на корисникот, ова не менува многу. Вие сте принудени да го решите тоа. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Многу луѓе и софтвер секојдневно ги блокираат Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Облак Clare вознемирува многу луѓе низ целиот свет.Погледнете во списокот и размислете дали усвојувањето Cloudflare на вашата страница е добро за корисничко искуство. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Која е целта на интернетот ако не можете да го направите она што го сакате?Повеќето луѓе кои ја посетуваат вашата веб-страница само ќе бараат други страници ако не можат да вчитаат веб-страница.Можеби не блокирате активно посетители, но стандардниот заштитен ид на Cloudflare е доволно строг за да блокира многу луѓе. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Не постои начин да се реши captcha без да се овозможат Javascript и Cookies.Cloudflare ги користи за да направи потпис на прелистувачот за да ве идентификува.Cloudflare треба да го знае вашиот идентитет за да одлучи дали сте eligeble за да продолжите со прелистување на страницата. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Корисниците на Tor и VPN корисниците исто така се жртва на Cloudflare.И двете решенија ги користат многу луѓе кои не можат да си дозволат нецензуриран интернет заради политиката на нивната земја / корпорација / мрежна мрежа или кој сака да додаде дополнителен слој за да ја заштити нивната приватност.Облак Clareflare бесрамно ги напаѓа тие луѓе, принудувајќи ги да го исклучат своето прокси решение. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Ако не сте пробале Tor до овој момент, ве охрабруваме да преземете Tor Browser и да ги посетите омилените веб-страници.Ви предлагаме да не се најавувате на веб-страницата на вашата банка или владината веб-страница или тие ќе ја обележат вашата сметка. Користете VPN за тие веб-страници. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Можеби ќе сакате да кажете „Тор е нелегален! Корисниците на Tor се криминални! Тор е лош! “. Не.Можеби научивте за Тор од телевизија, велејќи дека Тор може да се искористи за да пребарувате во темнината и да тргувате со оружје, лекови или чисто порно.Додека погоре изјавата е точно дека има многу веб-страница на пазарот каде што можете да купите такви артикли, овие страници честопати се појавуваат и на кларнет.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Тор беше развиен од американската армија, но сегашниот Тор е развиен со проектот Тор.Постојат многу луѓе и организации кои користат Тор, вклучувајќи ги и вашите идни пријатели.Значи, ако користите Cloudflare на вашата веб-страница, блокирате вистински луѓе.Loseе изгубите потенцијално пријателство и деловен договор. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  И нивната услуга DNS, 1.1.1.1, исто така ги филтрира корисниците од посета на веб-страницата со враќање на лажни IP адреси во сопственост на Cloudflare, IP локална, како што е „127.0.0.x“, или едноставно не враќаат ништо. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS исто така го расипува онлајн софтверот од апликација за паметни телефони до компјутерска игра, бидејќи на нивните лажни одговори на DNS.Cloudflare DNS не може да бара некои веб-страници на банка. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  И тука можеби ќе помислите,<br>Јас не користам Tor или VPN, зошто да се грижам?<br>Јас му верувам на Cloudflare маркетингот, зошто да се грижам<br>Мојата веб-страница е https зошто треба да се грижам | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Ако ја посетувате веб-страницата што користи Cloudflare, ги споделувате вашите информации не само на сопственикот на веб-страницата, туку и на Cloudflare.Вака работи обратниот прокси. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Невозможно е да се анализира без да се декриптира сообраќајот на TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare ги знае сите ваши податоци како што е сурова лозинка. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Облак може да се случи во секое време. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https на Cloudflare никогаш не е крај до крај. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Дали навистина сакате да ги споделите вашите податоци со Cloudflare, а исто така и со агенција со 3 букви? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Интернет-профилот на Интернет-корисникот е „производ“ што владата и големите технолошки компании сакаат да го купат. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Соопшти Министерството за внатрешна безбедност на САД:<br><br>Дали имате идеја колку се вредни податоците што ги имате? Дали постои некој начин да ни ги продадете тие податоци?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare исто така нуди бесплатна VPN услуга наречена „Cloudflare Warp“.Ако го користите, сите врски на вашиот паметен телефон (или вашиот компјутер) се испраќаат до серверите Cloudflare.Cloudflare може да знае на која веб-страница што ја прочитавте, со коментар што сте објавиле, со кого разговаравте, итн.Вие доброволно ги давате сите ваши информации на Cloudflare.Ако мислите „Дали се шегувате? Облак Clare е безбеден “. тогаш треба да научите како работи VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare рече дека нивната VPN услуга го прави брз вашиот интернет.Но, VPN вашата интернет-врска ја направи побавна од постојната врска. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Веќе знаете за скандалот ПРИСМ.Точно е дека АТ & Т му дозволува на НСА да ги копира сите податоци на Интернет за надзор. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Да речеме дека работите во НСА и сакате Интернет профил на секој граѓанин.Знаете дека повеќето од нив слепо веруваат во Cloudflare и ја користат - само една централизирана порта - за да ја заменуваат врската со серверот на нивната компанија (SSH / RDP), лична веб-страница, веб-страница за разговор, веб-страница на форумот, веб-страница на банката, веб-страница за осигурување, пребарувач, таен член единечна веб-страница, веб-страница на аукција, шопинг, видео веб-страница, веб-страница на NSFW и нелегална веб-страница.Исто така, знаете дека ја користат услугата DNS на Cloudflare ("1.1.1.1") и услугата VPN ("Cloudflare Warp") за "Безбедна! Побрзо! Подобро! “ Интернет искуствоКомбинирањето на истите со IP адресата на корисникот, отпечатокот од прелистувачот, колачињата и RAY-ID ќе биде корисно за да се изгради он-лајн профил на целта. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Вие ги сакате нивните податоци. Што ќе правиш? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare е саќе.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Бесплатно медо за секого. Некои жици се прикачени.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Не користете Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Децентрализирајте го интернетот.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Ве молиме, продолжете на следната страница:  "[Етика на Cloudflare](mk.ethics.md)"

---

<details>
<summary>_кликнете ме_

## Податоци и повеќе информации
</summary>


Ова складиште е список на веб-страници што стојат зад „Големиот Облак "ид“, блокирајќи ги корисниците на Тор и другите ЦДН.


**Податоци**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Корисници на Cloudflare](../cloudflare_users/)
* [Домени на Cloudflare](../cloudflare_users/domains/)
* [Корисници на не-Cloudflare CDN](../not_cloudflare/)
* [Анти-тор корисници](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Повеќе информации**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Преземи: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Оригиналниот eBook (ePUB) е избришан од BookRix GmbH заради повреда на авторските права на материјалот CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Билетот беше вандализиран многу пати.
  * [Избришан од проектот Тор.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Погледнете го билетот 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Последен архивски билет 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_кликнете ме_

## Што можеш да направиш?
</summary>

* [Прочитајте го нашиот список на препорачани активности и споделете го со вашите пријатели.](../ACTION.md)

* [Прочитајте го гласот на другите корисници и напишете ги мислите.](../PEOPLE.md)

* Пребарај нешто: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Ажурирајте го списокот на домени: [Наведете ги упатствата](../INSTRUCTION.md).

* [Додајте Cloudflare или настан поврзан со проектот во историјата.](../HISTORY.md)

* [Обидете се и напишете нова алатка / скрипта.](../tool/)

* [Еве неколку PDF / ePUB за да ги прочитате.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### За лажни сметки

Crimeflare знаат за постоење лажни сметки кои ги поддржуваат нашите официјални канали, било да е тоа Twitter, Facebook, Patreon, OpenCollective, Села итн.
**Никогаш не ја прашуваме вашата е-пошта.
Ние никогаш не го прашуваме вашето име.
Ние никогаш не го прашуваме вашиот идентитет.
Никогаш не ја прашуваме вашата локација.
Ние никогаш не ја бараме вашата донација.
Никогаш не бараме ваш преглед.
Никогаш не бараме од вас да следите на социјалните медиуми.
Ние никогаш не ги прашуваме вашите социјални медиуми.**

# НЕ ВЕРУВАТЕТЕ ДА СЕ СТРАБОТНИ СМЕТКИ



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)