# Didžioji debesų siena


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Sustabdykite „Cloudflare“


|  🖹  |  🖼 |
| --- | --- |
|  „Didysis debesų siena“ yra JAV įmonė „Cloudflare Inc.“.Ji teikia CDN (turinio pristatymo tinklo) paslaugas, DDoS mažinimą, interneto apsaugą ir paskirstytas DNS (domeno vardo serverio) paslaugas.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  „Cloudflare“ yra didžiausias pasaulyje MITM tarpinis serveris (atvirkštinis tarpinis serveris).„Cloudflare“ priklauso daugiau nei 80% CDN rinkos dalies, o „cloudflare“ vartotojų skaičius auga kiekvieną dieną.Jie išplėtė savo tinklą daugiau nei 100 šalių.„Cloudflare“ aptarnauja daugiau interneto srautų nei „Twitter“, „Amazon“, „Apple“, „Instagram“, „Bing“ ir „Wikipedia“ kartu.„Cloudflare“ siūlo nemokamą planą ir daugelis žmonių tuo naudojasi, užuot tinkamai sukonfigūravę savo serverius.Jie prekiavo privatumu dėl patogumo.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  „Cloudflare“ yra tarp jūsų ir jūsų žiniatinklio serverio, veikdamas kaip pasienio patrulių agentas.Negalite susisiekti su pasirinktu tikslu.Prisijungiate prie „Cloudflare“, o visa jūsų informacija yra iššifruojama ir perduodama skrendant. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Kilmės žiniatinklio serverio administratorius leido agentui - „Cloudflare“ - nuspręsti, kas gali pasiekti savo „žiniatinklio nuosavybę“ ir apibrėžti „ribojamą sritį“.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Pažvelkite į tinkamą vaizdą.Jūs manysite, kad „Cloudflare“ blokuos tik blogus vyrukus.Jūs manysite, kad „Cloudflare“ visada veikia internete (niekada nenusileidžia).Jūs manysite, kad teisėti robotai ir tikrinimo programos gali indeksuoti jūsų svetainę.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Tačiau tai visai netiesa.Debesų srautas be jokios priežasties blokuoja nekaltus žmones.Debesų liepsna gali sumažėti.„Cloudflare“ blokuoja teisėtus robotus.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Kaip ir bet kuri prieglobos paslauga, „Cloudflare“ nėra tobula.Pamatysite šį ekraną, net jei kilmės serveris veikia gerai.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Ar tikrai manote, kad „Cloudflare“ veikia 100%?Jūs net neįsivaizduojate, kiek kartų „Cloudflare“ nusileis.Jei „Cloudflare“ sumažėja, jūsų klientas negali pasiekti jūsų svetainės. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Tai vadinama atsižvelgiant į Didžiąją Kinijos užkardą, kuri atlieka panašų darbą, filtruodama daugybę žmonių, kad matytų interneto turinį (ty visus žemyninėje Kinijoje ir už jos ribų esančius žmones).Tuo pačiu metu tiems, kuriems nedaroma įtakos, matyti iš esmės skirtingas internetas, be cenzūros esantis internetas, pavyzdžiui, „tanko žmogaus“ įvaizdis ir „Tiananmenio aikštės protestų istorija“. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Debesų liepsna turi didelę galią.Tam tikra prasme jie kontroliuoja tai, ką mato galutinis vartotojas.Jums neleidžiama naršyti svetainėje dėl „Cloudflare“. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  „Cloudflare“ gali būti naudojamas cenzūrai. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Negalite peržiūrėti tinklalapio, kuriame uždengta debesų, jei naudojate nedidelę naršyklę, kuri „Cloudflare“ gali manyti, kad tai robotas (nes ne daug žmonių ja naudojasi). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Negalite išlaikyti šio invazinio „naršyklės patikrinimo“ neįjungę „Javascript“.Tai yra penkių (ar daugiau) sekundžių jūsų brangaus gyvenimo švaistymas. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  „Cloudflare“ taip pat automatiškai blokuoja teisėtus robotus / tikrinimo įrenginius, tokius kaip „Google“, „Yandex“, „Yacy“ ir API klientai.„Cloudflare“ aktyviai stebi „apeiti debesų liepsnos“ bendruomenę, norėdama sunaikinti teisėtus tyrimų robotus. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  „Cloudflare“ taip pat neleidžia daugeliui žmonių, turinčių prastą interneto ryšį, patekti į tinklalapius, esančius už jo (pavyzdžiui, jie gali būti už 7 ar daugiau NAT sluoksnių arba dalytis tuo pačiu IP, pavyzdžiui, viešuoju „Wifi“), nebent jie išspręstų kelis vaizdo CAPTCHA.Kai kuriais atvejais „Google“ patenkinti prireiks 10–30 minučių. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020 m. „Cloudflare“ perėjo iš „Google“ „Recaptcha“ į „hCaptcha“, nes „Google“ ketina imti mokestį už jo naudojimą.„Cloudflare“ sakė, kad jiems rūpi jūsų privatumas („tai padeda išspręsti susirūpinimą dėl privatumo“), tačiau tai akivaizdžiai yra melas.Viskas susiję su pinigais.„„ HCaptcha “leidžia svetainėms užsidirbti pinigų tenkinant šią paklausą ir blokuoti robotus bei kitokį piktnaudžiavimą“ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Vartotojo požiūriu, tai beveik nesikeičia. Esate priversti tai išspręsti. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  „Cloudflare“ kiekvieną dieną blokuoja daugybę žmonių ir programinę įrangą. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Debesų liepsna erzina daugelį žmonių visame pasaulyje.Pažvelkite į sąrašą ir pagalvokite, ar „Cloudflare“ pritaikymas jūsų svetainėje naudingas vartotojui. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Koks yra interneto tikslas, jei negalite padaryti to, ko norite?Daugelis žmonių, kurie lankosi jūsų svetainėje, tiesiog ieškos kitų puslapių, jei negalės įkelti tinklalapio.Galbūt aktyviai neblokuojate jokių lankytojų, tačiau „Cloudflare“ numatytoji užkarda yra pakankamai griežta, kad užblokuotų daugybę žmonių. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Neįmanoma išspręsti „captcha“ neįjungus „Javascript“ ir „Cookies“.„Cloudflare“ naudoja juos naršyklės parašui identifikuoti.„Cloudflare“ turi žinoti jūsų tapatybę, kad galėtų nuspręsti, ar norite toliau naršyti svetainėje. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  „Tor“ vartotojai ir VPN vartotojai taip pat yra „Cloudflare“ aukos.Abu sprendimus naudoja daugybė žmonių, kurie dėl savo šalies / korporacijos / tinklo politikos negali sau leisti necenzūruoto interneto arba kurie nori pridėti papildomą sluoksnį, kad apsaugotų savo privatumą.„Cloudflare“ begėdiškai puola tuos žmones, priversdami juos išjungti įgaliotojo serverio sprendimą. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Jei iki šiol neišbandėte „Tor“, raginame atsisiųsti „Tor“ naršyklę ir apsilankyti mėgstamose svetainėse.Mes siūlome neprisijungti prie savo banko ar vyriausybės tinklalapio, nes jie pažymės jūsų sąskaitą. Tose svetainėse naudokite VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Galbūt norėsite pasakyti: „Tor yra neteisėtas! „Tor“ vartotojai yra nusikalstami! Tor yra blogai! “. Ne.Apie „Tor“ galbūt sužinojote iš televizijos, sakydamas, kad „Tor“ gali būti naudojamas naršyti „darknet“ ir prekiauti ginklais, narkotikais ar „chid porn“.Nors minėtas teiginys yra teisingas, kad yra daugybė rinkos svetainių, kuriose galite nusipirkti tokių prekių, šios svetainės dažnai yra ir „Clearnet“ tinklalapyje.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  „Tor“ sukūrė JAV armija, tačiau dabartinį „Tor“ kuria „Tor“ projektas.Yra daug žmonių ir organizacijų, kurie naudoja „Tor“, įskaitant jūsų būsimus draugus.Taigi, jei jūs naudojate „Cloudflare“ savo svetainėje, jūs blokuojate tikrus žmones.Prarasite galimą draugystę ir verslo reikalus. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Jų DNS paslauga 1.1.1.1 taip pat pašalina vartotojus nuo apsilankymo svetainėje, grąžindama suklastotą IP adresą, priklausantį „Cloudflare“, „localhost“ IP, pvz., „127.0.0.x“, arba tiesiog nieko negrąžindama. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  „Cloudflare DNS“ taip pat nutraukia internetinę programinę įrangą nuo išmaniųjų telefonų programos iki kompiuterinio žaidimo dėl jų suklastoto DNS atsakymo.„Cloudflare“ DNS negali atlikti užklausų kai kuriose banko svetainėse. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Ir čia gali pagalvoti,<br>Aš nenaudoju „Tor“ ar VPN, kodėl man tai turėtų rūpėti?<br>Aš pasitikiu „Cloudflare“ rinkodara, kodėl man tai turėtų būti svarbu<br>Mano svetainė yra https, kodėl man tai turėtų rūpėti | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Jei lankotės svetainėje, kurioje naudojama „Cloudflare“, dalijatės savo informacija ne tik svetainės savininkui, bet ir „Cloudflare“.Taip veikia atvirkštinis tarpinis serveris. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Neįmanoma išanalizuoti iššifruojant TLS srautą. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  „Cloudflare“ žino visus jūsų duomenis, pvz., Neapdorotą slaptažodį. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  „Cloudbeed“ gali įvykti bet kada. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  „Cloudflare“ https nėra niekada tikslus. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Ar tikrai norite pasidalyti savo duomenimis su „Cloudflare“, taip pat su 3 raidžių agentūra? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Interneto vartotojo internetinis profilis yra „produktas“, kurį nori įsigyti vyriausybė ir didžiosios technologijų įmonės. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Sakė JAV vidaus saugumo departamentas:<br><br>Ar turite minčių, kokie vertingi jūsų turimi duomenys? Ar yra koks būdas parduoti mums tuos duomenis?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  „Cloudflare“ taip pat siūlo nemokamą VPN paslaugą pavadinimu „Cloudflare metmenys“.Jei ja naudojatės, visi jūsų išmaniojo telefono (arba kompiuterio) ryšiai siunčiami į „Cloudflare“ serverius.„Cloudflare“ gali žinoti, kurią svetainę perskaitėte, kokį komentarą paskelbėte, su kuo kalbėjotės ir pan.Jūs savanoriškai teikiate visą savo informaciją „Cloudflare“.Jei manote: „Jūs juokaujate? Debesų liepsna yra saugi. “ tada jūs turite sužinoti, kaip veikia VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  „Cloudflare“ teigė, kad jų VPT paslauga pagreitina jūsų internetą.Tačiau VPN daro jūsų interneto ryšį lėtesnį nei jūsų turimas. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Galbūt jau žinote apie PRISM skandalą.Tiesa, kad AT&T leidžia NSA nukopijuoti visus interneto duomenis stebėjimui. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Tarkime, kad dirbate NSA ir norite kiekvieno piliečio interneto profilio.Jūs žinote, kad dauguma jų aklai pasitiki „Cloudflare“ ir naudojasi juo - tik vienu centralizuotu šliuzu - norėdami perduoti savo įmonės serverio ryšį (SSH / RDP), asmeninę svetainę, pokalbių svetainę, forumo svetainę, banko svetainę, draudimo svetainę, paieškos variklį, slaptą narį. tik svetainė, aukcionų svetainė, apsipirkimas, vaizdo įrašų svetainė, NSFW svetainė ir neteisėta svetainė.Taip pat žinote, kad jie naudoja „Cloudflare“ DNS paslaugą („1.1.1.1“) ir VPN paslaugą („Cloudflare metmenys“) norėdami „Saugiai! Greičiau! Geriau! “ interneto patirtis.Juos sujungus su vartotojo IP adresu, naršyklės pirštų atspaudais, slapukais ir RAY-ID, bus naudinga kuriant tikslinį internetinį profilį. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Jūs norite jų duomenų. Ką tu darysi? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Debesų liepsna yra medaus puodas.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Nemokamas medus visiems. Kai kurios stygos pritvirtintos.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Nenaudokite „Cloudflare“.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Decentralizuokite internetą.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Prašau tęsti kitą puslapį:  "[Debesų liepsnos etika](lt.ethics.md)"

---

<details>
<summary>_Paspausk mane_

## Duomenys ir daugiau informacijos
</summary>


Ši saugykla yra svetainių, esančių už „Didžiosios debesies sienos“, blokuojančios „Tor“ vartotojus ir kitus CDN, sąrašas.


**Duomenys**
* [„Cloudflare Inc.“](../cloudflare_inc/)
* [„Cloudflare“ vartotojai](../cloudflare_users/)
* [„Cloudflare“ domenai](../cloudflare_users/domains/)
* [CDN vartotojai, kurie nėra „Cloudflare“](../not_cloudflare/)
* [„Anti-Tor“ vartotojai](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Daugiau informacijos**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * parsisiųsti: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Originalią el. Knygą (ePUB) ištrynė „BookRix GmbH“ dėl autorių teisių pažeidimo CC0 medžiagoje
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Bilietas buvo vandalizuotas tiek kartų.
  * [Išbraukė „Tor“ projektas.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Žr. Bilietą 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Paskutinio archyvo bilietas 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_Paspausk mane_

## Ką tu gali padaryti?
</summary>

* [Perskaitykite mūsų rekomenduojamų veiksmų sąrašą ir pasidalykite juo su draugais.](../ACTION.md)

* [Perskaitykite kito vartotojo balsą ir parašykite savo mintis.](../PEOPLE.md)

* Ko nors ieškokite: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Atnaujinkite domenų sąrašą: [Išvardykite instrukcijas](../INSTRUCTION.md).

* [Pridėkite „Cloudflare“ arba su projektu susijusį įvykį į istoriją.](../HISTORY.md)

* [Pabandykite ir parašykite naują įrankį / scenarijų.](../tool/)

* [Čia yra keletas perskaitytų PDF / ePUB.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Apie netikras sąskaitas

„Crimeflare“ žino apie netikrų paskyrų, apsimetančių mūsų oficialiais kanalais, buvimą „Twitter“, „Facebook“, „Patreon“, „OpenCollective“, „Village“ ir kt., Egzistavimą.
**Mes niekada neprašome jūsų el.
Mes niekada neklausiame tavo vardo.
Mes niekada neklausiame jūsų tapatybės.
Mes niekada neklausiame jūsų buvimo vietos.
Mes niekada neprašome jūsų aukos.
Mes niekada neprašome jūsų apžvalgos.
Mes niekada neprašome jūsų sekti socialinėje žiniasklaidoje.
Mes niekada neklausiame jūsų socialinės žiniasklaidos.**

# NETIKĖKITE FAKTINĖS SĄSKAITOS.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)