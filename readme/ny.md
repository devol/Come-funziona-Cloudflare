# Cloudwall Yabwino


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Imani Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "Great Cloudwall" ndi Cloudflare Inc., kampani ya U.S.Imapereka ma CDN (maukonde operekera zopereka), ma DDoS mayendedwe, chitetezo cha intaneti, ndikugawa ma DNS (seva ya dzina laulemu).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare ndiye projekiti yayikulu kwambiri padziko lonse ya MITM (njira yosinthira).Cloudflare imakhala ndi gawo loposa 80% ya gawo lamsika la CDN ndipo chiwerengero cha ogwiritsa ntchito Cloudflare chikukula tsiku lililonse.Akulitsa maukonde awo kumayiko opitilira 100.Cloudflare imapereka magalimoto ambiri pa intaneti kuposa Twitter, Amazon, Apple, Instagram, Bing & Wikipedia yophatikizidwa.Cloudflare ikupereka pulani yaulere ndipo anthu ambiri akuigwiritsa ntchito m'malo kukhazikitsa ma seva awo moyenera.Anasinthanitsa chinsinsi chifukwa chophweka.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare imakhala pakati panu ndikuchokera kwa webserver, amagwira ngati woyang'anira malire.Simungathe kulumikizana komwe mukupita.Mukualumikizana ndi Cloudflare ndipo chidziwitso chanu chonse chikulembedwanso ndikupatsidwa ntchentche. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Woyang'anira webserver woyambira adalola wothandizira - Cloudflare - kusankha yemwe angathe kulowa mu "tsamba lawebusayiti" ndikutanthauzira "malo okhawo".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Onani chithunzi choyenera.Mukuganiza kuti Cloudflare ikutchingira anyamata oyipa okha.Mukuganiza kuti Cloudflare imakhala pa intaneti nthawi zonse (osatsika).Muganiza kuti bots ndi amatsenga amatha kuwonetsa tsamba lanu.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Komabe sizowona konse ayi.Cloudflare ikutseka anthu osalakwa popanda chifukwa.Cloudflare ikhoza kutsika.Cloudflare imalepheretsa bots bots.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Monga ntchito yokhazikika iliyonse, Cloudflare siyabwino.Muwona chophimba ichi ngakhale seva yoyambira ikugwira bwino ntchito.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Kodi mukuganiza kuti Cloudflare ili ndi nthawi yokwanira 100%?Simukudziwa kuti Cloudflare imatsika kangati.Cloudflare ikatsika kasitomala wanu sangathe kulowa pa tsamba lanu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Amatchedwa izi potanthauza Great Firewall ya China yomwe imagwiranso ntchito yofanizira kuwonetsa anthu ambiri kuti asaone zolemba za intaneti (mwachitsanzo, aliyense ku China komanso anthu akunja).Munthawi yomweyo iwo omwe sanakhutire kuti awone tsamba losiyana kwambiri, tsamba lolephera kudziwika monga chithunzi cha "tanki man" ndi mbiri ya "ziwonetsero za" Tiananmen Square ". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare ili ndi mphamvu yayikulu.Mwanjira ina, iwo amalamulira zomwe womaliza amatha kuwona.Mukuletsedwa kusakatula tsambalo chifukwa cha Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare ikhoza kugwiritsidwa ntchito pakufufuza. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Simungathe kuwona tsamba lomwe limakhala ndi mitambo ngati mukugwiritsa ntchito bulawuza yaying'ono yomwe Cloudflare ingaganize kuti ndi bot (chifukwa sianthu ambiri omwe amagwiritsa ntchito). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Simungathe kudutsitsa "browser yanu" yowononga iyi popanda kuthandizira Javascript.Uku ndikuwonongerani masekondi asanu (kapena kuposa) a moyo wanu wamtengo wapatali. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare imangodziletsa okha ma robot / kukwawa monga Google, Yandex, Yacy, ndi makasitomala a API.Cloudflare ikuyang'anira anthu "odutsa Cloudflare" ammudzi ndi cholinga chophwanya malamulo ochita kafukufuku. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare mofananamo imalepheretsa anthu ambiri omwe ali ndi intaneti yolakwika kuti asalowemo masamba omwe ali kumbuyo kwake (mwachitsanzo, atha kukhala kumbuyo kwa zigawo za 7+ za NAT kapena kugawana IP yomweyo, mwachitsanzo pagulu la Wifi) pokhapokha atathetsa ma CAPTCHAs angapo.Nthawi zina, izi zimatenga mphindi 10 mpaka 30 kukwaniritsa Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Mu chaka cha 2020 Cloudflare anasintha kuchokera ku Recaptcha ya Google kupita ku hCaptcha popeza Google imafuna kulipiritsa kuti igwiritse ntchito.Cloudflare anakuwuzani kuti amasamala zinsinsi zanu ("zimathandiza kuthana ndi chinsinsi") koma izi ndi zabodza.Zonsezi ndizokhudza ndalama."HCaptcha imalola mawebusayiti kuti apange ndalama pochita izi pobisa ma bots ndi mitundu ina ya nkhanza" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Malinga ndi ogwiritsa ntchito, izi sizisintha kwambiri. Mukukakamizidwa kuzithetsa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Anthu ambiri ndi mapulogalamu ali oletsedwa ndi Cloudflare tsiku lililonse. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare imakwiyitsa anthu ambiri padziko lonse lapansi.Onani mndandandawo ndikuganiza ngati kukhazikitsa Cloudflare patsamba lanu ndikothandiza kwa ogwiritsa ntchito. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Kodi cholinga cha intaneti ndi chiyani ngati simungathe kuchita zomwe mukufuna?Anthu ambiri omwe amayendera tsamba lanu angoyang'ana masamba ena ngati sangathe kutsatsa masamba.Mwina simukuletsa alendo aliwonse, koma zopopera moto za Cloudflare ndizokhazikika kuti muletse anthu ambiri. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Palibe njira yothanirana ndi Captcha popanda kuyambitsa Javascript ndi Cookies.Cloudflare akuwagwiritsa ntchito kupanga siginecha ya asakatuli kuti ikudziwitseni.Cloudflare ikufunika kudziwa chizindikiritso chanu kuti musankhe ngati mukupitiliza kusakatula tsambalo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Ogwiritsa ntchito a Tor ndi ogwiritsa ntchito a VPN nawonso amachitidwa chipongwe ndi Cloudflare.Njira zonse ziwirizi zikugwiritsidwa ntchito ndi anthu ambiri omwe sangakwanitse kugula intaneti chifukwa cha mayiko awo / kampani kapena maukonde kapena amene akufuna kuwonjezera zachitetezo chawo.Cloudflare ikuwopseza anthu awa mopanda manyazi, kuwakakamiza kuti athane ndi yankho lawo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Ngati simunayesere Tor mpaka pano, tikukulimbikitsani kutsitsa Tor Browser ndikuchezera masamba omwe mumakonda.Tikukulimbikitsani kuti musalumikizane ndi tsamba lanu la banki kapena tsamba la boma kapena aletse akaunti yanu. Gwiritsani ntchito VPN pamasamba amenewo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Mungafune kunena kuti "Tor ndizosaloledwa! Ogwiritsa ntchito Tor ndiwachifwamba! Tor ndiyabwino! ".Mutha kuphunzirapo za Tor kuchokera pa wailesi yakanema, kuti Tor ikhoza kugwiritsidwa ntchito kuwunika mfuti zamtundu wakuda ndi malonda, mankhwala osokoneza bongo kapena chidole.Ngakhale mawu ali pamwambawa ndiowona kuti pali masamba ambiri pamsika pomwe mungagule zinthuzi, masamba amenewo amawonekeranso pa clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor idapangidwa ndi US Army, koma Tor yamakono imapangidwa ndi pro Tor.Pali anthu ambiri ndi mabungwe omwe amagwiritsa ntchito Tor kuphatikiza anzako amtsogolo.Chifukwa chake, ngati mukugwiritsa ntchito Cloudflare patsamba lanu mukuletsa anthu enieni.Mudzataya mwayi wokhala paubwenzi ndi bizinesi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Ndipo ntchito yawo ya DNS, 1.1.1.1, ikutanthauzanso ogwiritsa ntchito kuti ayambe kutsatsa tsambalo pobweza IP adilesi ya Cloudflare, IP yakomweko ngati "127.0.0.x", kapena osangobweza chilichonse. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS imaphwanyanso mapulogalamu a pa intaneti kuchokera ku pulogalamu ya smartphone kupita ku masewera apakompyuta chifukwa cha yankho lawo labodza la DNS.Cloudflare DNS silingayankhe pamasamba ena akubanki. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Ndipo apa mungaganize,<br>Sindikugwiritsa ntchito Tor kapena VPN, ndichifukwa chiyani ndiyenera kusamala?<br>Ndikudalira kutsatsa kwa Cloudflare, bwanji ndiyenera kusamala<br>Webusayiti yanga ndi https chifukwa chake ndiyenera kusamala | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Ngati mungayendere masamba omwe amagwiritsa ntchito Cloudflare, mukugawana zambiri osati kwa eni webusayiti komanso Cloudflare.Umu ndi momwe njira yokhotakhota imagwirira ntchito. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Ndizosatheka kusanthula popanda kuwongolera kuchuluka kwa TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare amadziwa data yanu yonse monga password yabata. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed ikhoza kuchitika nthawi iliyonse. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Mawonekedwe a Cloudflare'sS satha. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Kodi mukufunitsitsadi kugawana data ndi Cloudflare, komanso bungwe la 3-kalata? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Mbiri ya ogwiritsa ntchito pa intaneti ndi "chinthu" chomwe boma ndi makampani akuluakulu a tekinoloje akufuna kugula. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Unduna wa Zachitetezo ku U.S. unatero:<br><br>Kodi mukudziwa chilichonse chomwe deta yanu ili yofunikira? Kodi pali njira iliyonse yomwe mungatigulitsire deta imeneyi?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare imaperekanso ntchito yaulere ya VPN yotchedwa "Cloudflare Warp".Ngati mungagwiritse ntchito, kulumikizana kwanu konse kwa ma smartphone (kapena kompyuta yanu) kumatumizidwa ku Cloudflare seva.Cloudflare imatha kudziwa tsamba lomwe mwawerengera, zomwe mudalemba, omwe mwalankhula naye ndi zina zambiri.Mukupereka mwakufuna kwanu ku Cloudflare.Ngati mukuganiza kuti "Mukunena nthabwala? Cloudflare ndiotetezeka. ” ndiye muyenera kuphunzira momwe VPN imagwirira ntchito. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare adati ntchito yawo ya VPN imapangitsa intaneti yanu kukhala yachangu.Koma VPN imapangitsa kulumikizana kwanu kwaintaneti kukhala kochepera kuposa kulumikizidwa kwanu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Mutha kudziwa kale za vuto la PRISM.Ndizowona kuti AT&T imalola NSA kuti ikope zonse zapaintaneti kuti ziwonetsedwe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Tinene kuti mukugwira ntchito ku NSA, ndipo mukufuna mbiri yapa nzika iliyonse.Mukudziwa kuti ambiri amakhulupirira Cloudflare mosazindikira ndipo amagwiritsa ntchito - khomo limodzi lokhalo - kutsimikizira kulumikizana kwa seva ya kampani yawo (SSH / RDP), tsamba lanu, tsamba lawebusayiti, tsamba la webusayiti, tsamba la webusayiti, tsamba la webusayiti, tsamba la inshuwaransi, osaka zobisika - Webusayiti yokhayo, tsamba la malonda, kugula zinthu, tsamba lamavidiyo, NSFW, tsamba lawebusayiti.Mukudziwanso kuti amagwiritsa ntchito ntchito ya Cloudflare's DNS ("1.1.1.1") ndi ntchito ya VPN ("Cloudflare Warp") ya "Otetezeka! Mofulumirirako! Bwino! " intaneti.Kuphatikiza ndi adilesi ya IP ya ogwiritsa, zala zakusakatuli, ma cookie ndi RAY-ID ndizothandiza popanga mbiri ya intaneti. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Mukufuna zambiri. Mutani? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare ndi chisa cha uchi.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Uchi waulere kwa aliyense. Zingwe zina zomata.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Osagwiritsa ntchito Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Kwezani intaneti.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Chonde pitilizani patsamba lotsatila:  "[Makhalidwe Abwino a Cloudflare](ny.ethics.md)"

---

<details>
<summary>_dinani_

## Zambiri ndi Zambiri
</summary>


Bokosi ili ndi mndandanda wa masamba omwe ali kumbuyo kwa "The Cloudwall", akutseka ogwiritsa ntchito a Tor ndi ma CDN ena.


**Zambiri**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Ogwiritsa Ntchito Cloudflare](../cloudflare_users/)
* [Cloudflare Domain](../cloudflare_users/domains/)
* [Ogwiritsa ntchito a CDN omwe si a Cloudflare](../not_cloudflare/)
* [Ogwiritsa ntchito a Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Zambiri**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Tsitsani: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * EBook yoyambilira (ePUB) idachotsedwa ndi BookRix GmbH chifukwa chophwanya malamulo okhudzana ndi CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Tikiti idawonongedwa kambiri.
  * [Yachotsedwa ndi Tor Pro.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Onani tikiti 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Tikiti yomaliza yosungiramo 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_dinani_

## Kodi mungatani?
</summary>

* [Werengani mndandanda wathu wa zomwe mwalimbikitsa kuti mugawane ndi anzanu.](../ACTION.md)

* [Werengani mawu a ogwiritsa ntchito ena ndikulemba malingaliro anu.](../PEOPLE.md)

* Sakani kena kake: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Sinthani mndandanda wamtundu: [Malangizo mndandanda](../INSTRUCTION.md).

* [Onjezani Cloudflare kapena chochitika chokhudzana ndi polojekiti.](../HISTORY.md)

* [Yeserani & lembani chida chatsopano / script.](../tool/)

* [Nayi PDF / ePUB yoti muwerenge.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### About nkhani zabodza

Achifwamba amadziwa za kupezeka kwa nkhani zabodza zomwe zimatsata njira zathu, ngati ndi Twitter, Facebook, Patreon, OpenCollective, Midzi etc.
**Sitifunsa imelo yanu.
Sitifunsa dzina lanu.
Sitifunsa kuti ndinu ndani.
Sitifunsa konse komwe muli.
Sitifunsa konse zopereka zanu.
Sitifunsanso ndemanga yanu.
Sitikukupemphani kuti muzitsatira pazanema.
Sitikufunsani muma TV anu.**

# OSAKHULUPIRIRA MALANGIZO.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)