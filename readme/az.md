# Böyük Buludlu


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Cloudflare dayandırın


|  🖹  |  🖼 |
| --- | --- |
|  "Böyük Cloudwall" ABŞ şirkəti olan Cloudflare Inc-dir.Bu CDN (məzmun çatdırılma şəbəkəsi) xidmətləri, DDoS azaldılması, İnternet təhlükəsizliyi və paylanmış DNS (domen adı server) xidmətləri təqdim edir.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare dünyanın ən böyük MITM proxy (ters proxy).Cloudflare CDN bazar payının 80% -dən çoxuna sahibdir və cloudflare istifadəçilərinin sayı hər gün artır.Şəbəkələrini 100-dən çox ölkəyə qədər genişləndirdilər.Cloudflare Twitter, Amazon, Apple, Instagram, Bing və Wikipedia'dan daha çox veb trafikə xidmət edir.Cloudflare pulsuz plan təklif edir və bir çox insan serverlərini düzgün konfiqurasiya etmək əvəzinə istifadə edir.Rahatlıqdan çox gizlilik ticarət etdilər.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare, sərhəd patrul agenti kimi davranaraq aranızda bir veb-server arasında oturur.Seçdiyiniz məkana qoşula bilmirsiniz.Cloudflare-yə qoşulursunuz və bütün məlumatlarınızı şifrələnir və tez bir zamanda təhvil verilir. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Mənşəli veb-server administratoru, agentə - Cloudflare - "veb əmlakına" kimin daxil olacağını və "məhdud ərazini" müəyyənləşdirməyi qərara aldı.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Doğru görüntüyə baxın.Cloudflare'nin yalnız pis adamları blok edəcəyini düşünürsən.Cloudflare'nın hər zaman onlayn olduğunu düşünəcəksiniz (heç vaxt enməyin).Qanuni botlar və tarayıcıların veb saytınızı indeks edə biləcəyini düşünürsünüz.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Ancaq bunlar heç də doğru deyil.Cloudflare günahsız insanları heç bir səbəb olmadan mühasirəyə alır.Buludlar enə bilər.Cloudflare legit botları blok edir.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Hər hansı bir hosting xidməti kimi, Cloudflare də mükəmməl deyil.Mənşi server yaxşı işləyirsə belə bu ekranı görəcəksiniz.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Həqiqətən Cloudflare'nin 100% işləmə vaxtı varmı?Cloudflare neçə dəfə aşağı endiyindən xəbəriniz yoxdur.Cloudflare aşağı düşərsə, müştəri veb saytınıza daxil ola bilməz. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Bu, çox sayda insanı veb məzmunu (yəni materik Çində olanlar və xaricdəki insanlar) görməməsi üçün müqayisə olunan bir işi görən Çinin Böyük Firewallına deyilir.Eyni zamanda kəskin fərqli bir veb, "tank adamı" görüntüsü və "Tiananmen Meydanı etirazları" tarixi kimi bir senzuradan azad bir internet görmək üçün təsirlənməyənlər. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare böyük gücə malikdir.Müəyyən mənada son istifadəçinin nəyi gördüyünə nəzarət edirlər.Cloudflare səbəbiylə veb saytı nəzərdən keçirməyinizə mane olur. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare senzura üçün istifadə edilə bilər. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Cloudflare'nin bot olduğunu düşünən kiçik bir brauzerdən istifadə edirsinizsə, buludlu veb saytı görə bilməzsiniz (çünki çox adam istifadə etmir). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Javascript-i işə salmadan bu invaziv “brauzer yoxlamasından” keçə bilməzsiniz.Bu, dəyərli həyatınızın beş (və ya daha çox) saniyəsinin boşa çıxmasıdır. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare də avtomatik olaraq Google, Yandex, Yacy və API müştəriləri kimi qanuni robotları / tarayıcıları maneə törədir.Cloudflare, qanuni tədqiqat botlarını qırmaq niyyəti ilə "bypass cloudflare" cəmiyyətini fəal şəkildə izləyir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare, zəif internet bağlantısı olan bir çox insanın, birdən çox görüntü CAPTCHA-larını həll etmədiyi təqdirdə (məsələn, 7+ təbəqənin arxasında və ya eyni IP, məsələn ictimai Wifi-nin arxasında ola bilər) qarşısını alır.Bəzi hallarda bu, Google-dan razı qalmaq üçün 10-30 dəqiqə çəkəcəkdir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020-ci ildə Cloudflare, Google istifadə üçün ödəniş almaq niyyətində olduğu üçün Google'un Recaptcha'dan hCaptcha'a keçdi.Cloudflare sizə məxfiliyinizlə maraqlandıqlarını söylədi ("bu gizliliklə əlaqəli məsələni həll etməyə kömək edir"), amma bu açıq bir yalandır.Hamısı pula aiddir."HCaptcha, botları və digər sui-istifadə hallarının qarşısını alarkən veb saytlara bu tələbi ödəyən pul qazanmağa imkan verir" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  İstifadəçinin nöqteyi-nəzərindən bu çox da dəyişmir. Bunu həll etmək məcburiyyətində qalırsınız. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Hər gün bir çox insan və proqram Cloudflare tərəfindən bloklanır. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare, dünyada bir çox insanı əsəbiləşdirir.Siyahıya nəzər yetirin və saytınızdakı Cloudflare'yi qəbul etməyin istifadəçi təcrübəsi üçün yaxşı olub olmadığını düşünün. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  İstədiyinizi edə bilmirsinizsə, internetin məqsədi nədir?Veb saytınızı ziyarət edən insanların əksəriyyəti veb səhifəni yükləyə bilmədikləri təqdirdə başqa səhifələr axtaracaqlar.Hər hansı bir ziyarətçini aktiv şəkildə blok edə bilməzsiniz, ancaq Cloudflare'nın standart firewallı bir çox insanın qarşısını almaq üçün kifayətdir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Javascript və Cookies'i işə salmadan captcha həll etmək üçün bir yol yoxdur.Cloudflare, sizi tanımaq üçün brauzer imza atmaq üçün onlardan istifadə edir.Cloudflare, saytı gəzməyə davam etmək üçün uyğun olub olmadığınıza qərar vermək üçün şəxsiyyətinizi bilməlidir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor istifadəçiləri və VPN istifadəçiləri də Cloudflare qurbanıdırlar.Hər iki həll yolu, ölkəsi / korporasiyası / şəbəkə siyasəti səbəbiylə senzensiz internet əldə edə bilməyən və ya gizliliyini qorumaq üçün əlavə qat qatmaq istəyən bir çox insan tərəfindən istifadə olunur.Cloudflare utancaqlıqla həmin insanlara hücum edir, onları proxy həllini söndürməyə məcbur edir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Tor'u bu anadək sınamamısınızsa, Tor Brauzerini yükləməyi və sevdiyiniz veb saytları ziyarət etməyi məsləhət görürük.Bankınızın veb saytına və ya hökumət veb səhifəsinə girməməyinizi təklif edirik və ya hesabınızı bayraq edəcəklər. Bu veb saytlar üçün VPN istifadə edin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  “Tor qanunsuzdur! Tor istifadəçiləri cinayətkardır! Tor pisdir! ". Yox.Tor haqqında məlumatı televizordan öyrənə bilərsən, Torun qaranlıq şəbəkə və ticarət silahları, narkotiklər və ya uşaq pornolarına baxmaq üçün istifadə edilə biləcəyini söylədi.Yuxarıdakı ifadədə bu cür əşyaları satın ala biləcəyiniz bir çox bazar veb saytının olduğu həqiqət olsa da, həmin saytlar çox vaxt aydın şəbəkələrdə də görünür.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor ABŞ ordusu tərəfindən hazırlanmışdır, lakin Tor Tor layihəsi tərəfindən hazırlanmışdır.Tor istifadə edərək, gələcək dostlarınız da daxil olmaqla bir çox insan və təşkilatlar var.Beləliklə, veb saytınızdakı Cloudflare istifadə edirsinizsə, real insanlara mane olur.Potensial dostluq və iş müqavilənizi itirəcəksiniz. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Onların DNS xidməti, 1.1.1.1, istifadəçilərin Cloudflare, saxta IP ünvanlarını "127.0.0.x" kimi saxta IP adresini geri qaytarmaqla və ya sadəcə heç bir şey qaytarmamaqla istifadəçiləri veb saytına girmədən süzgəcdən keçirir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS də saxta DNS cavablarına görə onlayn proqramı smartfon tətbiqindən kompüter oyununa qədər pozur.Cloudflare DNS bəzi bank saytlarına sorğu göndərə bilmir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Və burada düşünə bilərsən,<br>Mən Tor və ya VPN istifadə etmirəm, niyə qayğı göstərməliyəm?<br>Cloudflare marketinqinə etibar edirəm, niyə qayğı göstərməyim lazımdır<br>Veb saytım https-dir, niyə qayğı göstərməliyəm | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Cloudflare istifadə edən veb saytı ziyarət etsəniz, məlumatlarınızı yalnız veb sayt sahibi ilə deyil, həm də Cloudflare də paylaşırsınız.Əks proxy işləyir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  TLS trafikinin şifrəsini açmadan təhlil etmək mümkün deyil. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare, xam parol kimi bütün məlumatlarınızı bilir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Bulud damağı hər an baş verə bilər. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare'nin https heç bitmir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Həqiqətən məlumatlarınızı Cloudflare, həmçinin 3 hərfli agentliklə bölüşmək istəyirsiniz? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  İnternet istifadəçisinin onlayn profili hökumətin və böyük texnoloji şirkətlərin satın almaq istədiyi "məhsuldur". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  ABŞ Daxili Təhlükəsizlik İdarəsi məlumat verdi:<br><br>Məlumatların nə qədər dəyərli olduğuna dair bir fikriniz varmı? Bu məlumatları bizə satacağınız bir yol varmı?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare həmçinin "Cloudflare Warp" adlı PULSUZ VPN xidmətini təklif edir.Onu istifadə etsəniz, bütün smartfon (və ya kompüteriniz) bağlantılar Cloudflare serverlərinə göndərilir.Cloudflare hansı veb saytı oxuduğunuzu, hansı şərh yazdığınız, kiminlə danışdığınız və s. Bilə bilər.Cloudflare-yə bütün məlumatlarınızı könüllü olaraq verirsiniz."Zarafat edirsən?" Buludlar təhlükəsizdir. " sonra VPN-nin necə işlədiyini öyrənməlisiniz. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare, VPN xidmətinin internetinizi sürətli etdiyini söylədi.Ancaq VPN internet bağlantınızızı mövcud bağlantınızdan daha yavaş edir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  PRİZM qalmaqalı haqqında artıq bilə bilərsən.Düzdür, AT&T NSA-ya bütün internet məlumatlarını nəzarət üçün kopyalamağa imkan verir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Deyək ki, NSA-da işləyirsiniz və hər bir vətəndaşın internet profilini istəyirsiniz.Onların əksəriyyətinin Cloudflare-ya kor-koranə güvəndiyini və bundan istifadə etdiyini - yalnız bir mərkəzləşdirilmiş şlüz - şirkətlərinin server bağlantısını (SSH / RDP), şəxsi veb saytını, söhbət saytını, forum veb saytını, bank veb saytını, sığorta veb saytını, axtarış motorunu, gizli üzvünü proksi etmək üçün istifadə etdiyini bilirsiniz. - yalnız veb sayt, auksion veb saytı, alış-veriş, video veb sayt, NSFW veb saytı və qanunsuz veb sayt.Cloudflare'nin DNS xidmətindən ("1.1.1.1") və VPN xidmətindən ("Cloudflare Warp") "Təhlükəsiz! Daha sürətli! Daha yaxşı! ” internet təcrübəsi.Onları istifadəçinin IP ünvanı, brauzerin barmaq izi, peçenye və RAY-ID ilə birləşdirmək hədəf onlayn profil yaratmaq üçün faydalı olacaqdır. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Onların məlumatlarını istəyirsən. Nə edəcəksən? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare bir bal qabıdır.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Hər kəs üçün pulsuz bal. Bəzi iplər bağlandı.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Cloudflare istifadə etməyin.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **İnterneti mərkəzsizləşdirin.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Xahiş edirəm növbəti səhifəyə davam edin:  "[Cloudflare etikası](az.ethics.md)"

---

<details>
<summary>_meni vurun_

## Məlumat və daha çox məlumat
</summary>


Bu depo Tor istifadəçilərini və digər CDN-ləri bloklayan "Böyük Bulud" arxasında olan saytların siyahısıdır.


**Məlumat**
* [Şirkət Adı Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare İstifadəçiləri](../cloudflare_users/)
* [Cloudflare Domains](../cloudflare_users/domains/)
* [Cloudflare olmayan CDN istifadəçiləri](../not_cloudflare/)
* [Anti-Tor istifadəçiləri](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Daha çox məlumat**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Yükləyin: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Orijinal eBook (ePUB) CCR materialının müəllif hüquqları pozuntusuna görə BookRix GmbH tərəfindən silindi
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Bilet dəfələrlə vandalizasiya edildi.
  * [Tor Layihəsi tərəfindən silinmişdir.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [34175 biletinə baxın.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Son arxiv bileti 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_meni vurun_

## Sən nə edə bilərsən?
</summary>

* [Tövsiyə olunan hərəkətlər siyahımızı oxuyun və dostlarınızla bölüşün.](../ACTION.md)

* [Digər istifadəçinin səsini oxuyun və fikirlərinizi yazın.](../PEOPLE.md)

* Bir şey axtarın: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Domen siyahısını yeniləyin: [Təlimatları sadalayın](../INSTRUCTION.md).

* [Tarixə Cloudflare və ya layihə ilə əlaqəli hadisə əlavə edin.](../HISTORY.md)

* [Yeni alət / skript yazın.](../tool/)

* [Burada oxumaq üçün bəzi PDF / ePUB var.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Saxta hesablar haqqında

Twitter, Facebook, Patreon, OpenCollective, Kəndlər və s. Kimi rəsmi kanallarımıza aid saxta hesabların mövcudluğunu Krflare bilir.
**E-poçtunuzu heç vaxt soruşmuruq.
Heç vaxt adınızı soruşmuruq.
Heç vaxt kimliyinizi soruşmuruq.
Heç vaxt yerinizi soruşmuruq.
Heç vaxt ianənizi istəmirik.
Rəyinizi heç vaxt soruşmuruq.
Heç vaxt sosial mediada izləməyinizi xahiş etmirik.
Heç vaxt sosial mediadan soruşmuruq.**

# YAXŞI HESAB VERMƏYİN.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)