# Suur pilvesein


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Peatage pilv


|  🖹  |  🖼 |
| --- | --- |
|  „Suur pilvesein” on USA ettevõte Cloudflare Inc.See pakub CDN (sisu edastamise võrk) teenuseid, DDoS leevendamist, Interneti turvalisust ja hajutatud DNS (domeeninimeserver) teenuseid.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare on maailma suurim MITM-puhverserver (vastupidine puhverserver).Cloudflare omab enam kui 80% CDN-i turuosast ja pilvvalgustuse kasutajate arv kasvab iga päevaga.Nad on laiendanud oma võrku enam kui 100 riiki.Cloudflare teenindab rohkem veebiliiklust kui Twitter, Amazon, Apple, Instagram, Bing ja Wikipedia kokku.Cloudflare pakub tasuta paketti ja paljud inimesed kasutavad seda oma serverite õige konfigureerimise asemel.Nad kauplesid privaatsuse üle mugavuse pärast.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare asub teie ja päritolu veebiserveri vahel, toimides nagu piiripatrulli agent.Teil ei ole võimalik valitud sihtkohaga ühendust luua.Olete ühenduses Cloudflare'iga ja kogu teie teave dekrüpteeritakse ja antakse üle lennult. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Päritolu veebiserveri administraator lubas agendil - Cloudflare - otsustada, kellel on juurdepääs nende „veebiatribuudile” ja määratleda „piiratud ala”.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Vaadake õiget pilti.Arvate, et Cloudflare blokeerib ainult pahad.Arvate, et Cloudflare on alati võrgus (ärge kunagi minge alla).Te arvate, et legitiimsed robotid ja indekseerijad saavad teie veebisaiti indekseerida.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Kuid need pole üldse tõsi.Pilvvalgus blokeerib süütuid inimesi ilma põhjuseta.Pilvesuhk võib väheneda.Cloudflare blokeerib õigustatud robotid.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Nagu iga hostimisteenus, pole ka Cloudflare täiuslik.Seda ekraani näete isegi siis, kui lähtepunktiserver töötab hästi.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Kas sa tõesti arvad, et Cloudflare on 100% kasutuses?Teil pole aimugi, mitu korda Cloudflare alla läheb.Kui Cloudflare langeb, ei pääse teie klient teie veebisaidile juurde. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Seda nimetatakse Hiina suureks tulemüüriks, mis teeb võrreldava töö paljude inimeste filtreerimiseks veebisisu nägemiseks (st kõik Mandri-Hiinas ja väljaspool inimesi).Samal ajal kui need, kellele see ei puuduta, näevad drastiliselt teistsugust, tsensuurivaba veebit, näiteks "tankimehe" pilti ja "Tiananmeni väljaku protestide ajalugu". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Pilvvalgus omab suurt võimu.Teatud mõttes kontrollivad nad seda, mida lõppkasutaja lõpuks näeb.Cloudflare'i tõttu pole teil veebisaiti võimalik sirvida. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Pilvvalgust saab kasutada tsensuuriks. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Pilvega kaetud veebisaiti ei saa vaadata, kui kasutate väiksemat brauserit, mis Cloudflare'i arvates võib olla robot (kuna seda ei kasuta paljud inimesed). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Seda sissetungivat brauserikontrolli ei saa läbi viia ilma Javascripti lubamata.See on teie väärtusliku elu viis (või enam) sekundit raiskamine. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare blokeerib automaatselt ka legitiimsed robotid / indekseerijad, nagu Google, Yandex, Yacy ja API kliendid.Cloudflare jälgib aktiivselt kogukonda „cloudflare bypass“, et katkestada legitiimsed uurimisobotid. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare takistab samamoodi paljudel halva Interneti-ühendusega inimestel juurdepääsu selle taga asuvatele veebisaitidele (näiteks võivad nad olla NAT-i vähemalt 7 kihti taga või jagada sama IP-d, näiteks avalikku Wifi), kui nad ei lahenda mitut pilti CAPTCHA-sid.Mõnel juhul kulub Google'i rahuldamiseks 10–30 minutit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Aastal 2020 läks Cloudflare Google'i Recaptchalt hCaptchale, kuna Google kavatseb selle kasutamise eest tasu võtta.Cloudflare ütles, et nad hoolivad teie privaatsusest („see aitab lahendada privaatsusega seotud probleeme”), kuid see on ilmselgelt vale.See kõik on seotud rahaga."HCaptcha võimaldab veebisaitidel teenida raha selle nõudmise teenimiseks, blokeerides samas robotid ja muud kuritarvitused" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Kasutaja seisukohast ei muuda see palju. Teid sunnitakse see lahendama. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare blokeerib iga päev paljusid inimesi ja tarkvara. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Pilvvalgus ärritab paljusid inimesi kogu maailmas.Vaadake loendit ja mõelge, kas Cloudflare'i kasutuselevõtt teie saidil on kasutajakogemusele kasulik. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Mis on Interneti eesmärk, kui te ei saa teha seda, mida soovite?Enamik inimesi, kes teie veebisaiti külastavad, otsivad lihtsalt teisi lehti, kui nad ei saa veebilehte laadida.Võimalik, et te ei blokeeri aktiivselt ühtegi külastajat, kuid Cloudflare'i vaikemüür on paljude inimeste blokeerimiseks piisavalt range. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Ilma JavaScripti ja küpsiseid lubamata ei saa captcha-d lahendada.Cloudflare kasutab neid teie tuvastamiseks brauseri allkirja andmiseks.Cloudflare peab teadma teie identiteeti, et otsustada, kas soovite saidi sirvimist jätkata. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tori kasutajad ja VPN-i kasutajad on ka Cloudflare'i ohver.Mõlemat lahendust kasutavad paljud inimesed, kes ei saa oma riigi / ettevõtte / võrgupoliitika tõttu lubada tsenseerimata Internetti või kes soovivad oma privaatsuse kaitsmiseks lisada lisakihi.Pilvvalgus ründab häbematult neid inimesi, sundides neid puhverserveri lahenduse välja lülitama. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Kui te ei proovinud Torit selle hetkeni, soovitame teil alla laadida Tor Browser ja külastada oma lemmikveebisaite.Soovitame teil mitte sisse logida oma panga veebisaidile või valitsuse veebilehele, vastasel juhul märgivad nad teie konto. Kasutage nende veebisaitide jaoks VPN-i. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Võite öelda: „Tor on ebaseaduslik! Tori kasutajad on kuritegelikud! Tor on halb! ". Ei.Võib-olla õppisite Tori kohta televisioonist, öeldes, et Torit saab kasutada darkneti sirvimiseks ja relvade, narkootikumide või jahtpornos kaubitsemiseks.Kuigi ülaltoodud väide vastab tõele, et turul on palju veebisaite, kus saate selliseid objekte osta, kuvatakse need saidid sageli ka Clearnetis.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tori töötas välja USA armee, kuid praeguse Tori arendas välja Tori projekt.On palju inimesi ja organisatsioone, kes kasutavad Tor-i, sealhulgas teie tulevased sõbrad.Niisiis, kui kasutate oma veebisaidil Cloudflare, blokeerite tõelised inimesed.Kaotad potentsiaalse sõpruse ja äritehingu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Ja nende DNS-teenus 1.1.1.1 filtreerib kasutajad välja ka veebisaidi külastamisest, tagastades Cloudflare'ile kuuluva võltsitud IP-aadressi, localhost IP-d, näiteks “127.0.0.x”, või tagastades lihtsalt mitte midagi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS rikub ka võrgutarkvara nutitelefonirakendusest arvutimängule, kuna see on võltsitud DNS-vastusega.Cloudflare DNS ei saa mõne panga veebisaidilt päringuid teha. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Ja siin võite mõelda,<br>Ma ei kasuta Tor-i ega VPN-i, miks peaks see mind huvitama?<br>Ma usaldan Cloudflare turundust, miks ma peaksin sellest hoolima?<br>Minu veebisait on https, miks ma peaksin sellest hoolima | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Kui külastate veebisaiti, mis kasutab Cloudflare, jagate oma teavet mitte ainult veebisaidi omanikule, vaid ka Cloudflare.Nii töötab vastupidine puhverserver. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  TLS-i liikluse dekrüpteerimata on võimatu analüüsida. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare teab kõiki teie andmeid, näiteks töötlemata parooli. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed võib juhtuda igal ajal. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare'i https pole kunagi otsast otsani. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Kas soovite tõesti jagada oma andmeid Cloudflare'iga ja ka kolmetähelise agentuuriga? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Interneti-kasutaja veebiprofiil on „toode”, mida valitsus ja suured tehnoloogiaettevõtted soovivad osta. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Teatas USA sisejulgeolekuministeerium:<br><br>Kas teil on aimugi, kui väärtuslikud on teie andmed? Kas saate mingil viisil müüa neid andmeid meile?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare pakub ka TASUTA VPN-teenust nimega “Cloudflare Warp”.Kui kasutate seda, saadetakse kõik nutitelefoni (või arvuti) ühendused Cloudflare'i serveritesse.Cloudflare saab teada, millist veebisaiti olete lugenud, millise kommentaari olete postitanud, kellega olete rääkinud jne.Edastate vabatahtlikult kogu teabe Cloudflare'ile.Kui arvate: “Nalja teete? Pilvetulekahju on turvaline. ” siis peate õppima, kuidas VPN töötab. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare ütles, et nende VPN-teenus muudab teie Interneti kiireks.Kuid VPN muudab teie Interneti-ühenduse aeglasemaks kui teie olemasolev ühendus. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Võite juba teada PRISMi skandaalist.On tõsi, et AT&T laseb NSA-l jäljendamiseks kogu Interneti-andmed kopeerida. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Oletame, et töötate NSA-s ja soovite iga kodaniku Interneti-profiili.Teate, et enamik neist usaldab pimesi Cloudflare'i ja kasutab seda - ainult ühte tsentraliseeritud lüüsi - oma ettevõtte serveri ühenduse (SSH / RDP), isikliku veebisaidi, vestluste veebisaidi, foorumi veebisaidi, panga veebisaidi, kindlustuse veebisaidi, otsimootori, salajase liikme proxy jaoks Ainult veebisait, oksjonisait, ostlemine, videoveebisait, NSFW veebisait ja ebaseaduslik veebisait.Samuti teate, et nad kasutavad Cloudflare'i DNS-teenust ("1.1.1.1") ja VPN-teenust ("Cloudflare Warp") funktsiooni „Turvaline! Kiiremini! Parem! ” Interneti-kogemus.Nende ühendamine kasutaja IP-aadressi, brauseri sõrmejälgede, küpsiste ja RAY-ID-ga on kasulik eesmärgi veebiprofiili loomiseks. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Sa tahad nende andmeid. Mida sa teed? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Pilvvalgus on kärgpott.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Kõigile tasuta mesi. Mõned stringid lisatud.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Ärge kasutage Cloudflare'i.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Detsentraliseerige Internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Jätkake järgmisel lehel:  "[Pilvanduse eetika](et.ethics.md)"

---

<details>
<summary>_klõpsa mulle_

## Andmed ja lisateave
</summary>


See hoidla on loetelu veebisaitidest, mis asuvad "Suure pilvaseina" taga, blokeerides Tor-kasutajaid ja muid CDN-e.


**Andmed**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare kasutajad](../cloudflare_users/)
* [Pilvede hägususe domeenid](../cloudflare_users/domains/)
* [CDN-i kasutajad, kes ei kasuta pilve](../not_cloudflare/)
* [Tor-vastased kasutajad](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Rohkem informatsiooni**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Lae alla: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * BookRix GmbH kustutas originaalse e-raamatu (ePUB) CC0 materjali autoriõiguse rikkumise tõttu
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Pilet vandaalitseti nii mitu korda.
  * [Kustutatud Tor-projekti poolt.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Vaata piletit 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Viimane arhiivipilet 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_klõpsa mulle_

## Mida sa teha saad?
</summary>

* [Lugege meie soovitatud toimingute loendit ja jagage seda oma sõpradega.](../ACTION.md)

* [Loe teise kasutaja häält ja kirjuta oma mõtted.](../PEOPLE.md)

* Otsige midagi: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Domeenide loendi värskendamine: [Loetle juhised](../INSTRUCTION.md).

* [Lisage ajalukku Cloudflare või projektiga seotud sündmus.](../HISTORY.md)

* [Proovige ja kirjutage uus tööriist / skript.](../tool/)

* [Siit saate lugeda mõnda PDF / ePUB-i.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Teave võltskontode kohta

Crimeflare teab võltskontode olemasolust, mis kehastab meie ametlikke kanaleid, olgu selleks Twitter, Facebook, Patreon, OpenCollective, Villages jne.
**Me ei küsi kunagi teie e-posti.
Me ei küsi kunagi teie nime.
Me ei küsi kunagi teie identiteeti.
Me ei küsi kunagi teie asukohta.
Me ei küsi kunagi teie annetust.
Me ei küsi kunagi teie arvustust.
Me ei palu teil kunagi sotsiaalmeedias jälgida.
Me ei küsi kunagi teie sotsiaalmeedialt.**

# ÄRGE USALDAKE VÕLAKONTE.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)