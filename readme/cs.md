# Velký cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Zastavte cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  „The Great Cloudwall“ je Cloudflare Inc., americká společnost.Poskytuje služby CDN (síť pro doručování obsahu), DDoS zmírňování, zabezpečení Internetu a distribuované služby DNS (server doménových jmen).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare je největší MITM proxy na světě (reverzní proxy) na světě.Cloudflare vlastní více než 80% podílu na trhu CDN a počet uživatelů cloudflare každým dnem roste.Rozšířili svou síť do více než 100 zemí.Cloudflare poskytuje více webového provozu než Twitter, Amazon, Apple, Instagram, Bing a Wikipedia dohromady.Cloudflare nabízí bezplatný tarif a mnoho lidí jej používá místo správné konfigurace svých serverů.Za účelem pohodlí vyměnili soukromí.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare sedí mezi vámi a webovým serverem původu a chová se jako agent pohraniční stráže.K vybranému cíli se nemůžete připojit.Připojujete se k Cloudflare a všechny vaše informace se dešifrují a předávají za běhu. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Správce původního webového serveru umožnil agentovi - Cloudflare - rozhodnout, kdo může přistupovat k jejich „webové službě“ a definovat „omezenou oblast“.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Podívejte se na správný obrázek.Budete si myslet, že Cloudflare blokuje pouze padouchy.Budete si myslet, že Cloudflare je vždy online (nikdy neklesne).Budete si myslet, že legitimní roboti a prohledávače mohou váš web indexovat.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  To však vůbec není pravda.Cloudflare bezdůvodně blokuje nevinné lidi.Cloudflare může jít dolů.Cloudflare blokuje legitimní roboty.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Stejně jako každá hostingová služba není Cloudflare dokonalá.Tato obrazovka se zobrazí, i když původní server funguje dobře.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Opravdu si myslíte, že Cloudflare má 100% provozuschopnost?Nemáte ponětí, kolikrát Cloudflare klesá.Pokud Cloudflare klesne, zákazník nemá přístup na váš web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Nazývá se to odkazem na Velký čínský firewall, který vykonává srovnatelnou práci při odfiltrování mnoha lidí od prohlížení webového obsahu (tj. Všichni v pevninské Číně a lidé venku).Zatímco ti, kteří nebyli ovlivněni, aby viděli draticky odlišný web, web bez cenzury, jako je obraz „tankového muže“ a historie „protestů na náměstí Nebeského klidu“. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare má velkou sílu.V jistém smyslu řídí to, co konečný uživatel nakonec uvidí.Nemůžete procházet web kvůli Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare lze použít pro cenzuru. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Pokud používáte menší prohlížeč, který si Cloudflare může myslet, že se jedná o robota (protože ho nepoužívá mnoho lidí), nemůžete si prohlédnout cloudový web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Tuto invazivní kontrolu prohlížeče nemůžete předat, aniž byste povolili Javascript.Toto je ztráta pěti (nebo více) sekund vašeho cenného života. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare také automaticky blokuje legitimní roboty / prolézací moduly, jako jsou klienti Google, Yandex, Yacy a API.Cloudflare aktivně monitoruje komunitu „obtok cloudflare“ s cílem prolomit legitimní výzkumné roboty. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare podobně brání mnoha lidem, kteří mají špatné připojení k internetu, v přístupu na webové stránky za ním (například mohou být za 7+ vrstvami NAT nebo sdílet stejnou IP, například veřejnou Wifi), pokud nevyřeší CAPTCHA s více obrázky.V některých případech to bude trvat 10 až 30 minut, než Google uspokojí. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  V roce 2020 Cloudflare přešel z Google Recaptcha na hCaptcha, protože Google má v úmyslu za jeho použití účtovat poplatek.Cloudflare vám řekla, že se starají o vaše soukromí („pomáhá to vyřešit problém týkající se ochrany soukromí“), ale je to zjevně lež.Je to všechno o penězích.„HCaptcha umožňuje webovým stránkám vydělávat peníze na uspokojení této poptávky, zatímco blokuje roboty a jiné formy zneužívání“ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Z pohledu uživatele se to moc nezmění. Jste nuceni to vyřešit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare každý den blokuje mnoho lidí a softwaru. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare obtěžuje mnoho lidí po celém světě.Podívejte se na seznam a přemýšlejte, zda je přijetí Cloudflare na vaše stránky dobré pro uživatelský dojem. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Jaký je účel internetu, pokud nemůžete dělat, co chcete?Většina lidí, kteří navštíví váš web, prostě hledá jiné stránky, pokud nemohou načíst webovou stránku.Možná nebudete aktivně blokovat žádné návštěvníky, ale výchozí firewall Cloudflare je dostatečně přísný, aby zablokoval mnoho lidí. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Neexistuje způsob, jak vyřešit captcha bez povolení Javascriptu a cookies.Cloudflare je používá k vytvoření podpisu v prohlížeči, který vás identifikuje.Cloudflare potřebuje znát vaši identitu, aby se rozhodl, zda máte nárok pokračovat v procházení webu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Uživatelé Tor a VPN uživatelé jsou také obětí Cloudflare.Obě řešení používá mnoho lidí, kteří si nemohou dovolit necenzurovaný internet kvůli své zemi / společnosti / síťové politice nebo kteří chtějí přidat další vrstvu k ochraně jejich soukromí.Cloudflare nehanebně útočí na tyto lidi a nutí je, aby vypnuli své proxy řešení. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Pokud jste do této chvíle nezkoušeli Tor, doporučujeme vám stáhnout si prohlížeč Tor Browser a navštívit své oblíbené webové stránky.Doporučujeme, abyste se nepřihlašovali na svůj bankovní web nebo vládní webovou stránku, nebo budou označit váš účet. Pro tyto weby použijte VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Možná budete chtít říct: „Tor je nezákonný! Uživatelé Tor jsou trestní! Tor je špatný! “Možná jste se o Torovi dozvěděli z televize a řekli, že Tor lze použít k prohlížení temných sítí a obchodu se zbraněmi, drogami nebo chid porno.I když výše uvedené tvrzení je pravdou, že existuje mnoho tržních webových stránek, kde si můžete takové předměty koupit, tyto weby se často objevují také na clearnetu.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor byl vyvinut americkou armádou, ale současný Tor je vyvinut projektem Tor.Existuje mnoho lidí a organizací, které používají Tor, včetně vašich budoucích přátel.Pokud tedy na svém webu používáte Cloudflare, blokujete skutečné lidi.Ztratíte potenciální přátelství a obchod. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  A jejich DNS služba, 1.1.1.1, také filtruje uživatele z návštěvy webu vrácením falešné IP adresy vlastněné Cloudflare, localhost IP jako „127.0.0.x“, nebo prostě nevrací nic. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS také přeruší online software z aplikace smartphone do počítačové hry kvůli jejich falešné odpovědi DNS.Cloudflare DNS nemůže dotazovat některé bankovní weby. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  A tady si můžete myslet,<br>Nepoužívám Tor nebo VPN, proč by mě to mělo zajímat?<br>Věřím marketingu Cloudflare, proč by mě to mělo zajímat<br>Můj web je https, proč by mě to mělo zajímat | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Pokud navštívíte web, který používá Cloudflare, sdílíte své informace nejen s majitelem webu, ale také s Cloudflare.Takto funguje reverzní proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Nelze analyzovat bez dešifrování přenosu TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare zná všechna vaše data, například nezpracované heslo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed se může stát kdykoli. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https Cloudflare nikdy nekončí. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Opravdu chcete sdílet svá data s Cloudflare a také s 3-dopisní agenturou? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Online profil uživatele internetu je „produkt“, který chce vláda a velké technologické společnosti koupit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Americké ministerstvo vnitřní bezpečnosti uvedlo:<br><br>Máte představu o tom, jak hodnotná jsou vaše data? Existuje způsob, jak byste nám tato data prodali?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare také nabízí bezplatnou službu VPN s názvem „Cloudflare Warp“.Pokud ji používáte, všechna připojení smartphonu (nebo vašeho počítače) se odesílají na servery Cloudflare.Cloudflare může vědět, na jakém webu jste si přečetli, jaký komentář jste zveřejnili, s kým jste mluvili atd.Dobrovolně předáváte všechny své informace společnosti Cloudflare.Pokud si myslíte: „Děláš si srandu? Cloudflare je bezpečná. “ pak se musíte naučit, jak funguje VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare uvedla, že jejich služba VPN zrychlí váš internet.VPN však vaše připojení k internetu pomalejší než vaše stávající připojení. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Možná už víte o skandálu PRISM.Je pravda, že AT&T umožňuje NSA kopírovat všechna internetová data pro dohled. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Řekněme, že pracujete v NSA a chcete internetový profil každého občana.Víte, že většina z nich slepě důvěřuje Cloudflare a používá jej - pouze jednu centralizovanou bránu - k proxy serveru pro připojení k firemnímu serveru (SSH / RDP), osobní webové stránky, chatovací webové stránky, webové stránky fóra, bankovní stránky, pojišťovací webové stránky, vyhledávače, tajný člen - pouze web, aukční web, nakupování, web pro video, web NSFW a nelegální web.Také víte, že používají službu DNS Cloudflare („1.1.1.1“) a službu VPN („Cloudflare Warp“) pro „Zabezpečeno! Rychleji! Lepší!" internetový zážitek.Jejich kombinace s IP adresou uživatele, otiskem prstu prohlížeče, cookies a RAY-ID bude užitečné při vytváření online profilu cíle. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Chcete jejich data. Co budeš dělat? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare je honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Med zdarma pro všechny. Některé řetězce připojeny.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Nepoužívejte Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Decentralizujte internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Pokračujte na další stránku:  "[Etika cloudflare](cs.ethics.md)"

---

<details>
<summary>_Klikni na mě_

## Data a další informace
</summary>


Toto úložiště je seznam webových stránek, které jsou za „Velkým cloudem“, blokující uživatele Tor a další CDN.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Uživatelé cloudflare](../cloudflare_users/)
* [Cloudflare Domains](../cloudflare_users/domains/)
* [Uživatelé CDN, kteří nejsou cloudflare](../not_cloudflare/)
* [Uživatelé Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Více informací**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Stažení: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Původní eKniha (ePUB) byla společností BookRix GmbH odstraněna z důvodu porušení autorských práv k materiálu CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Vstupenka byla tolikrát vandalizována.
  * [Vypuštěno projektem Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Viz tiket 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Poslední archiv 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_Klikni na mě_

## Co můžeš udělat?
</summary>

* [Přečtěte si náš seznam doporučených akcí a sdílejte jej se svými přáteli.](../ACTION.md)

* [Přečtěte si hlas jiného uživatele a napište své myšlenky.](../PEOPLE.md)

* Hledejte něco: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Aktualizujte seznam domén: [Seznam instrukcí](../INSTRUCTION.md).

* [Přidejte do historie Cloudflare nebo událost související s projektem.](../HISTORY.md)

* [Zkuste a napište nový nástroj / skript.](../tool/)

* [Zde je několik PDF / ePUB ke čtení.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### O falešných účtech

Crimeflare vědí o existenci falešných účtů vydávajících se za naše oficiální kanály, ať už jsou to Twitter, Facebook, Patreon, OpenCollective, Vesnice atd.
**Nikdy se neptáme na váš e-mail.
Nikdy se neptáme na vaše jméno.
Nikdy se vás nezeptáme na vaši totožnost.
Nikdy se zeptáme na vaši polohu.
Nikdy se nepožádáme o váš dar.
Nikdy se vás neptáme na vaši recenzi.
Nikdy vás nežádáme, abyste sledovali sociální média.
Nikdy se vás nezeptáme na vaše sociální média.**

# NESVĚŘUJTE FAKE ÚČTY.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)