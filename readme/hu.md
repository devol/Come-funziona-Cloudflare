# A nagy felhőfal


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Állítsa le a Cloudflare-t


|  🖹  |  🖼 |
| --- | --- |
|  A „nagy felhőfal” a Cloudflare Inc., az Egyesült Államok cég.CDN (tartalomszolgáltató hálózat) szolgáltatásokat, DDoS enyhítést, internetes biztonságot és elosztott DNS (domain név szerver) szolgáltatásokat nyújt.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  A Cloudflare a világ legnagyobb MITM proxyja (fordított proxy).A Cloudflare a CDN piaci részesedésének több mint 80% -át birtokolja, és a felhőkarcolók felhasználói száma nap mint nap növekszik.Több mint 100 országban kibővítették hálózatát.A Cloudflare több internetes forgalmat szolgál fel, mint a Twitter, az Amazon, az Apple, az Instagram, a Bing és a Wikipedia együttesen.A Cloudflare ingyenes tervet kínál, és sokan használják azt ahelyett, hogy kiszolgálóikat megfelelően konfigurálnák.Kereskedtek a magánélettel a kényelem felett.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  A Cloudflare közted és az eredeti webszerver között helyezkedik el, mint egy határőrizeti ügynök.Nem tud csatlakozni a választott célállomáshoz.Csatlakozik a Cloudflare-hez, és minden információt dekódol, és menet közben átad. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Az eredeti webszerver adminisztrátora megengedte az ügynöknek - a Cloudflare-nek - hogy eldöntse, ki férhet hozzá „webes tulajdonságához”, és meghatározza a „korlátozott területet”.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Vessen egy pillantást a megfelelő képre.Úgy gondolja, hogy a Cloudflare csak a rossz fiúkat blokkolja.Úgy gondolja, hogy a Cloudflare mindig online (soha nem megy le).Úgy gondolja, hogy a legit botok és a robotok indexelhetik webhelyét.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Ezek azonban egyáltalán nem igazak.A felhőkarcolás ok nélkül blokkolja az ártatlan embereket.A felhő tükröződése csökkenhet.A felhőkarcoló blokkolja a legit botokat.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Csakúgy, mint bármely hosting szolgáltatás, a Cloudflare sem tökéletes.Ezt a képernyőt akkor is láthatja, ha az eredeti kiszolgáló jól működik.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Valóban úgy gondolja, hogy a Cloudflare 100% -ban működik?Fogalma sincs, hányszor esik le a Cloudflare.Ha a Cloudflare csökken, az ügyfél nem fér hozzá az Ön webhelyéhez. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Ezt nevezik Kína Nagy Tűzfalának, amely hasonló feladatot végez, hogy kiszűrje sok embert az internetes tartalom megjelenítéséből (azaz Kínában a szárazföldön mindenkit és a kívül lévőket).Ugyanakkor azok számára, akiket nem érinti az, hogy látványosan eltérő web, a cenzúrától mentes web, például a „tank ember” képe és a „Tiananmen téri tiltakozások” története. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  A Cloudflare nagy erővel bír.Bizonyos értelemben ellenőrzik azt, amit a végfelhasználó lát.A Cloudflare miatt tilos a webhely böngészése. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  A felhő tükröződése felhasználható cenzúrára. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Nem tekintheti meg a felhőalapú webhelyet, ha kisebb böngészőt használ, amely a Cloudflare szerint bot (mert nem sok ember használja). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Ezt az invazív „böngészőellenőrzést” nem lehet átadni a Javascript engedélyezése nélkül.Ez értékes élete öt (vagy több) másodpercének pazarlása. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  A Cloudflare automatikusan blokkolja a legitim robotokat / bejárókat is, mint például a Google, a Yandex, a Yacy és az API ügyfelek.A Cloudflare aktívan figyeli a „bypass cloudflare” közösséget azzal a szándékkal, hogy megtörje a legit kutatási botokat. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  A Cloudflare hasonlóan sok olyan személy számára is megakadályozza, hogy rossz internet-kapcsolattal hozzáférjen a mögötte lévő weboldalakhoz (például a NAT több mint 7 rétegének mögött lehetnek, vagy ugyanazt az IP-t használhatják, például a nyilvános Wifi-hez), kivéve, ha több kép CAPTCHA-t oldnak meg.Bizonyos esetekben ez 10-30 percet vesz igénybe, hogy kielégítse a Google-t. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  A 2020-ban a Cloudflare a Google Recaptcha-ról a hCaptcha-ra váltott, mivel a Google szándékában áll díjat számolni a használatáért.A Cloudflare azt mondta, hogy törődik a magánéletével („ez segít az adatvédelmi aggályok kezelésében”), de ez nyilvánvalóan hazugság.A pénzről szól.„A hCaptcha lehetővé teszi a webhelyek számára, hogy pénzt keressenek ennek az igénynek, miközben blokkolják a botokat és a visszaélések más formáit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  A felhasználó szempontjából ez nem változik sokat. Ön kénytelen megoldani. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  A Cloudflare sok embert és szoftvert blokkol minden nap. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  A felhőkarcoló sok embert bosszant az egész világon.Vessen egy pillantást a listára, és gondolja át, hogy a Cloudflare elfogadása a webhelyén hasznos-ea felhasználói élmény szempontjából. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Mi a célja az internetnek, ha nem tudja megtenni azt, amit akar?A legtöbb webhelyet látogató ember csak akkor keres más oldalakat, ha nem tudnak betölteni egy weboldalt.Lehet, hogy nem blokkolja aktívan egyetlen látogatóját sem, de a Cloudflare alapértelmezett tűzfala elég szigorú ahhoz, hogy sok embert blokkoljon. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  A captcha nem oldható meg Javascript és Cookies engedélyezése nélkül.A Cloudflare arra használja őket, hogy böngésző aláírást készítsen az Ön azonosítása érdekében.A Cloudflare-nak tudnia kell az Ön személyazonosságát, hogy eldöntse, vajon képes-e folytatni a webhely böngészését. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  A Tor és a VPN felhasználók szintén a Cloudflare áldozatai.Mindkét megoldást sok ember használja, akik nem engedhetik meg maguknak cenzúrázatlan internet használatát országuk / vállalataik / hálózati politikájuk miatt, vagy akik további réteget akarnak felvenni magánéletük védelme érdekében.A felhőkarcoló szégyentelenül támadja ezeket az embereket, és arra kényszeríti őket, hogy kapcsolják ki a proxy-megoldásukat. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Ha ez idáig nem próbálta meg a Tor-ot, javasoljuk, hogy töltse le a Tor böngészőt, és látogasson el kedvenc webhelyére.Javasoljuk, hogy ne jelentkezzen be a bank webhelyére vagy a kormányzati weboldalra, különben megjelölik a számláját. Használja a VPN-t ezekre a webhelyekre. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Érdemes mondani: „A Tor tiltott! A Tor felhasználók bűncselekmények! Tor rossz! ". Nem.A Tor-ról a televízióból tudhatott meg, mondván, hogy a Tor segítségével böngészhetünk a sötéthálón, és fegyvereket, drogokat vagy chid pornót kereskedhetünk.Noha a fenti állítás igaz, hogy sok piaci weboldalon lehet ilyen cikkeket megvásárolni, ezek a webhelyek gyakran a clearnet-en is megjelennek.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  A Tor-ot az amerikai hadsereg fejlesztette ki, de a jelenlegi Tor-ot a Tor-projekt fejlesztette ki.Sok ember és szervezet használja a Tor-ot, beleértve a jövőbeli barátait is.Tehát, ha a Cloudflare-ot használja a webhelyén, akkor valódi embereket gátol.Elveszíti a lehetséges barátságot és üzleti üzletet. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  A DNS-szolgáltatásuk, az 1.1.1.1, szintén kiszűri a felhasználókat a webhely látogatásától, ha visszaadja a Cloudflare tulajdonában lévő hamis IP-címet, a localhost IP-t, például a „127.0.0.x”, vagy semmit sem ad vissza. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  A Cloudflare DNS hamis DNS-válasz miatt az online szoftvereket az okostelefon-alkalmazásoktól a számítógépes játékig is megszakítja.A Cloudflare DNS nem tudja lekérdezni egyes bank-webhelyeket. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  És itt azt gondolhatja,<br>Nem Tor-ot vagy VPN-t használom, miért érdekelne?<br>Bízom a Cloudflare marketingben, miért kellene odafigyelnem?<br>A webhelyem https, miért érdekelne | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Ha ellátogat olyan webhelyre, amely a Cloudflare-t használja, akkor az információkat nem csak a webhelytulajdonosoknak, hanem a Cloudflare-nak is megosztja.Így működik a fordított proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  A TLS-forgalom visszafejtése nélkül lehetetlen elemezni. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  A Cloudflare ismeri az összes adatát, például a nyers jelszót. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed bármikor megtörténhet. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  A Cloudflare https-je soha nem teljes. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Valóban meg akarja osztani adatait a Cloudflare-rel és a három betűvel rendelkező ügynökséggel is? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Az Internet felhasználó online profilja egy „termék”, amelyet a kormány és a nagy technológiai vállalatok meg akarnak vásárolni. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Az Egyesült Nemzetbiztonsági Minisztérium mondta:<br><br>Van ötlete, milyen értékes az Ön adatai? Van valamilyen módon eladni nekünk ezeket az adatokat?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  A Cloudflare INGYENES, „Cloudflare Warp” nevű VPN szolgáltatást is kínál.Ha használja, az összes okostelefon (vagy a számítógép) kapcsolata a Cloudflare szerverekre kerül.A Cloudflare tudja, hogy melyik weboldalt olvasott, milyen megjegyzést tett közzé, kikkel beszélt stb.Ön önként megadja az összes információt a Cloudflare-nak.Ha azt gondolod, hogy „viccelsz? A felhő tükröződése biztonságos. akkor meg kell tanulnia a VPN működését. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  A Cloudflare szerint a VPN-szolgáltatásuk gyorsítja az internetet.De a VPN lecsökkenti az internetkapcsolatot, mint a meglévő. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Lehet, hogy már tudott a PRISM-botrányról.Igaz, hogy az AT&T lehetővé teszi az NSA számára, hogy megfigyelés céljából másolja az összes internetes adatot. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Tegyük fel, hogy az NSA-nál dolgozik, és minden polgár internetes profilját szeretné.Tudja, hogy többségük vakon bízik a Cloudflare-ban, és csak egy központosított átjárót használ a vállalati szerver kapcsolat (SSH / RDP), személyes webhely, csevegő, fórum, bank, biztosítási webhely, keresőmotor, titkos tag proxyjára. - kizárólag weboldal, aukciós webhely, bevásárló, video webhely, NSFW webhely és illegális webhely.Ön is tudja, hogy a Cloudflare DNS szolgáltatását ("1.1.1.1") és a VPN szolgáltatást ("Cloudflare Warp") használják a “Biztonságos! Gyorsabb! Jobb!" internetes élmény.A felhasználó IP-címével, a böngésző ujjlenyomatával, a sütikkel és a RAY-ID-vel kombinálva hasznos lehet a cél online profiljának felépítéséhez. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Szeretnéd az adataikat. Mit fogsz csinálni? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **A Cloudflare egy mézeskanál.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Ingyenes méz mindenkinek. Néhány húr csatlakozik.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Ne használja a Cloudflare-t.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Decentralizálja az internetet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Folytassa a következő oldalra:  "[Cloudflare etika](hu.ethics.md)"

---

<details>
<summary>_kattints ide_

## Adatok és további információk
</summary>


Ez a lerakat azon webhelyek listája, amelyek a "Nagy felhőfal" mögött állnak, blokkolja a Tor felhasználóit és más CDN-ket.


**Adat**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare felhasználók](../cloudflare_users/)
* [Cloudflare tartományok](../cloudflare_users/domains/)
* [Nem Cloudflare CDN felhasználók](../not_cloudflare/)
* [Tor-ellenes felhasználók](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Több információ**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Letöltés: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Az eredeti e-könyvet (ePUB) a BookRix GmbH törölte a CC0 anyag szerzői jogi megsértése miatt
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * A jegyet oly sokszor vandalizálták.
  * [A Tor projekt törölte.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Lásd a 34175 jegyet.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Utolsó archív jegy 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_kattints ide_

## Mit tudsz csinálni?
</summary>

* [Olvassa el az ajánlott tevékenységek listáját, és ossza meg barátaival.](../ACTION.md)

* [Olvassa el a többi felhasználó hangját és írja meg gondolatait.](../PEOPLE.md)

* Keressen valamit: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Frissítse a domain listát: [Sorolja fel az utasításokat](../INSTRUCTION.md).

* [Adja hozzá a Cloudflare vagy a projekttel kapcsolatos eseményeket az előzményekhez.](../HISTORY.md)

* [Próbálja ki és írjon új szerszámot / szkriptet.](../tool/)

* [Itt van néhány olvasható PDF / ePUB.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### A hamis számlákról

A Crimeflare tud hamis számlák létezéséről, amelyek megszemélyesítik hivatalos csatornáinkat, legyen az Twitter, Facebook, Patreon, OpenCollective, falvak stb.
**Soha nem kérjük az e-mailt.
Soha nem kérdezzük a neved.
Soha nem kérdezzük meg személyazonosságát.
Soha nem kérdezzük a tartózkodási helyét.
Soha nem kérjük az adományt.
Soha nem kérjük az Ön véleményét.
Soha nem kérjük, hogy kövesse a közösségi médiában.
Soha nem kérdezzük a közösségi médiától.**

# Ne bízzon a hamis számlákon.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)