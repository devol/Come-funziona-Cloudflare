# عالي کلاوډوال


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## کلاوډ فلایر ودروئ


|  🖹  |  🖼 |
| --- | --- |
|  "لوی کلاوډ وال" د متحده ایالاتو شرکت ، کلاوډ فلایر شرکت دی.دا د CDN (د مینځپانګې رسولو شبکه) خدمات ، DDoS کمولو ، انټرنیټ امنیت ، او توزیع شوي DNS (ډومین نوم سرور) خدمات چمتو کوي.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  کلاوډ فلیر د نړۍ ترټولو لوی MITM پراکسي دی (ریورس پراکسي).کلاوډ فلایر د CDN په بازار کې تر 80٪ ډیر برخه لري او هره ورځ د کلاوډ فلایر کاروونکو شمیر مخ په ډیریدو دی.دوی خپلې شبکې له 100 څخه زیاتو هیوادونو ته غزولې دي.کلاوډ فلیر د ټویټر ، ایمیزون ، اپیل ، انسټاګرام ، بنگ او ویکیپیډیا ګډ څخه ډیر ویب ټرافیک خدمت کوي.کلاوډ فلایر وړیا پلان وړاندې کوي او ډیری خلک د دې په ځای د خپلو سرورونو د سم شکل ترتیبولو پرځای کاروي.دوی د اسانتیا پرځای د محرمیت تجارت کوي.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  کلاوډ فلایر ستاسو او اصلي ویبسرور تر مینځ ناست دی ، د سرحدي ګزمې اجنټ په څیر عمل کوي.تاسو نشئ کولی خپل ټاکل شوي منزل سره وصل شئ.تاسو کلاوډ فلیر سره وصل یاست او ستاسو ټول معلومات ضایع شوي او په الوتنه کې سپارل شوي دي. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  د اصلي ویبسرور مدیر ایجنټ - کلاوډ فلیر - ته اجازه ورکړه چې پریکړه وکړي چې څوک کولی شي د دوی "ویب ملکیت" ته لاسرسی ومومي او "محدود سیمه" تعریف کړي.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  سم عکس ته یو نظر وګورئ.تاسو به فکر وکړئ د کلاوډ فلیر یوازې بد هلکان.تاسو به فکر وکړئ چې کلاډ فلیر تل آنلاین وي (هیڅکله مه ښکته کیږئ).تاسو به فکر وکړئ چې قانوني بوټس او کریلر ستاسو ویب پا indexه شاخص کولی شي.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  په هرصورت دا په حقیقت کې ندي.کلاوډ فلایر بې ګناه خلکو ته د کوم دلیل مخه نیسي.Cloudflare ښکته تللی شي.Cloudflare د قانوني بوټونو مخه نیسي.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  لکه د هر کوربه توب خدمت په څیر ، کلاوډ فلیر بشپړ ندی.تاسو به دا پرده وګورئ که څه هم اصلي سرور ښه کار کوي.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  ایا تاسو واقعیا فکر کوئ Cloudflare 100 up وخت لري؟تاسو نه پوهیږئ چې څو ځله کلاوډ فلیر ښکته ځي.که Cloudflare ښکته راشي ستاسو پیرودونکي نشي کولی ستاسو ویب پا accessې ته لاسرسی ومومي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  دې ته د چین لوی فایر وال حواله ورکول ویل کیږي کوم چې د ویب مینځپانګې لیدلو څخه د ډیری انسانانو د فلټر کولو مقایسه دنده ترسره کوي (د بیلګې په توګه په چین کې هرڅوک او بهر خلک).پداسې حال کې چې په ورته وخت کې هغه کسان چې په مختلف ډول مختلف ویب لیدل کیږي ندي اغیزمن شوي ، د سینسرشپ څخه پاک ویب لکه د "ټانک مین" عکس او د "تیانمان چوک لاریونونو" تاریخ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  کلاوډ فلایر لوی ځواک لري.په مفهوم کې ، دوی هغه څه کنټرولوي چې د پای کارونکي یې په نهایت کې ګوري.تاسو د کلاوډ فلایر له امله د ویب پاowsې لټولو څخه منع شوی یاست. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  کلاوډ فلایر د سانسور لپاره کارول کیدی شي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  تاسو د کلاوډ فلډر ویب پا cannotه نشئ لیدلی که تاسو کوچني براوزر کاروئ کوم چې Cloudflare شاید فکر وکړي چې دا یو بوټ دی (ځکه چې ډیری خلک یې نه کاروي). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  تاسو د جاواسکریپټ وړولو پرته دا برید کونکی "براوزر چیک" نشي بریالی کولی.دا ستاسو د ارزښتناک ژوند پنځه (یا ډیر) ثانیې ضایع کول دي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  کلاوډ فلیر په اتوماتيک ډول د قانوني روبوټونو / کرولرانو لکه ګوګل ، یانډیکس ، یاسي ، او API مراجعینو مخه نیسي.کلاوډ فلایر د قانوني پلټنې بوټو ماتولو ارادې سره د "بای پاس کلاډ فلیر" ټولنه په فعاله توګه څاري. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  کلاوډ فلایر په ورته ډول د ډیری خلکو مخه نیسي څوک چې کمزوري انټرنیټ اتصال لري د دې تر شا ویب پا accessو ته لاسرسي څخه مخنیوی کوي (د مثال په توګه دوی کولی شي د NAT + 7+ پوړونو شاته وي یا د ورته IP شریکول ، د مثال په توګه عامه Wifi) پرته لدې چې دوی ډیری عکس CAPTCHA حل کړي.په ځینو مواردو کې ، دا به د ګوګل راضي کولو لپاره له 10 څخه تر 30 دقیقو وخت ونیسي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  په کال 2020 کې کلاوډ فلایر د ګوګل له ریپټا څخه HCaptcha ته بدل شو ځکه چې ګوګل د دې کارونې لپاره چارج کولو لپاره اراده لري.کلاوډ فلیر تاسو ته وویل چې دوی ستاسو محرمیت ته پاملرنه کوي ("دا د محرمیت اندیښنې حل کولو کې مرسته کوي") مګر دا په حقیقت کې درواغ دي.دا ټول د پیسو په اړه دي."hCaptcha ویب پا allowsو ته اجازه ورکوي د دې غوښتنې خدمت کولو پیسې وګوري پداسې حال کې چې بوټونه او د ناوړه ګټه اخیستنې نور ډولونه بندوي" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  د کارونکي لید له پلوه ، دا ډیر بدلون نه کوي. تاسو مجبور یاست چې دا یې حل کړئ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  ډیری انسانان او سافټویر هره ورځ د کلاوډ فلیر لخوا بلاک شوي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  کلاوډ فلایر په ټوله نړۍ کې ډیری خلک خپه کوي.په لیست کې یو نظر وګورئ او فکر وکړئ چې ایا ستاسو په سایټ کې د کلاوډ فلیر غوره کول د کارن تجربې لپاره ښه دي. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  د انټرنیټ هدف څه دی که تاسو نشئ کولی هغه څه چې تاسو یې غواړئ؟ډیری خلک چې ستاسو ویب پا visitې ته مراجعه کوي یوازې د نورو پا forو لپاره ګوري که چیرې دوی ویب پا canه نشي پورته کولی.تاسو ممکن په فعاله توګه د هر لیدونکي مخه ونه نیسي ، مګر د کلاوډ فلیر ډیفالټ فائر وال خورا سخت دی چې د ډیری خلکو مخه ونیسي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  د جاواسکریپټ او کوکیز فعالولو پرته د کیپچا حل کولو کومه لاره شتون نلري.کلاوډ فلیر د دوی د پیژندلو لپاره د براوزر لاسلیک جوړولو لپاره کاروي.کلاوډ فلایر اړتیا لري ستاسو شناخت وپیژني ترڅو پریکړه وکړي چې ایا تاسو د سایټ لټون ته دوام ورکولو لپاره مستحق یاست که نه. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  د تور کارونکي او VPN کارونکي هم د کلاوډ فلیر قرباني دي.دواړه حلونه د ډیری خلکو لخوا کارول کیږي څوک چې نشي کولی د خپل هیواد / کارپوریشن / شبکې تګلارې له امله غیر سنسیر شوي انټرنیټ نشي ترلاسه کولی یا څوک چې غواړي د خپل محرمیت خوندي کولو لپاره اضافي پرت اضافه کړي.کلاوډ فلایر په بې شرمۍ سره په دې خلکو برید کوي ، دوی دې ته اړ کوي چې خپل پراکسي حل بند کړي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  که تاسو تر دې شیبې پورې Tor هڅه نه کوئ ، موږ تاسو د تور براوزر ډاونلوډ کولو او ستاسو غوره ویب پا visitو څخه لیدو ته هڅوو.موږ تاسو ته وړاندیز کوو چې خپل بانک ویب پا orې یا حکومتي ویب پا toې ته ننوځئ یا دوی به ستاسو حساب بيرغ ته ورسوي. د دې ویب پا forو لپاره VPN وکاروئ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  تاسو ممکن وغواړئ ووایی "مشعل غیرقانوني دی! د تور کاروونکي مجرم دي! تور خراب دی! ". نه.تاسو ممکن د تور په اړه د تلویزیون څخه زده کړل ، ویل یې چې تور د تورینټ او سوداګرۍ ټوپکونو ، مخدره توکو یا کریډیټ فحش کې کارولو لپاره کارول کیدی شي.پداسې حال کې چې پورتنۍ څرګندونې ریښتیا دي چې د بازار ډیری ویب پا areې شتون لري چیرې چې تاسو کولی شئ ورته توکي واخلئ ، دا سایټونه اکثرا په کلینټ کې هم څرګندیږي.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  تور د متحده ایالاتو اردو لخوا رامینځته شوی و ، مګر اوسنی تور د تور پروژې لخوا رامینځته شوی.ډیری خلک او سازمانونه شتون لري چې ستاسو د راتلونکي ملګرو په شمول تور کاروي.نو ، که تاسو په خپله ویب پا onه کې کلاوډ فلیر کاروئ نو تاسو ریښتیني انسانان مخنیوی کوئ.تاسو به احتمالي ملګرتیا او سوداګرۍ معاملې له لاسه ورکړئ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  او د دوی DNS خدمت ، 1.1.1.1 ، کارونکي د کلاوډ فلیر ملکیت جعلي IP پتې ، "127.0.0.x" ، یا یوازې هیڅ بیرته نه راستنولو له لارې ویب پا visitingې څخه لیدو څخه فلټر کوي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  کلاوډ فلایر DNS د جعلي DNS ځواب له امله آنلاین سافټویر د سمارټ فون اپلیکشن څخه کمپیوټر لوبې ته ماتوي.Cloudflare DNS د ځینې بانک ویب پا queryو پوښتنه نشي کولی. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  او دلته شاید تاسو فکر وکړئ ،<br>زه تور یا VPN نه کاروم ، ولې باید پاملرنه وشي؟<br>زه د کلاوډ فلیر بازار موندنه باور لرم ، ولې باید پاملرنه وشي<br>زما ویب پا httه https ده ولې زه باید پاملرنه وکړم | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  که تاسو هغه ویب پا visitې ته مراجعه وکړئ کوم چې کلاوډ فلیر کاروي ، تاسو خپل معلومات نه یوازې د ویب پا butې مالک ته بلکې د کلاډ فلیر سره هم شریک کوئ.دا د ریورس پراکسي کار کوي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  د TLS ترافیک ضایع کولو پرته تحلیل کول ناممکن دي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare ستاسو ټول معلومات لکه خام پټنوم پیژني. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  کلاوډ بیډ هر وخت پیښیدای شي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  د Cloudflare https هیڅکله د پای څخه پای نه وي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  ایا تاسو واقعیا غواړئ خپل معلومات د کلاوډ فلایر ، او هم د 3-لیک ادارې سره شریک کړئ؟ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  د انټرنیټ کارونکي آنلاین پروفایل یو "محصول" دی چې حکومت او لوی ټیک شرکتونه یې غواړي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  د متحده ایالاتو د کورني امنیت ریاست وویل:<br><br>ایا تاسو کوم نظر لرئ هغه معلومات چې تاسو یې لرئ څومره ارزښت لري؟ ایا کومه لاره شتون لري چې تاسو موږ ته هغه ډاټا وپلورئ؟  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  کلاوډ فلایر د “کلاوډ فلیر وارپ” په نوم وړیا VPN خدمت وړاندې کوي.که تاسو دا وکاروئ ، نو ستاسو ټول سمارټ فون (یا ستاسو کمپیوټر) اړیکې د کلاوډ فلیر سرورونو ته لیږل شوي.کلاوډ فلایر کولی شي پوه شي چې تاسو کوم ویب پا readه لوستې ، کوم نظر مو چې پوسټ کړی ، له چا سره مو خبرې کړي ، او داسې نور.تاسو په داوطلبانه ډول خپل ټول معلومات کلاوډ فلایر ته ورکوئ.که تاسو فکر کوئ "ایا تاسو ټوکې کوئ؟ کلاوډ فلور خوندي دی. " بیا تاسو اړتیا لرئ زده کړئ چې VPN څنګه کار کوي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  کلاوډ فلایر وویل چې د دوی VPN خدمت ستاسو انټرنیټ ګړندي کوي.مګر VPN ستاسو د انټرنیټ اتصال ستاسو د اوسني پیوستون څخه ډیر ورو کوي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  تاسو شاید دمخه د PRISM رسوایی په اړه پوهیږئ.دا ریښتیا ده چې AT&T NSA ته اجازه ورکوي ترڅو د نظارت لپاره ټول انټرنیټ ډیټا کاپي کړي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  راځئ چې ووایو چې تاسو په NSA کې کار کوئ ، او تاسو د هر وګړي انټرنیټ پروفایل غواړئ.تاسو پوهیږئ چې ډیری یې په ړوند ډول د کلاوډ فلیر باور لري او دا کاروي - یوازې یو مرکزي ګیټ ویز - د خپل شرکت سرور ارتباط (SSH / RDP) ، شخصي ویب پا ،ه ، د چیټ ویب پا ،ه ، بانک ویب پا ،ه ، بیمه ویب پا ،ه ، د لټون انجن ، پټ غړي. یوازې ویب پا websiteه ، د لیلام ویب پا ،ه ، شاپنگ ، ویډیو ویب پا ،ه ، د NSFW ویب پا ،ه ، او غیرقانوني ویب پا .ه.تاسو هم پوهیږئ چې دوی د "خوندي کولو لپاره د کلاوډ فلایر د DNS خدمت (" 1.1.1.1 ") او VPN خدمت (" کلاوډ فلایر ریپ ") کاروي. ګړندی! غوره! د انټرنیټ تجربه.د کاروونکي IP پتې ، د براوزر فنګر چاپ ، کوکیز او RAY-ID سره د دوی یوځای کول به د هدف آنلاین پروفایل رامینځته کولو لپاره ګټور وي. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  تاسو د دوی ډاټا غواړئ. ته به څه وکړې؟ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **کلاوډ فلایر یو هینی پاټ دی.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **د هرچا لپاره وړیا شات. ځینې ​​تارونه یوځای شوي.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **کلاوډ فلیر مه کاروئ.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **انټرنیټ غیرمتمرکز کړئ.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    مهرباني وکړئ بل پا toې ته دوام ورکړئ:  "[د کلاوډ فلایر اخلاقيات](ps.ethics.md)"

---

<details>
<summary>_ما کلیک کړه_

## ډیټا او نور معلومات
</summary>


دا ذخیره د ویب پا ofو لیست دی چې د "لوی کلاوډ وال" شاته دي ، د تور کارونکي او نور CDNs بندوي.


**ډاټا**
* [کلاوډ فلایر شرکت.](../cloudflare_inc/)
* [د کلاوډ فلیر کاروونکي](../cloudflare_users/)
* [د کلاوډ فلایر ډومینونه](../cloudflare_users/domains/)
* [غیر کلاوډ فلیر CDN کارونکي](../not_cloudflare/)
* [د تور ضد کارونکي](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**نور مالومات**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * ډاونلوډ: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * اصلي eBook (ePUB) د BookRix GmbH لخوا د CC0 موادو د کاپي حق سرغړونې له امله حذف شوی و
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * ټیکټ څو ځله تباه شو.
  * [د تور پروژې لخوا حذف شوی.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [ټیکټ 34175 وګورئ.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [د ارشیف وروستی ټیکټ 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_ما کلیک کړه_

## تاسو څه کولی شئ؟
</summary>

* [زموږ د وړاندیز شوي کړنو لیست ولولئ او له خپلو ملګرو سره یې شریک کړئ.](../ACTION.md)

* [د بل کارونکي غږ ولولئ او خپل افکار ولیکئ.](../PEOPLE.md)

* یو څه وپلټئ: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* د ډومین لیست تازه کړئ: [لارښوونې لیست کړئ](../INSTRUCTION.md).

* [تاریخ ته د کلاوډ فلیر یا پروژې پورې اړوند پیښه اضافه کړئ.](../HISTORY.md)

* [نوې وسیله / سکریپټ ولیکئ.](../tool/)

* [دلته د لوستلو لپاره ځینې PDF / ePUB دی.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### د جعلي حسابونو په اړه

د کرمفلایر د جعلي حسابونو شتون په اړه پوهیږي چې زموږ د رسمي چینلونو نقش ګرځوي ، دا د ټویټر ، فیسبوک ، پیټریون ، خلاص کولیکټیو ، کلیوالو وغيره وي.
**موږ هیڅکله ستاسو بریښنالیک نه پوښتو.
موږ هیڅکله ستاسو نوم نه پوښتو.
موږ هیڅکله ستاسو هویت نه پوښتو.
موږ هیڅکله ستاسو ځای نه پوښتو.
موږ هیڅکله ستاسو د مرستې غوښتنه نه کوو.
موږ هیڅکله ستاسو بیاکتنه نه غواړو.
موږ هیڅکله له تاسو څخه په ټولنیزو رسنیو د تعقیب غوښتنه نه کوو.
موږ هیڅکله ستاسو ټولنیزې رسنۍ نه پوښتو.**

# جعلي اکاونټونه مه کاروئ.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)