# The Cloudwall Agung


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Mungkasi Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "Cloudwall Agung" yaiku Cloudflare Inc., perusahaan A.S.Nyedhiyani layanan CDN (jaringan pangiriman konten), mitigasi DDoS, keamanan Internet, lan layanan DNS (server jeneng domain).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare minangka proxy MITM paling gedhe ing donya (proxy reverse).Cloudflare ndarbeni luwih saka 80% pangsa pasar CDN lan jumlah pangguna awan awan mundhak saben dinane.Dheweke wis nggedhekake jaringan ing luwih saka 100 negara.Cloudflare ngladeni lalu lintas web luwih akeh tinimbang gabungan ing Twitter, Amazon, Apple, Instagram, Bing & Wikipedia.Cloudflare nawakake rencana gratis lan akeh wong nggunakake piranti kasebut tinimbang ngatur server kanthi bener.Dheweke dagang privasi amarga kepenak.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare lenggah ing antarane sampeyan lan webserver asal, tumindak kaya agen patroli wates.Sampeyan ora bisa nyambung menyang tujuan sing dipilih.Sampeyan nyambungake menyang Cloudflare lan kabeh informasi sampeyan lagi diculik lan diserahake kanthi mabur. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Pentadbir web asli ngidini agen kasebut - Cloudflare - mutusake sapa sing bisa ngakses "properti web" lan netepake "wilayah sing diwatesi".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Coba deleng gambar sing pas.Sampeyan bakal ngira Cloudflare mblokir mung wong sing ala.Sampeyan bakal mikir Cloudflare mesthi online (ora tau mudhun).Sampeyan bakal mikir bot sing sah lan crawler bisa ngindeks situs web sampeyan.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Nanging, iku ora bener.Cloudflare ngalangi wong sing ora salah tanpa sebab.Cloudflare bisa mudhun.Cloudflare mblokir bot sing sah.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Kaya wae layanan hosting, Cloudflare ora sampurna.Sampeyan bakal bisa ndeleng layar iki sanajan server asal wis mlaku.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Apa sampeyan mikir tenan nalika Cloudflare 100% wektu munggah?Sampeyan ora ngerti sepira kakehan Cloudflare mudhun.Yen Cloudflare mudhun, sampeyan ora bisa ngakses situs web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Iki diarani minangka referensi Agung Firewall China sing nindakake pakaryan nyaring nyaring akeh manungsa saka ndeleng konten web (yaiku saben wong ing daratan China lan wong ing njaba).Nalika iku uga ora kena pengaruh kanggo ndeleng situs web sing beda-beda kanthi dramatis, situs web censorship kayata gambar "tank man" lan riwayat "Tiananmen Square demonstrasi". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare nduweni kekuwatan gedhe.Ing pangertèn, dheweke ngontrol apa sing pungkasan pangguna.Sampeyan wis nyegah saka browsing situs web amarga Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare bisa digunakake kanggo censorship. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Sampeyan ora bisa ndeleng situs web cloudflared yen sampeyan nggunakake browser cilik sing Cloudflare bisa uga nganggep manawa iku bot (amarga ora akeh sing nggunakake). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Sampeyan ora bisa ngliwati "mriksa browser" iki tanpa mbisakake Javascript.Iki sampah limang (utawa luwih) detik regane sing larang regane. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare uga kanthi otomatis ngalangi robot / crawler legit kanthi otomatis kayata klien Google, Yandex, Yacy, lan API.Cloudflare kanthi aktif ngawasi komunitas "bypass cloudflare" kanthi tujuan kanggo ngrusak bot riset legit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare uga nyegah akeh wong sing duwe konektivitas internet sing kurang bisa ngakses situs web ing mburi (umpamane, bisa uga ana 7+ lapisan NAT utawa nuduhake IP sing padha, umpamane Wifi umum) kajaba ngrampungake pirang-pirang gambar CAPTCHA.Ing sawetara kasus, wektu iki bakal butuh 10 nganti 30 menit kanggo gawe marem Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Ing taun 2020 Cloudflare pindhah saka Google Recaptcha menyang hCaptcha amarga Google ngupayakake supaya bisa digunakake.Cloudflare ngandhani yen sampeyan njaga privasi ("mbantu ngatasi prihatin privasi") nanging iki jelas ngapusi.Iku kabeh babagan dhuwit."HCaptcha ngidini situs web nggawe dhuwit ngladeni permintaan iki nalika ngalangi bot lan jinis penyalahgunaan" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Saka perspektif pangguna, iki ora akeh diganti. Sampeyan kepeksa ngatasi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Akeh manungsa lan piranti lunak sing diblokir dening Cloudflare saben dina. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare ngganggu akeh wong ing saindenging jagad.Coba dhaptar lan pikirake yen milih Cloudflare ing situs sampeyan apik kanggo pangguna pangguna. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Apa tujuan internet yen sampeyan ora bisa nindakake apa sing sampeyan karep?Umume wong sing ngunjungi situs web sampeyan mung bakal nggoleki kaca liyane yen ora bisa mbukak kaca web.Sampeyan bisa uga ora ngalangi pengunjung kanthi aktif, nanging firewall standar Cloudflare cukup kanggo ngalangi wong akeh. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Ora ana cara kanggo ngrampungake captcha tanpa mbedakake Javascript lan Cookies.Cloudflare nggunakake dheweke kanggo nggawe tandha browser kanggo ngenali sampeyan.Cloudflare kudu ngerti identitas kanggo mutusake manawa sampeyan nduweni hak kanggo terus browsing situs. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Pangguna Tor lan pangguna VPN uga dadi korban Cloudflare.Kaloro solusi kasebut digunakake dening akeh wong sing ora bisa nggunakake internet tanpa pamrih amarga kabijakan negara / perusahaan / jaringan utawa sing pengin nambah lapisan tambahan kanggo nglindhungi privasi.Cloudflare ora kanthi isin nyerang wong-wong kasebut, amarga meksa supaya mateni solusi proxy kasebut. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Yen sampeyan durung nyoba Tor nganti wektu iki, kita ngajak sampeyan ndownload Tor Browser lan ngunjungi situs web sing disenengi.Kita saranake supaya ora mlebu ing situs web bank utawa kaca web pemerintah utawa bakal menehi bandara. Gunakake VPN kanggo situs web kasebut. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Sampeyan bisa uga pengin ujar "Tor pancen ilegal! Pangguna Tor kriminal! Tor ala! ". Ora.Sampeyan bisa uga sinau babagan Tor saka televisi, ujar Tor bisa digunakake kanggo browsing gelap lan bedhil perdagangan, obatan utawa porno.Nalika pratelan ing ndhuwur bener, ana akeh situs web pasar sing bisa tuku barang-barang kaya kasebut, situs-situs kasebut asring ditampilake ing clearnet uga.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor dikembangake dening Tentara AS, nanging Tor saiki dikembangake dening proyek Tor.Ana akeh wong lan organisasi sing nggunakake Tor kalebu kanca sabanjure.Dadi, yen sampeyan nggunakake Cloudflare ing situs web, sampeyan mblokir manungsa nyata.Sampeyan bakal kelangan kemungkinan persahabatan lan kesepakatan bisnis. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Lan layanan DNS sing, 1.1.1.1, uga nyaring pangguna saka ngunjungi situs web kanthi ngasilake alamat IP palsu sing diduweni dening Cloudflare, IP lokal kaya "127.0.0.x", utawa mung ngasilake apa-apa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS uga break piranti lunak online saka aplikasi smartphone menyang game komputer amarga jawaban DNS palsu.Cloudflare DNS ora bisa nggolek sawetara situs web bank. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Lan ing kene sampeyan bisa uga mikir,<br>Aku ora nggunakake Tor utawa VPN, kenapa aku kudu peduli?<br>Aku dipercaya pemasaran Cloudflare, kenapa aku kudu peduli<br>Situs webku yaiku https kenapa aku kudu peduli | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Yen sampeyan ngunjungi situs web sing nggunakake Cloudflare, sampeyan nuduhake informasi ora mung kanggo pemilik situs web, nanging uga Cloudflare.Iki sejatine tumindak proxy mbalikke. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Ora mokal kanggo nganalisa tanpa ngapusi lalu lintas TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare ngerti kabeh data kayata sandhi mentah. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Mendhung bisa kedadeyan kapan wae. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https Cloudflare ora tau pungkasan. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Apa sampeyan pengin nuduhake data karo Cloudflare, lan uga agensi 3 huruf? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Profil online pangguna Internet minangka "produk" sing dituku pemerintah lan perusahaan teknologi gedhe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Departemen Keamanan Dalam Negeri A.S.:<br><br>Apa sampeyan duwe ide babagan pentinge data sing nduwe? Apa ana sing bakal adol data kasebut?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare uga nawakake layanan VPN FREE sing diarani "Cloudflare Warp".Yen sampeyan nggunakake, kabeh smartphone (utawa komputer) sambungan dikirim menyang server Cloudflare.Cloudflare bisa ngerti situs web sing wis sampeyan baca, komentar apa sing wis dikirim, sing wis dikandhani, lsp.Sampeyan sengaja menehi kabeh informasi menyang Cloudflare.Yen sampeyan mikir "Apa sampeyan guyon? Cloudflare aman. " mula, sampeyan kudu sinau babagan cara VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare ujar layanan VPN nggawe internet sampeyan cepet.Nanging VPN nggawe sambungan internet luwih alon tinimbang sambungan sing wis ana. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Sampeyan bisa uga wis ngerti babagan skandal PRISM.Pancen AT&T ngidini NSA nyalin kabeh data internet kanggo ndjogo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Ayo ujar sampeyan lagi kerja ing NSA, lan sampeyan pengin profil internet saben warga.Sampeyan ngerti sing paling akeh dipercaya karo Cloudflare kanthi wuta lan nggunakake - mung siji gateway terpusat - kanggo proxy sambungan server perusahaan (SSH / RDP), situs web pribadi, situs web obrolan, situs web forum, situs web bank, situs web asuransi, mesin telusuran, anggota rahasia Situs web sing apik, situs lelang, blanja, situs web video, situs web NSFW, lan situs web sing ilegal.Sampeyan uga ngerti manawa nggunakake layanan DNS Cloudflare ("1.1.1.1") lan layanan VPN ("Cloudflare Warp") kanggo "Aman! Luwih cepet! Luwih apik! " pengalaman internet.Nggabungake karo alamat IP pangguna, sidik driji browser, cookie lan RAY-ID bakal migunani kanggo nggawe profil online target. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Sampeyan pengin data dheweke. Apa sing bakal ditindakake? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare minangka honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Madu gratis kanggo kabeh wong. Sawetara senar dipasang.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Aja nggunakake Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Nemtokake internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Mangga terus menyang kaca sabanjure:  "[Etika Cloudflare](jv.ethics.md)"

---

<details>
<summary>_klik kula_

## Data lan Informasi Liyane
</summary>


Repositori iki minangka dhaptar situs web sing ana ing mburi "The Great Cloudwall", ngalangi pangguna Tor lan CDN liyane.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Pangguna Cloudflare](../cloudflare_users/)
* [Domain Cloudflare](../cloudflare_users/domains/)
* [Pangguna CDN Non-Cloudflare](../not_cloudflare/)
* [Pangguna anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Kabar Liyane**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Download: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * EBook asli (ePUB) dibusak dening BookRix GmbH amarga pelanggaran hak cipta CC0 materi
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Tiket kasebut dirusak kaping pirang-pirang.
  * [Dibusak dening Projek Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Waca tiket 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Tiket arsip pungkasan 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_klik kula_

## Apa sampeyan bisa nggawe?
</summary>

* [Waca dhaptar tindakan sing disaranake lan wenehana bareng karo kanca-kanca.](../ACTION.md)

* [Waca swara pangguna liyane lan tulisake pikirane.](../PEOPLE.md)

* Nggoleki soko: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Nganyari dhaptar domain: [Dhaptar pandhuan](../INSTRUCTION.md).

* [Tambah Cloudflare utawa acara sing ana gandhengane karo sejarah.](../HISTORY.md)

* [Coba & nulis Alat / Skrip anyar.](../tool/)

* [Mangkene sawetara PDF / ePUB supaya bisa diwaca.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Babagan akun palsu

Crimeflare ngerti babagan orane akun palsu sing nduwe saluran resmi kita, yaiku Twitter, Facebook, Patreon, OpenCollective, Villages etc.
**Kita ora tau takon email sampeyan.
Aku ora njaluk jeneng sampeyan.
Kita ora tau takon babagan identitas.
Ora nate takon menyang lokasi sampeyan.
Aku ora nate njaluk sumbangan sampeyan.
Ora nate takon babagan review sampeyan.
Boten nate nyuwun supaya sampeyan tindakake ing media sosial.
Kita ora nate takon media sosial sampeyan.**

# OGO TRANCONG AKAUN AKUN.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)