# ታላቁ ደመናው


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Cloudflare ን ያቁሙ


|  🖹  |  🖼 |
| --- | --- |
|  “ታላቁ የደመናው ፋየርዎል” የአሜሪካ ኩባንያ ኩባንያ Cloudflare Inc.ሲዲኤንኤን (የይዘት ማቅረቢያ አውታረመረብ) አገልግሎቶችን ፣ DDoS ቅነሳን ፣ የበይነመረብ ደህንነት እና የተሰራጩ ዲ ኤን ኤስ (የጎራ ስም አገልጋይ) አገልግሎቶችን እየሰጠ ይገኛል ፡፡  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare የዓለም ትልቁ MITM ተኪ (ተቃራኒ ተኪ) ነው።Cloudflare ከ 80% በላይ የ CDN የገቢያ ድርሻ ያለው ሲሆን የደመና ፍሰት ተጠቃሚዎች ቁጥር በየቀኑ እያደገ ነው።አውታረመረባቸውን ከ 100 በላይ አገሮችን አስፋፍተዋል ፡፡Cloudflare በትዊተር ፣ በአማዞን ፣ በአፕል ፣ በ Instagram ፣ በ Bing እና በዊኪፔዲያ ከተጣመሩ የበለጠ የድር ትራፊክን ያገለግላል ፡፡Cloudflare ነፃ ዕቅድን እያቀረበ ነው እና ብዙ ሰዎች አገልጋዮቻቸውን በአግባቡ ከማዋቀር ይልቅ እየተጠቀሙበት ነው።እነሱ በተመቻቸ ሁኔታ ግላዊነትን ይሸጣሉ ፡፡  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  የደመና ፍላይር እንደ ድንበር ተቆጣጣሪ ወኪል በመሆን በእርስዎ እና በመነሻ አጭበርባሪ መካከል ይቀመጣል።ከተመረጡት መድረሻዎ ጋር መገናኘት አልቻሉም።ወደ Cloudflare እየተገናኙ ነው እና ሁሉም መረጃዎ እየበረረ እና ዝንብ ላይ እንዲሰጥ ተደርጓል። |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  የድርጣቢያ (አጭበርባሪ) አስተዳዳሪው ወኪል - Cloudflare - ወደ “ድር ንብረታቸው” መድረስ እና “የተከለከለ አካባቢ” ን ለመግለጽ እንዲወስን ተወካይ ፈቀደ።  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  ትክክለኛውን ምስል ይመልከቱ።ክላውድላየር መጥፎ ሰዎችን ብቻ ያግዳል ብለው ያስባሉ።Cloudflare ሁል ጊዜ መስመር ላይ እንደሆነ ያስባሉ (በጭራሽ አይወርድም)።ህጋዊ ቦቶች እና ብስኩቶች ድር ጣቢያዎን (ኢንዴክስ) ሊጠቁሙ ይችላሉ ብለው ያስባሉ ፡፡  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  ሆኖም እነዚያ በጭራሽ እውነት አይደሉም ፡፡Cloudflare ያለ ምንም ምክንያት የንጹሃን ሰዎችን እያገደው ነው።Cloudflare ወደ ታች መሄድ ይችላል።Cloudflare የሕግ መከለያዎችን ያግዳል ፡፡  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  ልክ እንደማንኛውም አስተናጋጅ አገልግሎት ፣ Cloudflare ፍጹም አይደለም።የመነሻ አገልጋዩ ምንም እንኳን በጥሩ ሁኔታ እየሠራ ቢሆንም እንኳን ይህን ማያ ገጽ ያዩታል።  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  በእውነቱ Cloudflare 100% ወቅታዊ ነው ብለው ያስባሉ?Cloudflare ስንት ጊዜ እንደሚወርድ ምንም አታውቅም።Cloudflare ከወረደ ደንበኛዎ ድር ጣቢያዎን መድረስ አይችልም። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  ይህ ብዙ ሰዎችን የድር ይዘትን እንዳያዩ ለማጣራት ተመሳሳይ የሆነ ሥራ የሚያከናውን የቻይና ታላቁ ፋየርዎልን በመጥቀስ (ማለትም በዋናነት ቻይና እና በውጭ ካሉ ሰዎች) ጋር ይገናኛል ፡፡በተመሳሳይ ጊዜ በእነዚያ ላይ ተፅእኖ ያልተፈፀመባቸው የተለያዩ ድርን ለማየት ፣ እንደ “ታንክ ሰው” ምስል እና የ “የ‹ ታንማንmen አደባባይ ›ን የመሰለ ታሪክ ሳንሱር ከማድረግ ነፃ የሆነ ድር ጣቢያ ፡፡ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare ታላቅ ኃይል አለው።በአንድ በኩል ፣ በመጨረሻ ተጠቃሚው የሚያየውን ይቆጣጠራሉ።በ Cloudflare ምክንያት ድር ጣቢያውን ከማሰስ ተከልክለዋል። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare ለመጥቀስ ሊያገለግል ይችላል። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Cloudflare ን ነው ብሎ ሊያስበው የሚችል ጥቃቅን አሳሽ የሚጠቀሙ ከሆነ በደመና የተጠቃ ድር ጣቢያ ማየት አይችሉም (ምክንያቱም ብዙ ሰዎች የሚጠቀሙት አይደለም)። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  ጃቫስክሪፕትን ሳያነቃቁ ይህንን ወራዳ “አሳሽ ቼክ” ማለፍ አይችሉም።ይህ ዋጋ ያለው ሕይወትዎ አምስት (ወይም ከዚያ በላይ) ሰከንዶች ባዶ ነው። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  በተጨማሪም Cloudflare እንደ Google ፣ Yandex ፣ Yacy እና ኤፒአይ ደንበኞች ያሉ ህጋዊ ሮቦቶችን / ብስክሌቶችን በራስ-ሰር ያግዳል።ህጋዊ የምርምር ቡጢዎችን ለማፍረስ በማሰብ Cloudflare የ “ደመና ደመና” ን ህብረተሰብ በንቃት ይከታተላል ፡፡ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare በተመሳሳይ ደካማ ደካማ የበይነመረብ ግንኙነት ያላቸው ብዙ ሰዎች ድር ጣቢያዎቹን ከኋላ እንዳያገኙ ይከለክላቸዋል (ለምሳሌ ፣ ከ 7+ ንብርብሮች በስተጀርባ ሊሆኑ ይችላሉ ወይም ተመሳሳይ አይፒን ያጋሩ ፣ ለምሳሌ ይፋዊ Wifi) ፣ ብዙ ምስሎችን ካፒአይፒኤዎችን ካልፈቱ በስተቀር ፡፡በአንዳንድ ሁኔታዎች Google ን ለማርካት ከ 10 እስከ 30 ደቂቃዎች ይወስዳል። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  እ.ኤ.አ. በ 2020 Google አጠቃቀሙ እንዲከፍል እያሰበ ደመናው ከ Google Recaptcha ወደ hCaptcha ተለው swል።Cloudflare የእርስዎን ግላዊነትዎን እንደሚንከባከቡ ነግረውዎታል (“የግላዊነት ጉዳዮችን ለማቃለል ይረዳል”) ግን ይህ በግልጽ ውሸት ነው ፡፡ሁሉም ስለ ገንዘብ ነው።“ኤችካፕቻቻ ድርጣቶች እና ሌሎች የመጎሳቆል ዓይነቶችን በሚያግዱበት ጊዜ ድርጣቢያዎች ለዚህ ፍላጎት የሚያገለግል ገንዘብ እንዲያገኙ ያስችላቸዋል” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  ከተጠቃሚው እይታ ይህ ብዙም አይለወጥም። እንዲፈቱት እየተገደዱ ነው። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  ብዙ ሰዎች እና ሶፍትዌሮች በየቀኑ በ Cloudflare እየታገዱ ናቸው። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare በዓለም ዙሪያ ያሉ ብዙ ሰዎችን ያስቆጣቸዋል።ዝርዝሩን ይመልከቱ እና የደመና ፍሰትን ጣቢያዎ ላይ መውሰድ ለተጠቃሚ ተሞክሮ ጥሩ ነው ብለው ያስቡ። |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  የሚፈልጉትን ማድረግ ካልቻሉ የበይነመረብ ዓላማ ምንድ ነው?ብዙ ድር ጣቢያዎን የሚጎበኙ ብዙ ሰዎች ድረ-ገጽ መጫን ካልቻሉ ብቻ ሌሎች ገጾችን ይፈልጋሉ ፡፡እርስዎ ማንኛውንም ጎብኝዎች በንቃት እያገዱ ላይሆኑ ይችላሉ ፣ ግን Cloudflare ነባሪ ፋየርዎል ብዙ ሰዎችን ለማገድ ጥብቅ ነው ፡፡ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  ጃቫስክሪፕት እና ኩኪዎችን ሳያነቃ ካፒቻውን ለመፍታት የሚያስችል መንገድ የለም።Cloudflare እርስዎን ለመለየት የአሳሽ ፊርማ ለማዘጋጀት እነሱን እየተጠቀመባቸው ነው።ጣቢያውን ማሰስ ለመቀጠል ብቁ መሆን አለመሆንዎን ለማወቅ Cloudflare ማንነትዎን ማወቅ አለበት። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  የቶር ተጠቃሚዎች እና ቪፒኤን ተጠቃሚዎች የ Cloudflare ሰለባ ናቸው።ሁለቱም መፍትሄዎች በአገራቸው / በኮርፖሬሽኑ / / የኔትዎርክ ፖሊሲያቸው ምክንያት ግላዊነትን ያልተጠበቀ በይነመረብን አቅም ለማይችሉ ብዙ ሰዎች ጥቅም ላይ ይውላሉ ወይም ግላዊነታቸውን ለመጠበቅ ተጨማሪ ንጣፍ ማከል በሚፈልጉ ሰዎች።Cloudflare የተኪ መፍትሔቸውን እንዲያጠፉ በማስገደድ በእነዚያ ሰዎች ላይ ያለ ምንም እፍረት እያጠቃ ነው። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  እስከዚህ ጊዜ ድረስ ቶርን ካልሞከሩት ቶር ብራውዘርን እንዲያወርዱ እና የሚወ favoriteቸውን ድር ጣቢያዎች እንዲጎበኙ እናበረታታዎታለን።ወደ ባንክዎ ድር ጣቢያ ወይም የመንግስት ድር ጣቢያ እንዳይገቡ እንመክርዎታለን ወይም እነሱ መለያዎን ይጠቁማሉ። ለእነዚያ ድር ጣቢያዎች VPN ይጠቀሙ። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  “ቶር ሕገወጥ ነው! የቶር ተጠቃሚዎች ወንጀለኞች ናቸው! ቶር መጥፎ ነው! ”ቁ.ቶር በጨለማ ለመደበቅ እና ጠመንጃዎችን ፣ መድኃኒቶችን ወይም የብልግና ወሲብን ለመሰረዝ ሊያገለግል ይችላል ከሚል ቴሌቪዥን ስለ ቶር ሊማሩ ይችላሉ ፡፡ከላይ ያለው መግለጫ እውነት ቢሆንም እንደዚህ ያሉትን ዕቃዎች መግዛት የሚችሉባቸው በርካታ የገቢያ ድርጣቢያዎች አሉ ፣ እነዚያ ጣቢያዎች ብዙውን ጊዜ በገቢያ ገበያው ላይም ይታያሉ ፡፡  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  ቶር የተገነባው በአሜሪካ ጦር ኃይል ሲሆን አሁን ያለው ቶርም በቶር ፕሮጄክት ተገንብቷል።የወደፊት ጓደኞችዎን ጨምሮ ቶርን የሚጠቀሙ ብዙ ሰዎች እና ድርጅቶች አሉ።ስለዚህ ፣ Cloudflare ን በድር ጣቢያዎ ላይ የሚጠቀሙ ከሆነ እውነተኛ ሰዎችን እያገዱ ነው።ሊሆኑ የሚችሉትን ጓደኝነት እና የንግድ ስምምነትን ያጣሉ ፡፡ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  የእነሱ የዲ ኤን ኤስ አገልግሎት 1.1.1.1 ፣ እንዲሁም እንደ “127.0.0.x” ያለ የደመና ፍሰት ፣ የአከባቢው የአይፒ አድራሻ (IPHx) የተሰኘውን የሐሰት የአይፒ አድራሻ በመመለስ ተጠቃሚዎችን ድርጣቢያ ከመጎብኘት ያጣራል። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  የደመና ፍላይ ዲ ኤን ኤስ እንዲሁ በሐሰተኛ ዲ ኤን ኤስ ምላሻቸው ምክንያት የመስመር ላይ ሶፍትዌርን ከስማርትፎን መተግበሪያ እስከ ኮምፒተር ጌም ያጠፋልCloudflare ዲ ኤን ኤስ አንዳንድ የባንክ ድር ጣቢያዎችን መጠይቅ አይችልም። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  እና እዚህ ሊያስቡ ይችላሉ<br>ቶር ወይም ቪፒኤን እየተጠቀምኩ አይደለም ፣ ለምንድነው እንክብካቤ የምሆነው?<br>የ Cloudflare ግብይትን አምናለሁ ፣ ለምንድነው ጥንቃቄ ማድረግ ያለብኝ?<br>የእኔን ድር ጣቢያ https ለምን ነው ጥንቃቄ ማድረግ ያለብኝ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Cloudflare ን የሚጠቀም ድር ጣቢያ ከጎበኙ መረጃዎን ለድር ጣቢያው ባለቤት ብቻ ሳይሆን ለ Cloudflare ጭምር እያጋሩ ነው።የተገላቢጦሽ ተኪው እንዴት እንደሚሰራ ነው። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  የ TLS ን ትራፊክ ሳይቀንስ መተንተን አይቻልም ፡፡ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare እንደ ጥሬ የይለፍ ቃል ያሉ ሁሉንም ውሂብዎን ያውቃል። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed በማንኛውም ጊዜ ሊከሰት ይችላል። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare's https ማለቂያ የለውም ማለቂያ የለውም። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  በእውነቱ የእርስዎን ውሂብ ለ Cloudflare እና እንዲሁም ለ3-ደብዳቤ ወኪል ማጋራት ይፈልጋሉ? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  የበይነመረብ ተጠቃሚ የመስመር ላይ መገለጫ መንግስት እና ትልልቅ የቴክኖሎጂ ኩባንያዎች ሊገዛቸው የሚፈልጉት “ምርት” ነው። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  የአሜሪካ የአገር ውስጥ ደህንነት መምሪያ አለ:<br><br>ያለዎት ውሂብ ምን ያህል ዋጋ ያለው እንደሆነ ምንም ሀሳብ አልዎት? ያንን ውሂብ ለእኛ የሚሸጡበት ማንኛውም መንገድ አለ?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare እንዲሁም “Cloudflare Warp” የተባለ ነፃ የቪ.ፒ.አይ. አገልግሎት ይሰጣል።ከተጠቀሙበት ሁሉም የእርስዎ ዘመናዊ ስልክ (ወይም ኮምፒተርዎ) ግንኙነቶች ወደ ክላውድላየር አገልጋዮች ይላካሉ።Cloudflare የትኛውን ድር ጣቢያ እንዳነበቡ ማወቅ ፣ ምን አስተያየት እንደለጠፉ ፣ ማንን እንዳነጋገሩት ፣ ወዘተ ማወቅ ይችላል።ሁሉንም መረጃዎን ለ Cloudflare ለመስጠት ፈቃደኛ ነዎት።“ቀልድ ነው? Cloudflare ደህንነቱ የተጠበቀ ነው። ” ከዚያ VPN እንዴት እንደሚሰራ መማር ያስፈልግዎታል። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare የእነሱ የ VPN አገልግሎት በይነመረብዎን ፈጣን ያደርግላቸዋል ብለዋል።ግን ቪፒኤን (በይነመረብ) ካለዎት ግንኙነት (ኢንተርኔት) ግንኙነት የበለጠ እንዲዘገይ ያደርገዋል ፡፡ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  ስለ PRISM ቅሌት ቀደም ሲል ያውቁ ይሆናል።AT&T ሁሉንም የበይነመረብ ውሂብ ለክትትል ለመቅዳት ለ NSA እውነት መሆኑ እውነት ነው። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  NSA ውስጥ ነው የሚሰሩት እንበል ፣ እናም የእያንዳንዱን ዜጋ የበይነመረብ በይነመረብ ይፈልጋሉ።አብዛኛዎቹ በስውር Cloudflare በጭፍን እንደሚተማመኑ እና እሱን እንደሚጠቀሙ ያውቃሉ - አንድ ማእከል ያለው በር - የኩባንያቸውን አገልጋይ ግንኙነት (ኤስኤስኤች / አርዲ ፒ) ፣ የግል ድር ጣቢያ ፣ የውይይት ድር ጣቢያ ፣ የመድረክ ድር ጣቢያ ፣ የባንክ ድርጣቢያ ፣ የመድን ድር ጣቢያ ፣ የፍለጋ ፕሮግራም ፣ ሚስጥራዊ አባል - ብቻ ድርጣቢያ ፣ የጨረታ ድርጣቢያ ፣ ግብይት ፣ ቪዲዮ ድርጣቢያ ፣ NSFW ድርጣቢያ እና ሕገ-ወጥ ድር ጣቢያ።እንዲሁም ለ “ደህንነቱ የተጠበቀ!” የደመናውላውን የዲ ኤን ኤስ አገልግሎት ("1.1.1.1") እና የቪ.ፒ.ኤን አገልግሎት ("Cloudflare Warp") እንደሚጠቀሙ ያውቃሉ። ፈጣን! የተሻለ! ” በይነመረብ ተሞክሮ።እነሱን ከተጠቃሚዎች IP አድራሻ ፣ ከአሳሽ የጣት አሻራ ፣ ከኩኪዎች እና ከ RAY-ID ጋር በማጣመር የ targetላማውን የመስመር ላይ መገለጫ ለመገንባት ጠቃሚ ነው። | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  የእነሱን ውሂብ ይፈልጋሉ። ምን ታደርጋለህ? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare የማር ማር ነው።** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **ነፃ ማር ለሁሉም ሰው። አንዳንድ ሕብረቁምፊዎች ተያይዘዋል።** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Cloudflare ን አይጠቀሙ።** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **በይነመረቡን ያጠናክሩ** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    እባክዎ ወደ ሚቀጥለው ገጽ ይቀጥሉ:  "[Cloudflare ሥነምግባር](am.ethics.md)"

---

<details>
<summary>_ጠቅ ያድርጉኝ_

## መረጃ እና ተጨማሪ መረጃ
</summary>


ይህ የመረጃ ማከማቻ የቶር ተጠቃሚዎችን እና ሌሎች ሲዲኤንዎችን የሚያግድ ከ “ታላቁ ደመናው” በስተጀርባ ያሉ የድርጣቢያዎች ዝርዝር ነው ፡፡


**ውሂብ**
* [Cloudflare Inc.](../cloudflare_inc/)
* [የደመና ፍሰት ተጠቃሚዎች](../cloudflare_users/)
* [Cloudflare ጎራዎች](../cloudflare_users/domains/)
* [Cloud-Blalare CDN ተጠቃሚዎች](../not_cloudflare/)
* [ፀረ-ቶር ተጠቃሚዎች](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**ተጨማሪ መረጃ**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * አውርድ: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * የመጀመሪያው የ eBook (ePUB) በቅጅ መብት በቅጂ መብት መጣስ ምክንያት በ BookRix GmbH ተሰር wasል
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * ትኬቱ ብዙ ጊዜ በቪዛ ተመቷል ፡፡
  * [በቶር ፕሮጀክት ተሰር .ል።](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [ቲኬት 34175 ን ይመልከቱ ፡፡](https://trac.torproject.org/projects/tor/ticket/34175)
  * [የመጨረሻው መዝገብ መዝገብ 24351።](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_ጠቅ ያድርጉኝ_

## ምን ማድረግ ትችላለህ?
</summary>

* [የሚመከሩ እርምጃዎችን ዝርዝር ያንብቡ እና ለጓደኞችዎ ያጋሩ ፡፡](../ACTION.md)

* [የሌላ ተጠቃሚን ድምጽ ያንብቡ እና ሀሳቦችዎን ይፃፉ ፡፡](../PEOPLE.md)

* የሆነ ነገር ይፈልጉ: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* የጎራ ዝርዝሩን አዘምን: [መመሪያዎችን ይዘርዝሩ](../INSTRUCTION.md).

* [ከ Cloudflare ወይም ከፕሮጄክት ጋር የተዛመደ ዝግጅት ወደ ታሪክ ያክሉ።](../HISTORY.md)

* [ይሞክሩ እና አዲስ መሣሪያ / ስክሪፕት።](../tool/)

* [ለማንበብ የተወሰኑ ፒዲኤፍ / ePUB እነሆ።](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### ስለ ሐሰተኛ መለያዎች

የወንጀል አስተላላፊ ኦፊሴላዊ ጣቢያዎቻችንን የሚያስመስሉ የሐሰት መለያዎች መኖር ፣ Twitter ፣ Facebook ፣ Patreon ፣ OpenCollective ፣ Village ወዘተ
**በጭራሽ ኢሜልዎን አንጠይቅም ፡፡
በጭራሽ ስምህን አንጠይቅም ፡፡
ማንነትዎን በጭራሽ አንጠይቅም ፡፡
አካባቢዎን በጭራሽ አንጠይቅም ፡፡
ልገሳዎን በጭራሽ አንጠይቅም ፡፡
ግምገማዎን በጭራሽ አንጠይቅም።
እኛ በማህበራዊ ሚዲያ እንዲከታተሉ በጭራሽ አንጠይቅም ፡፡
እኛ ማህበራዊ ሚዲያዎን አንጠይቅም ፡፡**

# መለያዎችን በጭራሽ አትታመኑ ፡፡



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)