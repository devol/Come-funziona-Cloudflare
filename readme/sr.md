# Велики облачни зид


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Зауставите Цлоудфларе


|  🖹  |  🖼 |
| --- | --- |
|  „Велики облачни зид“ је компанија Цлоудфларе Инц., америчка компанија.Пружа услуге ЦДН (мрежа за испоруку садржаја), ублажавање ДДоС-а, Интернет безбедност и дистрибуиране ДНС (сервер домена).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Цлоудфларе је највећи МИТМ проки на свету (обрнути проки).Цлоудфларе посједује више од 80% удјела на ЦДН тржишту, а број цлоудфларе корисника свакодневно расте.Мрежу су проширили на више од 100 земаља.Цлоудфларе опслужује више веб промета него Твиттер, Амазон, Аппле, Инстаграм, Бинг и Википедиа.Цлоудфларе нуди бесплатан план и многи људи га користе уместо да правилно конфигуришу своје сервере.Трговали су приватношћу због практичности.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Цлоудфларе сједи између вас и матичног веб сервера, понаша се попут агента граничне патроле.Не можете се повезати са одабраном дестинацијом.Повезујете се с Цлоудфларе-ом и сви се подаци дешифрирају и предају у покрету. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Администратор матичног веб сервера дозволио је агенту - Цлоудфларе - да одлучи ко може приступити њиховом „веб ентитету“ и дефинисати „ограничену област“.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Погледајте праву слику.Мислит ћете да Цлоудфларе блокира само негативце.Мислит ћете да је Цлоудфларе увијек на мрежи (никад не силазите).Мислит ћете да легални ботови и пауци могу индексирати вашу веб локацију.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Међутим, то уопште није тачно.Цлоудфларе без разлога блокира невине људе.Цлоудфларе може пасти.Цлоудфларе блокира законите робота.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Као и сваки хостинг услуга, Цлоудфларе није савршен.Овај екран ћете видети чак и ако матични сервер добро ради.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Да ли заиста мислите да Цлоудфларе има 100% продужетка?Немате појма колико пута се Цлоудфларе спусти.Ако Цлоудфларе падне, ваш клијент не може приступити вашој веб локацији. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  То се назива упућивањем на Велики кинески заштитни зид који обавља упоредни посао филтрирања многих људи од гледања веб садржаја (тј. Свих у континенталној Кини и људи изван ње).Док истовремено они који нису погођени виде дротично другачији веб, веб без цензуре, попут слике „тенкиста“ и историје протеста на „Тргу Тиананмен“. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Цлоудфларе поседује велику моћ.У одређеном смислу, они контролирају оно што крајњи корисник на крају види.Због Цлоудфларе-а спречени сте да прегледавате веб страницу. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Цлоудфларе се може користити за цензуру. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Не можете да видите веб локацију са облаком ако користите мањи претраживач за који Цлоудфларе можда мисли да је бот (јер га не користи много људи). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Не можете проћи ову инвазивну "провјеру" прегледача без омогућавања Јавасцрипта.Ово је губитак пет (или више) секунди вашег вредног живота. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Цлоудфларе такође аутоматски блокира законите робота / пузеће као што су Гоогле, Иандек, Иаци и АПИ клијенти.Цлоудфларе активно прати заједницу „бипасс цлоудфларе“ са намером да разбије законите истраживачке ботове. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Цлоудфларе слично спречава многе људе који имају лошу интернетску повезаност да приступе веб локацијама иза њих (на пример, могу да стоје иза 7+ слојева НАТ или деле исти ИП, на пример јавни Вифи), осим ако не реше вишеструке ЦАПТЦХА-ове.У неким случајевима ће требати 10 до 30 минута да Гоогле задовољи. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Године 2020. Цлоудфларе је прешао из Гооглеове Рецаптцха у хЦаптцха јер Гоогле намерава да наплати за његово коришћење.Цлоудфларе вам је рекао да им је стало до ваше приватности („помаже у решавању проблема приватности“), али ово је очигледно лаж.Све је у питању новца.„ХЦаптцха омогућава веб локацијама да зараде новац послужујући овај захтев, а истовремено блокира ботове и друге облике злостављања“ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Из перспективе корисника, то се не мења много. Присиљени сте да то решите. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Цлоудфларе свакодневно блокира многе људе и софтвер. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Цлоудфларе нервира многе људе широм света.Погледајте листу и размислите да ли је прихватање Цлоудфларе-а на вашој веб локацији добро за корисничко искуство. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Која је сврха интернета ако не можете радити оно што желите?Већина људи који посете вашу веб локацију једноставно ће потражити друге странице ако не могу да учитају веб страницу.Можда не блокирате активно посетиоце, али подразумевани фирефалл Цлоудфлареа је довољно строг да блокира многе људе. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Нема начина да се реши цаптцха без омогућавања Јавасцрипт и Цоокиес.Цлоудфларе их користи за прављење потписа у прегледачу за идентификацију.Цлоудфларе мора знати ваш идентитет да би одлучио да ли можете да наставите са прегледавањем сајта. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Тор корисници и ВПН корисници такође су жртва Цлоудфларе-а.Оба решења користе многи људи који не могу да приуште нецензурирани интернет због своје земље / корпорације / мрежне политике или који желе да додају додатни слој да заштите своју приватност.Цлоудфларе бесрамно напада те људе, присиљавајући их да искључе своје проки решење. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Ако до сада нисте пробали Тор, препоручујемо вам да преузмете Тор прегледач и посетите омиљене веб локације.Предлажемо вам да се не пријављујете на веб локацију своје банке или на веб страницу владе или ће они означити ваш рачун. Користите ВПН за те веб локације. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Могли бисте рећи "Тор је нелегалан! Корисници Тор-а су криминални! Тор је лош! ". Не.Можда сте о Тору сазнали од телевизије, рекавши да се Тор може користити за прегледавање даркнета и трговину оружјем, дрогом или порнографима.Иако је горња изјава тачна да постоји много веб локација на којима можете купити такве ствари, оне се често појављују и на цлеарнету.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Тор је развила америчка војска, али тренутни Тор развијен је пројектом Тор.Много је људи и организација који користе Тор укључујући и ваше будуће пријатеље.Дакле, ако користите Цлоудфларе на својој веб локацији, блокирате праве људе.Изгубит ћете потенцијално пријатељство и посао. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  А њихов ДНС сервис, 1.1.1.1, такође филтрира кориснике да посећују веб локацију враћајући лажну ИП адресу у власништву Цлоудфларе-а, лоцалхост ИП-а као што је „127.0.0.к“, или једноставно не враћају ништа. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Цлоудфларе ДНС такође руши мрежни софтвер од паметне апликације до рачунарске игре због лажног ДНС одговора.Цлоудфларе ДНС не може да упути неке веб странице банке. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  И овде можете помислити,<br>Не користим Тор или ВПН, зашто бих се бринуо?<br>Вјерујем у Цлоудфларе маркетинг, зашто бих се бринуо<br>Моја веб локација је хттпс зашто би ме требало занимати | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Ако посетите веб локацију која користи Цлоудфларе, своје податке делите не само власнику веб локације већ и Цлоудфларе.Овако ради обрнути проки. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Немогуће је анализирати без дешифровања ТЛС саобраћаја. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Цлоудфларе зна све ваше податке као што су необрађена лозинка. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Цлоудбеед се може догодити било када. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Цлоудфларе-ови хттпс никада не долазе до краја. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Да ли заиста желите да делите своје податке са Цлоудфларе-ом, такође агенцијом са 3 слова? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Интернет профил корисника Интернета је „производ“ који влада и велике технолошке компаније желе да купе. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Америчко одељење за унутрашњу безбедност је саопштило:<br><br>Имате ли идеју колико су ваши подаци вредни? Постоји ли неки начин да нам продате те податке?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Цлоудфларе такође нуди БЕСПЛАТНУ ВПН услугу под називом "Цлоудфларе Варп".Ако га користите, све везе вашег паметног телефона (или рачунара) шаљу се на Цлоудфларе сервере.Цлоудфларе може знати коју сте веб локацију прочитали, који коментар сте објавили, са ким сте разговарали итд.Све своје податке добровољно дајете Цлоудфларе-у.Ако мислите „да ли се шалиш? Цлоудфларе је сигуран. " онда морате да научите како функционише ВПН. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Цлоудфларе је рекао да њихова ВПН услуга убрзава ваш интернет.Али ВПН чине да ваша интернетска веза буде спорија од ваше постојеће. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Можда већ знате за скандал са ПРИЗМОМ.Тачно је да АТ&Т дозвољава НСА-у да копира све податке на интернету ради надзора. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Рецимо да радите у НСА и желите да имате Интернет профил сваког грађанина.Знате да већина њих слепо верује Цлоудфларе-у и користе га - само један централизовани пролаз - да посредује везу свог послужитељског предузећа (ССХ / РДП), личну веб локацију, веб локацију за цхат, веб страницу форума, веб локацију банке, веб страницу осигурања, претраживача, тајног члана - веб локација, веб локација аукције, куповина, веб локација, веб локација НСФВ и илегална веб локација.Такође знате да користе Цлоудфларе-ову ДНС услугу ("1.1.1.1") и ВПН услугу ("Цлоудфларе Варп") за "Сигурно! Брже! Боље! “ интернет искуство.Комбиновање њих са корисничком ИП адресом, отиском прста претраживача, колачићима и РАИ-ИД-ом ће бити корисно за изградњу мрежног профила циља. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Ви желите њихове податке. Шта ћеш ти урадити? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Цлоудфларе је медница.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Бесплатан мед за све. Неке жице су у прилогу.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Не користите Цлоудфларе.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Децентрализовати Интернет.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Наставите на следећу страницу:  "[Цлоудфларе етика](sr.ethics.md)"

---

<details>
<summary>_кликни ме_

## Подаци и више информација
</summary>


Ово складиште је листа веб локација које стоје иза „Великог облачног зида“, а блокирају Тор-ове кориснике и друге ЦДН-ове.


**Подаци**
* [Цлоудфларе Инц.](../cloudflare_inc/)
* [Корисници Цлоудфларе-а](../cloudflare_users/)
* [Цлоудфларе домене](../cloudflare_users/domains/)
* [ЦДН корисници који нису Цлоудфларе](../not_cloudflare/)
* [Корисници против Тор-а](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Више информација**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Преузимање: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Оригиналну е-књигу (еПУБ) је избрисао БоокРик ГмбХ због кршења ауторских права ЦЦ0 материјала
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Улазница је вандализована толико пута.
  * [Избрисао је Тор пројекат.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Погледајте карту 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Последња архивска карта 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_кликни ме_

## Шта можете да урадите?
</summary>

* [Прочитајте нашу листу препоручених акција и поделите је са пријатељима.](../ACTION.md)

* [Прочитајте глас другог корисника и напишите своје мисли.](../PEOPLE.md)

* Претражите нешто: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Ажурирајте листу домена: [Листа упутстава](../INSTRUCTION.md).

* [Додајте Цлоудфларе или догађај везан за пројекат у историју.](../HISTORY.md)

* [Покушајте и напишите нови алат / скрипту.](../tool/)

* [Ево неких ПДФ / еПУБ-а за читање.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### О лажним рачунима

Цримефларе зна за постојање лажних рачуна који намећу наше званичне канале, било да су то Твиттер, Фацебоок, Патреон, ОпенЦоллецтиве, Виллагегес итд.
**Никада не питамо вашу е-пошту.
Никад не питамо твоје име.
Никад не тражимо ваш идентитет.
Никад не питамо вашу локацију.
Никада не тражимо вашу донацију.
Никада не тражимо вашу рецензију.
Никада вас не тражимо да то пратите на друштвеним медијима.
Никада не питамо ваше друштвене медије.**

# Не верујте лажним рачунима.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)