# Cloudwallê Mezin


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Cloudflare rawestînin


|  🖹  |  🖼 |
| --- | --- |
|  "Cloudwall ya Mezin" Cloudflare Inc., pargîdaniya Amerîkî ye.Ew karûbarên CDN (tora ragihandinê ya naverokê), kêmkirina DDoS, ewlehiya Internetnternetê, û karûbarên DNS (navnîşa domain name) belavkirî peyda dike.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare nifûsê herî mezin ê MITMê (cîgirê berevajî) ya cîhanê ye.Cloudflare xwedan zêdetirî 80% beşa bazara CDN-ê ye û hejmara bikarhênerên cloudflare her roj zêde dibin.Wan tora xwe li zêdetirî 100 welatan de fireh kirine.Cloudflare ji tevheviya Twitter, Amazon, Apple, Instagram, Bing û Wikipedia bêtir seyrûsefera malperê xizmet dike.Cloudflare nexşeyek belaş pêşkêşî dike û gelek kes li şûna ku serverên xwe bi rengek rastîn saz bikin, wê bikar tînin.Wan li ser hêsantiriyê xwedîtiyê dikin.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare di navbera we û eslê webserverê de cih digire, mîna aktorek patrola sînorî tevdigere.Hûn ne dikarin bi cîhê bijartiya xwe ve girêdayî bikin.Hûn bi Cloudflare ve girêdayî ne û hemî agahdariya we ji bo firînê têne veşartin û radest kirin. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Rêvebirê malpera webserverê ya orjînal destûr da kiryarê - Cloudflare - da ku biryar bide ka kî dikare bigihêje "milkê malperê" û destnîşan bike "devera qedexe".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Awirek bavêjin wêneya rast.Hûn ê difikirin ku Cloudflare tenê merivên xirab asteng dikin.Hûn ê bifikirin ku Cloudflare her dem serhêl e (qet neçe jêrîn).Hûn ê botsên meşrû bifikirin û crawler dikarin malpera xwe index bikin.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Lêbelê ew qe ne rast in.Cloudflare bêyî sedemek mirovên bêguneh asteng dike.Cloudflare dikare biçûya.Cloudflare botên zagonî asteng dike.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Wekî her karûbarê mêvandariyê, Cloudflare ne serfiraz e.Hûn ê vê ekranê bibînin heta ku servera orjînal baş dixebite.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Bi rastî hûn difikirin ku Cloudflare xwedî 100% uptime?Tu fikra we tune ku çiqas car Cloudflare diçin.Ger Cloudflare daket, mişterî we nikare têkeve malpera we. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Ev tête navnîş kirin ku li wallerê Mezin ê ofînê ye ku xebatek berbiçav e ku ji mirovan re ji dîtina naveroka malperê (ango her kesê li landînê sereke û mirovên li derve) filitîne.Dema ku di heman demê de kesên ku bandor ne dikirin ku li ser cîhaziyek bi rengek cûrbecûr bibînin, malperek înternet ji sansûrkirinê wekî wêneyek ji "zilamê tank" û dîroka "protestoyên meydana Tiananmen". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare xwedan hêzek mezin heye.Di hişmendiyek de, ew kontrol dikin ku dawiya bikarhêner di dawiyê de çi dibîne.Hûn ji ber ku Cloudflare têne rûxandin li ser malpera we têne asteng kirin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare dikare ji bo sansorê bikin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Heke hûn gerokek piçûk bikar tînin, ku Cloudflare dikare difikire ku ew botek e (ji ber ku ne pir kesan bi kar tînin) hûn nekarin malperê cloudflared bibînin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Hûn nekarin JavaScript-ê vê "kontrolkirina gerokê" derbasbikin.Ev windakirina pênc (an zêdetir) seconds ji jiyana we ya hêja ye. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare jî bixweber robotên / crawlerên meşrû yên mîna Google, Yandex, Yacy, û API yên xerîdar asteng dike.Cloudflare bi rengek çalakî civaka "dorpêçkirina cloudflare" çavdêrî dike. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare bi heman awayî gelek mirovên ku pêwendiya înternetê ya belengaz jê dikin ji gihîştina malperên li pişt wê (mînakî, ew dikarin li piştê 7+ cûrbecûrên NAT bin an jî parvekirina heman IP, mînakî Wifi ya giştî) asteng bikin heya ku ew wêneya pirjimar CAPTCHA çareser bikin.Di hin rewşan de, ev ê 10-ê 30 hûrdeman bigire da ku Google têr bike. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Di sala 2020-an de Cloudflare ji Google-ê Recaptcha ji hCaptcha vekişand ji ber ku Google ji bo karanîna wê hêj heqê dravê dide.Cloudflare ji we re got ku ew lênihêriya nepenîtiya we dikin ("ew ji bo pirsgirêkek nepenîtiyê bipeyive") lê ev eşkere eşkere derew e.Ew hemî di derbarê drav de ye."HCaptcha destûrê dide malperan ku bi astengkirina bot û formên din ên destdirêjiyê drav didin vê daxwazê." | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Ji perspektîfa bikarhêner ve, ev zêde zêde neguheze. Hûn mecbûr in ku wê çareser bikin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Gelek mirov û nermalava her roj ji hêla Cloudflare ve têne asteng kirin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare gelek kesan li seranserê cîhanê aciz dike.Li navnîşê binihêrin û bifikirin gelo pêkanîna Cloudflare li ser malpera we ji bo ezmûna bikarhêner baş e. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Armanca înternetê heke hûn nekarin tiştê ku hûn dixwazin bikin?Piraniya kesên ku malpera we ziyaret dikin dê heke ew nikarin rûpelek malperê barkirinê li rûpelên din digerin.Dibe ku hûn ji bo ziyaretvanan çalak nekin asteng, lê dîwarê sepandina Cloudflare qasî hişk e ku ji bo gelek kesan asteng bike. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Enêdike ku Javascript û Cookies nekarin çareser bikin captcha.Cloudflare wan bi kar tîne da ku nîşana gerokerek çêbikin da ku hûn nas bikin.Cloudflare pêdivî ye ku hûn nasnameya xwe bizanibin da ku hûn biryarê bidin ku hûn eligeble in ku li geroka malperê berdewam bikin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Bikarhênerên Tor û VPN-ê jî qurbaniya Cloudflare ne.Her du çareserî ji hêla gelek kesên ku nekarin internetnternetê ya nehsandî ji ber siyaseta welatê xwe / pargîdanî / torê an jî yên ku dixwazin pêvekek din lê bixin ji bo parastina nepeniya xwe bikar tînin.Cloudflare bi şermî êrîşî wan kesan dike, bi zorê wan da ku çareseriya proxoyê xwe ji holê rakin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Heke hûn Torê heta vê gavê nexebitin, em ji we re teşwîq dikin ku Tor Browser dakêşin û malperên xweyên bijare bibînin.Em ji we re pêşniyar dikin ku hûn nekin navnîşa malpera xwe ya bankê an rûpelê hikûmetê an na ew ê hesabê we ala bikin. Ji bo wan malperan VPN bikar bînin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Dibe ku hûn bibêjin “Tor neqanûnî ye! Bikarhênerên Tor sûcdar in! Tor xirab e! ". Na.Hûn dikarin li ser Tor ji televîzyonê fêr bibin, bêjin Tor dikare were xebitandin da ku hûn tarîf û tarîyên bazirganî, derman an çîçox porn bazirganî bikin.Dema ku gotina jor rast e ku gelek malperên bazarê hene ku hûn dikarin tiştên wusa bikirin, ew malperan pir caran li clearnet jî têne xuya kirin.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor ji hêla Artêşa Amerîkî ve hat pêşve xistin, lê Torê niha bi hêla projeya Tor ve tê pêşve xistin.Gelek kes û rêxistin hene ku Tor bikar tînin tevî hevalên xwe yên pêşeroj.Ji ber vê yekê, heke hûn li ser malpera xwe Cloudflare bikar bînin hûn mirovên rastîn asteng dikin.Hûn ê hevaltiya potansiyel û peymana karsaziyê winda bikin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Service karûbarê wan DNS, 1.1.1.1, di heman demê de bikarhênerên ji malpera xwe vedigire jî bi vegerandina navnîşana IP ya derewîn a xwedan Cloudflare, IP-ya herêmî ya wekî "127.0.0.x", an tenê tiştek vedigere. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS jî ji ber ku bersiva wan a DNS-a derewîn nermalava serhêl ji serlêdana smartphone ji lîstika komputerê veqetîne.Cloudflare DNS nikare hin malperên bankê bipirse. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Here li vir hûn dikarin bifikirin,<br>Ez Tor an VPN bikar nakim, çima divê ez lênêrînim?<br>Ez ji kirrûbirra Cloudflare bawer dikim, çima divê ez lênêrînim<br>Malpera min https e çima divê ez nêrînim | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Heke hûn biçin serdana malperên ku Cloudflare bikar tînin, hûn agahiyên xwe ne tenê bi xwediyê malperê re, lê her weha Cloudflare jî parve dikin.Ev wilo dike ku merivên peyxama rever bikêr be. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Ne gengaz e ku meriv bêyî hilgirtina seyrûseya TLS analîz bike. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare hemî daneyên weyên wekî şîfreya xav dizanin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed dikare di her demê de çêdibe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https-a Cloudflare-ê qet carî dawiya-dawiya ne. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Hûn bi rastî dixwazin ku daneyên xwe bi Cloudflare, û her weha ajansa 3-nameyan re parve bikin? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Profîla serhêl a bikarhênerê Internetnternetê "hilberek" e ku hukûmet û pargîdaniyên mezin ên teknolojiyê dixwazin bikirin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Wezareta Ewlehiya Navxwe ya Y.D. got:<br><br>Ma hûn haya we jê heye ku daneya we çiqas nirxdar e? Bi awayek heye ku hûn wê daneyê ji me re bifroşin?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare di heman demê de karûbarê VPN FELAV bi navê "Cloudflare Warp" pêşkêşî dike.Heke hûn wê bikar tînin, hemî pêwendiyên smartphone (an komputera we) ji bo serverên Cloudflare têne şandin.Cloudflare dikare bi kîjan malperê hûn bixwînin, bi kîjan şîroveya ku we şand, hûn bi kî re bi wan re axaftin, hwd zanibin.Hûn dilxwazî ​​hemî agahdariya xwe ji Cloudflare re didin.Heke hûn difikirin “Ma hûn şok dikin? Cloudflare ewle ye. " hingê hûn hewce ne ku fêr bibin ka VPN çawa dixebite. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare got ku karûbarê wan VPN zûtirîn înternetê dike.Lê VPN têkiliya xwe ya înternetê ji ya girêdana weya heyî hêdî hêdî dike. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Dibe ku hûn berê di derbarê skandala PRISM de dizanin.Rast e ku AT&T dihêle NSA ku hemî daneyên internetnternetê ji bo çavdêriyê kopî bike. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Ka em bêjin hûn li NSA kar dikin, û hûn profîla internetnternetê ya her welatî dixwazin.Hûn dizanin piraniya wan bi Cloudflare-yê bawer dikin û ew bikar tînin - tenê yek deriyek navendî - ji bo girêdana serverê ya pargîdaniya wan (SSH / RDP), malperê kesane, malpera chat, malpera forumê, malperê bankê, malperê bîmeyê, motora lêgerînê, endamê veşartî. Malpera yekane, malpera ankand, kirînê, malpera vîdyoyê, malpera NSFW, û malpera neqanûnî.Hûn jî dizanin ku ew karûbarê DNS ya Cloudflare ("1.1.1.1") û karûbarê VPN ("Cloudflare Warp") ji bo "Ewlehî! Zûtir! Baştir!" ezmûna înternetê.Bihevxistina wan bi navnîşana IP-ya bikarhêner, şîfreya tilikê, cookies û RAY-ID dê ji bo avakirina profîla serhêl a kêrhatî be. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Hûn daneyên wan dixwazin. Tu ê çi bikî? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare honandî ye.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Belaş ji bo herkesî. Hin stûnên pêve çêdibin.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Cloudflare bikar neynin.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Thenternetê dexelal bikin.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Ji kerema xwe heya rûpelê din berdewam bikin:  "[Etîmolojiya Cloudflare](ku.ethics.md)"

---

<details>
<summary>_min bitikîne_

## Daneyên û Zêdetir Agahdarî
</summary>


Vê repoya navnîşek malperên ku li paş "The Cloudwall mezin" in, rawestandina bikarhênerên Tor û CDNên din e.


**Jimare**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Bikarhênerên Cloudflare](../cloudflare_users/)
* [Domainên Cloudflare](../cloudflare_users/domains/)
* [Bikarhênerên CDN yên ne-Cloudflare](../not_cloudflare/)
* [Bikarhênerên dijî-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Agahdariya bêtir**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Daxistin: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Ji ber binpêkirina copyright a madeya CC0-ê, pirtûka xweya eslî (ePUB) ji hêla BookRix GmbH ve hate jêbirin.
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Bilêt çend car hate taloqkirin.
  * [Ji hêla Projeya Tor ve hate jêbirin.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Bilêtê 34175 binihêrin.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Bilêta arşîvê ya paşîn 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_min bitikîne_

## Tu dikarî çi bikî?
</summary>

* [Navnîşa me ya kiryarên pêşniyaz bixwînin û wê bi hevalên xwe re parve bikin.](../ACTION.md)

* [Dengê bikarhênerê din bixwînin û ramanên xwe binivîsin.](../PEOPLE.md)

* Li tiştek bigerin: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Navnîşa domainê nûve bikin: [Rêbernameyên navnîş bikin](../INSTRUCTION.md).

* [Cloudflare an bûyerê têkildarî projeyê bi dîrokê ve zêde bike.](../HISTORY.md)

* [Vebijêrin / Nivîsbar / Nivîsbariya Nû binivîsin.](../tool/)

* [Li vir çend PDF / ePUB heye ku hûn bixwînin.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Li ser hesabên derewîn

Crimeflare di derbarê hebûna hesabên derewîn ên ku kanalên meya fermî de dijîn dizane, ew bibin Twitter, Facebook, Patreon, OpenCollective, Gundan hwd.
**Em qet e-nameya we dipirsin.
Em qet navê te napirsin.
Em qet nasnameya we dipirsin.
Em qet ji cîhê we nepirsin.
Em tu carî ji donê we napirsin.
Em tu carî napirsa we dipirsin.
Em qet ji we bipirsin ku hûn li ser medyaya civakî bişopînin.
Em tu carî ji medyaya civakî ya we dipirsin.**

# NAVNANN XWE NIKARIN.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)