# Cloudwall i Madh


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Ndaloni Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Great Cloudwall" është Cloudflare Inc., kompania amerikane.Ajo po ofron shërbime të CDN (shpërndarja e përmbajtjes), zbutjen e DDoS, sigurinë e Internetit dhe shërbimet e shpërndara DNS (emri i domain).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare është përfaqësuesi më i madh në botë i MITM (përfaqësuesi i kundërt).Cloudflare zotëron më shumë se 80% të pjesës së tregut CDN dhe numri i përdoruesve të cloudflare po rritet çdo ditë.Ata kanë zgjeruar rrjetin e tyre në më shumë se 100 vende.Cloudflare shërben më shumë trafik në internet sesa të kombinuar Twitter, Amazon, Apple, Instagram, Bing & Wikipedia.Cloudflare po ofron plan falas dhe shumë njerëz po e përdorin atë në vend që të konfigurojnë serverat e tyre siç duhet.Ata tregtuan intimitetin për lehtësi.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare ulet mes jush dhe serverit të origjinës, duke vepruar si një agjent i patrullës kufitare.Ju nuk jeni në gjendje të lidheni me destinacionin tuaj të zgjedhur.Ju jeni duke u lidhur me Cloudflare dhe të gjitha informacionet tuaja po deshifrohen dhe dorëzohen në fluturim. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Administratori i faqes së internetit të origjinës i lejoi agjentit - Cloudflare - të vendosë se kush mund të hyjë në "pronën e tyre në internet" dhe të përcaktojë "zonën e kufizuar".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Shikoni imazhin e duhur.Do të mendoni se Cloudflare bllokon vetëm njerëz të këqij.Do të mendoni se Cloudflare është gjithmonë në internet (kurrë mos u tërhiqni).Ju do të mendoni se bots legal dhe crawlers mund të indeksojnë faqen tuaj te internetit.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Megjithatë ato nuk janë aspak të vërteta.Cloudflare po bllokon njerëz të pafajshëm pa asnjë arsye.Cloudflare mund të zbresë.Cloudflare bllokon bots legal.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Ashtu si çdo shërbim pritës, Cloudflare nuk është i përsosur.Do ta shihni këtë ekran edhe nëse serveri i origjinës po funksionon mirë.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  A mendoni vërtet se Cloudflare ka 100% në kohë?Ju nuk e keni idenë se sa herë Cloudflare zbret.Nëse Cloudflare zbret klienti juaj nuk mund të hyni në faqen tuaj të internetit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Quhet kjo duke iu referuar Firewall-it të Madh të Kinës i cili bën një punë të krahasueshme për të filtruar shumë njerëz nga shikimi i përmbajtjes në internet (d.m.th. të gjithë në Kinë kontinentale dhe njerëz jashtë).Ndërsa në të njëjtën kohë, ata që nuk preken për të parë një rrjet në mënyrë dramatike të ndryshme, një ueb pa censurë siç është një imazh i "burrit tank" dhe historia e "protestave të Sheshit Tiananmen". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare posedon fuqi të madhe.Në një farë kuptimi, ata kontrollojnë atë që përdoruesi përfundimtar e sheh përfundimisht.Ju jeni të ndaluar të shikoni në faqen e internetit për shkak të Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare mund të përdoret për censurë. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Ju nuk mund të shikoni faqen e internetit me fllad nëse përdorni një shfletues të vogël, i cili Cloudflare mund të mendojë se është një bot (sepse jo shumë njerëz e përdorin atë). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Ju nuk mund ta kaloni këtë "kontroll të shfletuesit" pushtues pa aktivizuar Javascript.Kjo është një humbje prej pesë (ose më shumë) sekondash të jetës suaj të vlefshme. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare gjithashtu bllokon automatikisht robotë / zvarritës legjitime si klientët Google, Yandex, Yacy dhe API.Cloudflare po monitoron në mënyrë aktive komunitetin "anashkalojë cloudflare" me qëllim të prishjes së boteve të kërkimit të ligjshëm. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare në mënyrë të ngjashme parandalon shumë njerëz që kanë lidhje të dobët të internetit nga hyrja në faqet e internetit pas tij (për shembull, ata mund të jenë prapa 7+ shtresave të NAT ose të ndajnë të njëjtën IP, për shembull Wifi publike) përveç nëse zgjidhin shumë fotografi CAPTCHA.Në disa raste, kjo do të zgjasë 10 deri në 30 minuta për të kënaqur Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Në vitin 2020 Cloudflare kaloi nga Recaptcha e Google në hCaptcha pasi Google synon të ngarkojë për përdorimin e tij.Cloudflare ju tha që ata kujdesen për privatësinë tuaj ("ndihmon në adresimin e një shqetësimi për privatësinë") por kjo është padyshim një gënjeshtër.Ka të bëjë me paratë."HCaptcha lejon faqet e internetit të bëjnë para që i shërbejnë kësaj kërkese ndërsa bllokojnë bots dhe format e tjera të abuzimit" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Nga këndvështrimi i përdoruesit, kjo nuk ndryshon shumë. Ju jeni duke u detyruar ta zgjidhni atë. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Shumë njerëz dhe softuer po bllokohen çdo ditë nga Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare irriton shumë njerëz në mbarë botën.Shikoni në listë dhe mendoni nëse miratimi i Cloudflare në faqen tuaj është i mirë për përvojën e përdoruesit. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Cili është qëllimi i internetit nëse nuk mund të bëni atë që dëshironi?Shumica e njerëzve që vizitojnë faqen tuaj të internetit do të kërkojnë vetëm faqe të tjera nëse nuk mund të ngarkojnë një faqe në internet.Ju mund të mos bllokoni aktivisht asnjë vizitor, por zjarri i parazgjedhur i Cloudflare është mjaft i rreptë për të bllokuar shumë njerëz. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Nuk ka asnjë mënyrë për të zgjidhur captcha pa aktivizuar Javascript dhe Cookies.Cloudflare po i përdor ato për të bërë një nënshkrim të shfletuesit për t'ju identifikuar.Cloudflare duhet të dijë identitetin tuaj për të vendosur nëse jeni i zgjuar për të vazhduar shfletimin e faqes. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Përdoruesit e Tor dhe përdoruesit VPN janë gjithashtu një viktimë e Cloudflare.Të dyja zgjidhjet janë duke u përdorur nga shumë njerëz që nuk mund të përballojnë internetin e pa censuruar për shkak të politikës së vendit / korporatës / rrjetit të tyre ose që dëshiron të shtojë një shtresë shtesë për të mbrojtur privatësinë e tyre.Cloudflare po sulmon me paturpësi ata njerëz, duke i detyruar ata të fikin zgjidhjen e tyre të përfaqësuesit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Nëse nuk e keni provuar Tor deri në këtë moment, ju inkurajojmë të shkarkoni Tor Browser dhe të vizitoni faqet e internetit të preferuara.Ne ju sugjerojmë të mos hyni në faqen e internetit të bankës tuaj ose në faqen e qeverisë ose ata do të flamurojnë llogarinë tuaj. Përdorni VPN për ato faqe në internet. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Ju mund të dëshironi të thoni "Tor është i paligjshëm! Përdoruesit e Tor janë kriminelë! Tor është i keq! ". Jo.Ju mund të mësoni në lidhje me Tor nga televizioni, duke thënë se Tor mund të përdoret për të shfletuar armët e errëta dhe për të tregtuar armë, droga ose pornografi.Ndërsa deklarata e mësipërme është e vërtetë që ka shumë faqe në internet të tregut ku mund të blini artikuj të tillë, ato faqe shpesh shfaqen edhe në clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor u zhvillua nga Ushtria Amerikane, por Tor aktual është zhvilluar nga projekti Tor.Ka shumë njerëz dhe organizata që përdorin Tor përfshirë miqtë tuaj të ardhshëm.Pra, nëse jeni duke përdorur Cloudflare në faqen tuaj të internetit ju po bllokoni njerëzit e vërtetë.Do të humbni miqësinë e mundshme dhe marrëveshjen e biznesit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Dhe shërbimi i tyre DNS, 1.1.1.1, po filtron gjithashtu përdoruesit nga vizita në faqen e internetit duke kthyer IP adresën e rreme në pronësi të Cloudflare, IP localhost siç është "127.0.0.x", ose thjesht nuk kthen asgjë. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS gjithashtu prish softuerin online nga aplikacioni smartphone në lojën kompjuterike për shkak të përgjigjes së tyre të rreme DNS.Cloudflare DNS nuk mund të kërkojë disa uebfaqe të bankave. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Dhe këtu mund të mendoni,<br>Unë nuk jam duke përdorur Tor ose VPN, pse duhet të kujdesem?<br>Unë i besoj marketingut Cloudflare, pse duhet të kujdesem<br>Uebfaqja ime është https pse duhet të kujdesem | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Nëse vizitoni uebfaqen që përdor Cloudflare, po i ndani informacionet tuaja jo vetëm pronarit të faqes së internetit, por edhe Cloudflare.Kështu funksionon prokura e kundërt. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Shtë e pamundur të analizohet pa deshifruar trafikun TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare i di të gjitha të dhënat tuaja si fjalëkalimi i papërpunuar. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed mund të ndodhë në çdo kohë. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https e Cloudflare nuk është kurrë fund-për-fund. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  A doni vërtet t'i ndani të dhënat tuaja me Cloudflare, dhe gjithashtu agjensinë me 3 shkronja? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Profili në internet i përdoruesit të Internetit është një "produkt" që qeveria dhe kompanitë e mëdha të teknologjisë duan të blejnë. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Tha Departamenti amerikan i Sigurisë së Atdheut:<br><br>A keni ndonjë ide se sa të vlefshme janë të dhënat që keni? A ka ndonjë mënyrë që të na shesësh ato të dhëna?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare gjithashtu ofron shërbim VPN FALAS të quajtur “Cloudflare Warp”.Nëse e përdorni, të gjitha lidhjet e telefonit tuaj inteligjent (ose kompjuterin tuaj) dërgohen në serverët Cloudflare.Cloudflare mund të dijë me cilën faqe në internet keni lexuar, çfarë komenti keni postuar, me kë keni biseduar, etj.Ju jeni duke dhënë vullnetarisht të gjithë informacionin tuaj në Cloudflare.Nëse mendoni se “po bëni shaka? Cloudflare është i sigurt. " atëherë duhet të mësoni se si funksionon VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare tha se shërbimi i tyre VPN e bën internetin tuaj të shpejtë.Por VPN e bëjnë lidhjen tuaj të internetit më të ngadaltë se lidhja juaj ekzistuese. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Ju tashmë mund të dini për skandalin PRISM.Shtë e vërtetë që AT&T i lejon NSA të kopjojë të gjitha të dhënat e internetit për mbikëqyrje. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Le të themi se jeni duke punuar në NSA, dhe ju doni profilin e internetit të çdo qytetari.Ju e dini që shumica e tyre besojnë verbërisht në Cloudflare dhe e përdorin atë - vetëm një portë të centralizuar - për të lidhur lidhjen e serverit të kompanisë së tyre (SSH / RDP), uebfaqen personale, uebfaqen e chatit, faqen e internetit të forumit, faqen e internetit të bankës, faqen e internetit të sigurimeve, motorin e kërkimit, anëtarin e fshehtë një faqe në internet, ankand në internet, pazar, video në internet, Uebfaqe NSFW dhe uebfaqe të paligjshme.Ju gjithashtu e dini që ata përdorin shërbimin DNS të Cloudflare ("1.1.1.1") dhe shërbimin VPN ("Cloudflare Warp") për "Sigurt! Më shpejt! Më mirë! " përvoja në internet.Kombinimi i tyre me adresën IP të përdoruesit, gjurmët e gishtave të shfletuesit, cookies dhe RAY-ID do të jetë i dobishëm për të ndërtuar profilin në internet të synuar. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Ju dëshironi të dhënat e tyre. Cfare do te besh? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare është një huall mjalti.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Mjaltë falas për të gjithë. Disa tela të bashkangjitura.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Mos përdorni Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Decentralizoni internetin.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Ju lutemi vazhdoni në faqen tjetër:  "[Etika e cloud](sq.ethics.md)"

---

<details>
<summary>_me kliko mua_

## Të dhëna dhe më shumë informacion
</summary>


Kjo depo është një listë e faqeve të internetit që janë pas "The Great Cloudwall", duke bllokuar përdoruesit e Tor dhe CDN-të e tjera.


**Të dhënat**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Përdoruesit e cloud](../cloudflare_users/)
* [Domenet e cloud](../cloudflare_users/domains/)
* [Përdoruesit jo-cloud të CDN](../not_cloudflare/)
* [Përdoruesit e anti-torëve](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Më shumë informacion**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Shkarko: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * EBook origjinal (ePUB) u fshi nga BookRix GmbH për shkak të shkeljes së të drejtave të autorit të materialit CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Bileta u vandalizua shumë herë.
  * [Fshirë nga Tor Project.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Shih biletën 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Bileta e fundit e arkivit 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_me kliko mua_

## Cfare mund te besh?
</summary>

* [Lexoni listën tonë të veprimeve të rekomanduara dhe ndajeni atë me miqtë tuaj.](../ACTION.md)

* [Lexoni zërin e përdoruesit të tjerë dhe shkruani mendimet tuaja.](../PEOPLE.md)

* Kërkoni diçka: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Përditësoni listën e domenit: [Listoni udhëzimet](../INSTRUCTION.md).

* [Shtoni histori Cloudflare ose ngjarje të lidhura me projektin.](../HISTORY.md)

* [Provo dhe shkruaj mjetin / skenarin e ri.](../tool/)

* [Këtu keni disa PDF / ePUB për të lexuar.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Rreth llogarive të rreme

Crimeflare di për ekzistencën e llogarive të rreme që impersonojnë kanalet tona zyrtare, qoftë Twitter, Facebook, Patreon, OpenCollective, Fshatrat etj.
**Asnjëherë nuk kërkojmë emailin tuaj.
Asnjëherë nuk kërkojmë emrin tuaj.
Ne kurrë nuk pyesim identitetin tuaj.
Asnjëherë nuk pyesim vendndodhjen tuaj.
Asnjëherë nuk kërkojmë dhurimin tuaj.
Asnjëherë nuk kërkojmë vlerësimin tuaj.
Asnjëherë nuk ju kërkojmë të ndiqni në mediat sociale.
Asnjëherë nuk pyesim në median tënde sociale.**

# Mos u besoni llogaritjeve.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)