# El gran núvol


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Atureu Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Great Cloudwall" és Cloudflare Inc., l'empresa nord-americana.Ofereix serveis CDN (xarxa de lliurament de contingut), mitigació de DDoS, seguretat a Internet i serveis DNS distribuïts (servidor de noms de domini).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare és el proxy MITM més gran del món (servidor intermediari invers).Cloudflare posseeix més del 80% de la quota de mercat de CDN i el nombre d'usuaris de cloudflare creixen cada dia.Han ampliat la seva xarxa a més de 100 països.Cloudflare serveix més trànsit web que Twitter, Amazon, Apple, Instagram, Bing i Viquipèdia.Cloudflare ofereix un pla gratuït i molta gent l'utilitza en lloc de configurar els servidors adequadament.Van comerciar privadesa per comoditat.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare se situa entre vosaltres i el servidor web d'origen, actuant com un agent de patrulla de frontera.No podeu connectar-vos al destí escollit.Esteu connectant-vos a Cloudflare i tota la vostra informació es descifra i es lliura de forma ininterrompuda. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  L'administrador del servidor web d'origen va permetre a l'agent - Cloudflare - decidir qui pot accedir a la seva "propietat web" i definir "àrea restringida".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Mireu la imatge adequada.Pensareu que Cloudflare bloqueja només els dolents.Pensareu que Cloudflare sempre està en línia (no es tanca mai).Pensareu que els bots legals i els rastrejadors poden indexar el vostre lloc web.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Tanmateix, no són certs.Cloudflare està bloquejant persones innocents sense cap raó.La fallada de núvols pot baixar.Cloudflare bloqueja els bots legítims.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Igual que qualsevol servei d’allotjament, Cloudflare no és perfecte.Veureu aquesta pantalla encara que el servidor d'origen funcioni bé.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  De debò creieu que Cloudflare té un temps de funcionament 100%?No tens ni idea quantes vegades baixa Cloudflare.Si Cloudflare baixa, el vostre client no pot accedir al vostre lloc web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  D’això se’n fa referència al gran tallafoc de la Xina, que fa un treball comparable a l’hora de filtrar molts humans de veure contingut web (és a dir, tothom a la Xina continental i persones de fora).Si bé, al mateix temps, els no afectats van veure una web dràcticament diferent, una web lliure de censura com ara una imatge de “home del tanc” i la història de les “protestes a la plaça de Tiananmen”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare té un gran poder.En certa manera, controlen el que al final l'usuari final veu.Se li impedeix navegar pel lloc web a causa de Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare es pot utilitzar per a la censura. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  No podeu veure el lloc web cloudflared si feu servir un navegador menor que Cloudflare pot pensar que és un bot (perquè no molta gent l’utilitza). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  No podeu passar aquesta “comprovació del navegador” sense habilitar Javascript.Es tracta d’una pèrdua de cinc (o més) segons de la vostra valuosa vida. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare també bloqueja automàticament robots / rastrejadors legítims com ara clients de Google, Yandex, Yacy i API.Cloudflare segueix activament la comunitat "bypass cloudflare" amb la intenció de trencar els robots de recerca legítims. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  De manera similar, Cloudflare impedeix que moltes persones que tinguin una mala connexió a Internet accedeixin als llocs web que hi ha al darrere (per exemple, podrien estar al darrere de més de 7 capes de NAT o compartir una mateixa IP, per exemple, wifi pública) tret que resolguin múltiples CAPTCHA d’imatges.En alguns casos, aquest procés trigarà entre 10 i 30 minuts a satisfer Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  L'any 2020 Cloudflare va passar de Recaptcha a Google a hCaptcha, ja que Google té intenció de cobrar-ne el seu ús.Cloudflare li va dir que els importa la seva privadesa ("ajuda a respondre a una privacitat"), però evidentment és una mentida.Es tracta de diners.“HCaptcha permet als llocs web guanyar diners per atendre aquesta demanda alhora que bloquegen els robots i altres formes d’abús”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Des de la perspectiva de l’usuari, això no canvia gaire. Estàs obligat a solucionar-ho. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare està bloquejat molts humans i programari cada dia. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare molesta molta gent a tot el món.Doneu un cop d'ull a la llista i penseu si l’adopció de Cloudflare al vostre lloc és bona per a l’experiència de l’usuari. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Quin és l’objectiu d’internet si no podeu fer el que vulgueu?La majoria de les persones que visiten el vostre lloc web només buscaran altres pàgines si no poden carregar una pàgina web.És possible que no bloquegeu activament cap visitant, però el tallafoc predeterminat de Cloudflare és prou estricte per bloquejar moltes persones. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  No hi ha manera de resoldre el captcha sense habilitar Javascript i Cookies.Cloudflare els utilitza per fer una signatura del navegador per identificar-vos.Cloudflare necessita conèixer la vostra identitat per decidir si és elegible per continuar navegant pel lloc. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Els usuaris de Tor i els usuaris de VPN també són víctimes de Cloudflare.Ambdues solucions estan sent utilitzades per moltes persones que no poden pagar-se internet sense censura a causa de la seva política de país / corporació / xarxa o que volen afegir capes addicionals per protegir la seva privadesa.Cloudflare està atacant descaradament a aquestes persones, obligant-les a desactivar la seva solució de representació. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Si fins aquest moment no heu provat Tor, us recomanem que descarregueu Tor Browser i visiteu els vostres llocs web preferits.Us suggerim que no inicieu la sessió al lloc web bancari o a la pàgina web del govern o que marqui el vostre compte. Utilitzeu la VPN per a aquests llocs web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Potser voldreu dir: "Tor és il·legal! Els usuaris de Tor són criminals! Tor és dolent! ". No.Potser heu après a Tor des de la televisió, dient que Tor es pot utilitzar per navegar per darknet i canviar armes, drogues o porno xicotet.Si bé la declaració anterior és certa que hi ha molts llocs web del mercat on es poden comprar aquests articles, aquests llocs també apareixen sovint a claredat.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor va ser desenvolupat per US Army, però Tor actual està desenvolupat pel projecte Tor.Hi ha moltes persones i organitzacions que utilitzen Tor, inclosos els teus futurs amics.Així, si utilitzeu Cloudflare al vostre lloc web, bloquegeu humans reals.Perdràs possibles acords d’amistat i negocis. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  I el seu servei DNS, 1.1.1.1, també filtra els usuaris que visiten el lloc web retornant una adreça IP falsa propietat de Cloudflare, IP localhost com ara "127.0.0.x", o simplement no retornen res. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  El DNS de Cloudflare també trenca el programari en línia des de l'aplicació per a telèfons intel·ligents al joc d'ordinador a causa de la seva resposta DNS falsa.El DNS de Cloudflare no pot consultar alguns llocs web bancaris. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  I aquí podríeu pensar,<br>No estic utilitzant Tor o VPN, per què m’hauria d’importar?<br>Confio en el màrqueting de Cloudflare, per què m’hauria d’importar<br>El meu lloc web és https per què m’hauria d’importar | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Si visiteu el lloc web que utilitza Cloudflare, compartiu la informació no només amb el propietari del lloc web, sinó també Cloudflare.Així funciona el servidor intermediari invers. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  És impossible analitzar sense desxifrar el trànsit TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare coneix totes les vostres dades, com ara la contrasenya bruta. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  El núvol es pot produir en qualsevol moment. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  El https de Cloudflare mai no és de final. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Vols compartir les teves dades amb Cloudflare i també amb una agència de tres cartes? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  El perfil en línia d'un usuari d'Internet és un "producte" que el govern i les grans empreses de tecnologia volen comprar. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Va dir el Departament de Seguretat Nacional dels EUA:<br><br>Tens alguna idea de quina valor són les dades que tens? Hi ha alguna manera de vendre'ns aquestes dades?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare també ofereix un servei de VPN GRATU calledT, anomenat "Cloudflare Warp".Si l’utilitzeu, totes les connexions del vostre telèfon intel·ligent (o del vostre ordinador) s’envien als servidors de Cloudflare.Cloudflare pot saber en quin lloc web heu llegit, quin comentari heu publicat, amb qui heu parlat, etc.Esteu donant voluntàriament tota la vostra informació a Cloudflare.Si penses “estàs fent broma? Cloudflare és segur. " llavors heu d’aprendre com funciona la VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare va dir que el seu servei VPN fa que la vostra xarxa sigui ràpida.Però la VPN fa que la vostra connexió a Internet sigui més lenta que la vostra connexió existent. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Potser ja sabreu sobre l’escàndol PRISM.És cert que AT&T permet a NSA copiar totes les dades d’internet per a la seva vigilància. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Diguem que treballes a l’NSA i que desitges el perfil d’internet de cada ciutadà.Sabeu que la majoria d’ells confien cegament en Cloudflare i l’utilitzen (només una passarel·la centralitzada) per fer referència a la connexió del servidor de la seva empresa (SSH / RDP), lloc web personal, lloc web de xat, lloc web del fòrum, lloc web del banc, lloc web d’assegurances, motor de cerca, membre secret -un lloc web únic, lloc web de subhastes, compres, lloc web de vídeos, lloc web NSFW i lloc web il·legals.També sabeu que utilitzen el servei DNS de Cloudflare ("1.1.1.1") i el servei VPN ("Cloudflare Warp") per a "Secure! Més ràpid! Millor!" experiència a Internet.Si es combinen amb l’adreça IP de l’usuari, l’empremta digital del navegador, les cookies i el RAY-ID, serà útil per crear el perfil en línia del destinatari. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Vostè vol les seves dades. Què faràs? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare és un punt mel.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Mel gratis per a tothom. Algunes cadenes enganxades.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **No utilitzeu Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Descentralitzar Internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Continua a la pàgina següent:  "[Ètica del núvol del núvol](ca.ethics.md)"

---

<details>
<summary>_feu clic a mi_

## Dades i més informació
</summary>


Aquest dipòsit és una llista de llocs web que hi ha al darrere de "The Great Cloudwall", que bloquegen els usuaris de Tor i altres CDNs.


**Dades**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Usuaris de Cloudflare](../cloudflare_users/)
* [Domini de núvols incloents](../cloudflare_users/domains/)
* [Usuaris de CDN que no siguin Cloudflare](../not_cloudflare/)
* [Usuaris anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Més informació**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * descarregar: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * BookRix GmbH ha suprimit el llibre electrònic electrònic original (ePUB) a causa d'una infracció dels drets d'autor del material CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * El bitllet va ser vandalitzat tantes vegades.
  * [Suprimit pel Projecte Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Veure el bitllet 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Últim bitllet d’arxiu 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_feu clic a mi_

## Què pots fer?
</summary>

* [Llegiu la nostra llista d’accions recomanades i compartiu-la amb els vostres amics.](../ACTION.md)

* [Llegiu la veu d’un altre usuari i escriviu els vostres pensaments.](../PEOPLE.md)

* Busca alguna cosa: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Actualitzeu la llista de dominis: [Llista instruccions](../INSTRUCTION.md).

* [Afegiu Cloudflare o esdeveniment relacionat amb el projecte a l’historial.](../HISTORY.md)

* [Proveu i escrigui nova eina / script.](../tool/)

* [A continuació, es descriuen alguns PDF / ePUB.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Quant a comptes falsos

Informeu-vos de l'existència de comptes falsos que substitueixen els nostres canals oficials, ja sigui Twitter, Facebook, Patreon, OpenCollective, Villages, etc.
**Mai demanem el vostre correu electrònic.
Mai demanem el vostre nom.
Mai demanem la vostra identitat.
Mai preguntem la vostra ubicació.
Mai demanem la vostra donació.
Mai demanem la vostra crítica.
Mai us demanem que seguiu les xarxes socials.
Mai preguntem als vostres mitjans de comunicació social.**

# NO CONFIEU FACTURAR COMPTES.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)