# Το Μεγάλο Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Σταματήστε το Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  Το "The Great Cloudwall" είναι η Cloudflare Inc., η αμερικανική εταιρεία.Παρέχει υπηρεσίες CDN (δίκτυο παράδοσης περιεχομένου), μετριασμό DDoS, ασφάλεια Διαδικτύου και υπηρεσίες κατανεμημένου DNS (διακομιστής ονόματος τομέα).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Το Cloudflare είναι ο μεγαλύτερος διακομιστής μεσολάβησης MITM (αντίστροφος διακομιστής μεσολάβησης) στον κόσμο.Το Cloudflare κατέχει πάνω από το 80% του μεριδίου αγοράς του CDN και ο αριθμός των χρηστών cloudflare αυξάνεται καθημερινά.Έχουν επεκτείνει το δίκτυό τους σε περισσότερες από 100 χώρες.Το Cloudflare εξυπηρετεί περισσότερη κίνηση στο διαδίκτυο από το συνδυασμό Twitter, Amazon, Apple, Instagram, Bing & Wikipedia.Το Cloudflare προσφέρει δωρεάν πρόγραμμα και πολλοί άνθρωποι το χρησιμοποιούν αντί να ρυθμίζουν σωστά τους διακομιστές τους.Ανταλλάσσουν το απόρρητο λόγω ευκολίας.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Το Cloudflare βρίσκεται ανάμεσα σε εσάς και τον διακομιστή προέλευσης, ενεργώντας σαν πράκτορας περιπολίας συνόρων.Δεν μπορείτε να συνδεθείτε στον προορισμό που έχετε επιλέξει.Συνδέεστε στο Cloudflare και όλες οι πληροφορίες σας αποκρυπτογραφούνται και παραδίδονται εν κινήσει. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Ο διαχειριστής προέλευσης webserver επέτρεψε στον πράκτορα - Cloudflare - να αποφασίσει ποιος μπορεί να έχει πρόσβαση στην "ιδιοκτησία ιστού" του και να ορίσει "περιορισμένη περιοχή".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Ρίξτε μια ματιά στη σωστή εικόνα.Θα νομίζετε ότι το Cloudflare αποκλείει μόνο κακούς.Θα νομίζετε ότι το Cloudflare είναι πάντα συνδεδεμένο (ποτέ μην κατεβείτε).Θα νομίζετε ότι τα legit bots και τα προγράμματα ανίχνευσης μπορούν να ευρετηριάσουν τον ιστότοπό σας.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Ωστόσο, αυτά δεν είναι καθόλου αλήθεια.Το Cloudflare αποκλείει αθώους ανθρώπους χωρίς λόγο.Το Cloudflare μπορεί να μειωθεί.Το Cloudflare αποκλείει τα νόμιμα bots.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Όπως κάθε υπηρεσία φιλοξενίας, το Cloudflare δεν είναι τέλειο.Θα δείτε αυτήν την οθόνη ακόμη και αν ο διακομιστής προέλευσης λειτουργεί καλά.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Πιστεύετε πραγματικά ότι το Cloudflare έχει 100% χρόνο λειτουργίας;Δεν έχετε ιδέα πόσες φορές το Cloudflare κατεβαίνει.Εάν το Cloudflare πέσει, ο πελάτης σας δεν μπορεί να αποκτήσει πρόσβαση στον ιστότοπό σας. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Αυτό το ονομάζεται σε σχέση με το Μεγάλο Τείχος προστασίας της Κίνας, το οποίο κάνει ανάλογη δουλειά να φιλτράρει πολλούς ανθρώπους από την προβολή περιεχομένου ιστού (δηλ. Όλοι στην ηπειρωτική Κίνα και άτομα εκτός).Ενώ την ίδια στιγμή εκείνοι που δεν επηρεάζονται για να δουν έναν δραματικά διαφορετικό ιστό, έναν ιστό χωρίς λογοκρισία, όπως μια εικόνα του "tank man" και την ιστορία των διαδηλώσεων της πλατείας Tiananmen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Το Cloudflare διαθέτει μεγάλη δύναμη.Κατά μία έννοια, ελέγχουν αυτό που τελικά βλέπει ο τελικός χρήστης.Δεν επιτρέπεται η περιήγηση στον ιστότοπο λόγω του Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Το Cloudflare μπορεί να χρησιμοποιηθεί για λογοκρισία. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Δεν μπορείτε να δείτε τον ιστότοπο cloudflared εάν χρησιμοποιείτε μικρό πρόγραμμα περιήγησης που το Cloudflare μπορεί να πιστεύει ότι είναι bot (επειδή δεν το χρησιμοποιούν πολλοί άνθρωποι). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Δεν μπορείτε να περάσετε αυτόν τον επεμβατικό έλεγχο του προγράμματος περιήγησης χωρίς να ενεργοποιήσετε τη Javascript.Αυτό είναι χάσιμο πέντε (ή περισσότερων) δευτερολέπτων της πολύτιμης ζωής σας. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Το Cloudflare αποκλείει επίσης αυτόματα τα νομικά ρομπότ / προγράμματα ανίχνευσης όπως οι πελάτες Google, Yandex, Yacy και API.Το Cloudflare παρακολουθεί ενεργά την κοινότητα "bypass cloudflare" με σκοπό να σπάσει τα νόμιμα bots έρευνας. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Το Cloudflare εμποδίζει επίσης πολλά άτομα που έχουν κακή συνδεσιμότητα στο Διαδίκτυο να έχουν πρόσβαση σε ιστότοπους που βρίσκονται πίσω από αυτό (για παράδειγμα, ενδέχεται να βρίσκονται πίσω από 7+ επίπεδα NAT ή να μοιράζονται την ίδια IP, για παράδειγμα δημόσιο Wifi), εκτός εάν επιλύσουν πολλαπλές εικόνες CAPTCHA.Σε ορισμένες περιπτώσεις, θα χρειαστούν 10 έως 30 λεπτά για να ικανοποιηθεί η Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Το 2020 το Cloudflare άλλαξε από το Recaptcha της Google σε hCaptcha καθώς η Google σκοπεύει να χρεώσει για τη χρήση του.Το Cloudflare σας είπε ότι φροντίζει το απόρρητό σας («βοηθάει στην αντιμετώπιση ενός προβλήματος απορρήτου»), αλλά αυτό είναι προφανώς ψέμα.Είναι όλα σχετικά με τα χρήματα."Το hCaptcha επιτρέπει στους ιστότοπους να κερδίζουν χρήματα που εξυπηρετούν αυτήν τη ζήτηση, ενώ εμποδίζουν bots και άλλες μορφές κατάχρησης" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Από την πλευρά του χρήστη, αυτό δεν αλλάζει πολύ. Αναγκάζεστε να το λύσετε. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Πολλοί άνθρωποι και λογισμικό εμποδίζονται από το Cloudflare κάθε μέρα. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Το Cloudflare ενοχλεί πολλούς ανθρώπους σε όλο τον κόσμο.Ρίξτε μια ματιά στη λίστα και σκεφτείτε εάν η υιοθέτηση του Cloudflare στον ιστότοπό σας είναι καλή για την εμπειρία των χρηστών. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Ποιος είναι ο σκοπός του Διαδικτύου εάν δεν μπορείτε να κάνετε αυτό που θέλετε;Τα περισσότερα άτομα που επισκέπτονται τον ιστότοπό σας θα αναζητούν μόνο άλλες σελίδες εάν δεν μπορούν να φορτώσουν μια ιστοσελίδα.Ενδέχεται να μην αποκλείετε ενεργά κανέναν επισκέπτη, αλλά το προεπιλεγμένο τείχος προστασίας του Cloudflare είναι αρκετά αυστηρό ώστε να αποκλείει πολλά άτομα. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Δεν υπάρχει τρόπος να λύσετε το captcha χωρίς να ενεργοποιήσετε το Javascript και τα Cookies.Το Cloudflare τα χρησιμοποιεί για να κάνει μια υπογραφή προγράμματος περιήγησης για να σας αναγνωρίσει.Το Cloudflare πρέπει να γνωρίζει την ταυτότητά σας για να αποφασίσει εάν είστε επιλέξιμοι για να συνεχίσετε την περιήγηση στον ιστότοπο. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Οι χρήστες Tor και οι χρήστες VPN είναι επίσης θύματα του Cloudflare.Και οι δύο λύσεις χρησιμοποιούνται από πολλούς ανθρώπους που δεν μπορούν να αντέξουν οικονομικά internet χωρίς λογοκρισία λόγω της πολιτικής χώρας / εταιρίας / δικτύου τους ή που θέλουν να προσθέσουν επιπλέον επίπεδο για την προστασία του απορρήτου τους.Το Cloudflare επιτίθεται χωρίς ντροπή σε αυτούς τους ανθρώπους, αναγκάζοντάς τους να απενεργοποιήσουν τη λύση διακομιστή μεσολάβησης. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Εάν δεν δοκιμάσατε το Tor μέχρι αυτήν τη στιγμή, σας συνιστούμε να κατεβάσετε το Tor Browser και να επισκεφτείτε τους αγαπημένους σας ιστότοπους.Σας προτείνουμε να μην συνδεθείτε στον ιστότοπο της τράπεζάς σας ή στην κυβερνητική ιστοσελίδα ή θα επισημάνουν τον λογαριασμό σας. Χρησιμοποιήστε VPN για αυτούς τους ιστότοπους. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Ίσως θέλετε να πείτε "Το Tor είναι παράνομο! Οι χρήστες είναι εγκληματίες! Το Tor είναι κακό! ". Όχι.Μπορεί να μάθατε για το Tor από την τηλεόραση, λέγοντας ότι το Tor μπορεί να χρησιμοποιηθεί για να περιηγηθείτε στο darknet και να ανταλλάξετε όπλα, ναρκωτικά ή πορνό.Ενώ η παραπάνω δήλωση είναι αλήθεια ότι υπάρχουν πολλοί ιστότοποι της αγοράς όπου μπορείτε να αγοράσετε τέτοια είδη, αυτοί οι ιστότοποι εμφανίζονται συχνά και στο clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Το Tor αναπτύχθηκε από τον Στρατό των ΗΠΑ, αλλά το τρέχον Tor αναπτύχθηκε από το έργο Tor.Υπάρχουν πολλοί άνθρωποι και οργανισμοί που χρησιμοποιούν το Tor συμπεριλαμβανομένων των μελλοντικών φίλων σας.Έτσι, εάν χρησιμοποιείτε το Cloudflare στον ιστότοπό σας, αποκλείετε πραγματικούς ανθρώπους.Θα χάσετε πιθανή φιλία και επιχειρηματική συμφωνία. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Και η υπηρεσία DNS τους, 1.1.1.1, φιλτράρει επίσης τους χρήστες από την επίσκεψη στον ιστότοπο επιστρέφοντας ψεύτικη διεύθυνση IP που ανήκει στο Cloudflare, τοπική διεύθυνση IP όπως "127.0.0.x" ή απλώς δεν επιστρέφει τίποτα. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Το Cloudflare DNS διακόπτει επίσης το διαδικτυακό λογισμικό από την εφαρμογή smartphone σε παιχνίδι υπολογιστή λόγω της ψεύτικης απάντησης DNS.Το Cloudflare DNS δεν μπορεί να υποβάλει ερώτημα σε ορισμένους ιστότοπους τραπεζών. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Και εδώ μπορεί να σκεφτείτε,<br>Δεν χρησιμοποιώ Tor ή VPN, γιατί πρέπει να με νοιάζει;<br>Εμπιστεύομαι το Cloudflare marketing, γιατί πρέπει να με νοιάζει<br>Ο ιστότοπός μου είναι https γιατί πρέπει να με νοιάζει | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Εάν επισκεφθείτε ιστότοπο που χρησιμοποιεί το Cloudflare, κοινοποιείτε τις πληροφορίες σας όχι μόνο στον κάτοχο του ιστότοπου αλλά και στο Cloudflare.Έτσι λειτουργεί ο αντίστροφος διακομιστής μεσολάβησης. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Είναι αδύνατο να αναλυθεί χωρίς να αποκρυπτογραφηθεί η κυκλοφορία TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Το Cloudflare γνωρίζει όλα τα δεδομένα σας, όπως τον ακατέργαστο κωδικό πρόσβασης. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Το Cloudbeed μπορεί να συμβεί ανά πάσα στιγμή. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Το https του Cloudflare δεν είναι ποτέ από άκρο σε άκρο. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Θέλετε πραγματικά να μοιραστείτε τα δεδομένα σας με το Cloudflare, καθώς και με την εταιρεία με 3 γράμματα; | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Το διαδικτυακό προφίλ των χρηστών του Διαδικτύου είναι ένα «προϊόν» που θέλουν να αγοράσουν η κυβέρνηση και οι μεγάλες εταιρείες τεχνολογίας. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Το Υπουργείο Εσωτερικής Ασφάλειας των ΗΠΑ είπε:<br><br>Έχετε ιδέα πόσο πολύτιμα είναι τα δεδομένα που έχετε; Υπάρχει τρόπος να μας πουλήσετε αυτά τα δεδομένα;  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Το Cloudflare προσφέρει επίσης ΔΩΡΕΑΝ υπηρεσία VPN που ονομάζεται "Cloudflare Warp".Εάν το χρησιμοποιείτε, όλες οι συνδέσεις του smartphone (ή του υπολογιστή σας) αποστέλλονται σε διακομιστές Cloudflare.Το Cloudflare μπορεί να γνωρίζει σε ποιον ιστότοπο έχετε διαβάσει, ποιο σχόλιο έχετε δημοσιεύσει, σε ποιον έχετε μιλήσει κ.λπ.Προσφέρετε εθελοντικά όλες τις πληροφορίες σας στο Cloudflare.Αν νομίζετε ότι «αστειεύεστε; Το Cloudflare είναι ασφαλές. " τότε πρέπει να μάθετε πώς λειτουργεί το VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Η Cloudflare είπε ότι η υπηρεσία VPN τους κάνει το Διαδίκτυο γρήγορο.Όμως, το VPN κάνει τη σύνδεσή σας στο Διαδίκτυο πιο αργή από την υπάρχουσα σύνδεσή σας. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Ίσως γνωρίζετε ήδη για το σκάνδαλο PRISM.Είναι αλήθεια ότι η AT&T επιτρέπει στην NSA να αντιγράφει όλα τα δεδομένα διαδικτύου για παρακολούθηση. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Ας υποθέσουμε ότι εργάζεστε στην NSA και θέλετε το προφίλ διαδικτύου κάθε πολίτη.Γνωρίζετε ότι οι περισσότεροι από αυτούς εμπιστεύονται τυφλά το Cloudflare και το χρησιμοποιούν - μόνο μια κεντρική πύλη - για τη μεσολάβηση της σύνδεσης διακομιστή εταιρείας (SSH / RDP), προσωπικού ιστότοπου, ιστότοπου συνομιλίας, ιστότοπου φόρουμ, ιστότοπου τράπεζας, ιστότοπου ασφάλισης, μηχανής αναζήτησης, μυστικού μέλους -όνο ιστότοπος, ιστότοπος δημοπρασιών, ιστότοπους αγορών, βίντεο, ιστότοπος NSFW και παράνομος ιστότοπος.Γνωρίζετε επίσης ότι χρησιμοποιούν την υπηρεσία DNS του Cloudflare ("1.1.1.1") και την υπηρεσία VPN ("Cloudflare Warp") για το "Secure! Γρηγορότερα! Καλύτερα!" εμπειρία στο Διαδίκτυο.Ο συνδυασμός τους με τη διεύθυνση IP του χρήστη, το δακτυλικό αποτύπωμα του προγράμματος περιήγησης, τα cookie και το RAY-ID θα είναι χρήσιμο για τη δημιουργία του διαδικτυακού προφίλ του στόχου. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Θέλετε τα δεδομένα τους. Τι θα κάνεις? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Το Cloudflare είναι ένα honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Δωρεάν μέλι για όλους. Συνδέθηκαν ορισμένες χορδές.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Μην χρησιμοποιείτε το Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Αποκέντρωση του Διαδικτύου.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Συνεχίστε στην επόμενη σελίδα:  "[Ηθική Cloudflare](el.ethics.md)"

---

<details>
<summary>_κάντε κλικ με_

## Δεδομένα και περισσότερες πληροφορίες
</summary>


Αυτό το αποθετήριο είναι μια λίστα ιστότοπων που βρίσκονται πίσω από το "The Great Cloudwall", αποκλείοντας τους χρήστες Tor και άλλα CDN.


**Δεδομένα**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Χρήστες Cloudflare](../cloudflare_users/)
* [Τομείς Cloudflare](../cloudflare_users/domains/)
* [Χρήστες που δεν ανήκουν στο Cloudflare CDN](../not_cloudflare/)
* [Χρήστες Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Περισσότερες πληροφορίες**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Κατεβάστε: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Το αρχικό eBook (ePUB) διαγράφηκε από το BookRix GmbH λόγω παραβίασης πνευματικών δικαιωμάτων υλικού CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Το εισιτήριο βανδαλίστηκε πολλές φορές.
  * [Διαγράφηκε από το έργο Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Βλέπε εισιτήριο 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Τελευταίο εισιτήριο αρχειοθέτησης 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_κάντε κλικ με_

## Τι μπορείς να κάνεις?
</summary>

* [Διαβάστε τη λίστα των προτεινόμενων ενεργειών και μοιραστείτε τη με τους φίλους σας.](../ACTION.md)

* [Διαβάστε τη φωνή του άλλου χρήστη και γράψτε τις σκέψεις σας.](../PEOPLE.md)

* Αναζητήστε κάτι: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Ενημερώστε τη λίστα τομέων: [Λίστα λιστών](../INSTRUCTION.md).

* [Προσθέστε το Cloudflare ή το σχετικό με το έργο συμβάν στο ιστορικό.](../HISTORY.md)

* [Δοκιμάστε & γράψτε νέο εργαλείο / σενάριο.](../tool/)

* [Εδώ μπορείτε να διαβάσετε μερικά PDF / ePUB.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Σχετικά με ψεύτικους λογαριασμούς

Το Crimeflare ξέρει για την ύπαρξη πλαστών λογαριασμών που πλαστογραφούν τα επίσημα κανάλια μας, είτε πρόκειται για Twitter, Facebook, Patreon, OpenCollective, Villages κ.λπ.
**Δεν ζητάμε ποτέ το email σας.
Ποτέ δεν ρωτάμε το όνομά σας.
Ποτέ δεν ρωτάμε την ταυτότητά σας.
Δεν ρωτάμε ποτέ την τοποθεσία σας.
Δεν ζητάμε ποτέ τη δωρεά σας.
Δεν ζητάμε ποτέ την κριτική σας.
Ποτέ δεν σας ζητάμε να παρακολουθείτε τα μέσα κοινωνικής δικτύωσης.
Δεν ρωτάμε ποτέ τα κοινωνικά σας μέσα.**

# ΜΗΝ ΕΜΠΙΣΤΕΥΤΕΤΕ ΛΟΓΑΡΙΑΣΜΟΥ ΨΗΦΙΑΚΩΝ.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)