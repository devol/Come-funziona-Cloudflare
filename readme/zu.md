# I-Cloudwall enhle


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Misa i-Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  “I-Cloud Cloudwall” yinkampani iCloudflare Inc., inkampani yase-U.S.Inikezela ngezinsizakalo ze-CDN (inethiwekhi yokulethwa kokuqukethwe), ukuncishiswa kwe-DDoS, ukuphepha kwe-inthanethi, kanye nezinsizakalo ze-DNS (zesizinda segama lesizinda) ezisatshalaliswa.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  I-Cloudflare yommeleli omkhulu we-MITM emhlabeni (ummeleli ophambukayo).I-Cloudflare inabanini abangaphezulu kwama-80% wezabelo zemakethe ze-CDN kanye nenani labasebenzisi befu.Bangezelele inethiwekhi yabo emazweni angaphezu kwe-100.I-Cloudflare isebenza nge-web traffic kakhulu kune-Twitter, i-Amazon, i-Apple, i-Instagram, i-Bing ne-Wikipedia ehlanganisiwe.I-Cloudflare inikeza uhlelo lwamahhala futhi abantu abaningi bayayisebenzisa esikhundleni sokumisa amaseva abo kahle.Babedayisa ubumfihlo ngokungcono.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  I-Cloudflare ihlala phakathi kwakho nomsuka webserver, esebenza njenge-ejenti yokuhamba umngcele.Awukwazi ukuxhuma endaweni oyikhethile.Uxhuma ku-Cloudflare futhi yonke imininingwane yakho iyathuliswa futhi idluliselwe endibeni. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Umqondisi we-webserver odabuka wavumela i-ejenti - iCloudflare - ukuthi inqume ukuthi ngubani ongangena "empahleni yabo yewebhu" futhi ichaze "indawo ekhawulelwe".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Bheka isithombe esifanele.Uzocabanga ukuthi i-Cloudflare block kuphela abantu ababi.Uzocabanga ukuthi iCloudflare ihlala iku-inthanethi (ungaze wehle).Uzocabanga ukuthi ama-bots bots nabakhandi bangakhombisa iwebhusayithi yakho.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Kodwa-ke lokho akulona iqiniso nakancane.I-Cloudflare ivimba abantu abangenacala ngaphandle kwesizathu.I-Cloudflare ingaya phansi.I-Cloudflare ivimba ama-bots bots.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Njenganoma iyiphi insizakalo yokusingathwa, iCloudflare ayiphelele.Uzobona lesi sikrini noma ngabe iseva yomsuka isebenza kahle.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Ngabe ucabanga ukuthi i-Cloudflare ine-100% isikhathi esengeziwe?Awunangqondo ukuthi iCloudflare yehla kangaki.Uma iCloudflare yehla ikhasimende lakho alikwazi ukungena kuwebhusayithi yakho. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Lokhu kubizwa ngokuthi kubhekiselwa kwi-Great Firewall yaseChina eyenza umsebenzi wokuqhafaza abantu abaningi bangakuboni okuqukethwe kwewebhu (okusho wonke umuntu ezweni lase China kanye nabantu abangaphandle).Ngenkathi ngasikhathi sinye labo abangazange bathinteke ukubona iwebhu ehlukile kakhulu, iwebhu engenakho ukucabanga okufana nesithombe se- “tank man” nomlando wokubhikishelwa “kweTiananmen Square”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  I-Cloudflare inamandla amakhulu.Ngomqondo othile, balawula lokho okubonwa ngumsebenzisi ekugcineni.Uvinjelwe ukuphequlula iwebhusayithi ngenxa ye-Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  I-Cloudflare ingasetjenziselwa ukucubungula. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Awukwazi ukubuka iwebhusayithi efakwe amafu uma usebenzisa isiphequluli esincane i-Cloudflare engacabanga ukuthi yi-bot (ngoba ababaningi abantu abayisebenzisayo). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Awukwazi ukudlulisa lokhu "isiphequluli" esingahlaseli ngaphandle kokunika amandla iJavascript.Lokhu ukuchitha imizuzwana emihlanu (noma ngaphezulu) yempilo yakho ebalulekile. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  I-Cloudflare futhi ivimba ngokuzenzakalelayo amarobhothi / abakhaseli njenge-Google, Yandex, Yacy, kanye namakhasimende e-API.I-Cloudflare iqapha ngentshiseko umphakathi “odlula i-Cloudflare” umphakathi ngenhloso yokuphula umthetho wokucwaninga ngokusemthethweni. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  I-Cloudflare ngokufanayo ivimbela abantu abaningi abanokuxhumana okungaxhunyiwe kwe-inthanethi ukufinyelela amawebhusayithi ngemuva kwayo (ngokwesibonelo, bangaba ngemuva kwezendlalelo ezingama-7+ ze-NAT noma babelane nge-IP efanayo, ngokwesibonelo i-Wifi yomphakathi) ngaphandle kokuthi baxazulule ama-CAPTCHA amaningi wezithombe.Kwezinye izimo, lokhu kuzothatha imizuzu eyi-10 kuye kwengama-30 ukwanelisa iGoogle. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Ngonyaka ka-2020 iCloudflare ishintshe isuka kwiRecaptcha yakwaGoogle yaya ku-hCaptcha njengoba iGoogle ihlose ukuyokhokhisa ukusetshenziswa kwayo.I-Cloudflare ikutshele ukuthi bayayikhathalela ubumfihlo bakho ("kuyasiza ukulungisa ukukhathazeka kobumfihlo") kepha ngokusobala kungamanga.Imayelana nemali."I-hCaptcha ivumela amawebhusayithi ukwenza imali ukwenza lokhu ngenkathi kuvimba ama-bots nezinye izindlela zokuhlukumezeka" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Ngombono womsebenzisi, lokhu akushintshi kakhulu. Uphoqelelwa ukuba uyixazulule. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Abantu abaningi nesoftware bavinjwa yi-Cloudflare nsuku zonke. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  I-Cloudflare icasule abantu abaningi emhlabeni jikelele.Bheka uhlu bese ucabanga ukuthi ukwamukela i-Cloudflare kusayithi lakho kulungile kulwazi lomsebenzisi. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Yini inhloso ye-inthanethi uma ungakwazi ukwenza lokho okufunayo?Abantu abaningi abavakashela iwebhusayithi yakho bazomane babheke amanye amakhasi uma bengakwazi ukulayisha ikhasi lewebhu.Ungahle ungavimbeli ngenkani noma yiziphi izivakashi, kepha isibhengezo somlilo esizenzakalelayo se-Cloudflare siqine ngokwanele ukuvimba abantu abaningi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Ayikho indlela yokuxazulula i-Captcha ngaphandle kokunika amandla iJavascript kanye namakhukhi.I-Cloudflare iyisebenzisela ukwenza isiginesha yesiphequluli ukukubona.I-Cloudflare idinga ukwazi ubunikazi bakho ukunquma ukuthi ngabe u-eligeble ukuze uqhubeke ukuphequlula isiza. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Abasebenzisi be-Tor nabasebenzisi be-VPN nabo bayisisulu se-Cloudflare.Zombili lezi zixazululo zisetshenziswa ngabantu abaningi abangakwazi ukukhokhela i-inthanethi engafundisiwe ngenxa yenqubomgomo yezwe / yabo yenhlangano / yenethiwekhi noma abafuna ukwengeza ungqimba olungeziwe ukuvikela ubumfihlo babo.I-Cloudflare ihlasela ngokungenamahloni labo bantu, ibaphoqa ukuthi bacishe isixazululo sommeleli. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Uma ungazange uzame i-Tor kuze kube manje, sikukhuthaza ukulanda i-Tor Browser futhi uvakashele amawebhusayithi akho owathandayo.Siphakamisa ukuthi ungangeni ngemvume kuwebhusayithi yakho yasebhange noma ekhasini lewebhu likahulumeni noma bazomaka i-akhawunti yakho. Sebenzisa i-VPN kulawo mawebhusayithi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Ungafuna ukusho ukuthi “iTor ayikho emthethweni! Abasebenzisi be-Tor bayizigebengu! I-Tor is bad! ". Cha.Ungase ufunde ngeTor kumabonakude, uthi iTor ingasetshenziswa ukubhekisisa izibhamu ezimnyama nezokuhweba, izidakamizwa noma i-chid porn.Yize isitatimende esingenhla siliqiniso ukuthi ziningi iwebhusayithi zemakethe lapho ungathenga khona izinto ezinjalo, lawo masayithi nawo avela naku-clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  I-Tor yathuthukiswa Amasosha ase-US, kepha iTor yakhona yathuthukiswa yiphrojekthi yeTor.Kunabantu abaningi nezinhlangano ezisebenzisa iTor kufaka phakathi abangane bakho bakusasa.Ngakho-ke, uma usebenzisa iCloudflare kuwebhusayithi yakho uvimba abantu bangempela.Uzolahlekelwa ubungani obunokwenzeka kanye nesivumelwano sebhizinisi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Futhi insizakalo yabo ye-DNS, engu-1.1.1.1, iphinda ihlungise abasebenzisi ukuthi bavakashele iwebhusayithi ngokubuyisa ikheli le-IP elingelaphethwe yi-Cloudflare, IPh yasendaweni efana ne- “127.0.0.x”, noma ibuye nje. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  I-Cloudflare DNS ibuye igqekeze isoftware eku-inthanethi kusuka kuhlelo lokusebenza lwe-smartphone iye kumdlalo wekhompyutha ngenxa yempendulo yabo engamanga ye-DNS.I-Cloudflare DNS ayikwazi ukubuza amanye amawebhusayithi asebhange. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Futhi lapha ungacabanga,<br>Angisebenzisi iTor noma i-VPN, kungani kufanele nginakekele?<br>Ngiyethemba ukumaketha kwe-Cloudflare, kungani kufanele nginakekele<br>Iwebhusayithi yami yi-https kungani kufanele nginakekele | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Uma uvakashela iwebhusayithi esebenzisa i-Cloudflare, wabelana ngemininingwane yakho hhayi kumnikazi wewebhusayithi kuphela kepha futhi ne-Cloudflare.Le ndlela isebenza kanjena isebenza kanjani. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Akunakwenzeka ukuhlaziya ngaphandle kokubeka isiminyaminya i-TLS traffic. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  I-Cloudflare yazi yonke imininingwane yakho efana ne-password eluhlaza. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  I-Cloudbeed ingenzeka noma nini. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  I-Cloudflare's https ayikaze iphele-ukuphela. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Ngabe uyafuna ngempela ukwabelana ngemininingwane yakho ne-Cloudflare, kanye ne-ejensi eyi-3? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Iphrofayili yomsebenzisi e-inthanethi “umkhiqizo” uhulumeni nezinkampani ezinkulu ze-tech afuna ukuzithenga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  UMnyango Wezokuphepha KwaseMelika uthe:<br><br>Ngabe kukhona umbono wokuthi idatha yakho onayo ibaluleke kangakanani? Ingabe ikhona indlela ongasithengisa ngayo leyo datha?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  I-Cloudflare iphinde inikeze insizakalo ye-MAHHALA ye-VPN ebizwa nge- "Cloudflare Warp".Uma uyisebenzisa, konke ukuxhumana kwakho kwe-smartphone (noma ikhompyutha yakho) kuthunyelwa kumaseva we-Cloudflare.I-Cloudflare iyakwazi ukuthi iyiphi iwebhusayithi oyifundile, yikuphi okubhalile oyithumele, okhulume naye ngubani, njll.Ngokuzithandela unikezela yonke imininingwane yakho ku-Cloudflare.Uma ucabanga ukuthi "Uyahlekisa? I-Cloudflare iphephile. ” lapho-ke kufanele ufunde ukuthi i-VPN isebenza kanjani. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  I-Cloudflare ithe inkonzo yabo ye-VPN yenza i-inthanethi yakho isheshe.Kodwa i-VPN yenza ukuxhumana kwakho kwe-inthanethi kube kancane kunokuxhuma kwakho okukhona. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Kungenzeka ukuthi usuvele uyazi ngesehlakalo se-PRISM.Kuliqiniso ukuthi i-AT & T ivumela i-NSA ukuthi ikopishe yonke idatha ye-inthanethi ukuze ihlolwe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Masithi usebenza eNSA, futhi ufuna yonke imininingwane yendawo eyisakhamuzi.Uyazi ukuthi iningi labo lithembela ngokungagxili ku-Cloudflare futhi bayisebenzisa - isango elilodwa elihlanganisiwe - ukubhekela ukuxhumeka kweseva yenkampani yabo (i-SSH / RDP), iwebhusayithi yomuntu siqu, iwebhusayithi yokuxoxa, iwebhusayithi yeforamu, iwebhusayithi yebhange, iwebhusayithi yomshuwalense, injini yokusesha, ilungu eliyimfihlo Iwebhusayithi -yodwa, iwebhusayithi ye-auction, ukuyothenga, iwebhusayithi yevidiyo, iwebhusayithi yeNSFW, newebhusayithi engekho emthethweni.Uyazi futhi ukuthi basebenzisa insizakalo ye-DNS ye-Cloudflare ("1.1.1.1") nensizakalo ye-VPN ("Cloudflare Warp") ye "Londekile! Ngokushesha! Kungcono! ” isipiliyoni se-inthanethi.Ukubahlanganisa nekheli le-IP lomsebenzisi, isithonjana seminwe yesiphequluli, amakhukhi kanye ne-RAY-ID kuzosiza ekwakheni iphrofayili ye-inthanethi yethagethi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Ufuna idatha yabo. Uzokwenzani? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **I-Cloudflare iyi-honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Uju lwamahhala kuwo wonke umuntu. Imicu ethile inamathiselwe.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Ungasebenzisi i-Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Nika i-inthanethi i-intanethi.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Sicela uqhubeke ekhasini elilandelayo:  "[Izimiso Zokuziphatha ze-Cloudflare](zu.ethics.md)"

---

<details>
<summary>_ngichaze_

## Imininingwane Nolwazi Olunye
</summary>


Lokhu kugcina uhlu lwamawebhusayithi angemuva kwe- "The Cloudwall" enkulu, evimba abasebenzisi be-Tor namanye ama-CDN.


**Idatha**
* [I-Cloudflare Inc.](../cloudflare_inc/)
* [Abasebenzisi be-Cloudflare](../cloudflare_users/)
* [Izizinda ze-Cloudflare](../cloudflare_users/domains/)
* [Abasebenzisi be-CDN abanga-Cloudflare](../not_cloudflare/)
* [Abasebenzisi be-Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Imininingwane Ngini**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Landa: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * I-eBook yasekuqaleni (ePUB) isuswe yiBhukuRix GmbH ngenxa yokwephulwa kwamalungelo obunikazi bezinto ze-CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Ithikithi lonakaliswa kaningi.
  * [Kususwe yi-Tor Pro.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Bheka ithikithi 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Ithikithi lokugcina lokulondolozwa elingu-24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_ngichaze_

## Yini ongayenza?
</summary>

* [Funda uhlu lwethu lwezenzo ezinconyiwe bese wabelana ngalo nabangani bakho.](../ACTION.md)

* [Funda izwi lomunye umsebenzisi bese ubhala imicabango yakho.](../PEOPLE.md)

* Sesha okuthile: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Vuselela uhlu lwesizinda: [Imiyalo yohlu](../INSTRUCTION.md).

* [Faka i-Cloudflare noma umcimbi ohlobene neprojekthi emlandweni.](../HISTORY.md)

* [Zama futhi ubhale iThuluzi / Iskripthi esisha.](../tool/)

* [Nayi iPDP / ePUB okufanele uyifunde.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Mayelana nama-akhawuntimbumbulu

Ama-Crimeflare ayazi ngobukhona bama-akhawuntimbumbulu enza iziteshi zethu ezisemthethweni, kungaba yi-Twitter, i-Facebook, i-Patreon, i-OpenCollective, i-villages njll.
**Asikaze sibuze i-imeyili yakho.
Asikaze sibuze igama lakho.
Asikaze sibuze umazisi wakho.
Asikaze sibuze indawo yakho.
Asilokothi sicela umnikelo wakho.
Asikaze sibuze isibuyekezo sakho.
Asikaze sikucele ukuthi ulandele ezinkundleni zokuxhumana.
Asikaze sibuze imidiya yakho yezenhlalo.**

# UNGAQINISI UKUFINYELELA IMIHLA.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)