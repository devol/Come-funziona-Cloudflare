# Great Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Cloudflare токтот


|  🖹  |  🖼 |
| --- | --- |
|  "Great Cloudwall" бул Cloudflare Inc., АКШ компаниясы.Ал CDN (контент жеткирүү тармагы) кызматтарын, DDoS жумшартуу, Интернет коопсуздугу жана бөлүштүрүлгөн DNS (домендик аттын сервери) кызматтарын көрсөтөт.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare - дүйнөдөгү эң чоң MITM прокси (тескери прокси).Cloudflare CDN рыногунун 80% ашуун үлүшүнө ээ жана булуттарды колдонуучулардын саны күн сайын өсүүдө.Алар өз тармактарын 100дөн ашуун өлкөлөргө жайышты.Cloudflare Twitter, Amazon, Apple, Instagram, Bing & Wikipedia бириккенине караганда көбүрөөк веб-трафикти тейлейт.Cloudflare акысыз план сунуштайт жана көптөр өз серверлерин туура конфигурациялоонун ордуна, аны колдонушат.Алар купуялуулукту ыңгайлуулуктан жогору сатышкан.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare чек ара кызматынын агенти сыяктуу иш алып барып, веб-сервердин ортосунда отурат.Сиз тандаган көздөгөн жериңизге туташа албай жатасыз.Сиз Cloudflare'ге туташып жатасыз жана бардык маалыматыңыз шифрленип, учууга тапшырылууда. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Түпнуска веб-сервердин администратору - Cloudflare агентине "веб мүлкүнө" ким кире алаарын жана "чектелген аймакты" аныктоого мүмкүнчүлүк берген.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Туура сүрөттү карап көрүңүз.Cloudflare гана жаман балдар бөгөт деп ойлойсуз.Cloudflare ар дайым онлайн болот деп ойлойсуз (эч качан төмөн түшпөйт).Сиз мыйзамдуу боттор жана жөрмөлөгүч вебсайтыңызды индекстей алат деп ойлойсуз.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Бирок, бул такыр туура эмес.Cloudflare күнөөсүз адамдарды эч кандай себепсиз тосуп жатат.Булуттар түшүп кетиши мүмкүн.Cloudflare легалдуу ботторду блоктайт.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Бардык хостинг кызматтары сыяктуу эле, Cloudflare да мыкты эмес.Бул экранды баштапкы сервер жакшы иштеп жатса дагы көрө аласыз.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Чындыгында Cloudflare 100% иштөө убактысы бар деп ойлойсузбу?Cloudflare канча жолу төмөндөгөнүн билбейсиң.Cloudflare азайып кетсе, кардар вебсайтыңызга кире албайт. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Муну Кытайдын Улуу Брандмауэрине шилтеме кылып, көптөгөн адамдарды веб-мазмунун көрүүдөн четтетүү боюнча иш алып барган (б.а. материктик Кытайда жана анын чегиндеги адамдар).Ошол эле учурда, кескин айырмаланган желе, "танк адамынын" сүрөтү жана "Тяньаньмэнь аянтындагы нааразычылык акциясынын тарыхы" сыяктуу цензурасыз Интернетти көрүүгө эч ким жабыр тарткан жок. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare зор күчкө ээ.Кандайдыр бир мааниде, алар акыры колдонуучунун эмнени көрөрүн көзөмөлдөйт.Cloudflare'ден улам веб-сайтты карап чыгууга тыюу салынат. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare цензура үчүн колдонулушу мүмкүн. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Cloudflare аны бот деп ойлошу мүмкүн болгон анча-мынча браузерди колдонуп жатсаңыз, булутсуз веб-сайтты көрө албайсыз (анткени көп адамдар аны колдонушпайт). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Javascriptти иштетпестен, бул "браузерди текшерүүдөн" өтө албайсыз.Бул сиздин баалуу жашооңуздун беш (же андан көп) секунданты ысырап кылуу. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare ошондой эле Google, Yandex, Yacy жана API кардарлары сыяктуу мыйзамдуу роботторду / жөрмөлөгүчтөрдү автоматтык түрдө бөгөттөйт.Cloudflare мыйзамдуу изилдөө ботторун талкалоо ниетинде "булуттарды чагып өтүү" коомчулугун активдүү көзөмөлдөп турат. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare да ушундай эле интернет байланышы начар адамдардын көпчүлүгүнүн CAPTCHA сүрөттөрүн чечмейинче, анын артындагы веб-сайттарга кирүүсүнө жол бербейт (мисалы, алар 7+ катмардан артта калышы же бир эле IP, мисалы, коомдук Wifi).Айрым учурларда, Googleны канааттандыруу үчүн 10-30 мүнөт кетет. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020-жылы Cloudflare Google-дун Recaptcha-дан hCaptcha-га которулган, анткени Google аны пайдалангандыгы үчүн акы төлөйт.Cloudflare сиздин купуялыгыңызга кам көрөрүн айтты ("бул купуялык маселесин чечүүгө жардам берет"), бирок бул жалган.Баары акча жөнүндө."HCaptcha, ботторду жана башка кыянаттыктын түрлөрүн бөгөп, веб-сайттарга ушул талапка жооп берген акча табууга мүмкүнчүлүк берет" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Колдонуучунун көз карашы боюнча, бул анчалык деле өзгөрүлбөйт. Сиз аны чечүүгө аргасыз болуп жатасыз. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Күн сайын көптөгөн адамдар жана программалар Cloudflare тарабынан тосулуп жатат. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare дүйнө жүзү боюнча көптөгөн адамдарды кыжырдантат.Тизмени карап көрүңүз жана Cloudflare программасын колдонуучу тажрыйбасы үчүн жакшы деп ойлойсуз. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Эгер сиз каалаган нерсени жасай албасаңыз, анда интернеттин максаты эмнеде?Вебсайтыңызга кирген адамдардын көпчүлүгү веб-баракчаны жүктөй албаса, анда башка баракчаларды издей баштайт.Сиз конокторду жигердүү бөгөттөй албай жаткандырсыз, бирок Cloudflare демейки брандмауэр көптөгөн адамдарды бөгөт коюуга жетиштүү. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Javascript жана Cookies файлдарын иштетпестен, капчыктарды чечүү мүмкүн эмес.Cloudflare сизди таануу үчүн браузерден кол коет.Cloudflare сиз сайтты карап чыгууну улантууга мүмкүнчүлүгүңүз бар-жогун аныктоо үчүн сиздин инсандыгыңызды билиши керек. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor колдонуучулары жана VPN колдонуучулары дагы Cloudflare жабыр тартышат.Бул эки чечим тең өз өлкөсүнүн / корпорациясынын / тармактык саясатына байланыштуу сансыз интернетти сатып ала албаган же купуялуулугун коргоо үчүн кошумча катмар кошууну каалаган көптөгөн адамдар тарабынан колдонулат.Cloudflare бул адамдарга уятсыздык менен кол салып, алардын прокси чечимин өчүрүүгө аргасыз кылат. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Эгер ушул убакка чейин Tor аракетин көрбөсөңүз, Tor Браузерин жүктөп алып, сүйүктүү веб-сайттарыңызга кириңиз.Сиздин банк веб-сайтыңызга же өкмөттүн веб-баракчасына кирбөөнү сунуштайбыз, болбосо алар сиздин эсебиңизге желекчелерин коет. Ошол веб-сайттар үчүн VPN колдонуңуз. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  "Tor мыйзамсыз! Tor колдонуучулары кылмыштуу! Tor жаман! ". Жок.Tor жөнүндө теледен билгендиктен, Торду кара тор жана соода куралдары, баңги заттар же балдар порносун карап чыгуу үчүн колдонсо болот.Жогорудагы айтылгандай, мындай буюмдарды сатып алууга мүмкүн болгон базардын көптөгөн веб-сайттары бар экендиги чын, ошол сайттар көбүнчө clearnetте пайда болот.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor долбоорун АКШ армиясы иштеп чыккан, бирок азыркы Tor долбоору Tor долбоору тарабынан иштелип чыккан.Tor колдонгон көптөгөн адамдар жана уюмдар бар, алардын ичинде сиздин келечектеги досторуңуз да бар.Ошентип, сиз веб-сайтыңыздагы Cloudflare программасын колдонуп жатсаңыз, сиз чыныгы адамдарга тоскоолдук кылып жатасыз.Потенциалдуу достуктан жана ишкердик мамилелерден айрыласыз. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Алардын DNS кызматы, 1.1.1.1, ошондой эле Cloudflare'ге таандык жасалма IP дарегин, "127.0.0.x" сыяктуу жергиликтүү IP хостун кайтарып, жөн гана эч нерсе кайтарбастан колдонуучулардын веб-сайтка кирүүсүн чыпкалайт. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS жасалма DNS жообунан улам смартфондун колдонмосунан компьютердик оюнга чейин онлайн программасын бузат.Cloudflare DNS айрым банк вебсайттарын сурай албайт. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Бул жерде сиз ойлошуңуз мүмкүн,<br>Мен Tor же VPN колдонбойм, эмне үчүн мага кам көрүшүм керек?<br>Мен Cloudflare маркетингине ишенем, эмне үчүн кам көрүшүм керек<br>Менин веб-сайтыңыз - https, мен эмне үчүн кам көрүшүм керек | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Эгерде сиз Cloudflare колдонулган веб-сайтка кирсеңиз, анда маалыматты веб-сайттын ээсине гана эмес, Cloudflare-га да бөлүшүп жатасыз.Тескери прокси ушундайча иштейт. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  TLS трафикти шифрленбестен анализдөө мүмкүн эмес. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare сырдык сырсөз сыяктуу бардык маалыматыңызды билет. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Булут өсүшү каалаган убакта болушу мүмкүн. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare's https эч качан аягына чейин чыкпайт. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Cloudflare, ошондой эле 3 тамгадан турган агенттик менен бөлүшкүңүз келеби? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Интернеттеги колдонуучунун онлайн профили - бул өкмөт жана ири технологиялык компаниялар сатып алгысы келген "өнүм". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Бул тууралуу АКШнын Улуттук коопсуздук департаменти билдирди:<br><br>Сизде бар маалыматтар канчалык баалуу экендиги жөнүндө ойуңуз барбы? Бизге ошол маалыматты сата турган кандайдыр бир жол барбы?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare ошондой эле "Cloudflare Warp" деп аталган АКЫСЫЗ VPN кызматын сунуш кылат.Эгер сиз аны колдонсоңуз, смартфонуңуздун (же компьютериңиздин) туташуулары Cloudflare серверлерине жөнөтүлөт.Cloudflare сиз кайсы вебсайтты окугандыгыңызды, кандай комментарий жазганыңызды, ким менен сүйлөшкөнүңүздү ж.б. билиши мүмкүн.Сиз Cloudflareге өзүңүздүн бардык маалыматыңызды ыктыярдуу түрдө берип жатасыз.Эгер сиз "тамашалап жатасызбы? Булуттар коопсуз ». анда VPN кантип иштээрин билишиңиз керек. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare алардын VPN кызматы интернетти тез жасайт деди.Бирок VPN Интернет туташууңузду учурдагы туташууга караганда жайыраак кылат. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  PRISM жаңжалы жөнүндө мурунтан эле билишиңиз мүмкүн.Чындыгында, AT&T NSAга көзөмөл жүргүзүү үчүн бардык интернет маалыматтарын көчүрүүгө мүмкүнчүлүк берет. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  NSAда иштеп жатасыз дейли жана ар бир жарандын интернеттеги профилин каалайсыз.Алардын көпчүлүгү Cloudflare-ге ишенимсиз ишенишет жана аны колдонушат - бир гана борборлоштурулган шлюз - өз компаниясынын сервердик туташуусун (SSH / RDP), жеке веб-сайтты, чат веб-сайтын, форум веб-сайтын, банк веб-сайтын, камсыздандыруу веб-сайтын, издөө тутумун, жашыруун мүчөнү прокси кылуу үчүн. - бир гана веб-сайт, аукциондун веб-сайты, соода, видео вебсайт, NSFW веб-сайты жана мыйзамсыз веб-сайт.Ошондой эле алар Cloudflare'нин DNS кызматын ("1.1.1.1") жана VPN кызматын ("Cloudflare Warp") "Коопсуз! Тезирээк! Жакшы! ” интернет тажрыйбасы.Колдонуучунун IP дареги, браузердин манжа изи, кукилер жана RAY-ID менен айкалыштыруу максаттуу онлайн профилин түзүүдө пайдалуу болот. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Сиз алардын маалыматын каалайсыз. Сен эмне кыласың? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare - бал тору.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Баарына бекер бал. Кээ бир кылдар тиркелет.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Cloudflare колдонбоңуз.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Интернеттен ажыратуу.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Сураныч, кийинки бетке улантуу:  "[Cloudflare Ethics](ky.ethics.md)"

---

<details>
<summary>_мени бас_

## Маалыматтар жана кошумча маалыматтар
</summary>


Бул репозиторий - бул "Great Cloudwall" артында турган Tor колдонуучуларын жана башка CDNлерди бөгөттөгөн веб-сайттардын тизмеси.


**Маалыматтар**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare Users](../cloudflare_users/)
* [Cloudflare Domains](../cloudflare_users/domains/)
* [Cloudflare эмес CDN колдонуучулары](../not_cloudflare/)
* [Анти-Тор колдонуучулары](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Көбүрөөк маалымат**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Жүктөө: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Түпнуска электрондук китеп (ePUB) CCR материалынын автордук укугун бузгандыктан BookRix GmbH тарабынан жок кылынган
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Билет ушунча жолу бузулган.
  * [Төр долбоору тарабынан жок кылынды.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [34175 билетин караңыз.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Акыркы архивдик билет 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_мени бас_

## Сиз эмне кыла аласыз?
</summary>

* [Сунушталган аракеттердин тизмесин окуп, досторуңуз менен бөлүшүңүз.](../ACTION.md)

* [Башка колдонуучунун үнүн окуп, ойлоруңузду жазыңыз.](../PEOPLE.md)

* Бир нерсе изде: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Домен тизмесин жаңыртыңыз: [Көрсөтмөлөрдүн тизмеси](../INSTRUCTION.md).

* [Cloudflare же долбоорго байланыштуу окуяны тарыхка кошуңуз.](../HISTORY.md)

* [Жаңы Курал / Скриптти байкап көрүңүз.](../tool/)

* [Бул жерде окуу үчүн бир нече PDF / ePUB бар.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Жасалма эсептер жөнүндө

Твиттер, Фейсбук, Патреон, OpenCollective, Айылдар ж.б.у.с. биздин расмий каналдардын атын жамынган жасалма аккаунттардын бар экендигин билебиз.
**Электрондук почтаңызды эч качан сурабайбыз.
Биз сиздин атыңызды эч качан сурабайбыз.
Биз эч качан сиздин инсандыгыңызды сурабайбыз.
Сиздин жайгашкан жериңизди эч качан сурабайбыз.
Сиздин тартууңузду эч качан сурабайбыз.
Биз сиздин карап эч качан сурабайбыз.
Сизден эч качан социалдык медианы карманбаңыз.
Биз сиздин социалдык медиаңыздан эч качан сурабайбыз.**

# ЖАЛПЫ ЭСЕПТЕРГЕ ИШЕНҮҮ КЕРЕК.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)