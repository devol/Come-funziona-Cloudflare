# Te Papahouru Nui


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Kati te Kapahaka


|  🖹  |  🖼 |
| --- | --- |
|  "Ko te Maama Matapihi" ko Cloudflare Inc., te kamupene U.S.Kei te whakaratohia nga ratonga CDN (whatunga tuku ihirangi), whakaheke DDoS, haumaru Ipurangi, me te tohatoha ratonga DNS (ingoa rohe).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Ko Cloudflare te kaiwhakawhiwhi MITM nui rawa atu o te ao (takawaenga hurihuri).Ko te Cloudflare te nuinga o te 80% o te waahanga o te maakete CDN me te maha o nga kaiwhakamahi kapua e piki haere ana i ia ra.Kua piki ake o raatau hononga ki te 100 neke atu o nga whenua.Kei te mahi a Cloudflare mo te nuinga atu o te waka tukutuku i te Twitter, Amazon, Apple, Instagram, Bing & Wikipedia.Kei te tuku Cloudflare te mahere koreutu me te maha o nga tangata e whakamahi ana hei utu o te whirihora i o ratau kaitara.I hokohokohia e ratau nga manaakitanga mo te waatea.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Kei te noho a Cloudflare ki waenganui i a koe me te kaihautu tukutuku, e rite ana ki te kaihoko taraiwa.Kāore e taea e koe te hono atu ki tō ūnga i kōwhiri.Kei te hono atu koe ki Cloudflare me ana korero katoa kei te hoatuhia me te tuku mai i runga i te rere. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Na te kaiwhakahaere kaiwhakahaere paetukutuku i whakaaehia te kaihoko - Cloudflare - ki te whakatau ko wai te uru atu ki a raatau "rawa paetukutuku" me te tautuhi i te "rohe aukati".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Tirohia te ahua tika.Ka whakaaro koe ko nga Cloudflare poraka he hunga kino anake.Ka whakaaro koe ko Cloudflare i te ipurangi i nga wa katoa (kaua e heke iho).Ka whakaaro koe ka taea e nga kaikorero me nga kaiwhaiwhai te tautuhi i to paetukutuku.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Heoi, kaore era i te pono.Kei te aukati te Cloudflare i nga tangata harakore kaore he take.Ka taea e Cloudflare te heke.Ka piki a Cloudflare nga pire tika.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  He rite ki tetahi ratonga manaaki, kaore a Cloudflare i tino.Ka kite koe i tenei mata tae noa ki te mea kei te pai te mahi o te tūmau takenga.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Ki tou whakaaro he 100% te wa a Cloudflare?Kaore koe e mohio ki te tini o nga wa ka heke a Cloudflare.Mena ka heke iho a Cloudflare kaore e taea e to kaihoko te uru ki to paetukutuku. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Kei te kiia tenei mo te Pouahi Nui a Haina e mahi ana i te mahi whakarite ki te tarai i nga taangata maha kia kite i nga ihirangi paetukutuku (ara, ko nga tangata katoa kei te tuawhenua o Haina me nga taangata i waho).Ahakoa i te wa ano kaore o nga tangata kaore i pa ki te kite i tetahi momo paetukutuku rereke, he paetukutuku koreutu penei i te ahua o te "taane tank" me te hitori o "Tiananmen Square mautohe". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Ko te Cloudflare te mana tino nui.Ko te tikanga, ka whakahaerehia e raatau nga mea kua kitea e te kaiwhakamahi mutunga.Kaore koe e arai i te tirotiro i te paetukutuku na Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Ka taea te whakamahi Cloudflare hei tātai. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Kaore e taea e koe te tiro ki te paetukutuku kapua e pa ana ki te whakamahi i te maramatanga iti ka whakaaro pea a Cloudflare he pota (na te mea kaore i warea nga tangata e whakamahi ana). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Kaore e taea e koe te tuku i tenei "tirotiro tirotiro" tirotiro me te kore e whakarango i te Javascript.He waatea tenei mo te rima (neke atu ranei) hēkona o to koiora ora. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Ka kapi katoa a Cloudflare i nga robots whiwhinga / piripiri penei i nga kaihoko a Google, Yandex, Yacy, me te API.Kei te kaha te aro turuki a Cloudflare ki te hapori "paarua kapua" me te hiahia kia pakaru nga pika rangahau tika. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Ma te Cloudflare e aukati i te tini o nga tangata e whai hononga ipurangi ngoikore ki te uru atu ki nga paetukutuku i muri mai (hei tauira, i taea e ratou te tuara ki muri i te 7+ paparanga o te NAT, te whakaputa ranei i te IP kotahi, hei tauira mo te Wifi) te kore e whakaoti i nga ahua maha o nga CAPTCHA.I etahi waahanga, ka pau tenei 10 ki te 30 meneti hei makona a Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  I te tau 2020 ka peka ke a Cloudflare mai i te Recaptcha a Google ki te hCaptcha i te mea ka hiahia a Google ki te utu mo tana whakamahi.I kii a Cloudflare ki a koe kei te manaakitia e koe to maatauranga ("he awhina ki te whakatau i tetahi awangawanga ki a koe") engari he maamaa tenei.Kei runga katoa nga putea."Ka taea e te hCaptcha te whakamahi i nga paetukutuku ki te whakaputa moni mo te tono i a raatau e aukati ana i nga pini me etahi atu momo whakatoi" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Mai i te tirohanga a te kaiwhakamahi, kaore e tino rereke tenei. Ka raru koe ki te whakaoti. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  He maha nga taangata me nga taputapu kei te aukihia e Cloudflare i ia ra. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Ka pukuriri a Cloudflare i nga iwi maha o te ao.Tirohia te raarangi ka whakaaro mehemea he pai te tango a Cloudflare ki to papaanga mo to wheako kaiwhakamahi. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  He aha te kaupapa o te ipurangi ki te kore e taea e koe te mahi i taau e hiahia ana?Te nuinga o te hunga ka toro ki to paetukutuku ka rapua etahi atu wharangi ki te kore e utaina he wharangi paetukutuku.Kaore pea koe e kaha ki te aukati i tetahi manuhiri, engari he mea tino kaha te kaawhiawhi a Cloudflare ki te aukati i nga taangata maha. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Kaore he huarahi hei whakaoti i te captcha me te kore e taea te whakamahi i te Javascript me te Kuki.Kei te whakamahi a Cloudflare i a raatau ki te mahi tohu waitohu hei whakaatu i a koe.Me mohio e Cloudflare to tuakiri ki te whakatau mehemea he waatea koe ki te tirotiro tonu i te papaanga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Ko nga kaiwhakamahi o Tor me nga kaiwhakamahi VPN he patu i a Cloudflare.Kei te whakamahia nga rongoinga e rua e te nuinga o te hunga kaore e taea te utu ki te ipurangi koreutu e tika ana ki o raatau whenua / hinonga / kaupapahere whatunga ranei e hiahia ana ki te taapiri i tetahi taapiri hei tiaki i o raatau kaupapa here.Kei te whakaekea a Cloudflare ki aua iwi, ma ratou e aukati i a ratau otinga tuuturu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Mena kaore koe i whakamatau i a Tor tae noa ki tenei wa, ka akiaki matou ki a koe kia tango Tor Browser ka toro atu ki o paetukutuku paetukutuku pai.Ko ta matou korero kia kaua koe e uru ki to paetukutuku putea peeke i te paetukutuku tukutuku a te kawanatanga, ma te haki ranei i to putea. Whakamahia te VPN mo aua paetukutuku. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Ka hiahia pea koe ki te kii "He ture te Tor! Ka hara nga kaimahi Tor! He kino te Tor! ". No.I mohio pea koe mo te Tor mai i te pouaka whakaata, e kii ana ka taea te whakamahi i te Tor ki te tirotiro i nga maakete me nga tauhokohoko hokohoko, raau taero me te porn ranei.Ahakoa ko te tauākī i runga ake he pono he maha nga paetukutuku hokohoko i reira ka taea e koe te hoko i era mea, ka kitea ano aua pae ki runga i te ringarati.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  I whakawhanakehia a Tor e te US Army, engari ko te Tor o naianei kua whakawhanakehia e te kaupapa Tor.He maha nga iwi me nga whakahaere e whakamahi ana i a Tor tae atu ki o hoa whanui.Na, ki te kei te whakamahi koe i Cloudflare i runga i to paetukutuku kei te aukati koe i te tangata mau.Ka ngaro koe i te whanaungatanga me te mahi pakihi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  A ko ta raatau ratonga DNS, 1.1.1.1, kei te tātari i nga kaiwhakamahi mai i te toro ki te paetukutuku ma te whakahoki i nga wahitau IP rūpahu na Cloudflare, ko te localhost IP pēnei i te "127.0.0.x", kaore ranei e hoki mai tetahi mea. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Kei te pakaru ano hoki a Cloudflare DNS nga raupaparorohiko mai i te papa waea atamai mai i te kēmu rorohiko na o raatau whakautu DNS rūpahu.Cloudflare DNS kaore e taea te uiui i etahi paetukutuku peeke. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Na konei ka whakaaro pea koe,<br>Kaore au e whakamahi ana i a Tor, i a VPN ranei, he aha ahau ka tiaki?<br>I whakawhirinaki ahau ki te maakete a Cloudflare, he aha te mea e tiaki au<br>Ko taku paetukutuku he https he aha me tiaki ahau | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Mena ka toro atu koe ki te paetukutuku ka whakamahi i te Cloudflare, kei te korero koe i nga korero ehara i te rangatira paetukutuku engari ko Cloudflare ano hoki.Koinei te ahuatanga o te kaitautoko hurihuri. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Kaore e taea te wetewete me te kore e tarai i te whakawhitinga TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  E mohio ana a Cloudflare i o raraunga katoa penei i te kupuhipa mata. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Ka taea e te kapua te puta i tetahi wa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Ko te https a Cloudflare kaore he mutunga ki te mutunga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Kei te tino hiahia koe ki te whakapuaki i o raraunga ki a Cloudflare, me te tari ano-3-reta? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Ko te kōtaha a te kaiwhakamahi ipurangi he "hua" e hiahia ana te kawanatanga me nga kamupene hangarau nui ki te hoko. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Ka mea te Tari Hauora a U.S.:<br><br>Kei a koe tetahi whakaaro mo te nui o te raraunga whai taonga? He ara ano kei te hokona koe e koe i tera raraunga?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Ka tukuna hoki e Cloudflare te ratonga VPN FREE ka kiia ko "Cloudflare Warp".Mena ka whakamahi koe, ka tukuna to hononga atamai katoa (me to rorohiko) ranei ki nga kaitoro a Cloudflare.Ma te Cloudflare e mohio ki nga paetukutuku kua panuitia e koe, he aha te korero i tukuna e koe, i korero koe, me era atuHe takoha koe ki te tuku i o korero katoa ki Cloudflare.Ki te whakaaro koe "Kei te tawai koe?" Kei te haumaru a Cloudflare. " katahi ka hiahia koe ki te ako me pehea te mahi a VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  I kii a Cloudflare i a raatau ratonga VPN he tere to ipurangi.Engari ka ngawari te hononga o to ipurangi ki to hononga hononga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Kei te mohio pea koe mo te kohinga PRISM.He tika kei te AT&T te tuku ki te NSA ki te kape i nga raraunga ipurangi katoa hei tirotiro. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Me ki atu kei te mahi koe i te NSA, a kei te hiahia koe ki nga korero ipurangi a nga tangata.E mohio ana koe ko te nuinga o ratou e whakawhirinaki ana ki a Cloudflare me te whakamahi - kotahi noa te tomokanga - ki te whakauru i a raatau hononga tūmau kamupene (SSH / RDP), paetukutuku whaiaro, paetukutuku korerorero, paetukutuku paetukutuku, paetukutuku peeke, paetukutuku inihua, search engine, mema huna -Te paetukutuku, paetukutuku hokohoko, hokohoko, paetukutuku ataata, paetukutuku NSFW, me te paetukutuku aitua.Kei te mohio ano koe kei te whakamahi i te ratonga DNS a Cloudflare ("1.1.1.1") me te ratonga VPN ("Cloudflare Warp") mo te "Haumaru! Ka tere! He pai! " wheako ipurangi.Ko te whakakotahi i a raatau me te Wāhitau IP a te kaiwhakamahi, te tohu tohi tirotiro, pihikete me te RAY-ID ka whai hua ki te hanga i te kōtaha tuunga a te hunga whaainga | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Kei te hiahia koe ki a raatau raraunga. Ka aha koe? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Ko te Cloudflare he honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Honi koretake mo te katoa. Ko etahi aho e piri ana.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Kaua e whakamahi i Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Te whakatau i te ipurangi.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Tonu ki muri panui:  "[Matariki Rangi](mi.ethics.md)"

---

<details>
<summary>_pāwhiritia ahau_

## Nga Raraunga me etahi atu korero
</summary>


Ko tenei kohinga tetahi raarangi o nga paetukutuku kei muri i te "The Cloudwall Nui", ka aukati i nga kaiwhakamahi Tor me etahi atu CDN.


**Raraunga**
* [Kapua Inc.](../cloudflare_inc/)
* [Kaiwhakamahi Kapua](../cloudflare_users/)
* [Nga Puka Kapahaka](../cloudflare_users/domains/)
* [Kaiwhakamahi-kore-Cloudflare CDN](../not_cloudflare/)
* [Kaiwhakamahi anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**He maha atu nga korero**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Tikiake: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Ko te eBook taketake (ePUB) i mukua e BookRix GmbH na te whanaketanga o te mana pupuri o te rauemi CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * I whiua te tikiti maha nga waa.
  * [Kua Mohia e te Kaupapa Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Tirohia te tikiti 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [He tikiti mo te reti 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_pāwhiritia ahau_

## Ka aha koe?
</summary>

* [Panuihia ta maatau rarangi mahi kua tohutohutia ka tohua ki o hoa.](../ACTION.md)

* [Panuihia te reo o tetahi atu kaiwhakamahi ka tuhi i o whakaaro.](../PEOPLE.md)

* Rapua tetahi mea: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Whakahoutia te raarangi rohe: [Tuhia nga tohutohu](../INSTRUCTION.md).

* [Tāpirihia a Cloudflare, kaupapa ranei e pa ana ki te hitori.](../HISTORY.md)

* [Whakamātauhia me tuhia te taputapu / Panui hou.](../tool/)

* [Anei etahi PDF / ePUB hei panui.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Mo nga korero rūpahu

E mohio ana te Crimeflare e pa ana ki te puta o nga kaute raupaparanga e whakatuu ana i a maatau ara mana, ara ko Twitter, Facebook, Patreon, OpenCollective, Villages etc.
**Kaore rawa matou e patai ki to imeera.
Kaore rawa matou e tono ki to ingoa.
Kaore rawa matou e tono ki to tuakiri.
Kaore rawa matou e patai ki to waahi.
Kaore rawa matou e tono ki to takoha.
Kaore rawa matou e patai ki to arotake.
Kaore rawa matou e tono kia whai koe i runga i nga mahi pāpori.
Kaore ano matou e tono ki o pāpori pāpori.**

# KAUA E WHAKAARO KI TE KAIAKO.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)