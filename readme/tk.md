# Uly bulut


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## “Cloudflare” -ni duruzyň


|  🖹  |  🖼 |
| --- | --- |
|  “The Great Cloudwall” ABŞ-nyň “Cloudflare Inc.” kompaniýasydyr.CDN (mazmun gowşuryş ulgamy) hyzmatlaryny, DDoS azaltmak, internet howpsuzlygy we paýlanan DNS (domen ady serweri) hyzmatlaryny hödürleýär.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  “Cloudflare” dünýädäki iň uly MITM proksi (ters proksi).“Cloudflare” CDN bazaryndaky paýynyň 80% -den gowragyna eýelik edýär we bulutdan peýdalanýanlaryň sany günsaýyn artýar.Torlaryny 100-den gowrak ýurda ýaýratdylar.“Cloudflare” “Twitter”, “Amazon”, “Apple”, “Instagram”, “Bing” we “Wikipedia” -dan has köp web traffigine hyzmat edýär.“Cloudflare” mugt meýilnama hödürleýär we serwerlerini dogry sazlamagyň ýerine köp adam ony ulanýar.Amatlylykdan gizlinlik söwdasy etdiler.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  “Cloudflare” serhet gözegçilik gullugy ýaly hereket edip, gelip çykyş web serweriniň arasynda otyr.Saýlanan ýeriňize birigip bilmersiňiz.“Cloudflare” -e birikýärsiňiz we ähli maglumatlaryňyz şifrlenýär we uçuşda berilýär. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Gelip çykyş web serwer dolandyryjysy, "Cloudflare" agentine "web eýeçiligine" kimiň girip biljekdigini we "çäklendirilen meýdany" kesgitläp biljekdigini kesgitlemäge rugsat berdi.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Dogry surata göz aýlaň.“Cloudflare” diňe erbet adamlary bloklaýar diýip pikir edersiňiz.“Cloudflare” elmydama onlaýn (hiç haçan aşak düşmäň) diýip pikir edersiňiz.Kanuny botlar we gözlegçiler web sahypaňyzy indeksläp biler diýip pikir edersiňiz.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Emma bu asla dogry däl.“Cloudflare” bigünä adamlary sebäpsiz petikleýär.“Cloudflare” aşak düşüp biler.“Cloudflare” kanuny botlary bloklaýar.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Islendik hosting hyzmaty ýaly, “Cloudflare” hem kämil däl.Gelip çykyş serweri gowy işlese-de bu ekrany görersiňiz.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  “Cloudflare” -iň 100% iş wagty bar diýip pikir edýärsiňizmi?“Cloudflare” -iň näçe gezek aşak düşýändigini bilmeýärsiňiz.“Cloudflare” aşak düşse, müşderiňiz web sahypaňyza girip bilmez. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Bu, köp adamy web mazmunyny görmekden (ýagny Hytaýyň materiginde ýaşaýanlaryň hemmesi we daşyndaky adamlar) filtrlemek üçin deňeşdirip boljak bir işi ýerine ýetirýän Hytaýyň Beýik Firewall-yna degişlidir.Şol bir wagtyň özünde düýpgöter başga bir web görmäge täsir etmedikler, “tank adamynyň” şekili we “Týananmen meýdanyndaky protestleriň” taryhy ýaly senzurasyz web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  “Cloudflare” uly güýje eýe.Bir nukdaýnazardan, ahyrky ulanyjynyň ahyrky görýän zatlaryna gözegçilik edýärler.“Cloudflare” sebäpli web sahypasyna girmegiňiz gadagan edilýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  “Cloudflare” senzura üçin ulanylyp bilner. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  “Cloudflare” -iň bot diýip pikir edip biljek kiçijik brauzerini ulanýan bolsaňyz, “bulutly” web sahypasyny görüp bilmersiňiz (sebäbi köp adam ulanmaýar). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Javascript-i açmazdan, bu çozýan “brauzer barlagyndan” geçip bilmersiňiz.Bu gymmatly ömrüňiziň bäş (ýa-da has köp) sekuntyny ýitirmekdir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  “Cloudflare”, Google, Yandex, Yacy we API müşderileri ýaly kanuny robotlary / gözlegçileri awtomatiki usulda bloklaýar.“Cloudflare” kanuny gözleg botlaryny döwmek niýeti bilen “aýlanyp geçýän bulutlar” jemgyýetine işjeň gözegçilik edýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  “Cloudflare”, internet birikmesi pes bolan köp adamyň arkasyndaky web sahypalaryna girmeginiň öňüni alýar (mysal üçin, 7+ gatlak NAT gatlagynyň arkasynda bolup biler ýa-da birmeňzeş IP paýlaşyp biler, mysal üçin köpçülige açyk Wifi) CAPTCHA-lary çözmese.Käbir ýagdaýlarda Google-ny kanagatlandyrmak üçin 10-30 minut gerek bolar. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  “Cloudflare” 2020-nji ýylda “Google” -yň “Recaptcha” -dan “haptaptcha” geçdi, sebäbi Google ulanmak üçin töleg almak isleýär.“Cloudflare” size gizlinligiňize üns berýändiklerini aýtdy (“bu gizlinlik meselesini çözmäge kömek edýär”), ýöne bu ýalan.Bularyň hemmesi pul hakda."HCaptcha web sahypalaryna botlary we beýleki hyýanatçylykly ýollary petiklemek bilen bu islege hyzmat edip pul gazanmaga mümkinçilik berýär" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Ulanyjynyň nukdaýnazaryndan bu kän bir üýtgemez. Siz muny çözmäge mejbur bolýarsyňyz. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  “Cloudflare” tarapyndan her gün köp adam we programma üpjünçiligi petiklenýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  “Cloudflare” dünýädäki köp adamy gaharlandyrýar.Sanawa göz aýlaň we sahypaňyzda “Cloudflare” -ni kabul etmegiň ulanyjy tejribesi üçin peýdalydygyny pikir ediň. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Islän zadyňyzy edip bilmeýän bolsaňyz, internetiň maksady näme?Web sahypaňyza girýänleriň köpüsi, web sahypasyny ýükläp bilmese, başga sahypalary gözlär.Islänleriňize işjeň päsgel bermeýän bolmagyňyz mümkin, emma “Cloudflare” -niň deslapky gorag diwary köp adamy petiklemek üçin ýeterlikdir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Javascript we Cookies-i açmazdan, kapçany çözmegiň hiç hili usuly ýok.“Cloudflare” sizi tanamak üçin brauzere gol çekmek üçin ulanýar.“Cloudflare”, sahypa göz aýlamagy dowam etdirip biljekdigiňizi kesgitlemek üçin şahsyýetiňizi bilmelidir. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor ulanyjylary we VPN ulanyjylary hem “Cloudflare” -iň pidasy.Iki çözgüt hem öz ýurdy / korporasiýasy / tor syýasaty sebäpli senzurasyz interneti alyp bilmeýän ýa-da şahsy durmuşyny goramak üçin goşmaça gatlak goşmak isleýän köp adam tarapyndan ulanylýar.“Cloudflare” şol adamlara utançsyz hüjüm edýär, proksi çözgüdini öçürmäge mejbur edýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Şu wagta çenli Tor synap görmedik bolsaňyz, Tor brauzerini göçürip alyp, halaýan web sahypalaryňyza girmegiňizi maslahat berýäris.Bank web sahypaňyza ýa-da hökümet web sahypasyna girmezligiňizi maslahat berýäris, ýogsam hasabyňyzy bellärler. Şol web sahypalary üçin VPN ulanyň. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  “Tor bikanun! Tor ulanyjylary jenaýatçy! Tor erbet! ". No.ok.Tor hakda telewizorda öwrenip bilersiňiz, Tor garaňky torlara göz aýlamak we ýarag, neşe ýa-da pornografiýa söwdasy üçin ulanylyp bilner.Aboveokardaky aýdylanlar, şeýle zatlary satyn alyp boljak bazar web sahypasynyň köpdügine garamazdan, bu saýtlar köplenç klarnetde-de görünýär.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor ABŞ goşuny tarapyndan işlenip düzüldi, ýöne häzirki Tor Tor taslamasy tarapyndan işlenip düzüldi.Tor ulanýan geljekki dostlaryňyzy goşmak bilen köp adam we guramalar bar.Şeýlelik bilen, web sahypaňyzda “Cloudflare” ulanýan bolsaňyz, hakyky adamlary petikleýärsiňiz.Potensial dostlugy we işewürlik şertnamasyny ýitirersiňiz. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Şeýle hem, olaryň DNS hyzmaty, 1.1.1.1, ulanyjylary “Cloudflare” -niň eýeçiligindäki galp IP adresi, “127.0.0.x” ýaly ýerlihost IP-lerini yzyna gaýtarmak ýa-da hiç zat yzyna gaýtarmak arkaly ulanyjylary web sahypasyna girmekden süzýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  “Cloudflare DNS”, ýasama DNS jogaplary sebäpli smartfon programmasyndan kompýuter oýnuna onlaýn programma üpjünçiligini hem döwýär.“Cloudflare DNS” käbir bank web sahypalaryny sorap bilmeýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Ine, şu ýerde pikir edip bilersiňiz,<br>Tor ýa-da VPN ulanamok, näme üçin alada etmeli?<br>“Cloudflare” marketingine ynanýaryn, näme üçin alada etmeli?<br>Meniň web sahypam https, näme üçin alada etmeli? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  “Cloudflare” ulanýan web sahypasyna girseňiz, maglumatlaryňyzy diňe web sahypasynyň eýesine däl, “Cloudflare” -e hem paýlaşýarsyňyz.Ters proksi şeýle işleýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  TLS traffigini açarsyz seljermek mümkin däl. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  “Cloudflare” çig parol ýaly ähli maglumatlaryňyzy bilýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  “Cloudbeed” islendik wagt bolup biler. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  “Cloudflare” -iň https-i hiç wagt soňy däl. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  “Cloudflare” we 3 harply agentlik bilen hakykatdanam maglumatlaryňyzy paýlaşmak isleýärsiňizmi? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Internet ulanyjysynyň onlaýn tertibi hökümetiň we uly tehnologiýa kompaniýalarynyň satyn almak isleýän “önümi”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  ABŞ-nyň Içeri howpsuzlyk ministrligi aýtdy:<br><br>Maglumatlaryňyzyň näderejede gymmatlydygy barada pikiriňiz barmy? Bize şol maglumatlary satmagyň ýoly barmy?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  “Cloudflare” şeýle hem “Cloudflare Warp” atly mugt VPN hyzmatyny hödürleýär.Ony ulansaňyz, ähli smartfonyňyz (ýa-da kompýuteriňiz) birikmeleri “Cloudflare” serwerlerine iberilýär.“Cloudflare” haýsy web sahypasyny okandygyňyzy, haýsy teswir ýazandygyňyzy, kim bilen gürleşendigiňizi we ş.m. bilip biler.“Cloudflare” -e ähli maglumatlaryňyzy meýletin berýärsiňiz.“Degişýärsiňizmi? “Cloudflare” howpsuz. ” soň VPN-iň nähili işleýändigini öwrenmeli. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare, VPN hyzmatynyň internetiňizi çaltlaşdyrýandygyny aýtdy.Vöne VPN internet birikmäňizi bar bolan birikmäňizden haýal edýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  PRISM skandaly hakda eýýäm bilýän bolmagyňyz mümkin.“AT&T” NSA-a gözegçilik üçin ähli internet maglumatlaryny göçürmäge mümkinçilik berýär. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  NSA-da işleýärsiňiz diýeliň we her bir raýatyň internet profilini isleýärsiňiz.Olaryň köpüsiniň “Cloudflare” -e kör ynanýandyklaryny we kompaniýanyň serwer birikmesine (SSH / RDP), şahsy web sahypasyna, söhbetdeşlik web sahypasyna, forum web sahypasyna, bank web sahypasyna, ätiýaçlandyryş web sahypasyna, gözleg motoryna, gizlin agzasyna proksi etmek üçin diňe bir merkezleşdirilen şlýuzdan peýdalanýandygyny bilýärsiňiz. - diňe web sahypasy, auksion web sahypasy, söwda, wideo web sahypasy, NSFW web sahypasy we bikanun web sahypasy.Şeýle hem, “Howpsuz” üçin Cloudflare-iň DNS hyzmatyny ("1.1.1.1") we VPN hyzmatyny ("Cloudflare Warp") ulanýandygyny bilýärsiňiz! Çalt! Has gowusy! ” internet tejribesi.Ulanyjynyň IP adresi, brauzer barmak yzy, gutapjyklar we RAY-ID bilen birleşdirmek, nyşanyň onlaýn profilini döretmek üçin peýdaly bolar. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Olaryň maglumatlaryny isleýärsiňiz. Näme ederdiň? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **“Cloudflare” bal balydyr.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Hemmeler üçin mugt bal. Käbir setirler birikdirildi.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **“Cloudflare” ulanmaň.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Interneti merkezden daşlaşdyrmak.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Indiki sahypa dowam etmegiňizi haýyş edýäris:  "[Bulutly etika](tk.ethics.md)"

---

<details>
<summary>_maňa basyň_

## Maglumat we has giňişleýin maglumat
</summary>


Bu ammar, Tor ulanyjylaryny we beýleki CDN-leri bloklaýan "The Great Cloudwall" -yň arkasynda durýan web sahypalarynyň sanawy.


**Maglumatlar**
* [“Cloudflare Inc.”](../cloudflare_inc/)
* [“Cloudflare” ulanyjylary](../cloudflare_users/)
* [“Cloudflare” domenleri](../cloudflare_users/domains/)
* [“Cloudflare” däl CDN ulanyjylary](../not_cloudflare/)
* [Anti-Tor ulanyjylary](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Köp maglumat**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Göçürip al: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Asyl eBook (ePUB), CCR materialynyň awtorlyk hukuklarynyň bozulmagy sebäpli BookRix GmbH tarapyndan pozuldy.
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Bilet şeýle köp gezek weýran edildi.
  * [Tor taslamasy tarapyndan öçürildi.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [34175 biletine serediň.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Iň soňky arhiw bileti 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_maňa basyň_

## Sen näme edip bilýärsiñ?
</summary>

* [Maslahat berilýän hereketleriň sanawyny okaň we dostlaryňyz bilen paýlaşyň.](../ACTION.md)

* [Beýleki ulanyjynyň sesini okaň we pikirleriňizi ýazyň.](../PEOPLE.md)

* Bir zady gözläň: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Domen sanawyny täzeläň: [Görkezmeleri sanaň](../INSTRUCTION.md).

* [“Cloudflare” ýa-da taslama bilen baglanyşykly wakany taryhda goşuň.](../HISTORY.md)

* [Täze gural / skript ýazyp görüň.](../tool/)

* [Ine, okamak üçin birnäçe PDF / ePUB.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Galp hasaplar hakda

Jenaýat işi, Twitter, Facebook, Patreon, OpenCollective, Oba we ş.m. resmi kanallarymyzy görkezýän galp hasaplaryň bardygyny bilýär.
**E-poçtaňyzy hiç haçan soramaýarys.
Biz hiç haçan adyňyzy soramaýarys.
Hiç haçan şahsyýetiňizi soramaýarys.
Hiç haçan ýerleşýän ýeriňizi soramaýarys.
Haýyr-sahawatyňyzy hiç haçan soramaýarys.
Hiç haçan synyňyzy soramaýarys.
Sosial mediýada yzarlamagyňyzy hiç haçan haýyş etmeýäris.
Sosial mediýaňyzdan hiç haçan soramaýarys.**

# AKAL .YŞ HASABATLARA ynanmaň.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)