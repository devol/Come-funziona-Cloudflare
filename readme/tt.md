# Зур болыт


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Cloudflare-ны туктатыгыз


|  🖹  |  🖼 |
| --- | --- |
|  "Зур Cloudwall" - АКШ компаниясе Cloudflare Inc.Ул CDN (эчтәлек җибәрү челтәре) хезмәтләрен, DDoS йомшарту, Интернет куркынычсызлыгы, таратылган DNS (домен исеме серверы) хезмәтләрен күрсәтә.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare - дөньядагы иң зур MITM прокси (кире прокси).Cloudflare CDN базар өлешенең 80% тан артык өлешенә ия һәм болыт утларын кулланучылар саны көннән-көн арта.Алар челтәрен 100 дән артык илгә киңәйттеләр.Cloudflare Twitter, Amazon, Apple, Instagram, Bing & Wikipedia кушылганга караганда күбрәк веб-трафикка хезмәт күрсәтә.Cloudflare бушлай план тәкъдим итә һәм күпләр аны серверларын дөрес конфигурацияләү урынына кулланалар.Алар хосусыйлыкны уңайлыклар белән сәүдә иттеләр.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare сезнең белән веб-сервер арасында утыра, чик патруле агенты кебек.Сез сайлаган юнәлешкә тоташа алмыйсыз.Сез Cloudflare белән тоташасыз һәм барлык мәгълүматларыгыз шифрлана һәм чебенгә бирелә. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Веб-сервер администраторы агентка - Cloudflare - кемгә "веб-милеккә" керә алуын һәм "чикләнгән өлкәне" билгеләргә рөхсәт бирде.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Дөрес рәсемгә карагыз.Cloudflare начар кешеләрне генә блоклый дип уйлыйсыз.Cloudflare гел онлайнда дип уйлыйсыз (беркайчан да төшмәгез).Сез легитим ботлар һәм крейлерлар сезнең вебсайтыгызны индексацияли алыр дип уйлыйсыз.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Ләкин бу бөтенләй дөрес түгел.Cloudflare гаепсез кешеләрне сәбәпсез блоклый.Болыт утлары төшәргә мөмкин.Cloudflare легит ботларны блоклый.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Төрле хостинг хезмәте кебек, Cloudflare да камил түгел.Чыгыш серверы яхшы эшләсә дә, сез бу экранны күрерсез.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Сез ничек уйлыйсыз, Cloudflare 100% эш вакыты бар?Cloudflare ничә тапкыр төшүен сез белмисез.Cloudflare төшсә, сезнең клиент сезнең вебсайтка керә алмый. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Бу Кытайның Бөек Firewall'ына сылтама белән атала, ул чагыштырмача эшне веб-эчтәлекне күрүдән фильтрлау буенча эш итә (ягъни Кытай материкларында һәм чит ил кешеләре).Шул ук вакытта бөтенләй башка вебны күрергә тәэсир итмәгән кешеләр, "танк кешесе" образы һәм "Тяньянмен мәйданындагы протестлар" тарихы кебек цензурасыз веб. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare зур көчкә ия.Ниндидер мәгънәдә, алар соңгы кулланучының күргәннәрен контрольдә тоталар.Cloudflare аркасында сезгә сайтны карау тыела. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare цензура өчен кулланылырга мөмкин. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Cloudflare аны бот дип уйларга мөмкин булган кечкенә браузерны куллансагыз, сез болытлы утлы сайтны карый алмыйсыз (чөнки аны күп кеше кулланмый). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Сез бу инвазив "браузер тикшерүен" Javascript кушмыйча уза алмыйсыз.Бу сезнең кыйммәтле тормышыгызның биш (яки күбрәк) секундын әрәм итү. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare шулай ук ​​Google, Yandex, Yacy, API клиентлары кебек легитим роботларны / крейлерларны автоматик рәвештә блоклый.Cloudflare легаль тикшеренү ботларын сындыру нияте белән "әйләнеп узучы болыт" җәмгыятен актив күзәтә. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare шулай ук ​​начар интернет тоташуы булган күп кешеләргә аның артындагы вебсайтларга керергә комачаулый (мәсәлән, алар 7+ катлам артында булырга мөмкин яки бер үк IP бүлешергә мөмкин, мәсәлән, ачык Wifi), алар берничә рәсем CAPTCHA'ны чишмәсәләр.Кайбер очракларда Google-ны канәгатьләндерү өчен 10-30 минут вакыт кирәк булачак. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020-нче елда Cloudflare Google'ның Recaptcha'тан hCaptcha'ка күчә, чөнки Google аны куллану өчен түләргә тели.Cloudflare сезгә сезнең хосусыйлыгыгыз турында кайгыртуларын әйтте ("бу хосусыйлык проблемаларын чишәргә ярдәм итә"), ләкин бу ялган.Барысы да акча турында."HCaptcha вебсайтларга бу таләпне тәэмин итү өчен акча эшләргә мөмкинлек бирә, ботларны һәм башка хокук бозуларны блоклый." | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Кулланучылар күзлегеннән караганда, бу күп үзгәрми. Сез аны чишәргә мәҗбүр буласыз. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Күпчелек кеше һәм программа тәэминаты Cloudflare тарафыннан көн саен блоклана. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare бөтен дөнья кешеләрен рәнҗетә.Исемлеккә күз салыгыз һәм Cloudflare-ны сайтыгызда куллану кулланучылар тәҗрибәсе өчен яхшымы-юкмы дип уйлагыз. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Сез теләгәнне эшли алмасагыз, Интернетның максаты нинди?Сезнең вебсайтка керүчеләрнең күбесе веб-битне йөкли алмасалар, башка битләрне эзләячәкләр.Сез килүчеләрне актив рәвештә блокламыйсыз, ләкин Cloudflare'ның демократик саклагычлары күп кешеләрне тыяр өчен җитәрлек. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Javascript һәм Cookies'ны рөхсәт итмичә капчаны чишү мөмкинлеге юк.Cloudflare аларны танып белү өчен браузер имзасы ясау өчен куллана.Cloudflare сайтны карауны дәвам итәр өчен сезнең хокукыгызны белергә тиеш. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor кулланучылары һәм VPN кулланучылары шулай ук ​​Cloudflare корбаны.Ике чишелешне дә үз илләре / корпорациясе / челтәр политикасы аркасында цензурасыз интернет ала алмаган яки шәхси тормышын саклау өчен өстәмә катлам өстәргә теләгән кешеләр куллана.Cloudflare оятсыз рәвештә ул кешеләргә һөҗүм итә, аларны прокси чишелешен сүндерергә мәҗбүр итә. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Әгәр дә сез бу мизгелгә кадәр Торны сынап карамаган булсагыз, без сезне Tor браузерын йөкләргә һәм яраткан вебсайтларга керергә тәкъдим итәбез.Без сезнең банк вебсайтына яки дәүләт веб-битенә кермәскә тәкъдим итәбез, яисә алар сезнең счетыгызны флаглаячаклар. Бу вебсайтлар өчен VPN кулланыгыз. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Сез “Тор законсыз! Тор кулланучылар криминал! Тор начар! ". No.к.Сез Тор турында телевизордан белә аласыз, Тор караңгы челтәрне карау һәм мылтыклар, наркотиклар яки порнография сәүдә итү өчен кулланыла ала.Aboveгарыда әйтелгәннәр дөрес, базар сайтлары бик күп, анда сез мондый әйберләрне сатып ала аласыз, ул сайтлар еш кына кларнетта да күренә.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Тор АКШ армиясе тарафыннан эшләнгән, ләкин хәзерге Tor Тор проекты белән эшләнгән.Торны кулланган кешеләр һәм оешмалар бик күп, сезнең булачак дусларыгыз да бар.Шулай итеп, сез үзегезнең сайтта Cloudflare кулланасыз икән, сез чын кешеләрне блоклыйсыз.Сез потенциаль дуслыкны һәм бизнес килешүен югалтачаксыз. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Theirәм аларның DNS хезмәте, 1.1.1.1, шулай ук ​​кулланучыларны Cloudflare карамагындагы ялган IP адресны, "127.0.0.x" кебек локальхост IP-ны кире кайтарып, яки бернәрсә дә кире кайтармыйча фильтрлый. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS шулай ук ​​ялган DNS җаваплары аркасында смартфон кушымтасыннан компьютер уенына онлайн программаларны өзә.Cloudflare DNS кайбер банк сайтларын сорый алмый. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Менә сез уйларга мөмкин,<br>Мин Tor яки VPN кулланмыйм, нигә мин кайгыртырга тиеш?<br>Cloudflare маркетингына ышанам, нигә мин кайгыртырга тиеш<br>Минем сайт - https, нигә мин кайгыртырга тиеш | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Cloudflare кулланган вебсайтка керсәгез, сез үз мәгълүматыгызны вебсайт хуҗасына гына түгел, Cloudflare белән дә бүлешәсез.Кире прокси шулай эшли. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  TLS трафигын шифрламыйча анализлау мөмкин түгел. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare чимал серсүз кебек бөтен мәгълүматыгызны белә. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed теләсә кайсы вакытта булырга мөмкин. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare'ның https беркайчан да бетми. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Сез үзегезнең мәгълүматны Cloudflare, шулай ук ​​3 хәрефле агентлык белән бүлешергә телисезме? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Интернет кулланучының онлайн профиле - хөкүмәт һәм эре технология компанияләре сатып алырга теләгән "продукт". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  АКШ Милли Куркынычсызлык Департаменты хәбәр итте:<br><br>Сездә булган мәгълүматлар никадәр кыйммәт? Безгә бу мәгълүматны сату ысулы бармы?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare шулай ук ​​"Cloudflare Warp" дип аталган бушлай VPN хезмәтен тәкъдим итә.Әгәр дә сез аны куллансагыз, бөтен смартфон (яки компьютер) тоташулары Cloudflare серверларына җибәрелә.Cloudflare кайсы вебсайтны укыганыгызны, нинди аңлатма урнаштырганыгызны, кем белән сөйләшкәнегезне белә ала.Сез үз мәгълүматыгызны Cloudflare'ка бирәсез."Шаярасыңмы?" Болыт куркынычсыз. " аннары сезгә VPNның ничек эшләвен белергә кирәк. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare аларның VPN хезмәте сезнең интернетны тиз итә, диде.Ләкин VPN сезнең интернетка тоташуыгызны хәзерге тоташуга караганда әкренрәк итә. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Сез PRISM җәнҗалы турында белә аласыз.Дөрес, AT&T NSAга күзәтү өчен барлык интернет мәгълүматларын күчерергә мөмкинлек бирә. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Әйтик, сез NSAда эшлисез, һәм сез һәрбер гражданның интернет профилен телисез.Син беләсең, аларның күбесе сукырларча Cloudflare ышаналар һәм аны кулланалар - бер үзәкләштерелгән шлюз - компания серверына тоташу (SSH / RDP), шәхси сайт, чат сайты, форум сайты, банк сайты, страховка сайты, эзләү системасы, яшерен әгъза. - бердәнбер сайт, аукцион сайты, кибет, видео сайт, NSFW сайты, һәм законсыз сайт.Сез шулай ук ​​Cloudflare'ның DNS сервисын ("1.1.1.1") һәм VPN сервисын ("Cloudflare Warp") "Куркынычсыз" өчен кулланганнарын беләсез! Тизрәк! Яхшырак! " Интернет тәҗрибәсе.Аларны кулланучының IP адресы, браузерның бармак эзе, cookies һәм RAY-ID белән берләштерү максатчан онлайн профиль төзү өчен файдалы булачак. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Сез аларның мәгълүматларын телисез. Нишләячәксең? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare - бал корты.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Барысы өчен бушлай бал. Кайбер кыллар бәйләнгән.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Cloudflare кулланмагыз.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Интернетны үзәкләштермә.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Зинһар, киләсе биткә дәвам итегез:  "[Cloudflare этикасы](tt.ethics.md)"

---

<details>
<summary>_миңа басыгыз_

## Мәгълүматлар һәм күбрәк мәгълүмат
</summary>


Бу склад "Tor Cloudwall" артында торучы, Tor кулланучыларын һәм башка CDNларны блоклаучы вебсайтлар исемлеге.


**Мәгълүмат**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare кулланучылары](../cloudflare_users/)
* [Cloudflare доменнары](../cloudflare_users/domains/)
* [Cloudflare булмаган CDN кулланучылары](../not_cloudflare/)
* [Анти-Тор кулланучылары](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Күбрәк мәгълүмат**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Йөкләү: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Оригиналь электрон китап (ePUB) BookRix GmbH тарафыннан CC0 материалының авторлык хокукларын бозуы аркасында бетерелде.
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Билет күп тапкыр вандализацияләнде.
  * [Тор проекты белән бетерелде.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [34175 билетны карагыз.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Соңгы архив билеты 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_миңа басыгыз_

## Сез нәрсә эшли аласыз?
</summary>

* [Тәкъдим ителгән чаралар исемлеген укыгыз һәм дусларыгыз белән бүлешегез.](../ACTION.md)

* [Башка кулланучының тавышын укыгыз һәм үз фикерләрегезне языгыз.](../PEOPLE.md)

* Берәр нәрсә эзлә: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Домен исемлеген яңарту: [Күрсәтмәләр](../INSTRUCTION.md).

* [Cloudflare яки проект белән бәйле вакыйганы тарихка өстәгез.](../HISTORY.md)

* [Яңа Корал / Скриптны карагыз.](../tool/)

* [Менә берничә PDF / ePUB укырга.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Ялган счетлар турында

Crimeflare безнең рәсми каналларны күрсәтүче ялган счетлар барлыгы турында белә, Твиттер, Facebook, Патрон, OpenCollective, Авыллар һ.б.
**Без сезнең электрон почтагызны беркайчан да сорамыйбыз.
Без беркайчан да исемеңне сорамыйбыз.
Без беркайчан да сезнең шәхесегезне сорамыйбыз.
Без сезнең урыныгызны беркайчан да сорамыйбыз.
Без сезнең иганәгезне беркайчан да сорамыйбыз.
Без сезнең рецензияне беркайчан да сорамыйбыз.
Без сездән беркайчан да социаль медиага иярүегезне сорамыйбыз.
Без сезнең социаль медиадан беркайчан да сорамыйбыз.**

# ЯЛГАН Хисапларга ышанмагыз.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)