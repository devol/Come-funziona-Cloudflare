# Igicu kinini


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Hagarika Igicu


|  🖹  |  🖼 |
| --- | --- |
|  “The Great Cloudwall” ni Cloudflare Inc, isosiyete yo muri Amerika.Itanga serivisi za CDN (imiyoboro yo gutanga ibirimo), kugabanya DDoS, umutekano wa interineti, no gutanga serivisi za DNS (seriveri yizina rya seriveri).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare ni porokisi nini ya MITM ku isi (proxy ya reaction).Cloudflare ifite imigabane irenga 80% ya CDN yisoko kandi umubare wabakoresha ibicu uragenda wiyongera buri munsi.Baguye imiyoboro yabo mu bihugu birenga 100.Cloudflare ikora traffic traffic kurusha Twitter, Amazon, Apple, Instagram, Bing & Wikipedia hamwe.Cloudflare itanga gahunda yubuntu kandi abantu benshi barayikoresha aho kugena seriveri zabo neza.Bagurishaga ubuzima bwite kugirango byorohe.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare yicaye hagati yawe ninkomoko ya seriveri, ikora nkumukozi ushinzwe irondo kumupaka.Ntushobora guhuza aho wahisemo.Urimo guhuza na Cloudflare kandi amakuru yawe yose arimo gufungurwa no gutangwa hejuru. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Inkomoko ya seriveri ya seriveri yemereye umukozi - Cloudflare - guhitamo uwashobora kugera kuri "imitungo y'urubuga" no gusobanura "agace kabujijwe".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Reba ku ishusho iboneye.Uzatekereza Cloudflare ihagarika abasore babi gusa.Uzatekereza ko Cloudflare ihora kumurongo (ntuzigere umanuka).Uzatekereza ko bots zemewe na crawlers zishobora kwerekana urubuga rwawe.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Icyakora ibyo ntabwo arukuri.Cloudflare ibuza inzirakarengane nta mpamvu.Igicu kirashobora kumanuka.Cloudflare ihagarika bots zemewe.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Nka serivise iyo ari yo yose yakira, Cloudflare ntabwo itunganye.Uzabona iyi ecran nubwo seriveri yinkomoko ikora neza.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Uratekereza rwose ko Cloudflare ifite amasaha 100%?Ntabwo uzi inshuro Cloudflare imanuka.Niba Cloudflare yamanutse umukiriya wawe ntashobora kugera kurubuga rwawe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Yiswe ibi byerekeranye na Firewall nini yo mubushinwa ikora akazi kagereranywa ko gushungura abantu benshi kugirango babone ibiri kurubuga (nukuvuga abantu bose bo kumugabane wUbushinwa nabantu hanze).Mugihe kimwe icyarimwe abatagize ingaruka zo kubona urubuga rutandukanye cyane, urubuga rutagenzurwa nkigishusho cy "tank man" n'amateka y "imyigaragambyo ya Tiananmen Square". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare ifite imbaraga zikomeye.Mu buryo bumwe, bagenzura ibyo umukoresha wa nyuma abona.Urabujijwe kureba kurubuga kubera Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare irashobora gukoreshwa mugukurikirana. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Ntushobora kureba urubuga rwibicu niba ukoresha mushakisha ntoya Cloudflare ishobora gutekereza ko ari bot (kuko ntabwo abantu benshi bayikoresha). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Ntushobora gutsinda iyi "mushakisha igenzura" udashoboye Javascript.Ibi ni uguta amasegonda atanu (cyangwa arenga) yubuzima bwawe bwagaciro. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare nayo ihita ihagarika robot zemewe / zikurura nka Google, Yandex, Yacy, nabakiriya ba API.Cloudflare ikurikirana byimazeyo umuryango "bypass cloudflare" ugamije guca bots ubushakashatsi bwemewe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare nayo ibuza abantu benshi bafite umurongo wa enterineti udahagije kugera kurubuga rwihishe inyuma (urugero, barashobora kuba inyuma ya 7+ ya NAT cyangwa bagabana IP imwe, urugero nka Wifi rusange) keretse bakemuye amashusho menshi CAPTCHAs.Rimwe na rimwe, ibi bizatwara iminota 10 kugeza 30 kugirango uhaze Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Mu mwaka wa 2020 Cloudflare yavuye muri Recaptcha ya Google yerekeza kuri hCaptcha nkuko Google ishaka kwishyuza ikoreshwa ryayo.Cloudflare yakubwiye ko bita ku buzima bwawe bwite (“bifasha gukemura ibibazo byihariye”) ariko biragaragara ko ari ibinyoma.Byose bijyanye n'amafaranga.“HCaptcha yemerera imbuga za interineti kubona amafaranga akoresha iki cyifuzo mu gihe zibuza bots n'ubundi buryo bwo guhohoterwa” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Ukurikije uko abakoresha babibona, ibi ntabwo bihinduka cyane. Uhatirwa kubikemura. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Abantu benshi na software birahagarikwa na Cloudflare burimunsi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare irakaza abantu benshi kwisi.Reba kurutonde hanyuma utekereze niba kwemeza Cloudflare kurubuga rwawe ari byiza kuburambe bwabakoresha. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Intego ya interineti niyihe ntego niba udashobora gukora ibyo ushaka?Abantu benshi basura urubuga rwawe bazareba izindi page gusa niba badashobora gupakira urubuga.Urashobora kuba udahagarika cyane abashyitsi, ariko firewall ya Cloudflare isanzwe irakomeye bihagije kugirango uhagarike abantu benshi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Nta buryo bwo gukemura capcha udashoboje Javascript na kuki.Cloudflare irabakoresha kugirango bakore umukono wa mushakisha kugirango bakumenye.Cloudflare ikeneye kumenya umwirondoro wawe kugirango uhitemo niba wemerewe gukomeza gushakisha kurubuga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Abakoresha Tor hamwe nabakoresha VPN nabo barahohotewe na Cloudflare.Ibisubizo byombi birakoreshwa nabantu benshi badashobora kugura interineti idafite uruhushya kubera igihugu cyabo / isosiyete / politiki y'urusobe cyangwa bashaka kongeramo urwego rwo kurinda ubuzima bwabo bwite.Cloudflare yibasiye abo bantu nta soni, ibahatira guhagarika igisubizo cyabo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Niba utagerageje Tor kugeza magingo aya, turagutera inkunga yo gukuramo Tor Browser no gusura urubuga ukunda.Turagusaba kutinjira kurubuga rwa banki cyangwa kurubuga rwa leta cyangwa bazashyira ahagaragara konte yawe. Koresha VPN kururwo rubuga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Urashobora kuvuga ngo "Tor iremewe! Abakoresha Tor ni abagizi ba nabi! Tor ni mbi! ". Oya.Urashobora kwiga ibya Tor kuri tereviziyo, ukavuga ko Tor ishobora gukoreshwa mugushakisha umwijima no gucuruza imbunda, ibiyobyabwenge cyangwa porunogarafiya.Mugihe ibyavuzwe haruguru arukuri ko hariho urubuga rwisoko rwinshi aho ushobora kugura ibintu nkibi, izo mbuga akenshi zigaragara no kuri clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor yatunganijwe ningabo z’Amerika, ariko Tor iriho ubu yatunganijwe numushinga wa Tor.Hariho abantu benshi nimiryango ikoresha Tor harimo ninshuti zawe zizaza.Noneho, niba ukoresha Cloudflare kurubuga rwawe urabuza abantu nyabo.Uzatakaza ubucuti bushoboka nubucuruzi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Serivise yabo ya DNS, 1.1.1.1, nayo irayungurura abakoresha gusura urubuga mugusubiza aderesi ya IP yibinyoma ifitwe na Cloudflare, IP ya localhost nka "127.0.0.x", cyangwa ntacyo usubiza. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS nayo isenya software kumurongo kuva porogaramu ya terefone kugeza kumikino ya mudasobwa kubera igisubizo cyabo cya DNS.Cloudflare DNS ntishobora kubaza imbuga zimwe na zimwe za banki. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Kandi hano ushobora gutekereza,<br>Ntabwo nkoresha Tor cyangwa VPN, kuki nakwitaho?<br>Nizeye ibicuruzwa bya Cloudflare, kuki nkwiye kubyitaho<br>Urubuga rwanjye ni https kuki nkwiye kubyitaho | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Niba usuye urubuga rukoresha Cloudflare, ntabwo usangira amakuru yawe na nyiri urubuga gusa ahubwo na Cloudflare.Nuburyo proxy ya reaction ikora. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Ntibishoboka gusesengura udafunguye traffic traffic TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare izi amakuru yawe yose nkibanga ryibanga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Igicu kirashobora kubaho igihe icyo aricyo cyose. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare ya https ntabwo yigeze iherezo-iherezo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Urashaka rwose gusangira amakuru yawe na Cloudflare, kandi n'ikigo cy'inyuguti 3? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Umwirondoro wumukoresha wa interineti ni "ibicuruzwa" leta nisosiyete nini yikoranabuhanga ishaka kugura. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Minisiteri ishinzwe umutekano mu gihugu cya Amerika yavuze:<br><br>Waba ufite igitekerezo cyuko amakuru ufite afite agaciro? Hariho uburyo bwo kutugurisha ayo makuru?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare itanga kandi serivisi ya VPN KUBUNTU yitwa "Cloudflare Warp".Niba uyikoresha, ama terefone yawe yose (cyangwa mudasobwa yawe) yoherejwe kuri seriveri ya Cloudflare.Cloudflare irashobora kumenya urubuga wasomye, igitekerezo washyizeho, uwo waganiriye, nibindi.Urimo gutanga kubushake amakuru yawe yose kuri Cloudflare.Niba utekereza “Urasetsa? Igicu gifite umutekano. ” noneho ugomba kwiga uko VPN ikora. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare yavuze ko serivisi zabo za VPN zituma interineti yawe yihuta.Ariko VPN ituma umurongo wa enterineti utinda kurenza aho uhurira. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Urashobora kuba usanzwe uzi ibya PRISM scandal.Nukuri ko AT&T ireka NSA ikoporora amakuru yose ya enterineti kugirango ikurikiranwe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Reka tuvuge ko ukorera muri NSA, kandi ushaka umwirondoro wa interineti wa buri muturage.Urabizi ko benshi muribo bizeye buhumyi Cloudflare kandi barayikoresha - irembo rimwe gusa ryibanze - kugirango bahagararire seriveri yabo ya seriveri (SSH / RDP), urubuga rwumuntu, urubuga rwibiganiro, urubuga rwihuriro, urubuga rwa banki, urubuga rwubwishingizi, moteri ishakisha, umunyamuryango wibanga -urubuga gusa, urubuga rwa cyamunara, guhaha, urubuga rwa videwo, urubuga rwa NSFW, nurubuga rutemewe.Uzi kandi ko bakoresha serivise ya DNS ya Cloudflare ("1.1.1.1") na serivisi ya VPN ("Cloudflare Warp") kuri "Umutekano! Byihuta! Ibyiza! ” uburambe bwa interineti.Kubahuza hamwe na aderesi ya IP yumukoresha, igikumwe cya mushakisha, kuki na RAY-ID bizagira akamaro mukubaka intego kumurongo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Urashaka amakuru yabo. Uzakora iki? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare ni ubuki.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Ubuki bwubusa kuri buri wese. Imirongo imwe ifatanye.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Ntukoreshe Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Kwegereza ubuyobozi bwa interineti.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Nyamuneka komeza kurupapuro rukurikira:  "[Imyitwarire ya Cloudflare](rw.ethics.md)"

---

<details>
<summary>_kanda_

## Amakuru hamwe nandi makuru
</summary>


Ububiko ni urutonde rwurubuga ruri inyuma ya "The Great Cloudwall", rukumira abakoresha Tor nizindi CDN.


**Amakuru**
* [Yamazaki Inc.](../cloudflare_inc/)
* [Abakoresha Igicu](../cloudflare_users/)
* [Igicu](../cloudflare_users/domains/)
* [Abakoresha CDN itari Cloudflare](../not_cloudflare/)
* [Abakoresha anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Ibisobanuro byinshi**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Kuramo: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Igitabo cyumwimerere (ePUB) cyasibwe na BookRix GmbH kubera kuvutswa uburenganzira bwibikoresho bya CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Itike yangiritse inshuro nyinshi.
  * [Byasibwe numushinga wa Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Reba itike 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Amatike yanyuma yububiko 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_kanda_

## Wakora iki?
</summary>

* [Soma urutonde rwibikorwa byasabwe hanyuma ubisangire n'inshuti zawe.](../ACTION.md)

* [Soma ijwi ryabandi bakoresha hanyuma wandike ibitekerezo byawe.](../PEOPLE.md)

* Shakisha ikintu: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Kuvugurura urutonde: [Andika amabwiriza](../INSTRUCTION.md).

* [Ongeraho Cloudflare cyangwa umushinga ujyanye nibyabaye mumateka.](../HISTORY.md)

* [Gerageza & andika Igikoresho gishya / Inyandiko.](../tool/)

* [Hano hari PDF / ePUB yo gusoma.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Ibyerekeye konti mpimbano

Crimeflare izi kubyerekeye kubaho kwa konti mpimbano yigana imiyoboro yacu yemewe, yaba Twitter, Facebook, Patreon, OpenCollective, Imidugudu nibindi.
**Ntabwo twigera tubaza imeri yawe.
Ntabwo twigera tubaza izina ryawe.
Ntabwo twigera tubaza umwirondoro wawe.
Ntabwo twigera tubaza aho uherereye.
Ntabwo dusaba inkunga yawe.
Ntabwo twigera dusaba gusubiramo.
Ntabwo twigera tugusaba gukurikira kurubuga rusange.
Ntabwo twigera tubaza imbuga nkoranyambaga.**

# NTIWIZERE KONTI Z'IMPANO.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)