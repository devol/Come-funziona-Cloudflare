# Il-Cloudwall il-Kbir


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Waqqaf Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Great Cloudwall" hija Cloudflare Inc., il-kumpanija tal-Istati Uniti.Huwa jipprovdi servizzi CDN (netwerk ta 'konsenja ta' kontenut), mitigazzjoni DDoS, sigurtà tal-Internet, u servizzi distribwiti ta 'DNS (server tal-isem tad-dominju).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare huwa l-akbar prokura MITM fid-dinja (reverse prokura).Cloudflare għandha aktar minn 80% tas-sehem tas-suq CDN u l-għadd ta 'utenti ta' cloudflare qed jikber kuljum.Huma kabbru n-netwerk tagħhom għal aktar minn 100 pajjiż.Cloudflare jservi iktar traffiku tal-web minn Twitter, Amazon, Apple, Instagram, Bing u Wikipedia.Cloudflare qed toffri pjan b'xejn u ħafna nies qed jużawha minflok jikkonfiguraw is-servers tagħhom kif suppost.Huma nnegozjati l-privatezza fuq konvenjenza.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare tpoġġi bejnek u webserver tal-oriġini, li taġixxi bħal aġent tal-għassa tal-fruntiera.M'intix kapaċi tikkonnettja mad-destinazzjoni magħżula tiegħek.Int qed tikkonnettja ma 'Cloudflare u l-informazzjoni kollha tiegħek qed tiġi decriptata u mgħoddija fuq il-fly. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  L-amministratur tan-websajt tal-oriġini ppermetta lill-aġent - Cloudflare - jiddeċiedi min jista 'jkollu aċċess għall- "proprjetà web" tagħhom u jiddefinixxi "żona ristretta".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Agħti ħarsa lejn l-immaġni t-tajba.Inti taħseb li Cloudflare jimblokka biss guys ħżiena.Inti taħseb li Cloudflare huwa dejjem onlajn (qatt ma jinżel).Għandek taħseb li bots leġittimi u crawlers jistgħu indiċjaw il-websajt tiegħek.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Madankollu dawk mhumiex vera xejn.Cloudflare qed timblokka nies innoċenti mingħajr ebda raġuni.Cloudflare jista 'jinżel.Cloudflare jimblokka bots leġittimi.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  L-istess bħal kull servizz ta 'akkoljenza, Cloudflare mhuwiex perfett.Se tara din l-iskrin anke jekk is-server tal-oriġini jkun qed jaħdem tajjeb.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Int tassew taħseb li Cloudflare għandu 100% ħin ta 'xogħol?M'għandek l-ebda idea kemm-il darba Cloudflare jinżel.Jekk Cloudflare jinżel il-klijent tiegħek ma jistax jaċċessa l-websajt tiegħek. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Dan jissejjaħ b'referenza għall-Firewall il-Kbir taċ-Ċina li jagħmel xogħol komparabbli li jiffiltra ħafna bnedmin milli jaraw kontenut tal-web (jiġifieri kulħadd fiċ-Ċina kontinentali u nies barra).Filwaqt li fl-istess ħin dawk li mhumiex affettwati biex jaraw web dratically differenti, web ħielsa miċ-ċensura bħal immaġni ta '"raġel tat-tank" u l-istorja ta' "protesti fi Pjazza Tiananmen". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare għandu qawwa kbira.F’ċertu sens, huma jikkontrollaw dak li l-utent aħħari jara fl-aħħar mill-aħħar.Int imħolli milli tfittex il-websajt minħabba Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare jista 'jintuża għaċ-ċensura. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Ma tistax tara websajt cloudflared jekk qed tuża browser minuri li Cloudflare jista 'jaħseb li huwa bot (għax mhux ħafna nies jużawha). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Ma tistax tgħaddi din il-'kontroll tal-browser 'invażiv mingħajr ma tippermetti Javascript.Dan huwa ħela ta 'ħames (jew aktar) sekondi tal-ħajja siewja tiegħek. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare awtomatikament jimblokka robots / crawlers leġittimi bħal Google, Yandex, Yacy, u l-klijenti API.Cloudflare qed tissorvelja b'mod attiv il-komunità "bypass cloudflare" bl-intenzjoni li tkisser bots leġittimi ta 'riċerka. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare bl-istess mod jipprevjeni ħafna nies li għandhom konnettività ħażina bl-internet milli jaċċessaw il-websajts warajha (pereżempju, jistgħu jkunu wara 7+ saffi ta 'NAT jew jaqsmu l-istess IP, pereżempju Wifi pubbliċi) sakemm ma jsolvux CAPTCHAs ta' immaġni multipli.F'xi każijiet, dan jieħu 10 sa 30 minuta biex tissodisfa Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Fis-sena 2020 Cloudflare qaleb mir-Recaptcha ta ’Google għal hCaptcha hekk kif Google biħsiebu jiċċarġja għall-użu tiegħu.Cloudflare qallek li jimpurtahom il-privatezza tiegħek ("dan ​​jgħin biex jindirizza tħassib dwar il-privatezza") iżda din hija ovvjament gidba.Huwa kollox dwar il-flus.“HCaptcha tippermetti lill-websajts jagħmlu l-flus li jaqdu din id-domanda waqt li jimblokkaw l-bot u forom oħra ta 'abbuż” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Mill-perspettiva ta 'l-utent, dan ma jinbidilx ħafna. Int tkun sfurzat issolviha. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Ħafna bnedmin u software qed jiġu mblukkati minn Cloudflare kuljum. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare tdejqu ħafna nies madwar id-dinja.Agħti ħarsa lejn il-lista u aħseb jekk l-adozzjoni ta 'Cloudflare fis-sit tiegħek hix tajba għall-esperjenza tal-utent. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  X'inhu l-iskop tal-internet jekk ma tistax tagħmel dak li trid?Ħafna nies li jżuru l-websajt tiegħek ser ifittxu biss paġni oħra jekk ma jistgħux jgħabbu paġna web.Jista 'jkun li ma tkunx qed timblokka b'mod attiv kwalunkwe viżitatur, iżda l-firewall default ta' Cloudflare huwa strett biżżejjed biex jimblokka ħafna nies. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  M'hemm l-ebda mod kif tissolva l-captcha mingħajr ma tippermetti Javascript u Cookies.Cloudflare qed tużahom biex tagħmel firma tal-browser biex tidentifika lilek.Cloudflare jeħtieġ li tkun taf l-identità tiegħek biex tiddeċiedi jekk intix eliġibbli biex tkompli tfittex is-sit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Utenti Tor u utenti VPN huma wkoll vittma ta 'Cloudflare.Iż-żewġ soluzzjonijiet qed jintużaw minn ħafna nies li ma jistgħux jaffordjaw internet bla ċensura minħabba l-politika tal-pajjiż / korporazzjoni / netwerk tagħhom jew li jixtiequ jżidu saff żejjed biex jipproteġu l-privatezza tagħhom.Cloudflare qed jattakka bla mistħija lil dawk in-nies, u ġiegħelhom itfu s-soluzzjoni ta 'prokura tagħhom. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Jekk ma ppruvajtx Tor sa dan il-mument, aħna nħeġġiġkom biex tniżżel Tor Browser u żżur il-websajts favoriti tiegħek.Nissuġġerixxu li ma tidħolx fil-websajt tal-bank jew fil-paġna web tal-gvern jew li jtajru l-kont tiegħek. Uża VPN għal dawk il-websajts. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Inti tista 'tixtieq tgħid "Tor huwa illegali! L-utenti tat-Tor huma kriminali! Tor huwa ħażin! ". Le.Għandek mnejn tgħallimt dwar Tor mit-televiżjoni, tgħid li Tor jista 'jintuża biex tibbrawżja darknet u tinnegozja pistoli, drogi jew porn chid.Filwaqt li d-dikjarazzjoni hawn fuq hija vera li hemm ħafna websajt tas-suq minn fejn tista 'tixtri oġġetti bħal dawn, dawk is-siti ħafna drabi jidhru fuq clearnet ukoll.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor ġie żviluppat mill-Armata Amerikana, iżda t-Tor kurrenti huwa żviluppat mill-proġett Tor.Hemm ħafna nies u organizzazzjonijiet li jużaw Tor inklużi l-ħbieb futuri tiegħek.Allura, jekk qed tuża Cloudflare fuq il-websajt tiegħek qed timblokka bnedmin reali.Int titlef negozju ta 'ħbiberija u negozju potenzjali. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  U s-servizz DNS tagħhom, 1.1.1.1, qed jiffiltra wkoll lill-utenti milli jżuru l-websajt billi jirritornaw indirizz IP falz li huwa proprjetà ta 'Cloudflare, localhost IP bħal "127.0.0.x", jew sempliċement jirritornaw xejn. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS ikisser ukoll softwer onlajn minn app smartphone għal-logħba tal-kompjuter minħabba r-risposta falza tagħhom tad-DNS.Cloudflare DNS ma jistax jistaqsi xi websajts tal-bank. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  U hawn tista 'taħseb,<br>M’iniex nuża Tor jew VPN, għaliex għandi nieħu ħsieb?<br>Jiena nafda s-suq ta 'Cloudflare, għaliex għandi nieħu ħsieb<br>Il-websajt tiegħi hija https għaliex għandi nieħu ħsieb | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Jekk iżżur websajt li tuża Cloudflare, tkun qed taqsam l-informazzjoni tiegħek mhux biss lis-sid tal-websajt iżda wkoll lil Cloudflare.Dan huwa kif taħdem il-prokura b'lura. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Huwa impossibbli li tiġi analizzata mingħajr ma jiġi decriptat it-traffiku TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare jaf id-dejta kollha tiegħek bħal password mhux maħdum. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed jista 'jiġri f'kull ħin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Il-https ta ’Cloudflare qatt ma huma tarf sa tarf. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Int verament trid taqsam id-dejta tiegħek ma 'Cloudflare, u wkoll ma' aġenzija ta '3 ittri? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Il-profil onlajn ta 'utent tal-Internet huwa "prodott" li l-gvern u l-kumpaniji l-kbar tat-teknoloġija jridu jixtru. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Id-Dipartiment tas-Sigurtà Interna tal-Istati Uniti qal:<br><br>Għandek xi idea kemm hi siewja d-dejta li għandek? Hemm xi mod kif tbiegħna dik id-dejta?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare joffri wkoll servizz VPN B'XEJN imsejjaħ "Cloudflare Warp".Jekk tużah, il-konnessjonijiet kollha tal-smartphone tiegħek (jew tal-kompjuter tiegħek) jintbagħtu lis-servers Cloudflare.Cloudflare tista 'tkun taf liema websajt qrajt, liema kumment stazzjonajt, ma' min tkellimt, eċċ.Int volontarju li tagħti l-informazzjoni kollha tiegħek lil Cloudflare.Jekk taħseb "Int qed tiċċajta? Cloudflare huwa sigur. " mela trid titgħallem kif taħdem il-VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare qal li s-servizz VPN tagħhom jagħmel l-internet tiegħek malajr.Imma VPN tagħmel il-konnessjoni tal-internet tiegħek aktar bil-mod mill-konnessjoni eżistenti tiegħek. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Inti tista 'tkun taf diġà dwar l-iskandlu tal-PRISM.Huwa veru li AT&T iħallik lill-NSA tikkopja d-dejta kollha tal-internet għas-sorveljanza. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Ejja ngħidu li qed taħdem fl-NSA u li trid il-profil tal-internet ta 'kull ċittadin.Taf li ħafna minnhom qed jafdaw bl-addoċċ Cloudflare u jużawha - gateway ċentralizzat wieħed biss - biex jip prokuraw il-konnessjoni tas-server tal-kumpanija tagħhom (SSH / RDP), websajt personali, websajt taċ-chat, websajt tal-forum, websajt tal-bank, websajt tal-assigurazzjoni, magna tat-tiftix, membru sigriet websajt unika, websajt tal-irkant, xiri, websajt tal-vidjow, websajt NSFW, u websajt illegali.Int taf ukoll li jużaw is-servizz DNS ta 'Cloudflare ("1.1.1.1") u s-servizz VPN ("Cloudflare Warp") għal "Sikura! Aktar mgħaġġel! Aħjar! " Esperjenza fl-internet.L-għaqda tagħhom mal-indirizz IP tal-utent, il-marki tas-swaba tal-browser, il-cookies u l-ID RAY se jkunu utli biex jinbena l-profil onlajn tal-mira. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Int trid id-dejta tagħhom. X'ser taghmel? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare huwa pot tal-għasel.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Għasel b'xejn għal kulħadd. Xi kordi mehmuża.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Tużax Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Iddeċentralizza l-internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Jekk jogħġbok kompli l-paġna li jmiss:  "[Etika Cloudflare](mt.ethics.md)"

---

<details>
<summary>_ikklikkja fija_

## Dejta u Aktar Informazzjoni
</summary>


Dan ir-repożitorju huwa lista ta 'websajts li huma wara "Il-Great Cloudwall", li jimblokka l-utenti Tor u CDNs oħra.


**Dejta**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Utenti ta 'Cloudflare](../cloudflare_users/)
* [Dominji Cloudflare](../cloudflare_users/domains/)
* [Utenti CDN mhux Cloudflare](../not_cloudflare/)
* [Utenti ta 'kontra t-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Iktar informazzjoni**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Niżżel: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * L-eBook oriġinali (ePUB) ġie mħassar minn BookRix GmbH minħabba ksur tad-drittijiet tal-awtur ta 'materjal CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Il-biljett kien vandalizzat tant drabi.
  * [Imħassar mill-Proġett Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Ara l-biljett 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [L-aħħar biljett tal-arkivju 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_ikklikkja fija_

## X'tista tagħmel?
</summary>

* [Aqra l-lista tal-azzjonijiet rakkomandati tagħna u aqsamha mal-ħbiebek.](../ACTION.md)

* [Aqra leħen ta 'utent ieħor u ikteb il-ħsibijiet tiegħek.](../PEOPLE.md)

* Fittex xi ħaġa: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Aġġorna l-lista tad-domain: [Elenka l-istruzzjonijiet](../INSTRUCTION.md).

* [Żid Cloudflare jew avveniment relatat mal-proġett mal-istorja.](../HISTORY.md)

* [Ipprova u ikteb Għodda / Skript ġdid.](../tool/)

* [Hawn xi PDF / ePUB biex taqra.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Dwar kontijiet foloz

Crimeflare taf dwar l-eżistenza ta 'kontijiet foloz li jimponu l-kanali uffiċjali tagħna, kemm jekk huma Twitter, Facebook, Patreon, OpenCollective, Villages eċċ.
**Aħna qatt ma nitolbu l-email tiegħek.
Aħna qatt ma nitolbu ismek.
Aħna qatt ma nitolbu l-identità tiegħek.
Aħna qatt ma nistaqsu l-post tiegħek.
Aħna qatt ma nitolbu d-donazzjoni tiegħek.
Aħna qatt ma nitolbu r-reviżjoni tiegħek.
Aħna qatt ma nitolbok issegwi fuq il-midja soċjali.
Aħna qatt ma nitolbu l-midja soċjali tiegħek.**

# MA TIDDEFIDIXXI L-KONTIJIET FAKE.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)