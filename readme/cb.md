# Ang Dakong Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Hunong ang Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "Ang Dakong Cloudwall" mao ang Cloudflare Inc., nga kompanya sa Estados Unidos.Naghatag kini mga serbisyo sa CDN (content delivery network), DDoS mitigation, seguridad sa Internet, ug gipang-apod-apod nga mga serbisyo sa DNS (domain name server).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Ang Cloudflare mao ang labing kadaghan sa MITM proxy (reverse proxy) sa tibuuk kalibutan.Ang Cloudflare nanag-iya labaw pa sa 80% sa bahin sa merkado sa CDN ug ang ihap sa mga naggamit sa cloudflare matag adlaw.Gipalapad nila ang ilang network sa kapin sa 100 ka mga nasud.Nag-alagad ang Cloudflare sa daghang web traffic kaysa sa panagsama sa Twitter, Amazon, Apple, Instagram, Bing & Wikipedia.Ang Cloudflare nagtanyag libre nga plano ug daghang mga tawo ang naggamit niini imbis nga i-configure sa husto ang ilang mga server.Gibaligya nila ang pagkapribado tungod sa kasayon.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Ang Cloudflare naglingkod sa taliwala nimo ug gigikanan sa webserver, nga naglihok sama sa usa ka ahente patrol agent.Dili ka makakonektar sa imong gipili nga padulngan.Nakakonekta ka sa Cloudflare ug ang tanan nimo nga kasayuran gi-decryption ug gitugyan sa langaw. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Gitugotan sa tagdumala sa webserver nga gigikanan ang ahente - Cloudflare - nga magdesisyon kung kinsa ang maka-access sa ilang "kabtangan sa web" ug ipasabut ang "higpit nga lugar".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Tan-awa ang husto nga imahe.Naghunahuna ka nga ang Cloudflare block dili maayo nga mga tawo.Naghunahuna ka nga ang Cloudflare kanunay nga online (dili gyud moadto).Maghunahuna ka nga ang mga lehitimo nga bot ug ang mga crawler mahimong mag-indeks sa imong website.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Bisan pa, dili kana tinuod.Gipugngan sa Cloudflare ang mga inosenteng tawo nga wala’y hinungdan.Ang Cloudflare mahimo nga manaog.Gibabagan sa Cloudflare ang mga lehitimo nga bot.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Sama sa bisan unsang serbisyo sa pag-host, dili perpekto ang Cloudflare.Makita nimo kini nga screen bisan kung ang gigikanan nga server nagtrabaho nga maayo.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Naghunahuna ba kamo nga ang Cloudflare adunay 100% nga uptime?Wa nimo mahibal-an kung pila ka beses nga manaog ang Cloudflare.Kung ang Cloudflare manaog ang imong kustomer dili maka-access sa imong website. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Gitawag kini kini sa pagtumong sa Great Firewall sa China nga adunay usa ka susama nga trabaho sa pagsala sa daghang mga tawo gikan sa pagtan-aw sa sulud sa web (ie ang tanan sa mainland China ug mga tawo sa gawas).Samtang sa samang higayon kadtong wala maapektuhan makakita sa lainlain nga web, usa ka web nga wala’y censorship sama sa usa ka imahen sa "tank man" ug ang kasaysayan sa "Tiananmen Square protesta". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Ang Cloudflare adunay daghang gahum.Sa usa ka pagkasabut, kontrolado nila kung unsa ang katapusan nga nakita sa tiggamit.Gipugngan ka gikan sa pag-browse sa website tungod sa Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Ang Cloudflare mahimong magamit sa pag-censorship. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Dili nimo mahimo tan-awon ang website nga cloudflared kung mogamit ka nga menor de edad nga browser nga mahimo hunahunaon sa Cloudflare nga kini usa ka bot (tungod kay dili daghang mga tawo ang mogamit niini). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Dili nimo maipasa ang nagsasamak nga "tseke sa browser" nga wala pagpahanas sa Javascript.Kini usa ka basura sa lima (o daghan pa) nga mga segundo sa imong bililhon nga kinabuhi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Ang Cloudflare awtomatiko nga gibabagan ang mga legit robots / crawler sama sa mga kliyente sa Google, Yandex, Yacy, ug API.Ang Cloudflare aktibo nga nag-monitor sa "bypass cloudflare" nga komunidad nga adunay katuyoan nga mabungkag ang mga bot sa panukiduki. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Ang Cloudflare parehas nga nagpugong sa daghang mga tawo nga adunay dili maayo nga koneksyon sa internet gikan sa pag-access sa mga website sa luyo niini (pananglitan, mahimo sila nga sa likod sa 7+ mga layer sa NAT o pagpaambit sa parehas nga IP, pananglitan sa publiko nga Wifi) gawas kung masulbad nila ang daghang mga imahe sa CAPTCHA.Sa pipila ka mga kaso, kini molungtad sa 10 hangtod 30 minuto aron matagbaw ang Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Sa tuig 2020 ang Cloudflare gibalhin gikan sa Recaptcha sa Google ngadto sa hCaptcha samtang ang Google nagtinguha nga maningil alang sa paggamit niini.Giingnan ka sa Cloudflare nga giatiman nila ang imong privacy ("makatabang kini nga matubag ang usa ka kabalaka sa privacy") apan kini klaro nga bakak.Naa tanan bahin sa kuwarta."Gitugotan sa hCaptcha ang mga website nga makagasto sa pag-alagad sa kini nga panginahanglan samtang gibabagan ang mga bot ug uban pang mga porma sa pag-abuso" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Sa panglantaw sa tiggamit, dili kini mabag-o. Napilitan ka sa pagsulbad niini. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Daghang mga tawo ug software ang gibabagan sa Cloudflare matag adlaw. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Gikulbaan sa Cloudflare ang daghang mga tawo sa tibuuk kalibutan.Tan-awa ang lista ug hunahunaa kung ang pagsagop sa Cloudflare sa imong site maayo alang sa kasinatian sa gumagamit. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Unsa ang katuyoan sa internet kung dili nimo mahimo ang imong gusto?Kadaghanan sa mga tawo nga mobisita sa imong website mangita lang sa ubang mga panid kung dili sila maka-load sa usa ka webpage.Mahimo nga dili ka aktibo nga nagbabag sa bisan kinsa nga mga bisita, apan ang default nga firewallare sa Cloudflare istrikto nga igo aron mapugngan ang daghang mga tawo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Wala’y paagi nga masulbad ang captcha kung wala gitugot ang Javascript ug Cookies.Gigamit sila sa Cloudflare aron makahimo sila usa ka pirma sa browser aron maila ka.Kinahanglan mahibal-an sa Cloudflare ang imong identidad aron makadesisyon kung ikaw angayan ka ba nga magpadayon sa pag-browse sa site. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Ang mga tiggamit sa Tor ug mga gumagamit sa VPN biktima usab sa Cloudflare.Ang parehas nga solusyon gigamit sa daghang mga tawo nga dili makabayad sa internet nga wala’y bayad tungod sa ilang nasud / korporasyon / palisiya sa network o gusto nga magdugang dugang nga layer aron mapanalipdan ang ilang privacy.Wala maulaw ang pag-atake sa Cloudflare sa mga tawo, nga gipugos nila nga isalikway ang ilang proxy solution. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Kung wala nimo pagsulay si Tor hangtod niining panahona, gidasig ka namon nga mag-download sa Tor Browser ug bisitahan ang imong mga paboritong website.Gisugyot namon kanimo nga dili mag-login sa imong website sa bangko o webpage sa gobyerno o i-flag ang imong account. Paggamit VPN alang sa mga website. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Tingali gusto nimong isulti nga “Tor ang illegal! Mga tiggamit sa Tor mga kriminal! Daotan ang Tor! ". Dili.Tingali nahibal-an nimo ang bahin sa Tor gikan sa telebisyon, nag-ingon nga ang Tor mahimong magamit sa pag-browse sa darknet ug mga baril sa patigayon, mga droga o chid porn.Samtang ang pahayag sa ibabaw tinuod nga adunay daghang website sa merkado diin mahimo nimo mapalit ang ingon nga mga butang, kana nga mga site kanunay nga makita usab sa clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Ang Tor naugmad sa US Army, apan ang karon nga Tor naugmad sa proyekto nga Tor.Adunay daghang mga tawo ug mga organisasyon nga naggamit sa Tor lakip na ang imong umaabot nga mga higala.Mao nga, kung naggamit ka Cloudflare sa imong website gibabagan nimo ang tinuud nga mga tawo.Mawala ang imong mahimo nga panaghigalaay ug kasabutan sa negosyo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Ug ang ilang serbisyo sa DNS, 1.1.1.1, usab ang pagsala sa mga gumagamit gikan sa pagbisita sa website pinaagi sa pagbalik sa peke nga IP address nga gipanag-iya sa Cloudflare, localhost IP sama sa "127.0.0.x", o ibalik ra ang bisan unsa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Ang Cloudflare DNS usab nagbungkag sa online software gikan sa smartphone app hangtod sa dula sa kompyuter tungod sa ilang peke nga tubag sa DNS.Ang Cloudflare DNS dili makapangutana sa pipila ka mga website sa bangko. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Ug tingali maghunahuna ka,<br>Wala ako naggamit sa Tor o VPN, nganong mag-amping ako?<br>Nagsalig ako sa pagpamaligya sa Cloudflare, nganong mag-atiman ako<br>Ang akong website mao ang https ngano nga kinahanglan ko mag-atiman | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Kung nagbisita ka sa website nga gigamit ang Cloudflare, gipaambit nimo ang imong kasayuran dili lamang sa tag-iya sa website kondili usab ang Cloudflare.Ingon niini naglihok ang reverse proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Imposible nga mag-analisar kung wala gi-deculate ang trapiko sa TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Nahibal-an sa Cloudflare ang tanan nimong data sama sa hilaw nga password. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Mahitabo ang panganod bisan unsang orasa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Ang Cloudpsare ni https wala’y katapusan hangtod sa katapusan. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Gusto ba nimong ipakigbahin ang imong datos sa Cloudflare, ug usab 3 nga sulat sa ahensya? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Ang profile sa online nga tiggamit sa Internet usa ka “produkto” nga gustong paliton sa gobyerno ug mga dagkong kompaniya sa tech. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Ang Department of Homeland Security sa Estados Unidos miingon:<br><br>Aduna ka bay ideya kung unsa ka hinungdanon ang datos nga naa nimo? Aduna bay paagi aron ibaligya kanamo ang datos?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Nagtanyag usab ang Cloudflare FREE serbisyo sa VPN nga gitawag nga "Cloudflare Warp".Kung gigamit nimo kini, ang tanan nga mga koneksyon sa imong smartphone (o imong computer) gipadala sa mga server sa Cloudflare.Mahibal-an sa Cloudflare kung unsang website ang imong nabasa, kung unsang komentaryo ang imong gi-post, kinsa imong nakigsulti, ug uban paBoluntaryo nga imong gihatag ang tanan nimo nga kasayuran sa Cloudflare.Kung naghunahuna ka nga “Nagselos ka? Luwas ang Cloudflare. ” nan kinahanglan nimo nga mahibal-an kung giunsa ang paglihok sa VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Giingon ni Cloudflare nga ang ilang serbisyo sa VPN naghimo sa imong internet nga paspas.Bisan pa ang VPN naghimo sa imong koneksyon sa internet nga hinay kaysa imong naa nga koneksyon. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Tingali nahibal-an na nimo ang iskandalo sa PRISM.Tinuod nga ang AT&T nagtugot sa NSA nga kopyahon ang tanan nga datos sa internet alang sa pagpaniid. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Ingnon ta nga nagtrabaho ka sa NSA, ug gusto nimo ang matag profile sa internet sa mga lungsuranon.Nahibal-an nimo ang kadaghanan sa mga kini nga wala’y pagsalig sa Cloudflare ug gigamit kini - usa ra ang sentralisado nga gateway - aron pag-proxy sa ilang koneksyon sa kumpanya sa kompaniya (SSH / RDP), personal nga website, chat website, forum website, website sa bangko, website sa paniguro, search engine, tinago nga miyembro -Bahin nga website, subasta sa website, subasta, pagpamaligya, website sa video, website sa NSFW, ug ilegal nga website.Nahibal-an mo usab nga gigamit nila ang serbisyo sa Cloudflare's DNS ("1.1.1.1") ug serbisyo sa VPN ("Cloudflare Warp") alang sa "Secure! Mas tulin! Mas maayo! ” kasinatian sa internet.Ang paghiusa kanila sa IP address sa gumagamit, fingerprint sa browser, cookies ug RAY-ID mahimong mapuslanon sa pagtukod sa profile sa target sa target. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Gusto nimo ang ilang datos. Unsa kaha imong buhaton? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Ang Cloudflare usa ka honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Libre nga dugos alang sa tanan. Ang pila nga mga pisi nga gilakip.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Ayaw paggamit Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Deskripsyon sa internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Palihug magpadayon sa sunod nga panid:  "[Mga Etika sa Cloudflare](cb.ethics.md)"

---

<details>
<summary>_pag-klik kanako_

## Data ug Dugang nga Impormasyon
</summary>


Ang kini nga repositoryo usa ka lista sa mga website nga sa luyo sa "The Great Cloudwall", pag-block sa mga tiggamit sa Tor ug uban pang mga CDN.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Mga Gumagamit sa Cloudflare](../cloudflare_users/)
* [Mga Pundo sa Cloudflare](../cloudflare_users/domains/)
* [Mga tiggamit nga Non-Cloudflare CDN](../not_cloudflare/)
* [Ang mga tiggamit nga Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Dugang nga Impormasyon**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Pag-download: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Ang orihinal nga eBook (ePUB) giwagtang sa BookRix GmbH tungod sa paglapas sa copyright sa materyal nga CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Daghang beses nga gi-vandalize ang ticket.
  * [Giwagtang sa Tor Project.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Makita ang tiket 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Katapusan nga tiket sa archive 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_pag-klik kanako_

## Unsay imong mahimo?
</summary>

* [Basaha ang among lista sa girekomenda nga mga aksyon ug ipaambit kini sa imong mga higala.](../ACTION.md)

* [Basaha ang tingog sa ubang tiggamit ug isulat ang imong mga hunahuna.](../PEOPLE.md)

* Pagpangita usa ka butang: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Pag-update sa lista sa domain: [Ilista ang mga panudlo](../INSTRUCTION.md).

* [Pagdugang Cloudflare o hitabo nga may kalabutan sa proyekto sa kasaysayan.](../HISTORY.md)

* [Sulayi & pagsulat bag-ong Tool / Script.](../tool/)

* [Niini ang pila ka PDF / ePUB nga mabasa.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Bahin sa mga peke nga account

Nahibal-an sa Crimeflare ang mahitungod sa paglungtad sa mga peke nga mga account nga nag-ingon sa among mga opisyal nga kanal, mao ra ang Twitter, Facebook, Patreon, OpenCollective, Villages etc.
**Wala gyud kami mangutana sa imong email.
Dili kami mangutana sa imong ngalan.
Wala gyud kami mangutana sa imong pagkatawo.
Wala gyud kami mangutana sa imong lokasyon.
Wala gyud kami mangayo sa imong donasyon.
Wala kami gipangayo sa imong pagsusi.
Dili kami hangyoon nga sundon nimo sa social media.
Wala gyud kami mangutana sa imong social media.**

# AYAW PAGTUON NGA MAKAPAKAON SA AKLAT.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)