# Le Grand Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Arrêtez Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  «The Great Cloudwall» est Cloudflare Inc., la société américaine.Il fournit des services CDN (réseau de distribution de contenu), l'atténuation DDoS, la sécurité Internet et des services DNS (serveur de noms de domaine) distribués.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare est le plus grand proxy MITM (proxy inverse) au monde.Cloudflare détient plus de 80% de la part de marché CDN et le nombre d'utilisateurs de cloudflare augmente chaque jour.Ils ont étendu leur réseau à plus de 100 pays.Cloudflare sert plus de trafic Web que Twitter, Amazon, Apple, Instagram, Bing et Wikipedia réunis.Cloudflare propose un plan gratuit et de nombreuses personnes l'utilisent au lieu de configurer correctement leurs serveurs.Ils ont troqué la confidentialité plutôt que la commodité.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare se situe entre vous et le serveur Web d'origine, agissant comme un agent de patrouille aux frontières.Vous ne pouvez pas vous connecter à la destination choisie.Vous vous connectez à Cloudflare et toutes vos informations sont décryptées et transmises à la volée. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  L'administrateur du serveur Web d'origine a permis à l'agent - Cloudflare - de décider qui peut accéder à leur «propriété Web» et de définir «zone restreinte».  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Jetez un œil à la bonne image.Vous penserez que Cloudflare ne bloque que les méchants.Vous penserez que Cloudflare est toujours en ligne (ne tombe jamais en panne).Vous penserez que les robots et les robots d'exploration légitimes peuvent indexer votre site Web.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Cependant, ce n'est pas du tout vrai.Cloudflare bloque des personnes innocentes sans raison.Cloudflare peut tomber.Cloudflare bloque les robots légitimes.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Comme tout service d'hébergement, Cloudflare n'est pas parfait.Vous verrez cet écran même si le serveur d'origine fonctionne bien.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Pensez-vous vraiment que Cloudflare a 100% de disponibilité?Vous ne savez pas combien de fois Cloudflare tombe en panne.Si Cloudflare tombe en panne, votre client ne peut pas accéder à votre site Web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  On l'appelle ainsi en référence au Grand Pare-feu de Chine qui fait un travail comparable en empêchant de nombreux humains de voir le contenu Web (c'est-à-dire tout le monde en Chine continentale et les gens à l'extérieur).Alors qu'en même temps, ceux qui ne sont pas touchés voient un réseau radicalement différent, un réseau sans censure comme une image de «l'homme de char» et l'histoire des «manifestations de la place Tiananmen». | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare possède une grande puissance.Dans un sens, ils contrôlent ce que l'utilisateur final voit finalement.Vous ne pouvez pas naviguer sur le site Web à cause de Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare peut être utilisé pour la censure. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Vous ne pouvez pas afficher le site Web cloudflared si vous utilisez un navigateur mineur que Cloudflare peut penser qu'il s'agit d'un bot (car peu de gens l'utilisent). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Vous ne pouvez pas passer cette «vérification du navigateur» invasive sans activer Javascript.C'est une perte de cinq (ou plus) secondes de votre précieuse vie. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare bloque également automatiquement les robots / crawlers légitimes tels que les clients Google, Yandex, Yacy et API.Cloudflare surveille activement la communauté «contourner le cloudflare» dans le but de briser les robots de recherche légitimes. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare empêche de la même manière de nombreuses personnes qui ont une mauvaise connectivité Internet d'accéder aux sites Web derrière elle (par exemple, elles pourraient être derrière plus de 7 couches de NAT ou partager la même IP, par exemple le Wifi public) à moins qu'elles ne résolvent plusieurs CAPTCHA d'images.Dans certains cas, cela prendra 10 à 30 minutes pour satisfaire Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  En 2020, Cloudflare est passé de Recaptcha de Google à hCaptcha, car Google a l'intention de facturer son utilisation.Cloudflare vous a dit qu'ils se soucient de votre vie privée («cela aide à résoudre un problème de confidentialité»), mais c'est évidemment un mensonge.Tout est question d’argent."HCaptcha permet aux sites Web de gagner de l'argent en répondant à cette demande tout en bloquant les bots et autres formes d'abus" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Du point de vue de l'utilisateur, cela ne change pas grand-chose. Vous êtes obligé de le résoudre. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  De nombreux humains et logiciels sont bloqués par Cloudflare chaque jour. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare agace de nombreuses personnes dans le monde.Jetez un œil à la liste et demandez-vous si l'adoption de Cloudflare sur votre site est bonne pour l'expérience utilisateur. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  À quoi sert Internet si vous ne pouvez pas faire ce que vous voulez?La plupart des internautes qui visitent votre site Web rechercheront simplement d’autres pages s’ils ne parviennent pas à en charger une.Vous ne bloquez peut-être aucun visiteur, mais le pare-feu par défaut de Cloudflare est suffisamment strict pour bloquer de nombreuses personnes. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Il n'y a aucun moyen de résoudre le captcha sans activer Javascript et les cookies.Cloudflare les utilise pour créer une signature de navigateur afin de vous identifier.Cloudflare a besoin de connaître votre identité pour décider si vous êtes éligible pour continuer à naviguer sur le site. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Les utilisateurs de Tor et les utilisateurs de VPN sont également victimes de Cloudflare.Les deux solutions sont utilisées par de nombreuses personnes qui ne peuvent pas se permettre une connexion Internet non censurée en raison de la politique de leur pays / entreprise / réseau ou qui souhaitent ajouter une couche supplémentaire pour protéger leur vie privée.Cloudflare attaque sans vergogne ces personnes, les forçant à désactiver leur solution proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Si vous n’avez pas essayé Tor jusqu’à ce moment, nous vous encourageons à télécharger le navigateur Tor et à visiter vos sites Web préférés.Nous vous suggérons de ne pas vous connecter au site Web de votre banque ou à la page Web du gouvernement, sinon ils marqueront votre compte. Utilisez VPN pour ces sites Web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Vous voudrez peut-être dire: «Tor est illégal! Les utilisateurs de Tor sont criminels! Tor est mauvais! ". Non.Vous pourriez avoir entendu parler de Tor à la télévision, en disant que Tor peut être utilisé pour parcourir le darknet et échanger des armes à feu, de la drogue ou du porno chid.Bien que la déclaration ci-dessus soit vraie, il existe de nombreux sites Web du marché sur lesquels vous pouvez acheter de tels articles, mais ces sites apparaissent souvent également sur clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor a été développé par l'armée américaine, mais l'actuel Tor est développé par le projet Tor.De nombreuses personnes et organisations utilisent Tor, y compris vos futurs amis.Donc, si vous utilisez Cloudflare sur votre site Web, vous bloquez de vrais humains.Vous perdrez votre amitié potentielle et votre affaire. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Et leur service DNS, 1.1.1.1, empêche également les utilisateurs de visiter le site Web en renvoyant une fausse adresse IP appartenant à Cloudflare, une adresse IP localhost telle que «127.0.0.x», ou simplement ne renvoyant rien. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS rompt également les logiciels en ligne de l'application pour smartphone au jeu sur ordinateur en raison de leur fausse réponse DNS.Cloudflare DNS ne peut pas interroger certains sites Web bancaires. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Et ici vous pourriez penser,<br>Je n'utilise ni Tor ni VPN, pourquoi devrais-je m'en soucier?<br>Je fais confiance au marketing Cloudflare, pourquoi devrais-je m'en soucier<br>Mon site Web est https pourquoi devrais-je m'en soucier | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Si vous visitez un site Web utilisant Cloudflare, vous partagez vos informations non seulement avec le propriétaire du site Web, mais également avec Cloudflare.C'est ainsi que fonctionne le proxy inverse. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Il est impossible d'analyser sans décrypter le trafic TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare connaît toutes vos données telles que le mot de passe brut. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed peut arriver à tout moment. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Le https de Cloudflare n'est jamais de bout en bout. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Voulez-vous vraiment partager vos données avec Cloudflare, mais aussi avec une agence 3 lettres? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Le profil en ligne de l'internaute est un «produit» que le gouvernement et les grandes entreprises technologiques veulent acheter. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Le département américain de la Sécurité intérieure a déclaré:<br><br>Avez-vous une idée de la valeur des données dont vous disposez? Pouvez-vous nous vendre ces données d'une manière ou d'une autre?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare propose également un service VPN GRATUIT appelé «Cloudflare Warp».Si vous l'utilisez, toutes les connexions de votre smartphone (ou de votre ordinateur) sont envoyées aux serveurs Cloudflare.Cloudflare peut savoir quel site Web vous avez lu, quel commentaire vous avez publié, à qui vous avez parlé, etc.Vous êtes volontaire et donnez toutes vos informations à Cloudflare.Si vous pensez «vous plaisantez? Cloudflare est sécurisé. » alors vous devez apprendre comment fonctionne le VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare a déclaré que son service VPN rend votre Internet rapide.Mais le VPN rend votre connexion Internet plus lente que votre connexion existante. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Vous connaissez peut-être déjà le scandale PRISM.Il est vrai qu'AT & T permet à la NSA de copier toutes les données Internet à des fins de surveillance. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Disons que vous travaillez à la NSA et que vous voulez le profil Internet de chaque citoyen.Vous savez que la plupart d'entre eux font aveuglément confiance à Cloudflare et l'utilisent - une seule passerelle centralisée - pour proxy leur connexion au serveur d'entreprise (SSH / RDP), site Web personnel, site de chat, site de forum, site de banque, site d'assurance, moteur de recherche, membre secret -uniquement site Web, site Web d'enchères, magasinage, site Web vidéo, site Web NSFW et site Web illégal.Vous savez également qu'ils utilisent le service DNS de Cloudflare ("1.1.1.1") et le service VPN ("Cloudflare Warp") pour "Secure! Plus rapide! Mieux!" expérience Internet.Les combiner avec l'adresse IP, l'empreinte digitale du navigateur, les cookies et le RAY-ID de l'utilisateur sera utile pour créer le profil en ligne de la cible. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Vous voulez leurs données. Que vas-tu faire? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare est un pot de miel.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Du miel gratuit pour tout le monde. Certaines conditions sont attachées.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **N'utilisez pas Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Décentraliser Internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Veuillez continuer à la page suivante:  "[Éthique de Cloudflare](fr.ethics.md)"

---

<details>
<summary>_clique moi_

## Données et plus d'informations
</summary>


Ce référentiel est une liste de sites Web derrière "The Great Cloudwall", bloquant les utilisateurs de Tor et d'autres CDN.


**Les données**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Utilisateurs Cloudflare](../cloudflare_users/)
* [Domaines Cloudflare](../cloudflare_users/domains/)
* [Utilisateurs CDN non Cloudflare](../not_cloudflare/)
* [Utilisateurs Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Plus d'information**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Télécharger: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Le livre électronique original (ePUB) a été supprimé par BookRix GmbH en raison d'une violation du droit d'auteur du matériel CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Le billet a été vandalisé tant de fois.
  * [Supprimé par le projet Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Voir le billet 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Dernier ticket d'archive 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_clique moi_

## Que pouvez-vous faire?
</summary>

* [Lisez notre liste d'actions recommandées et partagez-la avec vos amis.](../ACTION.md)

* [Lisez la voix des autres utilisateurs et écrivez vos pensées.](../PEOPLE.md)

* Rechercher quelque chose: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Mettre à jour la liste des domaines: [Liste des instructions](../INSTRUCTION.md).

* [Ajoutez Cloudflare ou un événement lié au projet à l'historique.](../HISTORY.md)

* [Essayez et écrivez un nouvel outil / script.](../tool/)

* [Voici quelques PDF / ePUB à lire.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### À propos des faux comptes

Crimeflare connaît l'existence de faux comptes se faisant passer pour nos canaux officiels, que ce soit Twitter, Facebook, Patreon, OpenCollective, Villages, etc.
**Nous ne demandons jamais votre email.
Nous ne demandons jamais votre nom.
Nous ne demandons jamais votre identité.
Nous ne demandons jamais votre emplacement.
Nous ne demandons jamais votre don.
Nous ne demandons jamais votre avis.
Nous ne vous demandons jamais de suivre sur les réseaux sociaux.
Nous ne demandons jamais à vos médias sociaux.**

# NE FAITES PAS CONFIANCE AUX FAUX COMPTES.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)