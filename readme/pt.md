# The Great Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Parar Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  “The Great Cloudwall” é a Cloudflare Inc., empresa dos Estados Unidos.Ela está fornecendo serviços de CDN (rede de distribuição de conteúdo), mitigação de DDoS, segurança da Internet e serviços de DNS distribuído (servidor de nome de domínio).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare é o maior proxy MITM (proxy reverso) do mundo.A Cloudflare detém mais de 80% da participação no mercado de CDN e o número de usuários do cloudflare está crescendo a cada dia.Eles expandiram sua rede para mais de 100 países.Cloudflare atende mais tráfego da web do que Twitter, Amazon, Apple, Instagram, Bing e Wikipedia combinados.Cloudflare está oferecendo plano gratuito e muitas pessoas estão usando em vez de configurar seus servidores corretamente.Eles trocaram privacidade por conveniência.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare fica entre você e o servidor da web de origem, agindo como um agente de patrulha de fronteira.Você não consegue se conectar ao destino escolhido.Você está se conectando ao Cloudflare e todas as suas informações estão sendo descriptografadas e entregues na hora. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  O administrador do servidor de origem permitiu ao agente - Cloudflare - decidir quem pode acessar sua “propriedade da web” e definir “área restrita”.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Dê uma olhada na imagem certa.Você vai pensar que o Cloudflare bloqueia apenas os bandidos.Você pensará que o Cloudflare está sempre online (nunca caia).Você pensará que bots e crawlers legítimos podem indexar seu site.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  No entanto, isso não é verdade.Cloudflare está bloqueando pessoas inocentes sem motivo.Cloudflare pode cair.Cloudflare bloqueia bots legítimos.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Assim como qualquer serviço de hospedagem, Cloudflare não é perfeito.Você verá essa tela mesmo se o servidor de origem estiver funcionando bem.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Você realmente acha que o Cloudflare tem 100% de disponibilidade?Você não tem ideia de quantas vezes o Cloudflare cai.Se o Cloudflare cair, seu cliente não poderá acessar seu site. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  É chamado assim em referência ao Grande Firewall da China, que faz um trabalho comparável de filtrar muitos humanos ao verem o conteúdo da web (ou seja, todos na China continental e pessoas de fora).Enquanto, ao mesmo tempo, aqueles não são afetados por ver uma teia draticamente diferente, uma teia livre de censura, como uma imagem de “homem tanque” e a história de “protestos na Praça Tiananmen”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare possui grande poder.Em certo sentido, eles controlam o que o usuário final vê.Você está impedido de navegar no site por causa do Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare pode ser usado para censura. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Você não pode ver o site cloudflared se você estiver usando um navegador menor que o Cloudflare pode pensar que é um bot (porque muitas pessoas não o usam). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Você não pode passar por essa “verificação do navegador” invasiva sem habilitar o Javascript.Isso é uma perda de cinco (ou mais) segundos de sua valiosa vida. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  O Cloudflare também bloqueia automaticamente robôs / rastreadores legítimos, como Google, Yandex, Yacy e clientes API.A Cloudflare está monitorando ativamente a comunidade de “contornar o cloudflare” com a intenção de quebrar bots de pesquisa legítimos. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  O Cloudflare evita que muitas pessoas com conectividade de Internet ruim acessem os sites por trás dele (por exemplo, eles podem estar atrás de 7 camadas de NAT ou compartilhando o mesmo IP, por exemplo Wifi público), a menos que resolvam vários CAPTCHAs de imagem.Em alguns casos, isso levará de 10 a 30 minutos para satisfazer o Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  No ano de 2020, a Cloudflare trocou o Recaptcha do Google pelo hCaptcha, já que o Google pretende cobrar por seu uso.Cloudflare disse que eles se preocupam com sua privacidade (“isso ajuda a resolver uma questão de privacidade”), mas isso é obviamente uma mentira.É tudo sobre dinheiro.“O hCaptcha permite que os sites ganhem dinheiro atendendo a essa demanda enquanto bloqueia bots e outras formas de abuso” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Da perspectiva do usuário, isso não muda muito. Você está sendo forçado a resolvê-lo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Muitos humanos e softwares estão sendo bloqueados pelo Cloudflare todos os dias. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare irrita muitas pessoas ao redor do mundo.Dê uma olhada na lista e pense se adotar o Cloudflare em seu site é bom para a experiência do usuário. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Qual é o propósito da Internet se você não pode fazer o que quer?A maioria das pessoas que visitam seu site irá apenas procurar outras páginas se não conseguirem carregar uma página da web.Você pode não estar bloqueando ativamente nenhum visitante, mas o firewall padrão do Cloudflare é estrito o suficiente para bloquear muitas pessoas. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Não há como resolver o captcha sem habilitar Javascript e Cookies.A Cloudflare os está usando para fazer uma assinatura do navegador para identificá-lo.A Cloudflare precisa saber sua identidade para decidir se você está qualificado para continuar navegando no site. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Os usuários do Tor e VPN também são vítimas do Cloudflare.Ambas as soluções estão sendo usadas por muitas pessoas que não podem pagar pela Internet sem censura devido à política de seu país / empresa / rede ou que desejam adicionar uma camada extra para proteger sua privacidade.O Cloudflare está atacando descaradamente essas pessoas, forçando-as a desligar sua solução de proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Se você não experimentou o Tor até agora, encorajamos você a baixar o navegador Tor e visitar seus sites favoritos.Sugerimos que você não faça login no site do seu banco ou na página do governo ou eles sinalizarão sua conta. Use VPN para esses sites. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Você pode querer dizer “Tor é ilegal! Os usuários do Tor são criminosos! Tor é ruim! ". Não.Você pode aprender sobre o Tor na televisão, dizendo que o Tor pode ser usado para navegar na darknet e trocar armas, drogas ou pornografia infantil.Embora seja verdade a afirmação acima de que existem muitos sites de mercado onde você pode comprar esses itens, esses sites costumam aparecer no clearnet também.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  O Tor foi desenvolvido pelo Exército dos EUA, mas o Tor atual é desenvolvido pelo projeto Tor.Existem muitas pessoas e organizações que usam o Tor, incluindo seus futuros amigos.Então, se você estiver usando Cloudflare em seu site, você está bloqueando humanos reais.Você perderá uma amizade potencial e negócios. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  E o serviço DNS, 1.1.1.1, também impede que os usuários visitem o site, retornando endereços IP falsos de propriedade do Cloudflare, IP localhost como “127.0.0.x” ou simplesmente não retorna nada. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS também quebra software online de aplicativo de smartphone para jogo de computador por causa de sua resposta DNS falsa.Cloudflare DNS não pode consultar alguns sites de bancos. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  E aqui você pode pensar,<br>Não estou usando Tor ou VPN, por que devo me importar?<br>Eu confio no marketing do Cloudflare, por que deveria me importar<br>Meu site é https, por que devo me importar | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Se você visitar um site que usa Cloudflare, estará compartilhando suas informações não apenas com o proprietário do site, mas também com o Cloudflare.É assim que funciona o proxy reverso. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  É impossível analisar sem descriptografar o tráfego TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare conhece todos os seus dados, como senha bruta. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed pode acontecer a qualquer momento. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  O https da Cloudflare nunca é ponta a ponta. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Você realmente deseja compartilhar seus dados com a Cloudflare e também com a agência de 3 letras? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  O perfil online do usuário da Internet é um "produto" que o governo e as grandes empresas de tecnologia desejam comprar. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Departamento de Segurança Interna dos EUA disse:<br><br>Você tem ideia de quão valiosos são os dados que você tem? Existe alguma maneira de nos vender esses dados?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  A Cloudflare também oferece um serviço VPN GRATUITO chamado “Cloudflare Warp".Se você usá-lo, todas as conexões do seu smartphone (ou computador) são enviadas para os servidores Cloudflare.A Cloudflare pode saber qual site você leu, que comentário postou, com quem conversou, etc.Você é voluntário ao fornecer todas as suas informações à Cloudflare.Se você pensa “Você está brincando? Cloudflare é seguro. ” então você precisa aprender como funciona a VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare disse que seu serviço VPN torna sua internet mais rápida.Mas a VPN torna sua conexão com a Internet mais lenta do que a conexão existente. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Você já deve saber sobre o escândalo do PRISM.É verdade que a AT&T permite que a NSA copie todos os dados da Internet para vigilância. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Digamos que você esteja trabalhando na NSA e queira o perfil de cada cidadão na Internet.Você sabe que a maioria deles confia cegamente no Cloudflare e o usa - apenas um gateway centralizado - para fazer proxy da conexão do servidor da empresa (SSH / RDP), site pessoal, site de bate-papo, site de fórum, site de banco, site de seguros, mecanismo de pesquisa, membro secreto - apenas site, site de leilão, compras, site de vídeo, site NSFW e site ilegal.Você também sabe que eles usam o serviço DNS da Cloudflare ("1.1.1.1") e o serviço VPN ("Cloudflare Warp") para “Secure! Mais rápido! Melhor!" experiência na Internet.Combiná-los com o endereço IP do usuário, impressão digital do navegador, cookies e RAY-ID será útil para construir o perfil online do alvo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Você quer seus dados. O que você vai fazer? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare é um honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Mel grátis para todos. Algumas cordas presas.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Não use Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Descentralize a Internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Por favor, continue na próxima página:  "[Ética Cloudflare](pt.ethics.md)"

---

<details>
<summary>_clique em mim_

## Dados e mais informações
</summary>


Este repositório é uma lista de sites que estão por trás do "The Great Cloudwall", bloqueando usuários do Tor e outros CDNs.


**Dados**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Usuários Cloudflare](../cloudflare_users/)
* [Domínios Cloudflare](../cloudflare_users/domains/)
* [Usuários que não são do Cloudflare CDN](../not_cloudflare/)
* [Usuários Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Mais Informações**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Baixar: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * O e-book original (ePUB) foi excluído pela BookRix GmbH devido à violação de direitos autorais do material CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * O ingresso foi vandalizado várias vezes.
  * [Excluído pelo Projeto Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Veja o bilhete 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Último tíquete de arquivo 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_clique em mim_

## O que você pode fazer?
</summary>

* [Leia nossa lista de ações recomendadas e compartilhe com seus amigos.](../ACTION.md)

* [Leia a voz de outro usuário e escreva suas idéias.](../PEOPLE.md)

* Procure algo: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Atualize a lista de domínios: [Instruções de lista](../INSTRUCTION.md).

* [Adicionar Cloudflare ou evento relacionado ao projeto ao histórico.](../HISTORY.md)

* [Experimente e escreva uma nova ferramenta / script.](../tool/)

* [Aqui estão alguns PDF / ePUB para ler.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Sobre contas falsas

Crimeflare sabe da existência de contas falsas se passando por nossos canais oficiais, seja Twitter, Facebook, Patreon, OpenCollective, Villages etc.
**Nunca pedimos seu e-mail.
Nunca perguntamos seu nome.
Nunca perguntamos sua identidade.
Nunca perguntamos sua localização.
Nunca pedimos sua doação.
Nós nunca pedimos sua revisão.
Nunca pedimos que você acompanhe nas redes sociais.
Nunca perguntamos às suas redes sociais.**

# NÃO CONFIE EM CONTAS FALSAS.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)