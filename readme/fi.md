# Suuri pilvaseinä


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Pysäytä pilvipallo


|  🖹  |  🖼 |
| --- | --- |
|  ”The Great Cloudwall” on USA: n Cloudflare Inc.Se tarjoaa CDN (sisällönjakeluverkko) -palveluita, DDoS-lieventämistä, Internet-tietoturvaa ja hajautettuja DNS (domain name server) -palveluita.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare on maailman suurin MITM-välityspalvelin (käänteinen välityspalvelin).Cloudflare omistaa yli 80% CDN: n markkinaosuudesta, ja pilvipallojen käyttäjien määrä kasvaa päivittäin.He ovat laajentaneet verkostoaan yli 100 maahan.Cloudflare palvelee enemmän verkkoliikennettä kuin Twitter, Amazon, Apple, Instagram, Bing ja Wikipedia yhdessä.Cloudflare tarjoaa ilmaisen suunnitelman, ja monet ihmiset käyttävät sitä sen sijaan, että määrittäisivät palvelimensa oikein.He vaihtoivat yksityisyyttä mukavuuden vuoksi.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare istuu sinun ja alkuperäisen verkkopalvelimen välissä toimimalla kuin rajavartiolaitos.Et voi muodostaa yhteyttä valittuun määränpäähän.Olet yhteydessä Cloudflareen ja kaikki tietosi puretaan ja luovutetaan lennossa. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Alkuperäisen verkkopalvelimen järjestelmänvalvoja salli edustajan - Cloudflaren - päättää, kuka voi käyttää heidän ”verkkoominaisuuttaan” ja määrittää ”rajoitetun alueen”.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Katso oikea kuva.Luulet Cloudflare estävän vain pahat pojat.Luulet Cloudflaren olevan aina online-tilassa (älä koskaan mene alas).Luulet legit-robotit ja indeksoijat indeksoivan verkkosivustosi.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Ne eivät kuitenkaan ole lainkaan totta.Pilvipallo estää viattomia ihmisiä ilman syytä.Pilvipallo voi laskea.Cloudflare estää legit botit.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Kuten mikä tahansa isännöintipalvelu, Cloudflare ei ole täydellinen.Näet tämän näytön, vaikka alkuperäispalvelin toimisi hyvin.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Luuletko todella, että Cloudflaressa on 100% käyttöaika?Sinulla ei ole aavistustakaan kuinka monta kertaa Cloudflare laskee.Jos Cloudflare laskee, asiakas ei voi käyttää verkkosivustoasi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Sitä kutsutaan tähän viitaten Kiinan suureen palomuuriin, joka tekee vastaavanlaisen työn suodattamalla monet ihmiset verkkosivun näkemisestä (ts. Kaikki Manner-Kiinassa ja ulkopuolella olevat ihmiset).Samanaikaisesti ne, joille ei vaikuteta näkevän drattisesti erilaista verkkoa, sensuurista vapaata verkkoa, kuten kuvaa "tankman miehestä" ja "Tiananmenin aukion mielenosoitusten historiaa". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Pilvipalolla on suuri voima.Tietyssä mielessä he hallitsevat sitä, mitä loppukäyttäjä lopulta näkee.Sinua ei voi selata verkkosivulla Cloudflaren vuoksi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Pilvipalloa voidaan käyttää sensuurissa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Et voi katsella pilvikatkaistua verkkosivustoa, jos käytät pienempää selainta, jonka Cloudflare voi ajatella olevan botti (koska monet ihmiset eivät käytä sitä). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Et voi hyväksyä tätä tunkeutuvaa “selaintarkistusta” ottamatta Javascriptiä käyttöön.Tämä on viiden (tai useamman) sekunnin tuhlaus arvokasta elämääsi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare estää myös automaattisesti lailliset robotit / indeksoijat, kuten Google, Yandex, Yacy ja API-asiakkaat.Cloudflare seuraa aktiivisesti ”ohittaa cloudflare” -yhteisöä tarkoituksenaan rikkoa legitiimi tutkimusrobotti. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare estää myös monia ihmisiä, joilla on huono Internet-yhteys, pääsemään sen takana oleville verkkosivustoille (esimerkiksi, he voivat olla vähintään 7 NAT-kerroksen takana tai jakaa samaa IP: tä, esimerkiksi julkista Wifiä), elleivät he ratkaise useita kuvan CAPTCHA-ratkaisuja.Joissakin tapauksissa Google-palvelun tyydyttäminen vie 10–30 minuuttia. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Vuonna 2020 Cloudflare vaihtoi Googlen Recaptchasta hCaptchaan, koska Google aikoo veloittaa käytöstä.Cloudflare kertoi, että he välittävät yksityisyydestäsi ("se auttaa poistamaan tietosuojaongelmia"), mutta tämä on ilmeisesti valhe.Kyse on rahasta."HCaptcha antaa verkkosivustoille ansaita rahaa palvelemaan tätä kysyntää estämällä robotteja ja muita väärinkäytöksiä" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Käyttäjän kannalta tämä ei muuta paljon. Sinut pakotetaan ratkaisemaan se. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare estää monia ihmisiä ja ohjelmistoja päivittäin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Pilvipallo ärsyttää monia ihmisiä ympäri maailmaa.Katso luetteloa ja ajattele, onko Cloudflaren omaksuminen sivustollesi käyttökokemuksen kannalta hyvä. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Mikä on Internetin tarkoitus, jos et voi tehdä mitä haluat?Suurin osa verkkosivustoosi käyvistä ihmisistä etsii vain muita sivuja, jos he eivät voi ladata verkkosivua.Et ehkä estä aktiivisesti ketään vierailijoita, mutta Cloudflaren oletuspalomuuri on riittävän tiukka estämään monia ihmisiä. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Captchaa ei voi ratkaista ilman JavaScriptin ja evästeiden käyttöönottoa.Cloudflare käyttää niitä tehdä selaimen allekirjoitus tunnistaaksesi sinut.Cloudflaren on tiedettävä henkilöllisyytesi päättääksesi, haluatko jatkaa sivuston selaamista. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor-käyttäjät ja VPN-käyttäjät ovat myös Cloudflaren uhri.Molempia ratkaisuja käyttävät monet ihmiset, joilla ei ole varaa sensuroimattomaan Internetiin maan / yrityksen / verkkopolitiikan takia tai jotka haluavat lisätä ylimääräisen kerroksen yksityisyyden suojaamiseksi.Cloudflare hyökkää häpeämättömästi nuoihin ihmisiin ja pakottaa heidät sammuttamaan välityspalvelimen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Jos et yrittänyt Toria tähän mennessä, suosittelemme, että lataat Tor-selaimen ja vierailet suosikkisivustoillasi.Suosittelemme, että et kirjaudu sisään pankkisivustollesi tai viranomaisten verkkosivuille. Muuten he ilmoittavat tilillesi. Käytä VPN-verkkoa näillä verkkosivustoilla. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Haluat ehkä sanoa: "Tor on laiton! Tor-käyttäjät ovat rikollisia! Tor on huono! ". Ei.Saatat oppia Torista televisiosta, sanomalla, että Toria voidaan käyttää selaamaan darknettiä ja vaihtamaan aseita, huumeita tai chid-pornoa.Vaikka yllä oleva toteamus on totta, että markkinoiden verkkosivustoilla on paljon, joista voit ostaa tällaisia ​​tuotteita, nämä sivustot näkyvät usein myös clearnetissä.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Torin kehitti Yhdysvaltain armeija, mutta nykyisen Torin kehitti Tor-projekti.On monia ihmisiä ja organisaatioita, jotka käyttävät Toria mukaan lukien tulevat ystäväsi.Joten jos käytät Cloudflare -sovellusta verkkosivustollasi, estät oikeita ihmisiä.Menetät mahdolliset ystävyys- ja yrityssopimukset. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Ja heidän DNS-palvelunsa 1.1.1.1 suodattaa myös käyttäjät käyttämättä verkkosivustoa palauttamalla Cloudflaren omistaman väärennetyn IP-osoitteen, localhost IP: n, kuten “127.0.0.x”, tai vain palauttamatta mitään. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS katkaisee myös online-ohjelmistot älypuhelinsovelluksesta tietokonepeliin, koska ne ovat vääriä DNS-vastauksia.Cloudflare DNS ei voi tehdä hakuja joillekin pankkisivustoille. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Ja täällä saatat ajatella,<br>En käytä Toria tai VPN: ää, miksi minun pitäisi välittää?<br>Luotan Cloudflare-markkinointiin, miksi minun pitäisi välittää<br>Sivustoni on https, miksi minun pitäisi välittää | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Jos vierailet verkkosivustolla, joka käyttää Cloudflare-palvelua, jaat tietosi paitsi verkkosivuston omistajalle myös Cloudflare-sivustolle.Näin käänteinen välityspalvelin toimii. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  On mahdotonta analysoida purkamatta TLS-liikennettä. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare tietää kaikki tietosi, kuten raa'an salasanan. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed voi tapahtua milloin tahansa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflaren https ei ole koskaan kokonaisvaltainen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Haluatko todella jakaa tietosi Cloudflaren ja myös 3-kirjeisen toimiston kanssa? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Internetin käyttäjän online-profiili on ”tuote”, jonka hallitus ja suuret teknologiayritykset haluavat ostaa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Yhdysvaltain sisäisen turvallisuuden laitos sanoi:<br><br>Onko sinulla idea, kuinka arvokasta sinulla olevat tiedot ovat? Onko jollain tapa myydä meille tietoja?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare tarjoaa myös ILMAISEN VPN-palvelun nimeltään “Cloudflare Warp”.Jos käytät sitä, kaikki älypuhelimesi (tai tietokoneesi) yhteydet lähetetään Cloudflare-palvelimille.Cloudflare voi tietää minkä verkkosivuston olet lukenut, minkä kommentin olet lähettänyt, kenen kanssa olet puhunut jne.Annoit vapaaehtoisesti kaikki tietosi Cloudflarelle.Jos luulet “vitsailetko? Pilvipallo on turvallinen. ” sitten sinun on opittava, kuinka VPN toimii. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare sanoi, että heidän VPN-palvelunsa tekee Internetistäsi nopean.Mutta VPN tekee Internet-yhteytestäsi hitaampaa kuin nykyinen yhteys. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Saatat jo tietää PRISM-skandaalista.On totta, että AT&T antaa NSA: lle kopioida kaikki Internet-tiedot seurantaa varten. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Oletetaan, että työskentelet NSA: ssa ja haluat jokaisen kansalaisen Internet-profiilin.Tiedät, että useimmat heistä luottavat sokeasti Cloudflareen ja käyttävät sitä - vain yhtä keskitettyä yhdyskäytävää - välittääkseen yrityksen palvelinyhteyttä (SSH / RDP), henkilökohtaista verkkosivustoa, chat-verkkosivustoa, foorumin verkkosivustoa, pankin verkkosivustoa, vakuutussivustoa, hakukonetta, salaa jäsentä - vain verkkosivusto, huutokauppasivusto, ostokset, video-, NSFW- ja laiton verkkosivusto.Tiedät myös, että he käyttävät Cloudflaren DNS-palvelua ("1.1.1.1") ja VPN-palvelua ("Cloudflare Warp") "Suojattu! Nopeammin! Paremmin!" Internet-kokemus.Niiden yhdistäminen käyttäjän IP-osoitteeseen, selaimen sormenjälkeen, evästeisiin ja RAY-ID: hen on hyödyllistä rakentaa kohteen online-profiilia. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Haluat heidän tietojaan. Mitä aiot tehdä? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare on hunajapotti.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Ilmainen kulta kaikille. Jotkut jouset kiinnitetty.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Älä käytä Cloudflare-laitetta.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Hajauttaa Internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Jatka seuraavalle sivulle:  "[Cloudflare-etiikka](fi.ethics.md)"

---

<details>
<summary>_napsauta minua_

## Tiedot ja lisätiedot
</summary>


Tämä säilytystila on luettelo verkkosivustoista, jotka ovat "Suuren pilvaseinän" takana ja estävät Tor-käyttäjiä ja muita CDN-levyjä.


**Tiedot**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare-käyttäjät](../cloudflare_users/)
* [Cloudflare-verkkotunnukset](../cloudflare_users/domains/)
* [Ei pilvipallo CDN-käyttäjät](../not_cloudflare/)
* [Anti-Tor-käyttäjät](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Lisää tietoa**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * ladata: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * BookRix GmbH poisti alkuperäisen e-kirjan (ePUB) CC0-aineiston tekijänoikeusrikkomusten takia
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Lippu vandaalisti niin monta kertaa.
  * [Poistanut Tor-projekti.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Katso lippu 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Viimeinen arkistolippu 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_napsauta minua_

## Mitä voit tehdä?
</summary>

* [Lue suositeltujen toimien luettelo ja jaa se ystävillesi.](../ACTION.md)

* [Lue toisen käyttäjän ääni ja kirjoita ajatuksesi.](../PEOPLE.md)

* Etsi jotain: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Päivitä verkkotunnusluettelo: [Lista ohjeet](../INSTRUCTION.md).

* [Lisää Cloudflare tai projektiin liittyvä tapahtuma historiaan.](../HISTORY.md)

* [Kokeile ja kirjoita uusi työkalu / komentosarja.](../tool/)

* [Tässä on joitain luettavia PDF / ePUB-tiedostoja.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Tietoja vääriä tilejä

Crimeflare tietää väärennettyjen tilien olemassaolosta, jossa esiintyy virallisia kanaviamme, olivatpa ne sitten Twitter, Facebook, Patreon, OpenCollective, Villages jne.
**Emme koskaan kysy sähköpostiosoitettasi.
Emme koskaan kysy nimeäsi.
Emme koskaan kysy henkilöllisyyttäsi.
Emme koskaan kysy sijaintiasi.
Emme koskaan pyydä lahjoitustasi.
Emme koskaan pyydä arvosteluasi.
Emme koskaan pyydä sinua seuraamaan sosiaalista mediaa.
Emme koskaan kysy sosiaalista mediaasi.**

# ÄLÄ LUOTTA VAKAUSKIRJAA.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)