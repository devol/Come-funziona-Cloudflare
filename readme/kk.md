# Ұлы бұлт


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Cloudflare тоқтатыңыз


|  🖹  |  🖼 |
| --- | --- |
|  «Ұлы бұлт» - бұл Cloudflare Inc., АҚШ компаниясы.Ол CDN (мазмұнды жеткізу желісі), DDoS әсерін азайту, Интернет қауіпсіздігі және таратылған DNS (домендік атау сервері) қызметтерін ұсынады.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare - әлемдегі ең үлкен MITM прокси (кері прокси).Cloudflare CDN нарығының 80% -дан астам үлесіне ие және бұлтты пайдаланушылар саны күн сайын өсуде.Олар өз желісін 100-ден астам елге кеңейтті.Cloudflare Twitter, Amazon, Apple, Instagram, Bing және Википедияға қарағанда көбірек веб-трафикке қызмет етеді.Cloudflare тегін жоспар ұсынады және көптеген адамдар өздерінің серверлерін дұрыс конфигурациялаудың орнына оны қолданады.Олар құпиялылықты ыңғайлылықпен сатты.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare сіздің араңызда және шекаралық патрульдік агент сияқты әрекет ететін веб-сервер арасында отырады.Сіз таңдалған бағытқа қосыла алмайсыз.Сіз Cloudflare-ге қосылдыңыз және сіздің барлық ақпаратыңыз шифрланған және шұғыл түрде беріледі. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Бастапқы веб-сервер әкімшісі Cloudflare агентіне «веб-мүлікке» кімнің кіре алатынын шешуге және «шектеулі аймақты» анықтауға мүмкіндік берді.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Дұрыс суретке қараңыз.Сіз Cloudflare тек жаман жігіттерді бұғаттайды деп ойлайсыз.Сіз Cloudflare әрқашан желіде деп ойлайсыз (ешқашан төмендемеңіз).Сіз заңды боттар мен тексерушілер веб-сайтыңызды индекстей алады деп ойлайсыз.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Алайда бұлар мүлдем дұрыс емес.Cloudflare жазықсыз адамдарды ешқандай себепсіз қоршауға алуда.Бұлттар төмен түсуі мүмкін.Cloudflare заңды боттарды блоктайды.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Кез-келген хостинг қызметі сияқты, Cloudflare де керемет емес.Бастапқы сервер жақсы жұмыс істесе де, сіз бұл экранды көресіз.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Сіз шынымен Cloudflare 100% жұмыс уақыты бар деп ойлайсыз ба?Cloudflare неше рет төмендейтінін сіз білмейсіз.Егер Cloudflare төмендесе, тұтынушы веб-сайтыңызға кіре алмайды. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Мұны Қытайдың Ұлы брандмауэріне сілтеме жасай отырып атайды, ол көптеген адамдарға веб-мазмұнды (мысалы, материктік Қытайдағы адамдар мен сырттағылардың бәрін) көруге тыйым салады.Сонымен бірге мүлдем басқа интернетті, «танк адамы» бейнесі мен «Тяньаньмэнь алаңындағы наразылықтардың» тарихы сияқты цензурасыз вебті көре алмайтындарға. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Бұлттар үлкен күшке ие.Бір мағынада, олар түпкі пайдаланушының не көретінін басқарады.Cloudflare веб-сайтына кіруге тыйым салынады. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Бұлттарды цензура үшін пайдалануға болады. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Cloudflare оны бот деп санайтын шамалы шолғышты қолдансаңыз, бұлтты веб-сайтты көре алмайсыз (өйткені оны көп адам пайдаланбайды). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Сіз Javascript-ті қоспай-ақ осы инвазивті «шолғышты тексеруден» өте алмайсыз.Бұл сіздің өміріңіздің бес (немесе одан да көп) секундының босқа шығуы. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare сонымен қатар Google, Yandex, Yacy және API клиенттері сияқты заңды роботтарды / тексерушілерді автоматты түрде блоктайды.Cloudflare заңды зерттеу боттарын бұзу ниетімен «айналып өтетін бұлттар» қауымдастығын белсенді түрде бақылап отырады. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare осылайша Интернет байланысы нашар адамдардың көпшілігінің CAPTCHA бірнеше кескінін шешпейінше, олардың артындағы веб-сайттарға кіруіне жол бермейді (мысалы, олар 7 қабатты NAT-тың артында болуы немесе сол IP-ны, мысалы, Wifi-мен бөлісуі мүмкін).Кейбір жағдайларда бұл Google-ді қанағаттандыру үшін 10-30 минутты алады. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020 жылы Cloudflare Google-дің Recaptcha-нан hCaptcha-ға ауысады, өйткені Google оны пайдаланғаны үшін ақы алғысы келеді.Cloudflare сізге сіздің құпиялылығыңыз туралы қамқорлық жасайтынын айтты («бұл құпиялылық мәселесін шешуге көмектеседі»), бірақ бұл анық емес.Мұның бәрі ақшаға қатысты.«HCaptcha сайттарға боттар мен басқа да қиянат түрлеріне тыйым салу арқылы осы сұранысты қанағаттандыруға мүмкіндік береді» | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Пайдаланушының көзқарасы бойынша бұл айтарлықтай өзгермейді. Сіз оны шешуге мәжбүр боласыз. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Күн сайын Cloudflare көптеген адамдар мен бағдарламалық жасақтаманы блоктайды. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Бұлттар әлемдегі көптеген адамдарды алаңдатады.Тізімді қарап, Cloudflare-ді өз сайтыңызға енгізу пайдаланушы тәжірибесі үшін пайдалы ма деп ойлаңыз. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Егер сіз өзіңіз қалаған нәрсені жасай алмасаңыз, интернеттің мақсаты не?Веб-сайтыңызға кіретін адамдардың көпшілігі, егер олар веб-бетті жүктей алмаса, басқа парақтарды іздейді.Сіз келушілерді белсенді түрде блоктамайтын шығарсыз, бірақ Cloudflare стандартты брандмауэрі көптеген адамдарды бұғаттауға жеткілікті қатаң. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Javascript және Cookie файлдарын қоспай-ақ, интернетті шешу мүмкін емес.Cloudflare оларды сізді анықтау үшін шолғышқа қол қою үшін қолданады.Сайтты қарауды жалғастыра алмайтындығыңызды білу үшін Cloudflare сіздің жеке басыңызды білуі керек. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor пайдаланушылары мен VPN пайдаланушылары да Cloudflare құрбаны болып табылады.Екі шешімді де ел / корпорация / желілік саясатына байланысты бағаланбаған Интернетке қол жеткізе алмайтын немесе жеке өмірін қорғау үшін қосымша қабат қосқысы келетін көптеген адамдар қолданады.Cloudflare бұл адамдарға ұятсыз түрде шабуыл жасайды, оларды прокси шешімін өшіруге мәжбүр етеді. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Егер сіз Торды осы уақытқа дейін көрмесеңіз, Tor Browser жүктеп, сүйікті веб-сайттарыңызға кіруге кеңес береміз.Банк веб-сайтына немесе үкіметтің веб-парағына кірмеуіңізді немесе олар сіздің шотыңызға жалауша қоюды ұсынамыз. Бұл веб-сайттар үшін VPN пайдаланыңыз. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Сіз «Tor заңсыз! Тор қолданушылары қылмыстық! Tor жаман! ». Жоқ.Сіз Tor туралы теледидардан білген боларсыз, Торды қараңғы торлар мен мылтық, есірткі немесе балалар порносын қарауға қолдануға болады.Жоғарыда айтылғандай, мұндай заттарды сатып алуға болатын көптеген нарықтық веб-сайттар бар екендігі рас, сол сайттар көбінесе clearnet-те пайда болады.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Торды АҚШ армиясы әзірледі, бірақ қазіргі Tor жүйесін Tor жобасы әзірледі.Tor қолданатын көптеген адамдар мен ұйымдар бар, олардың ішінде сіздің болашақ достарыңыз бар.Сонымен, егер сіз өзіңіздің веб-сайтыңыздағы Cloudflare-ді қолдансаңыз, сіз шынайы адамдарды бұғаттайсыз.Сіз ықтимал достық пен іскерлік келісімді жоғалтасыз. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Олардың DNS қызметі, 1.1.1.1, сонымен қатар, қолданушыларды Cloudflare, жалған IP-мекен-жайы, мысалы «127.0.0.x» сияқты жалған IP-мекен-жайын қайтару немесе жай ғана ештеңе қайтармай веб-сайтқа кіруге тыйым салады. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS жалған DNS жауабы себебінен интернеттегі бағдарламалық жасақтаманы смартфон қосымшасынан компьютер ойынына дейін бұзады.Cloudflare DNS кейбір банк веб-сайттарын сұрай алмайды. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Міне, сіз ойлануыңыз мүмкін,<br>Мен Tor немесе VPN-ді қолданбаймын, мен неге қамқорлық жасауым керек?<br>Мен Cloudflare маркетингіне сенемін, маған неге қамқорлық керек<br>Менің веб-сайтыңыз - https, неге мен қамқорлық жасауым керек | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Егер сіз Cloudflare қолданатын веб-сайтқа кірсеңіз, сіз ақпаратыңызды тек веб-сайт иесімен ғана емес, сонымен бірге Cloudflare-мен де бөлісесіз.Кері прокси осылай жұмыс істейді. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  TLS трафиктің шифрын шешпей талдау мүмкін емес. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare сіздің құпия деректер сияқты барлық деректеріңізді біледі. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Бұлт ауруы кез-келген уақытта болуы мүмкін. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare-дің https ешқашан аяқталмайды. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Cloudflare, сонымен қатар 3 әріптік агенттікпен өз деректеріңізді бөліскіңіз келе ме? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Интернеттегі пайдаланушының желідегі профилі - бұл мемлекет пен ірі технологиялық компаниялар сатып алғысы келетін «өнім». | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Бұл туралы АҚШ ішкі қауіпсіздік департаменті хабарлады:<br><br>Сізде бар деректердің қаншалықты құнды екендігі туралы ойыңыз бар ма? Сізге бұл деректерді сатудың қандай да бір тәсілі бар ма?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare сонымен қатар «Cloudflare Warp» деп аталатын ТЕГІН VPN қызметін ұсынады.Егер сіз оны қолдансаңыз, барлық смартфондарыңыз (немесе компьютерлеріңіз) Cloudflare серверлеріне жіберіледі.Cloudflare сіз қандай веб-сайтты оқығаныңызды, қандай пікір қалдырғаныңызды, кіммен сөйлескеніңізді және т.б. біле алады.Сіз барлық ақпаратты Cloudflare-ге бересіз.Егер сіз «қалжыңдасаңыз? Бұлттар қауіпсіз ». содан кейін сіз VPN қалай жұмыс істейтінін білуіңіз керек. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare олардың VPN қызметі сіздің интернетіңізді жылдам етеді дейді.Бірақ VPN Интернет байланысын бұрыннан бар байланыспен баяу етеді. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Сіз PRISM жанжалы туралы бұрыннан білетін шығарсыз.AT&T NSA-ға барлық Интернеттегі деректерді бақылауға мүмкіндік беретіні ақиқат. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Сіз NSA-да жұмыс істеп жатырсыз және әр азаматтың интернет-профилін алғыңыз келеді делік.Сіз олардың көпшілігі Cloudflare-ге сенбейтінін және оны пайдаланып жатқанды - тек бір орталықтандырылған шлюзді - компанияның сервер қосылымын (SSH / RDP), жеке веб-сайтты, чат веб-сайтты, форум веб-сайтын, банк веб-сайтын, сақтандыру веб-сайтын, іздеу жүйесін, құпия мүшені проксидеу үшін қолданады. - тек веб-сайт, аукцион веб-сайт, сауда, бейне веб-сайт, NSFW веб-сайты және заңсыз веб-сайт.Сондай-ақ, олар Cloudflare DNS қызметін («1.1.1.1») және VPN қызметін («Cloudflare Warp») «Қауіпсіз! Тезірек! Жақсырақ! » интернет тәжірибесі.Оларды пайдаланушының IP мекенжайымен, шолғыштың саусақ ізімен, cookie файлдарымен және RAY-ID-мен біріктіру мақсатты онлайн профилін құру үшін пайдалы болады. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Сіз олардың деректерін қалайсыз. Сен не істейсің? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare - бұл бал құмыра.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Барлығына тегін бал. Кейбір ішектер бекітілген.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Cloudflare қолданбаңыз.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Интернетті орталықсыздандырыңыз.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Келесі бетке өтіңіз:  "[Бұлттар этикасы](kk.ethics.md)"

---

<details>
<summary>_мені басыңыз_

## Деректер және қосымша ақпарат
</summary>


Бұл репозиторий дегеніміз Tor пайдаланушыларын және басқа да CDN-дерді бұғаттайтын «Ұлы бұлт» артында тұрған веб-сайттардың тізімі.


**Деректер**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare пайдаланушылары](../cloudflare_users/)
* [Cloudflare домендері](../cloudflare_users/domains/)
* [Cloudflare емес CDN пайдаланушылары](../not_cloudflare/)
* [Торға қарсы қолданушылар](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Көбірек ақпарат**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Жүктеу: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Түпнұсқа электронды кітап (ePUB) CCR материалына авторлық құқықты бұзғаны үшін BookRix GmbH тарапынан жойылды
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Билет бірнеше рет бұзылды.
  * [Тор жобасымен жойылды.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [34175 билетін қараңыз.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Соңғы мұрағат билеті 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_мені басыңыз_

## Сіз не істей аласыз?
</summary>

* [Ұсынылған әрекеттер тізімін оқып, достарыңызбен бөлісіңіз.](../ACTION.md)

* [Басқа пайдаланушының дауысын оқып, өз ойларыңызды жазыңыз.](../PEOPLE.md)

* Бірдеңе ізде: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Домен тізімін жаңартыңыз: [Нұсқаулықтарды тізімдеңіз](../INSTRUCTION.md).

* [Cloudflare немесе жоба туралы оқиғаны тарихқа қосыңыз.](../HISTORY.md)

* [Жаңа құрал / сценарийді қолданып көріңіз және жазыңыз.](../tool/)

* [Мұнда оқу үшін бірнеше PDF / ePUB бар.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Жасанды шоттар туралы

Twitter, Facebook, Patreon, OpenCollective, Village және т.б. сияқты ресми каналдарымызды бейнелейтін жалған аккаунттардың болуы туралы Крффлар біледі.
**Біз ешқашан электрондық поштадан сұрамаймыз.
Біз сіздің атыңызды ешқашан сұрамаймыз.
Біз ешқашан сіздің жеке басыңызды сұрамаймыз.
Біз сіздің орналасқан жеріңізді ешқашан сұрамаймыз.
Біз сіздің қайырымдылық көмегіңізді ешқашан сұрамаймыз.
Біз сіздің пікіріңізді ешқашан сұрамаймыз.
Біз сізден ешқашан әлеуметтік желілерді қарауды сұрамаймыз.
Біз ешқашан әлеуметтік желілерден сұрамаймыз.**

# ЖАЛПЫ ЕСЕПТІЛІКТЕРДІ БІЛМЕҢІЗ.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)