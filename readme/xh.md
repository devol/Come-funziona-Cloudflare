# I-Cloudwall enkulu


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Misa i-Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  “I-Cloudwall enkulu” yiCloudflare Inc., inkampani yase-U.S.Inikezela ngeenkonzo zeCDN (zokuhambisa umxholo) iinkonzo, ukunciphisa i-DDoS, ukhuseleko lwe-Intanethi, kunye neenkonzo zokuhanjiswa kwe-DNS (iseva yegama lesizinda) iinkonzo.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  I-Cloudflare yeyona proxy MITM inkulu yehlabathiI-Cloudflare inabanini ngaphezulu kwe-80% yesabelo sentengiso yeCDN kunye nenani labasebenzisi befu.Baye bandisa uthungelwano lwabo kumazwe angaphezu kwe-100.I-Cloudflare isebenza ngakumbi kwi-web traffic kune-Twitter, Amazon, Apple, Instagram, Bing & Wikipedia zidibeneyo.I-Cloudflare inikezela ngesicwangciso simahla kwaye abantu abaninzi bayayisebenzisa endaweni yokuseta iiseva zabo ngokufanelekileyo.Babeshishina ngasese ngokulula.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  I-Cloudflare ihlala phakathi kwakho kunye ne-webserver yemvelaphi, isebenza njengearhente yokujikeleza komda.Awukwazi ukudibanisa kwindawo oya kuyo oyikhethileyo.Uqhagamshela kwi-Cloudflare kwaye lonke ulwazi lwakho luyenziwa luchwechwe kwaye lunikezelwe kubhabho. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Umlawuli we-webserver yemvelaphi wavumela iarhente - iCloudflare - ukuba ithathe isigqibo sokuba ngubani onokuthi afikelele “kwipropathi yewebhu” kunye nokuchaza "indawo ethintelweyo"  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Jonga umfanekiso ochanekileyo.Uya kucinga ukuba i-Cloudflare block kuphela ngaba bantu babi.Uya kucinga ukuba iCloudflare ihlala ikwi-Intanethi (ungaze uye ezantsi).Uya kucinga ukuba i-bots bots kunye nabakhweli banokwalathisa iwebhusayithi yakho.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Nangona kunjalo ayisiyonyani konke konke.I-Cloudflare ivimba abantu abamsulwa ngaphandle kwesizathu.I-Cloudflare inokuhla.I-Cloudflare ibhloka i-bots bots.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Njengayo nayiphi na inkonzo yokubamba, iCloudflare ayifezekanga.Uza kubona esi sikrini nokuba iseva yemvelaphi isebenza kakuhle.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Ngaba ucinga ukuba iCloudflare ine-100% yexesha elongezelelweyo?Awunalo nofifi lokuba iCloudflare ihla nini.Ukuba iCloudflare ihla umthengi wakho akakwazi ukufikelela kwiwebhusayithi yakho. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Ibizwa le nto ngokubhekisele kwi-Great Firewall yase-China eyenza umsebenzi othelekiswayo wokuhlutha abantu abaninzi ukuba babone umxholo wewebhu (okt wonke umntu kwilizwe lase China kunye nabantu bangaphandle).Ngeli xesha linye abo bangachaphazelekanga ukuba babone iwebhu eyahlukileyo eyahlukileyo, iwebhu engenakubalwa okufana nomfanekiso we "tank man" kunye nembali yoqhankqalazo "lweTiananmen Square". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  I-Cloudflare inamandla amakhulu.Ngendlela, balawula okubonwa ngumsebenzisi wokugqibela.Uthintelwe ukukhangela iwebhusayithi ngenxa ye-Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  I-Cloudflare inokusetyenziselwa ukucubungula. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Awunako ukujonga iwebhusayithi efakwe lilifu ukuba usebenzisa isiphequluli esincinci i-Cloudflare enokucinga ukuba yi-bot (kuba abantu abaninzi abayisebenzisi). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Awunakho ukugqithisa eli 'hlolisiso lokukhangela' ngaphandle kokuvumela iJavascript.Oku kuyinkcitho imizuzwana emihlanu (okanye engaphezulu) yobomi bakho obuxabisekileyo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  I-Cloudflare ikwathintela ngokuzenzekelayo iirobhothi / abakhweli abanjengoGoogle, Yandex, Yacy, kunye nabaxhasi be-API.I-Cloudflare ibeka iliso elisebenzayo “lokudlula kwilifu” elijolise eluntwini ngenjongo yokwaphula i-bots esemthethweni yophando. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  I-Cloudflare ngokufanayo ithintela abantu abaninzi abanonxibelelwano olubi lwe-Intanethi ukufikelela kwiiwebhusayithi ezingasemva kwayo (umzekelo, banokuba semva kwe-7+ layter ye-NAT okanye babelane nge-IP efanayo, umzekelo kwi-Wifi yoluntu) ngaphandle kokuba bacombulula imifanekiso emininzi yeCAPTCHA.Ngamanye amaxesha, oku kuya kuthatha imizuzu eli-10 ukuya kwengama-30 ukwanelisa uGoogle. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Ngonyaka ka-2020 iCloudflare yatshintshwa isuka kwiRecaptcha kaGoogle iye kwi-hCaptcha njengoko uGoogle anqwenela ukubiza imali ngokusetyenziswa kwayo.I-Cloudflare ikuxelele ukuba bayayikhathalela imfihlo yakho ("iyanceda ukulungisa ingxaki yabucala") kodwa obu bubuxoki.Imalunga nemali."I-hCaptcha ivumela iiwebhusayithi ukuba zenze imali ekukhonzeni ibango ngelixa zithintela i-bots kunye nezinye iindlela zokuphathwa gadalala" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Ngombono womsebenzisi, oku akutshintshi kakhulu. Uyanyanzelwa ukuba usisombulule. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Uninzi lwabantu kunye nesoftware ivaliwe yi-Cloudflare yonke imihla. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  I-Cloudflare icaphukisa abantu abaninzi kwihlabathi liphela.Jonga uluhlu kwaye ucinge ukuba ukwamkela i-Cloudflare kwindawo yakho kulungile kumava omsebenzisi. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Yintoni injongo ye-intanethi ukuba awukwazi ukwenza le nto uyifunayo?Uninzi lwabantu abatyelela iwebhusayithi yakho baya kujonga amanye amaphepha ukuba abanakukwazi ukulayisha iphepha lewebhu.Ungangabinakho ukuvimba abakhenkethi, kodwa isibonda somlilo esingagugiyo se-Cloudflare singqongqo ngokwaneleyo ukubhloka abantu abaninzi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Akukho ndlela yokusombulula i-captcha ngaphandle kokuvumela iJavascript kunye neeKuki.I-Cloudflare bayayisebenzisa ukwenza utyikityo lwesiphequluli ukukuchonga.I-Cloudflare idinga ukuba sazi isazisi sakho ukuze uthathe isigqibo sokuba ngaba ungowokuqala na ukukhangela indawo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Abasebenzisi be-Tor kunye nabasebenzisi beVPN balixhoba le-Cloudflare.Zombini izisombululo zisetyenziswa ngabantu abaninzi abangakwaziyo ukufikelela kwi-Intanethi ngenxa yelizwe labo / umgaqo-nkqubo / womgaqo-nkqubo okanye abafuna ukongeza ungqimba ukukhusela ubumfihlo babo.I-Cloudflare ihlasela ngokungenantlonelo abo bantu, ibanyanzela ukuba bacime isisombululo sabo sommeleli. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Ukuba awuzami iTor ukuza kuthi ga kulo mzuzu, siyakukhuthaza ukuba ukhuphele umkhangeli zincwadi weTor kwaye undwendwele iiwebhusayithi zakho ozithandayo.Sicebisa ukuba ungangeni kwiwebhusayithi yakho yebhanki okanye iphepha lewebhu likarhulumente okanye baya kuyifaka iflegi yeakhawunti yakho. Sebenzisa i-VPN kwezi webhusayithi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Unokufuna ukuthi “ITor ayikho semthethweni! Abasebenzisi beTor balwaphulo mthetho! Tor kubi! ".Unokufunda ngeTor kumabonwakude, esithi iTor ingasetyenziselwa ukukhangela i-darknet kunye nokuthengisa ngemipu, iziyobisi okanye i-chid.Ngelixa ingxelo apha ngasentla iyinyani ukuba kukho iwebhusayithi eninzi yentengiso apho ungathenga izinto ezinjalo, ezo ndawo zivame ukubonakala nakwi-clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  I-Tor yaphuhliswa ngumkhosi wase-US, kodwa iTor yangoku iphuhliswa yiprojekthi yeTor.Baninzi abantu kunye nemibutho esebenzisa iTor kubandakanya nabahlobo bakho bangomso.Ke, ukuba usebenzisa iCloudflare kwiwebhusayithi yakho uvimba abantu bokwenyani.Uyakuphulukana nobuhlobo obunokubakho kunye nesivumelwano seshishini. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Kwaye inkonzo yabo ye-DNS, 1.1.1.1, ikwacoca nabasebenzisi ukuba bayityelele iwebhusayithi ngokubuyisela idilesi ye-IP engeyo-Cloudflare, IPhost yendawo enje nge "127.0.0.x", okanye ungabuyisi kwanto. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  I-Cloudflare DNS ikwaqhawuka isoftware kwi-intanethi ukuya kwi-software yomdlalo wekhompyuter ngenxa yempendulo yabo engeyiyo ye-DNS.I-Cloudflare DNS ayinakho ukubuza ezinye iiwebhusayithi zebhanki. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Kwaye apha unokucinga,<br>Andisebenzisi iTor okanye iVPN, kutheni kufuneka ndinyamekele?<br>Ndiyathemba ukuthengisa kukaCloudflare, kutheni kufanelekile ukuba ndikhathale<br>Indawo yam yewebhu yi-https kutheni kufuneka ndinyamekele | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Ukuba undwendwela iwebhusayithi esebenzisa i-Cloudflare, wabelana ngolwazi lwakho kungekuko kumnini wewebhusayithi kuphela, kodwa nakwi-Cloudflare.Yindlela esebenza ngayo le proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Akunakwenzeka ukuba uhlalutye ngaphandle kokubhala i-TLS traffic. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  I-Cloudflare iyayazi yonke idatha yakho njengegama eligqithisiweyo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  I-Cloudbeed inokwenzeka nangaliphi na ixesha. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  I-https ye-Cloudflare's ayisokuze iphele-ukuphela. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Ngaba uyafuna ngokwenene ukuba wabelane ngedatha yakho nge-Cloudflare, kunye nearhente yeeleta ezintathu? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Iprofayile yomsebenzisi kwi-Intanethi “yimveliso” urhulumente kunye neenkampani ezinkulu zezobugcisa ezifuna ukuyithenga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  ISebe lezokhuseleko laseKhaya lase-U.S:<br><br>Ngaba unayo nayiphi na imbono ukuba ibaluleke kangakanani idatha onayo? Ngaba ikhona indlela onokusithengisa ngayo loo datha?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  I-Cloudflare ikwabonelela ngenkonzo yasimahla ye-VPN ebizwa ngokuba yi "Cloudflare Warp".Ukuba uyayisebenzisa, yonke i-smartphone yakho (okanye ikhompyuter yakho) iqhagamshelwe kwiiseva ze-Cloudflare.I-Cloudflare iyakwazi ukuba yeyiphi iwebhusayithi oyifundileyo, uluvo luni oyithumileyo, othethe naye, njl njl.Ngokuzithandela unikezela lonke ulwazi lwakho kwi-Cloudflare.Ukuba ucinga “Ngaba uyaqhula? I-Cloudflare ikhuselekile. ” emva koko kufuneka ufunde ukuba isebenza njani i-VPN | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  I-Cloudflare ithe inkonzo yabo yeVPN yenza i-intanethi yakho ikhawuleze.Kodwa i-VPN yenza uqhagamshelo lwakho lwe-Intanethi kancinci kancinci kunonxibelelwano lwakho olukhoyo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Unokuba sele uyazi malunga ne-PRISM yokukhwaza.Kuyinyani ukuba i-AT & T ivumela i-NSA ukuba ikope yonke idatha ye-Intanethi ukuze ihlolwe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Masithi usebenza eNSA, kwaye ufuna yonke iprofayili yabasebenzi be-Intanethi.Uyazi uninzi lwabo luthembele ku-Cloudflare kwaye luzisebenzisa-kuphela isango elinye elingumbindi - ukumisela uqhagamshelo lweseva yabo yenkampani (SSH / RDP), iwebhusayithi yobuqu, iwebhusayithi yokuncokola, iwebhusayithi yeforam, iwebhusayithi ye-intanethi, i-inshurensi iwebhusayithi, i-injini yokukhangela, ilungu eliyimfihlo. Iwebhusayithi-enye, iwebhusayithi yeefandesi, iivenkile, ividiyo yevidiyo, iwebhusayithi yeNSFW, kunye newebhusayithi engekho mthethweni.Uyazi ukuba basebenzisa inkonzo ye-DNS ye-Cloudflare ("1.1.1.1") kunye nenkonzo ye-VPN ("Cloudflare Warp") ngo “Khuselekile! Ngesantya! Ngcono! ” amava e-intanethi.Ukuzidibanisa nedilesi ye-IP yomsebenzisi, i-browserprint zeminwe, iicokies kunye ne-RAY-ID ziya kuba luncedo ekwakheni iprofayile ye-intanethi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Ufuna idatha yabo. Uza kwenza ntoni? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **I-Cloudflare yidosi yobusi.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Ubusi basimahla kumntu wonke. Imitya ethile iqhotyoshelwe.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Sukusebenzisa iCloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Yabela i-intanethi** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Nceda uqhubeke kwiphepha elilandelayo:  "[Ukuziphatha okuhle kwe-Cloudflare](xh.ethics.md)"

---

<details>
<summary>_Ndicofe_

## Idatha kunye noLwazi ngakumbi
</summary>


Olu luhlu luluhlu lwewebhusayithi ezingasemva "kweyona Cloudwall", ezithintela abasebenzisi beTor kunye nezinye ii-CDN.


**Idatha**
* [I-Cloudflare Inc.](../cloudflare_inc/)
* [Abasebenzisi be-Cloudflare](../cloudflare_users/)
* [I-Cloudflare Domain](../cloudflare_users/domains/)
* [Abasebenzisi be-CDN abanga-Cloudflare](../not_cloudflare/)
* [Abasebenzisi be-Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Iinkcukacha ezithe xaxe**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Khuphela: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * I-eBook yasekuqaleni (i-ePUB) icinywe yi-BookRix GmbH ngenxa yokophula umthetho wezinto ze-CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Itikiti lonakaliswa kaninzi.
  * [Icinyiwe yiProjekti yeTor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Jonga itikiti 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Itikiti lokugcina eligcinwe ngo-24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_Ndicofe_

## Ungenzani?
</summary>

* [Funda uluhlu lwethu lwezenzo ezinconyelwayo kwaye wabelane ngazo nabahlobo bakho.](../ACTION.md)

* [Funda izwi lomnye umsebenzisi kwaye ubhale iingcinga zakho.](../PEOPLE.md)

* Khangela into: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Hlaziya uluhlu lwedomeyini: [Uluhlu lwemiyalelo](../INSTRUCTION.md).

* [Yongeza i-Cloudflare okanye umcimbi onxulumene neprojekthi kwimbali.](../HISTORY.md)

* [Zama & ubhale isiXhobo esitsha / iskripthi.](../tool/)

* [Nantsi enye yePDF / ePUB ekufuneka uyifunde.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Malunga neeakhawunti ezingeyonyani

Ulwaphulo-mthetho lwazi malunga nobukho beeakhawunti ezingezizo ezilingisa iziteshi zethu ezisemthethweni, nokuba ngaba yi-Twitter, i-Facebook, iPatreon, i-OpenCollective, iilali njl.
**Soze siyibuze i-imeyile yakho.
Soze sibuze igama lakho.
Soze sibuze isazisi sakho.
Soze sibuze indawo yakho.
Soze sibuze umnikelo wakho.
Soze sibuze uhlaziyo lwakho.
Asikhe sikucele ukuba ulandele kwimidiya yoluntu.
Soze sibuze kwimidiya yakho yoluntu.**

# SUKAQINILE UKUFUMANA IINGXAKI.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)