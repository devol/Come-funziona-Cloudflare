# Masalah Etika

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

"Aja ndhukung perusahaan iki sing ora ana etika"

"Perusahaan sampeyan ora bisa dipercaya. Sampeyan ngaku nglaporake DMCA nanging akeh tuntutan hukum supaya ora ditindakake."

"Dheweke mung censor wong-wong sing takon karo etika."

"Aku nganggep sing bener ora cocog lan didhelikake saka tampilan umum."  -- [phyzonloop](https://twitter.com/phyzonloop)


---


<details>
<summary>klik kula

## CloudFlare spams wong
</summary>


Cloudflare ngirim email spam menyang pangguna non-Cloudflare.

- Mung ngirim email menyang pelanggan sing wis milih
- Yen pangguna ujar "mandeg", banjur mungkasi ngirim email

Iku prasaja. Nanging Cloudflare ora peduli.
Cloudflare ujar nggunakake layanan kasebut bisa mungkasi kabeh spammer utawa penyerang.
Kepiye carane bisa mandhegake Cloudflare tanpa ngaktifake Cloudflare?


| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspam01.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspam03.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspam02.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspambrittany.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspamtwtr.jpg) |

</details>

---

<details>
<summary>klik kula

## Copot tinjauan pangguna
</summary>


Tinjauan negatif sensor sensor Cloudflare.
Yen sampeyan ngirim teks anti-Cloudflare ing Twitter, sampeyan bisa entuk balesan saka karyawan Cloudflare kanthi pesen "Ora, ora".
Yen sampeyan ngirim review negatif ing situs review, dheweke bakal nyoba menehi sensor.


| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfcenrev_01.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfcenrev_02.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfcenrev_03.jpg) |

</details>

---

<details>
<summary>klik kula

## Nuduhake informasi pribadi pangguna
</summary>


Cloudflare duwe masalah gangguan sing akeh.
Cloudflare nuduhake informasi pribadi saka wong-wong sing ngeluh babagan situs sing dadi tuan rumah.
Dheweke kadang njaluk sampeyan menehi ID sing sejati.
Yen sampeyan ora pengin diganggu, nyerang, digeget utawa dipateni, sampeyan luwih adoh saka situs web Cloudflared.


| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_what.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_swat.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_kill.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_threat.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_dox.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_ex1.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_ex2.jpg) |

</details>

---

<details>
<summary>klik kula

## Panyuwunan perusahaan sumbangan kontribusi
</summary>


CloudFlare njaluk sumbangan amal.
Mesthine kaget manawa perusahaan Amerika bakal njaluk amal bebarengan karo organisasi nirlaba sing nduweni sabab sing apik.
Yen sampeyan seneng ngalangi wong utawa mbuang wektu wong liya, sampeyan bisa uga pengin nggawe pizza kanggo karyawan Cloudflare.


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdonate.jpg)

</details>

---

<details>
<summary>klik kula

## Situs sing nolak
</summary>


Apa sing bakal sampeyan lakoni yen situs sampeyan mudhun?
Ana laporan manawa Cloudflare mbusak konfigurasi pangguna utawa layanan mandeg tanpa menehi peringatan, meneng.
Kita saranake sampeyan nemokake panyedhiya sing luwih apik.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftmnt.jpg)

</details>

---

<details>
<summary>klik kula

## Diskriminasi vendor Browser
</summary>


CloudFlare menehi perawatan luwih disenengi kanggo wong-wong sing nggunakake Firefox nalika menehi perawatan musuhan kanggo pangguna sing dudu Tor-Browser liwat Tor.
Pangguna Tor sing kanthi bener nolak kanggo nglakokake javascript tanpa gratis uga nampa perawatan musuhan.
Ketimpangan akses iki minangka penyalahgunaan netralitas lan penyalahgunaan kekuwatan.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/browdifftbcx.gif)

- Ngiwa: Tor Browser, Tengen: Chrome. Alamat IP sing padha.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/browserdiff.jpg)

- Ngiwa: Kunci Javascript Tor Browser, Aktif Cookie
- Tengen: Aktifake Javascript Chrome, Cilik Cookie

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfsiryoublocked.jpg)

- QuteBrowser (browser cilik) tanpa Tor (Clearnet IP)

| ***Browser*** | ***Akses perawatan*** |
| --- | --- |
| Tor Browser (Javascript aktif) | akses dileksanakake |
| Firefox (Javascript aktif) | akses diremehake |
| Chromium (Javascript aktif) | akses diremehake |
| Chromium or Firefox (Javascript dipatèni) | akses ditolak |
| Chromium or Firefox (Cookie dipatèni) | akses ditolak |
| QuteBrowser | akses ditolak |
| lynx | akses ditolak |
| w3m | akses ditolak |
| wget | akses ditolak |


Napa nggunakake tombol Audio kanggo ngatasi tantangan gampang?

Ya, ana tombol audio, nanging mesthi ora bisa digunakake ing Tor.
Sampeyan bakal entuk pesen iki nalika sampeyan ngeklik:

```
Coba maneh mengko
Komputer utawa jaringan sampeyan bisa ngirim pitakon otomatis.
Kanggo nglindhungi pangguna, kita ora bisa ngetrapake panjaluk sampeyan saiki.
Kanggo rincian liyane, bukak kaca pitulung
```

</details>

---

<details>
<summary>klik kula

## Penindasan pemilih
</summary>


Pamilih ing negara-negara AS ndaftar kanggo milih pungkasan situs web sekretaris negara ing negara sing dipanggoni.
Pejabat sekretaris negara sing dikuasai Republik melu opresi pemilih kanthi ngunjungi situs web sekretaris negara liwat Cloudflare.
Perawatan Cloudflare kanggo pangguna Tor, posisi MITM minangka pusat pengawasan global, lan peran ngrugekake sakabehe nggawe calon pamilih ora gelem ndhaptar.
Liberal utamane ngrasuk privasi.
Formulir pamilih ngumpulake informasi sensitif babagan penyelenggaraan politik pemilih, alamat fisik pribadi, nomer keamanan sosial, lan tanggal lair.
Umume negara mung nggawe subset saka informasi kasebut, nanging Cloudflare ndeleng kabeh informasi kasebut nalika ana wong sing milih.

Elinga yen registrasi kertas ora ngalangi Cloudflare amarga sekretaris karyawan staf data data negara bisa nggunakake situs web Cloudflare kanggo mlebu data kasebut.

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfvotm_01.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfvotm_02.jpg) |

- Change.org minangka situs web sing misuwur kanggo ngumpulake swara lan tumindak.
“wong ing endi-endi miwiti kampanye, nggedhekake pendukung, lan nggarap keputusan sing nggawe solusi kanggo nyopir solusi.”
Sayange, akeh wong ora bisa ndeleng perubahan.org amarga panyaring agresif Cloudflare.
Dheweke disekat kanggo menehi tandha petisyen kasebut, saengga ora kalebu saka proses demokratis.
Nggunakake platform non-cloudflared liyane kayata OpenPetition mbantu mbatasi masalah kasebut.

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/changeorgasn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/changeorgtor.jpg) |

- "Project Athenian" Cloudflare nawakake perlindungan level perusahaan gratis kanggo situs web pemilihan negara lan lokal.
Dheweke kandha, "konstituen bisa ngakses informasi pemilihan lan registrasi pamilih" nanging iki ngapusi amarga akeh wong sing ora bisa nggoleki situs kasebut.

</details>

---

<details>
<summary>klik kula

## Nglimbangake pilihan pangguna
</summary>


Yen sampeyan milih metu, sampeyan ngarepake ora nampa email babagan iki.
Cloudflare nglirwakake pilihan pangguna lan nuduhake data karo perusahaan pihak katelu tanpa idin saka pelanggan.
Yen sampeyan nggunakake rencana gratis, dheweke kadang ngirim email menyang sampeyan njaluk tuku langganan saben wulan.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfviopl_tp.jpg)

</details>

---

<details>
<summary>klik kula

## Lying babagan mbusak data pangguna
</summary>


Miturut blog pelanggan ex-cloudflare iki, Cloudflare ngapusi babagan mbusak akun.
Saiki, akeh perusahaan sing njaga data sawise sampeyan ditutup utawa mbusak akun.
Umume perusahaan sing apik nyebutake babagan kabijakan privasi.
Cloudflare? Ora.

```
2019-08-05 CloudFlare ngirim konfirmasi yen dheweke bakal ngilangi akunku.
2019-10-02 Aku nampa email saka CloudFlare "amarga aku iki pelanggan"
```

Cloudflare ora ngerti babagan tembung "mbusak".
Yen bener-bener diilangi, kenapa bekas pelanggan iki entuk email?
Dheweke uga ujar manawa kabijakan privasi Cloudflare ora nyebutake.

```
Kabijakan privasi anyar dheweke ora bisa nyebutake penylametan data sajrone setaun.
```

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfviopl_notdel.jpg)

Kepiye sampeyan bisa dipercaya Cloudflare yen kabijakan privasi dheweke LIE?

</details>

---

<details>
<summary>klik kula

## Terus informasi pribadhi
</summary>


Mbusak akun Cloudflare level hard.

```
Kirim tiket dhukungan nganggo kategori "Akun",
lan njaluk mbusak akun ing awak pesen.
Sampeyan ora duwe domain utawa kertu kredit sing dipasang ing akun sadurunge njaluk pambusakan.
```

Sampeyan bakal nampa email konfirmasi iki.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cf_deleteandkeep.jpg)

"Kita wis miwiti proses panjalukan pambusakan sampeyan" nanging "Kita bakal terus nyimpen informasi pribadi".

Apa sampeyan bisa "dipercaya"?

</details>

---

## Aliaj informoj

- Joseph Sullivan (Joe Sullivan) ([Cloudflare CSO](https://twitter.com/eastdakota/status/1296522269313785862))
  - [Ex-Uber security head charged in connection with the cover-up of a 2016 hack that affected 57 million customers](https://www.businessinsider.com/uber-data-hack-security-head-joe-sullivan-charged-cover-up-2020-8)
  - [Former Chief Security Officer For Uber Charged With Obstruction Of Justice](https://www.justice.gov/usao-ndca/pr/former-chief-security-officer-uber-charged-obstruction-justice)


---

## Mangga terus menyang kaca sabanjure:   [Swara Cloudflare](../PEOPLE.md)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/freemoldybread.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg)
