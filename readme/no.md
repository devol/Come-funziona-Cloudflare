# The Great Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Stopp Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  “The Great Cloudwall” er Cloudflare Inc., det amerikanske selskapet.Det leverer CDN-tjenester (innholdsleveringsnettverk), DDoS-begrensning, Internett-sikkerhet og distribuerte DNS-tjenester (domenenavnserver).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare er verdens største MITM-proxy (omvendt proxy).Cloudflare eier mer enn 80% av markedsandelen i CDN, og antallet brukere av cloudflare vokser hver dag.De har utvidet nettverket til mer enn 100 land.Cloudflare serverer mer nettrafikk enn Twitter, Amazon, Apple, Instagram, Bing og Wikipedia til sammen.Cloudflare tilbyr gratis plan og mange bruker den i stedet for å konfigurere serverne sine ordentlig.De handlet privatliv over bekvemmelighet.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare sitter mellom deg og opprinnelseswebserver, og fungerer som en grensepatruljemiddel.Du kan ikke koble til den valgte destinasjonen.Du kobler deg til Cloudflare, og all informasjonen din blir dekryptert og overlevert på farta. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Opphavsrettsserveradministratoren tillot agenten - Cloudflare - å bestemme hvem som kan få tilgang til sin "webeiendom" og definere "begrenset område".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Ta en titt på riktig bilde.Du vil tro Cloudflare blokkerer bare skurkene.Du vil tro at Cloudflare alltid er online (gå aldri ned).Du vil tro at legitime roboter og gjennomsøkere kan indeksere nettstedet ditt.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Imidlertid er det ikke sant i det hele tatt.Cloudflare blokkerer uskyldige mennesker uten grunn.Cloudflare kan gå ned.Cloudflare blokkerer legit roboter.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Akkurat som enhver hosting-tjeneste er Cloudflare ikke perfekt.Du vil se dette skjermbildet selv om opprinnelsesserveren fungerer bra.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Tror du virkelig Cloudflare har 100% oppetid?Du aner ikke hvor mange ganger Cloudflare går ned.Hvis Cloudflare går ned, kan ikke kunden få tilgang til nettstedet ditt. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Det kalles dette med henvisning til Great Firewall of China som gjør en sammenlignbar jobb med å filtrere ut mange mennesker fra å se webinnhold (dvs. alle i fastlands-Kina og folk utenfor).Samtidig som de som ikke ble berørt, ser et dratisk annerledes nett, en web som er fri for sensur, for eksempel et bilde av "tankmann" og historien til "Den himmelske freds plass-protester". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare besitter stor kraft.På en måte kontrollerer de hva sluttbrukeren til syvende og sist ser.Du er forhindret fra å surfe på nettstedet på grunn av Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare kan brukes til sensur. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Du kan ikke se cloudflared nettsted hvis du bruker mindre nettleser som Cloudflare kan tro at det er en bot (fordi ikke mange bruker den). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Du kan ikke passere denne invasive "nettlesersjekken" uten å aktivere Javascript.Dette er bortkastet fem (eller flere) sekunder av ditt verdifulle liv. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare blokkerer også automatiske legit roboter / gjennomsøkere som Google-, Yandex-, Yacy- og API-klienter.Cloudflare overvåker aktivt “bypass cloudflare” -samfunnet med en hensikt å bryte legitime forskningsboter. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare forhindrer på samme måte at mange mennesker som har dårlig internettforbindelse, får tilgang til nettstedene bak seg (for eksempel kan de være bak 7+ lag NAT eller dele samme IP, for eksempel offentlig Wifi), med mindre de løser flere CAPTCHA-bilder.I noen tilfeller vil dette ta 10 til 30 minutter å tilfredsstille Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  I 2020 byttet Cloudflare fra Googles Recaptcha til hCaptcha ettersom Google har til hensikt å ta betalt for bruken.Cloudflare fortalte at de bryr deg om personvernet ditt (“det hjelper å ta opp et privatlivsproblem”), men dette er tydeligvis en løgn.Det handler om penger."HCaptcha lar nettsteder tjene penger som betjener dette kravet mens de blokkerer roboter og andre former for misbruk" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Fra brukerens perspektiv endres ikke dette mye. Du blir tvunget til å løse det. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Mange mennesker og programvare blir blokkert av Cloudflare hver dag. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare irriterer mange mennesker over hele verden.Ta en titt på listen og tenk om å ta i bruk Cloudflare på nettstedet ditt er bra for brukeropplevelsen. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Hva er hensikten med internett hvis du ikke kan gjøre det du vil?De fleste som besøker nettstedet ditt, vil bare se etter andre sider hvis de ikke kan laste inn en webside.Det er mulig at du ikke blokkerer noen besøkende aktivt, men Cloudflares standard brannmur er streng nok til å blokkere mange mennesker. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Det er ingen måte å løse captcha uten å aktivere Javascript og Cookies.Cloudflare bruker dem til å lage en nettlesersignatur for å identifisere deg.Cloudflare trenger å vite din identitet for å bestemme om du er kvalifisert til å fortsette å surfe på nettstedet. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor-brukere og VPN-brukere er også et offer for Cloudflare.Begge løsningene brukes av mange mennesker som ikke har råd til usensurert internett på grunn av sitt land / selskap / nettverkspolitikk, eller som ønsker å legge til ekstra lag for å beskytte deres privatliv.Cloudflare angriper skamløst disse menneskene og tvinger dem til å slå av proxy-løsningen deres. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Hvis du ikke prøvde Tor før dette øyeblikket, oppfordrer vi deg til å laste ned Tor Browser og besøke favorittnettstedene dine.Vi foreslår at du ikke logger inn på banknettstedet ditt eller den offentlige nettsiden, ellers vil de flagge kontoen din. Bruk VPN for disse nettstedene. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Det kan være lurt å si “Tor er ulovlig! Tor-brukere er kriminelle! Tor er dårlig! ". Nei.Du lærte kanskje om Tor fra TV og sa at Tor kan brukes til å bla gjennom mørknet og bytte våpen, narkotika eller chid porn.Selv om utsagnet ovenfor er sant at det er mange markedsnettsteder der du kan kjøpe slike varer, vises disse nettstedene ofte på clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor ble utviklet av den amerikanske hæren, men nåværende Tor er utviklet av Tor-prosjektet.Det er mange mennesker og organisasjoner som bruker Tor inkludert dine fremtidige venner.Så hvis du bruker Cloudflare på nettstedet ditt, blokkerer du virkelige mennesker.Du vil miste potensielt vennskap og forretningsavtale. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Og deres DNS-tjeneste, 1.1.1.1, filtrerer også brukere fra å besøke nettstedet ved å returnere falske IP-adresser eid av Cloudflare, localhost IP som "127.0.0.x", eller bare returnere ingenting. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS bryter også online programvare fra smarttelefon-app til dataspill på grunn av deres falske DNS-svar.Cloudflare DNS kan ikke spørre noen banknettsteder. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Og her tenker du kanskje,<br>Jeg bruker ikke Tor eller VPN, hvorfor skal jeg bry meg?<br>Jeg stoler på markedsføring av Cloudflare, hvorfor skal jeg bry meg<br>Nettstedet mitt er https hvorfor skal jeg bry meg | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Hvis du besøker nettstedet som bruker Cloudflare, deler du informasjonen din ikke bare til nettstedseieren, men også Cloudflare.Slik fungerer den omvendte proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Det er umulig å analysere uten å dekryptere TLS-trafikk. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare kjenner alle dataene dine, for eksempel rått passord. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed kan skje når som helst. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflares https er aldri ende til ende. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Vil du virkelig dele dataene dine med Cloudflare, og også byrå med 3 bokstaver? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Internett-brukerens online profil er et "produkt" som regjeringen og store teknologiselskaper ønsker å kjøpe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  U.S. Department of Homeland Security sa:<br><br>Har du noen anelse om hvor verdifulle dataene du har er? Er det noen måte du vil selge oss disse dataene på?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare tilbyr også GRATIS VPN-tjeneste kalt “Cloudflare Warp”.Hvis du bruker den, blir alle smarttelefonforbindelsene dine (eller datamaskinen) sendt til Cloudflare-servere.Cloudflare kan vite hvilket nettsted du har lest, hvilken kommentar du har lagt ut, hvem du har snakket med osv.Du gir frivillig all din informasjon til Cloudflare.Hvis du tenker “Spøker du? Cloudflare er sikkert. ” så må du lære hvordan VPN fungerer. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare sa at VPN-tjenesten deres gjør internett raskt.Men VPN gjør internettforbindelsen din tregere enn din eksisterende tilkobling. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Du vet kanskje allerede om PRISM-skandalen.Det er sant at AT&T lar NSA kopiere alle internettdata for overvåking. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  La oss si at du jobber på NSA, og at du vil ha hver innbyggers internettprofil.Du vet at de fleste av dem stoler blindt på Cloudflare og bruker den - bare en sentralisert gateway - til å proxy selskapsservertilkoblingen (SSH / RDP), personlig hjemmeside, chatnettsted, forumnettsted, banknettsted, forsikringsnettsted, søkemotor, hemmelig medlem -nett nettsted, auksjonsnettsted, shopping, video-nettsted, NSFW-nettsted og ulovlig nettsted.Du vet også at de bruker Cloudflares DNS-tjeneste ("1.1.1.1") og VPN-tjeneste ("Cloudflare Warp") for "Secure! Raskere! Bedre!" internettopplevelse.Å kombinere dem med brukerens IP-adresse, nettleserens fingeravtrykk, informasjonskapsler og RAY-ID vil være nyttig for å bygge målets online profil. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Du vil ha dataene deres. Hva vil du gjøre? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare er en honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Gratis honning for alle. Noen strenger festet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Ikke bruk Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Desentraliser internett.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Fortsett til neste side:  "[Cloudflare-etikk](no.ethics.md)"

---

<details>
<summary>_Klikk på meg_

## Data og mer informasjon
</summary>


Dette depotet er en liste over nettsteder som ligger bak "The Great Cloudwall", som blokkerer Tor-brukere og andre CDN-er.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare-brukere](../cloudflare_users/)
* [Cloudflare-domener](../cloudflare_users/domains/)
* [Ikke-Cloudflare CDN-brukere](../not_cloudflare/)
* [Anti-Tor brukere](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Mer informasjon**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * nedlasting: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Den opprinnelige eBok (ePUB) ble slettet av BookRix GmbH på grunn av brudd på copyright av CC0-materiale
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Billetten ble vandalisert så mange ganger.
  * [Slettet av Tor-prosjektet.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Se billett 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Siste arkivbillett 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_Klikk på meg_

## Hva kan du gjøre?
</summary>

* [Les listen over anbefalte handlinger og del den med vennene dine.](../ACTION.md)

* [Les stemmer fra andre brukere og skriv tankene dine.](../PEOPLE.md)

* Søk på noe: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Oppdater domenelisten: [Liste instruksjoner](../INSTRUCTION.md).

* [Legg til Cloudflare eller prosjektrelatert hendelse i historien.](../HISTORY.md)

* [Prøv og skriv nytt verktøy / skript.](../tool/)

* [Her er noen PDF / ePUB å lese.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Om falske kontoer

Crimeflare vet om eksistensen av falske kontoer som utgir seg for våre offisielle kanaler, det være seg Twitter, Facebook, Patreon, OpenCollective, Villages etc.
**Vi spør aldri e-posten din.
Vi spør aldri navnet ditt.
Vi spør aldri din identitet.
Vi spør aldri hvor du befinner deg.
Vi ber deg aldri om donasjonen din.
Vi ber aldri om anmeldelsen din.
Vi ber deg aldri følge på sosiale medier.
Vi spør aldri sosiale medier.**

# PÅTRYKK IKKE REGNSKAPER.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)