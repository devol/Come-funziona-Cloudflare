# Tường mây vĩ đại


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Dừng Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  “Great Cloudwall” là Cloudflare Inc., công ty của Hoa Kỳ.Nó đang cung cấp các dịch vụ CDN (mạng phân phối nội dung), giảm thiểu DDoS, bảo mật Internet và dịch vụ DNS (máy chủ tên miền) phân tán.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare là proxy MITM (proxy ngược) lớn nhất thế giới.Cloudflare sở hữu hơn 80% thị phần CDN và số lượng người dùng cloudflare đang tăng lên mỗi ngày.Họ đã mở rộng mạng lưới của mình đến hơn 100 quốc gia.Cloudflare phục vụ nhiều lưu lượng truy cập web hơn Twitter, Amazon, Apple, Instagram, Bing & Wikipedia cộng lại.Cloudflare đang cung cấp gói miễn phí và nhiều người đang sử dụng nó thay vì định cấu hình máy chủ của họ đúng cách.Họ đánh đổi sự riêng tư hơn sự tiện lợi.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare nằm giữa bạn và máy chủ web gốc, hoạt động như một nhân viên tuần tra biên giới.Bạn không thể kết nối với điểm đến đã chọn của mình.Bạn đang kết nối với Cloudflare và tất cả thông tin của bạn đang được giải mã và chuyển giao nhanh chóng. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Quản trị viên máy chủ web gốc cho phép tác nhân - Cloudflare - quyết định ai có thể truy cập vào “thuộc tính web” của họ và xác định “khu vực hạn chế”.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Hãy nhìn vào hình ảnh bên phải.Bạn sẽ nghĩ Cloudflare chỉ chặn kẻ xấu.Bạn sẽ nghĩ rằng Cloudflare luôn trực tuyến (không bao giờ ngừng hoạt động).Bạn sẽ nghĩ rằng các chương trình và trình thu thập thông tin hợp pháp có thể lập chỉ mục trang web của bạn.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Tuy nhiên những điều đó hoàn toàn không đúng.Cloudflare đang chặn những người vô tội mà không có lý do.Cloudflare có thể ngừng hoạt động.Cloudflare chặn các bot hợp pháp.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Cũng giống như bất kỳ dịch vụ lưu trữ nào, Cloudflare không hoàn hảo.Bạn sẽ thấy màn hình này ngay cả khi máy chủ gốc đang hoạt động tốt.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Bạn có thực sự nghĩ rằng Cloudflare có 100% thời gian hoạt động không?Bạn không biết Cloudflare gặp sự cố bao nhiêu lần.Nếu Cloudflare ngừng hoạt động, khách hàng của bạn không thể truy cập trang web của bạn. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Nó được gọi là như vậy liên quan đến Tường lửa lớn của Trung Quốc, thực hiện một công việc tương đương là lọc ra nhiều người khỏi xem nội dung web (tức là tất cả mọi người ở Trung Quốc đại lục và những người bên ngoài).Trong khi đồng thời những người không bị ảnh hưởng có thể nhìn thấy một trang web hoàn toàn khác, một trang web không bị kiểm duyệt chẳng hạn như hình ảnh “người xe tăng” và lịch sử của “các cuộc biểu tình trên Quảng trường Thiên An Môn”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare sở hữu sức mạnh tuyệt vời.Theo một nghĩa nào đó, họ kiểm soát những gì người dùng cuối cuối cùng nhìn thấy.Bạn bị chặn duyệt trang web do Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare có thể được sử dụng để kiểm duyệt. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Bạn không thể xem trang web cloudflared nếu bạn đang sử dụng trình duyệt nhỏ mà Cloudflare có thể nghĩ rằng đó là bot (vì không có nhiều người sử dụng nó). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Bạn không thể vượt qua “kiểm tra trình duyệt” xâm lấn này mà không bật Javascript.Đây là sự lãng phí năm (hoặc hơn) giây trong cuộc đời quý giá của bạn. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare cũng tự động chặn các rô bốt / trình thu thập thông tin hợp pháp như Google, Yandex, Yacy và các ứng dụng khách API.Cloudflare đang tích cực theo dõi cộng đồng “bỏ qua cloudflare” với mục đích phá vỡ các chương trình nghiên cứu hợp pháp. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Tương tự như vậy, Cloudflare ngăn nhiều người có kết nối internet kém truy cập vào các trang web đằng sau nó (ví dụ: họ có thể đứng sau hơn 7 lớp NAT hoặc chia sẻ cùng một IP, chẳng hạn như Wifi công cộng) trừ khi họ giải quyết nhiều CAPTCHA hình ảnh.Trong một số trường hợp, điều này sẽ mất từ ​​10 đến 30 phút để làm hài lòng Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Vào năm 2020, Cloudflare chuyển từ Google’s Recaptcha sang hCaptcha vì Google dự định tính phí cho việc sử dụng nó.Cloudflare nói với bạn rằng họ quan tâm đến quyền riêng tư của bạn (“nó giúp giải quyết mối quan tâm về quyền riêng tư”) nhưng đây rõ ràng là một lời nói dối.Đó là tất cả về tiền.“HCaptcha cho phép các trang web kiếm tiền từ nhu cầu này trong khi chặn bot và các hình thức lạm dụng khác” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Từ quan điểm của người dùng, điều này không thay đổi nhiều. Bạn đang bị buộc phải giải quyết nó. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Nhiều người và phần mềm đang bị Cloudflare chặn mỗi ngày. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare làm phiền nhiều người trên thế giới.Hãy xem danh sách và nghĩ xem việc sử dụng Cloudflare trên trang web của bạn có tốt cho trải nghiệm người dùng hay không. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Mục đích của Internet là gì nếu bạn không thể làm những gì bạn muốn?Hầu hết những người truy cập trang web của bạn sẽ chỉ tìm kiếm các trang khác nếu họ không thể tải một trang web.Bạn có thể không chủ động chặn bất kỳ khách truy cập nào, nhưng tường lửa mặc định của Cloudflare đủ nghiêm ngặt để chặn nhiều người. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Không có cách nào để giải quyết captcha mà không bật Javascript và Cookie.Cloudflare đang sử dụng chúng để tạo chữ ký trình duyệt nhằm nhận dạng bạn.Cloudflare cần biết danh tính của bạn để quyết định xem bạn có đủ điều kiện để tiếp tục duyệt trang web hay không. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Người dùng Tor và người dùng VPN cũng là nạn nhân của Cloudflare.Cả hai giải pháp đang được sử dụng bởi nhiều người không có khả năng mua internet không bị kiểm duyệt do chính sách quốc gia / công ty / mạng của họ hoặc những người muốn thêm lớp bổ sung để bảo vệ quyền riêng tư của họ.Cloudflare đang tấn công những người đó một cách vô liêm sỉ, buộc họ phải tắt giải pháp proxy của mình. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Nếu bạn chưa dùng thử Tor cho đến thời điểm này, chúng tôi khuyến khích bạn tải xuống Tor Browser và truy cập các trang web yêu thích của bạn.Chúng tôi khuyên bạn không nên đăng nhập vào trang web ngân hàng hoặc trang web của chính phủ, nếu không họ sẽ gắn cờ tài khoản của bạn. Sử dụng VPN cho các trang web đó. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Bạn có thể muốn nói “Tor là bất hợp pháp! Người dùng Tor là tội phạm! Tor tệ lắm! ". Không.Bạn có thể biết về Tor từ truyền hình, nói rằng Tor có thể được sử dụng để duyệt darknet và buôn bán súng, ma túy hoặc khiêu dâm chid.Mặc dù tuyên bố trên là đúng rằng có rất nhiều trang web thị trường nơi bạn có thể mua những mặt hàng như vậy, nhưng những trang web đó cũng thường xuất hiện trên mạng rõ ràng.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor được phát triển bởi US Army, nhưng Tor hiện tại được phát triển bởi dự án Tor.Có rất nhiều người và tổ chức sử dụng Tor bao gồm cả những người bạn trong tương lai của bạn.Vì vậy, nếu bạn đang sử dụng Cloudflare trên trang web của mình, bạn đang chặn người thật.Bạn sẽ mất đi tình bạn tiềm năng và giao dịch kinh doanh. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Và dịch vụ DNS của họ, 1.1.1.1, cũng đang lọc người dùng truy cập trang web bằng cách trả lại địa chỉ IP giả do Cloudflare sở hữu, IP localhost như “127.0.0.x”, hoặc không trả lại gì. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS cũng phá vỡ phần mềm trực tuyến từ ứng dụng điện thoại thông minh đến trò chơi máy tính vì câu trả lời DNS giả mạo của chúng.Cloudflare DNS không thể truy vấn một số trang web ngân hàng. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Và ở đây bạn có thể nghĩ,<br>Tôi không sử dụng Tor hoặc VPN, tại sao tôi phải quan tâm?<br>Tôi tin tưởng tiếp thị Cloudflare, tại sao tôi phải quan tâm<br>Trang web của tôi là https tại sao tôi phải quan tâm | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Nếu bạn truy cập trang web sử dụng Cloudflare, bạn đang chia sẻ thông tin của mình không chỉ với chủ sở hữu trang web mà còn với Cloudflare.Đây là cách hoạt động của proxy ngược. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Không thể phân tích mà không giải mã lưu lượng TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare biết tất cả dữ liệu của bạn, chẳng hạn như mật khẩu thô. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed có thể xảy ra bất cứ lúc nào. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https của Cloudflare không bao giờ là kết thúc. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Bạn có thực sự muốn chia sẻ dữ liệu của mình với Cloudflare và cả đại lý 3 chữ cái không? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Hồ sơ trực tuyến của người dùng Internet là một “sản phẩm” mà chính phủ và các công ty công nghệ lớn muốn mua. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Bộ An ninh Nội địa Hoa Kỳ cho biết:<br><br>Bạn có biết dữ liệu bạn có giá trị như thế nào không? Có cách nào bạn muốn bán cho chúng tôi dữ liệu đó không?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare cũng cung cấp dịch vụ VPN MIỄN PHÍ có tên “Cloudflare Warp”.Nếu bạn sử dụng nó, tất cả các kết nối điện thoại thông minh (hoặc máy tính của bạn) sẽ được gửi đến máy chủ Cloudflare.Cloudflare có thể biết bạn đã đọc trang web nào, bạn đã đăng nhận xét nào, bạn đã nói chuyện với ai, v.v.Bạn tự nguyện cung cấp tất cả thông tin của mình cho Cloudflare.Nếu bạn nghĩ rằng “Bạn đang nói đùa? Cloudflare an toàn. ” thì bạn cần tìm hiểu cách thức hoạt động của VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare cho biết dịch vụ VPN của họ giúp Internet của bạn nhanh hơn.Nhưng VPN làm cho kết nối internet của bạn chậm hơn kết nối hiện có của bạn. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Bạn có thể đã biết về vụ bê bối PRISM.Đúng là AT&T cho phép NSA sao chép tất cả dữ liệu internet để giám sát. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Giả sử bạn đang làm việc tại NSA và bạn muốn có hồ sơ trên internet của mọi công dân.Bạn biết đấy, hầu hết họ đang tin tưởng vào Cloudflare một cách mù quáng và sử dụng nó - chỉ có một cổng tập trung - để ủy quyền kết nối máy chủ công ty của họ (SSH / RDP), trang web cá nhân, trang web trò chuyện, trang web diễn đàn, trang web ngân hàng, trang web bảo hiểm, công cụ tìm kiếm, thành viên bí mật -chỉ trang web, trang web đấu giá, mua sắm, trang web video, trang web NSFW và trang web bất hợp pháp.Bạn cũng biết rằng họ sử dụng dịch vụ DNS của Cloudflare ("1.1.1.1") và dịch vụ VPN ("Cloudflare Warp") cho mục “Bảo mật! Nhanh hơn! Tốt hơn!" kinh nghiệm internet.Kết hợp chúng với địa chỉ IP của người dùng, vân tay trình duyệt, cookie và RAY-ID sẽ hữu ích để xây dựng hồ sơ trực tuyến của mục tiêu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Bạn muốn dữ liệu của họ. Bạn sẽ làm gì? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare là một honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Mật ong miễn phí cho tất cả mọi người. Một số chuỗi đính kèm.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Không sử dụng Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Phân quyền internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Vui lòng tiếp tục sang trang tiếp theo:  "[Đạo đức Cloudflare](vi.ethics.md)"

---

<details>
<summary>_nhấp vào đây_

## Dữ liệu và Thông tin khác
</summary>


Kho lưu trữ này là danh sách các trang web đứng sau "Bức tường mây vĩ đại", chặn người dùng Tor và các CDN khác.


**Dữ liệu**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Người dùng Cloudflare](../cloudflare_users/)
* [Miền Cloudflare](../cloudflare_users/domains/)
* [Người dùng CDN không phải Cloudflare](../not_cloudflare/)
* [Người dùng Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Thêm thông tin**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Tải xuống: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Sách điện tử gốc (ePUB) đã bị BookRix GmbH xóa do vi phạm bản quyền tài liệu CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Vé đã bị phá hoại rất nhiều lần.
  * [Đã bị xóa bởi Dự án Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Xem vé 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Phiếu lưu trữ cuối cùng 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_nhấp vào đây_

## Bạn có thể làm gì?
</summary>

* [Đọc danh sách các hành động được đề xuất của chúng tôi và chia sẻ nó với bạn bè của bạn.](../ACTION.md)

* [Đọc giọng nói của người dùng khác và viết suy nghĩ của bạn.](../PEOPLE.md)

* Tìm kiếm thứ gì đó: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Cập nhật danh sách miền: [Liệt kê hướng dẫn](../INSTRUCTION.md).

* [Thêm Cloudflare hoặc sự kiện liên quan đến dự án vào lịch sử.](../HISTORY.md)

* [Thử và viết Công cụ / Tập lệnh mới.](../tool/)

* [Đây là một số PDF / ePUB để đọc.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Về tài khoản giả mạo

Crimeflare biết về sự tồn tại của các tài khoản giả mạo mạo danh các kênh chính thức của chúng tôi, có thể là Twitter, Facebook, Patreon, OpenCollective, Villages, v.v.
**Chúng tôi không bao giờ hỏi email của bạn.
Chúng tôi không bao giờ hỏi tên của bạn.
Chúng tôi không bao giờ hỏi danh tính của bạn.
Chúng tôi không bao giờ hỏi vị trí của bạn.
Chúng tôi không bao giờ yêu cầu đóng góp của bạn.
Chúng tôi không bao giờ yêu cầu đánh giá của bạn.
Chúng tôi không bao giờ yêu cầu bạn theo dõi trên mạng xã hội.
Chúng tôi không bao giờ hỏi phương tiện truyền thông xã hội của bạn.**

# KHÔNG TIN TƯỞNG TÀI KHOẢN GIẢ.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)