# The Great Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Zatrzymaj Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  „The Great Cloudwall” to amerykańska firma Cloudflare Inc.Świadczy usługi CDN (sieć dostarczania treści), przeciwdziałanie atakom DDoS, bezpieczeństwo w Internecie i rozproszone usługi DNS (serwer nazw domen).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare to największy na świecie serwer proxy MITM (odwrotny serwer proxy).Cloudflare posiada ponad 80% udziału w rynku CDN, a liczba użytkowników Cloudflare rośnie każdego dnia.Rozszerzyli swoją sieć do ponad 100 krajów.Cloudflare obsługuje większy ruch internetowy niż łącznie Twitter, Amazon, Apple, Instagram, Bing i Wikipedia.Cloudflare oferuje bezpłatny plan i wiele osób korzysta z niego zamiast prawidłowo konfigurować swoje serwery.Zamienili prywatność na wygodę.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare znajduje się między tobą a pierwotnym serwerem internetowym, działając jak agent patrolu granicznego.Nie możesz połączyć się z wybranym miejscem docelowym.Łączysz się z Cloudflare i wszystkie Twoje informacje są odszyfrowywane i przekazywane w locie. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Główny administrator serwera internetowego pozwolił agentowi - Cloudflare - zdecydować, kto może uzyskać dostęp do ich „usługi internetowej” i zdefiniować „obszar zastrzeżony”.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Spójrz na właściwy obraz.Pomyślisz, że Cloudflare blokuje tylko złych facetów.Pomyślisz, że Cloudflare jest zawsze online (nigdy nie opuszczaj).Pomyślisz, że legalne boty i roboty indeksujące mogą indeksować Twoją witrynę.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Jednak to wcale nie jest prawda.Cloudflare blokuje niewinnych ludzi bez powodu.Cloudflare może spaść.Cloudflare blokuje legalne boty.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Podobnie jak każda usługa hostingowa, Cloudflare nie jest doskonały.Zobaczysz ten ekran, nawet jeśli serwer pochodzenia działa dobrze.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Czy naprawdę uważasz, że Cloudflare ma 100% czasu sprawności?Nie masz pojęcia, ile razy Cloudflare spada.Jeśli Cloudflare przestanie działać, Twój klient nie będzie mógł uzyskać dostępu do Twojej witryny. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Nazywa się to w odniesieniu do Wielkiego Chińskiego Firewall'a, który wykonuje podobne zadanie odfiltrowywania wielu ludzi przed oglądaniem treści internetowych (tj. Wszystkich w Chinach kontynentalnych i ludzi poza nimi).Podczas gdy jednocześnie ci, którzy nie zostali dotknięci, widzą zupełnie inną sieć, sieć wolną od cenzury, taką jak wizerunek „czołgisty” i historię „protestów na placu Tiananmen”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare posiada wielką moc.W pewnym sensie kontrolują to, co ostatecznie widzi użytkownik.Nie możesz przeglądać tej witryny z powodu Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare może być używany do cenzury. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Nie możesz wyświetlić witryny Cloudflared, jeśli używasz pomniejszej przeglądarki, o której Cloudflare może myśleć, że jest botem (ponieważ niewiele osób z niej korzysta). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Nie możesz przejść tego inwazyjnego „sprawdzenia przeglądarki” bez włączenia Javascript.To strata pięciu (lub więcej) sekund twojego cennego życia. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare automatycznie blokuje również legalne roboty / roboty, takie jak klienci Google, Yandex, Yacy i API.Cloudflare aktywnie monitoruje społeczność „omijania cloudflare” z zamiarem złamania legalnych botów badawczych. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare podobnie uniemożliwia wielu osobom, które mają słabą łączność z Internetem, dostęp do stron internetowych za nim (na przykład mogą znajdować się za 7+ warstwami NAT lub mieć ten sam adres IP, na przykład publiczne Wi-Fi), chyba że rozwiążą kilka CAPTCHA obrazów.W niektórych przypadkach zajmie to od 10 do 30 minut, zanim Google będzie zadowolony. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  W roku 2020 Cloudflare przeszedł z Google Recaptcha na hCaptcha, ponieważ Google zamierza pobierać opłaty za jej użycie.Cloudflare powiedział ci, że dba o twoją prywatność („pomaga rozwiązać problem dotyczący prywatności”), ale to oczywiście kłamstwo.Chodzi o pieniądze.„HCaptcha pozwala stronom internetowym zarabiać na tym żądaniu, blokując boty i inne formy nadużyć” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Z punktu widzenia użytkownika to niewiele się zmienia. Jesteś zmuszony go rozwiązać. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare blokuje codziennie wielu ludzi i oprogramowanie. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare irytuje wiele osób na całym świecie.Spójrz na listę i zastanów się, czy przyjęcie Cloudflare w Twojej witrynie jest dobre dla wygody użytkownika. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Jaki jest cel internetu, jeśli nie możesz robić tego, co chcesz?Większość osób, które odwiedzają Twoją witrynę, będzie po prostu szukać innych stron, jeśli nie mogą załadować strony internetowej.Być może nie blokujesz aktywnie żadnych odwiedzających, ale domyślna zapora sieciowa Cloudflare jest wystarczająco rygorystyczna, aby blokować wiele osób. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Nie ma możliwości rozwiązania captcha bez włączenia Javascript i Cookies.Cloudflare używa ich do tworzenia podpisu przeglądarki, aby Cię zidentyfikować.Cloudflare musi znać Twoją tożsamość, aby zdecydować, czy kwalifikujesz się do kontynuowania przeglądania witryny. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Użytkownicy Tora i użytkownicy VPN również są ofiarami Cloudflare.Oba rozwiązania są używane przez wiele osób, które nie mogą sobie pozwolić na nieocenzurowany internet ze względu na politykę kraju / korporacji / sieci lub chcą dodać dodatkową warstwę w celu ochrony swojej prywatności.Cloudflare bezwstydnie atakuje tych ludzi, zmuszając ich do wyłączenia swojego rozwiązania proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Jeśli nie próbowałeś Tora do tej chwili, zachęcamy do pobrania przeglądarki Tor Browser i odwiedzenia ulubionych witryn internetowych.Sugerujemy, aby nie logować się do witryny banku lub strony rządowej, w przeciwnym razie oznaczy Twoje konto. Używaj VPN dla tych witryn. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Możesz chcieć powiedzieć „Tor jest nielegalny! Użytkownicy Tora są przestępcami! Tor jest zły! ”. Nie.Możesz dowiedzieć się o Torze z telewizji, mówiąc, że Tora można używać do przeglądania darknetu i handlu bronią, narkotykami lub dziecięcą pornografią.Chociaż powyższe stwierdzenie jest prawdą, że istnieje wiele witryn internetowych, na których można kupić takie produkty, witryny te często pojawiają się również w Clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor został opracowany przez armię amerykańską, ale obecny Tor jest rozwijany przez projekt Tor.Jest wiele osób i organizacji, które używają Tora, w tym twoi przyszli przyjaciele.Tak więc, jeśli używasz Cloudflare w swojej witrynie, blokujesz prawdziwych ludzi.Stracisz potencjalną przyjaźń i umowę biznesową. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Ich usługa DNS, 1.1.1.1, również odfiltrowuje użytkowników z odwiedzania strony internetowej, zwracając fałszywy adres IP należący do Cloudflare, adres IP lokalnego hosta, taki jak „127.0.0.x”, lub po prostu nie zwracając niczego. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS również przerywa oprogramowanie online od aplikacji na smartfony do gry komputerowej z powodu fałszywej odpowiedzi DNS.Cloudflare DNS nie może wysyłać zapytań do witryn niektórych banków. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  I tutaj możesz pomyśleć,<br>Nie używam Tora ani VPN, dlaczego powinno mnie to obchodzić?<br>Ufam marketingowi Cloudflare, dlaczego powinno mnie to obchodzić<br>Moja witryna to https, dlaczego powinno mnie to obchodzić | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Jeśli odwiedzasz witrynę, która korzysta z Cloudflare, udostępniasz swoje informacje nie tylko właścicielowi witryny, ale także Cloudflare.Tak działa odwrotne proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Nie można przeprowadzić analizy bez odszyfrowania ruchu TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare zna wszystkie Twoje dane, takie jak surowe hasło. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed może się zdarzyć w każdej chwili. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Protokół https Cloudflare nigdy nie jest kompleksowy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Czy na pewno chcesz udostępnić swoje dane Cloudflare, a także trzyliterowej agencji? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Profil online internauty to „produkt”, który rząd i duże firmy technologiczne chcą kupić. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Departament Bezpieczeństwa Wewnętrznego Stanów Zjednoczonych powiedział:<br><br>Czy masz pojęcie, jak cenne są Twoje dane? Czy jest jakiś sposób, żebyś sprzedał nam te dane?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare oferuje również DARMOWĄ usługę VPN o nazwie „Cloudflare Warp”.Jeśli go używasz, wszystkie połączenia twojego smartfona (lub komputera) są wysyłane do serwerów Cloudflare.Cloudflare może wiedzieć, którą witrynę przeczytałeś, jaki komentarz opublikowałeś, z kim rozmawiałeś itp.Podajesz wszystkie swoje informacje Cloudflare dobrowolnie.Jeśli myślisz „Żartujesz? Cloudflare jest bezpieczny ”. wtedy musisz się dowiedzieć, jak działa VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare powiedział, że ich usługa VPN przyspiesza Twój internet.Ale VPN sprawia, że ​​twoje połączenie internetowe jest wolniejsze niż istniejące. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Być może już wiesz o skandalu PRISM.Prawdą jest, że AT&T zezwala NSA na kopiowanie wszystkich danych internetowych w celu nadzoru. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Załóżmy, że pracujesz w NSA i chcesz mieć profil internetowy każdego obywatela.Wiesz, że większość z nich ślepo ufa Cloudflare i używa go - tylko jednej scentralizowanej bramy - do proxy połączenia z serwerem firmy (SSH / RDP), osobistej strony internetowej, strony czatu, strony forum, strony banku, strony ubezpieczeniowej, wyszukiwarki, tajnego członka -tylko witryna internetowa, witryna aukcyjna, witryna handlowa, witryna wideo, witryna NSFW i witryna nielegalna.Wiesz również, że używają usługi DNS Cloudflare („1.1.1.1”) i usługi VPN („Cloudflare Warp”) w celu zapewnienia bezpieczeństwa! Szybciej! Lepszy!" doświadczenie w Internecie.Połączenie ich z adresem IP użytkownika, odciskami palców przeglądarki, plikami cookie i RAY-ID będzie przydatne do zbudowania profilu online celu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Chcesz ich danych. Co zrobisz? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare to honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Darmowy miód dla każdego. Trochę sznurków.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Nie używaj Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Zdecentralizuj internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Przejdź do następnej strony:  "[Etyka Cloudflare](pl.ethics.md)"

---

<details>
<summary>_Kliknij_

## Dane i więcej informacji
</summary>


To repozytorium to lista stron internetowych, które stoją za „Wielką zaporą chmurową”, blokując użytkowników Tora i inne sieci CDN.


**Dane**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Użytkownicy Cloudflare](../cloudflare_users/)
* [Domeny Cloudflare](../cloudflare_users/domains/)
* [Użytkownicy spoza Cloudflare CDN](../not_cloudflare/)
* [Użytkownicy Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Więcej informacji**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Pobieranie: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Oryginalny eBook (ePUB) został usunięty przez BookRix GmbH z powodu naruszenia praw autorskich do materiału CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Bilet był wielokrotnie zdewastowany.
  * [Usunięte przez Projekt Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Zobacz bilet 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Ostatni bilet archiwalny 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_Kliknij_

## Co możesz zrobić?
</summary>

* [Przeczytaj naszą listę zalecanych działań i podziel się nią ze znajomymi.](../ACTION.md)

* [Przeczytaj głos innych użytkowników i napisz swoje myśli.](../PEOPLE.md)

* Szukaj czegoś: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Zaktualizuj listę domen: [Lista instrukcji](../INSTRUCTION.md).

* [Dodaj Cloudflare lub wydarzenie związane z projektem do historii.](../HISTORY.md)

* [Spróbuj napisać nowe narzędzie / skrypt.](../tool/)

* [Oto kilka plików PDF / ePUB do przeczytania.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### O fałszywych kontach

Crimeflare wie o istnieniu fałszywych kont podszywających się pod nasze oficjalne kanały, czy to Twitter, Facebook, Patreon, OpenCollective, Villages itp.
**Nigdy nie pytamy o Twój e-mail.
Nigdy nie pytamy o twoje imię.
Nigdy nie pytamy o twoją tożsamość.
Nigdy nie pytamy o Twoją lokalizację.
Nigdy nie prosimy o darowizny.
Nigdy nie prosimy o Twoją recenzję.
Nigdy nie prosimy Cię o śledzenie w mediach społecznościowych.
Nigdy nie pytamy o Twoje media społecznościowe.**

# NIE ufaj fałszywym kontom.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)