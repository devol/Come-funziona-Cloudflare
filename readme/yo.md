# Awọsanma Nla


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Duro Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "Awọsanma Nla" naa ni Cloudflare Inc., ile-iṣẹ U.S.O n pese awọn iṣẹ CDN (nẹtiwọọki ifijiṣẹ akoonu), idinku DDoS, aabo Intanẹẹti, ati pinpin awọn iṣẹ olupin (olupin orukọ orukọ).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare jẹ aṣoju MITM ti o tobi julọ ni agbaye (aṣoju aṣoju).Cloudflare ni diẹ sii ju 80% ti ipin ọja CDN ati nọmba awọn olumulo Cloudflare n dagba ni ọjọ kọọkan.Wọn ti fẹ nẹtiwọki wọn si awọn orilẹ-ede to ju ọgọrun lọ.Cloudflare ṣe iṣẹ ijabọ wẹẹbu diẹ sii ju Twitter, Amazon, Apple, Instagram, Bing & Wikipedia ni idapo.Cloudflare n funni ni ọfẹ ọfẹ ati ọpọlọpọ eniyan lo o dipo atunṣeto awọn olupin wọn daradara.Wọn ta ọja aṣiri lori irọrun.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare joko laarin iwọ ati onkọwe webserver, o n ṣe bi aṣoju oluba aala.O ko ni anfani lati sopọ si opin irin ajo rẹ.O n ṣopọ si Cloudflare ati pe gbogbo alaye rẹ ti ni ipinnu ati fifun lori fifo. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Alakoso webserver ipilẹṣẹ gba oluranlowo lọwọ - Cloudflare - lati pinnu tani o le wọle si “ohun-ini wẹẹbu” wọn ati ṣalaye “agbegbe ihamọ”.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Wo aworan ti o tọ.Iwọ yoo ronu bulọọki Cloudflare nikan awọn eniyan buruku.Iwọ yoo ro pe Cloudflare nigbagbogbo wa lori ayelujara (rara rara).O yoo ro awọn botini to tọ ati awọn onigita le ṣe atọka oju opo wẹẹbu rẹ.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Sibẹsibẹ awọn iyẹn kii ṣe otitọ rara.Cloudflare n dena awọn eniyan alaiṣẹ laisi idi.Cloudflare le lọ silẹ.Awọn bulọọki Cloudflare legit bot.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  O kan bi iṣẹ alejo gbigba eyikeyi, Cloudflare ko pe.Iwọ yoo wo iboju yii paapaa ti olupin ipilẹṣẹ n ṣiṣẹ daradara.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Ṣe o ronu tootọ ni Cloudflare ni akoko 100%?O ko ni imọran iye igba ti Cloudflare n lọ silẹ.Ti Cloudflare ba lọ si alabara rẹ ko le wọle si oju opo wẹẹbu rẹ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  A pe ni eyi ni tọka si Ogiriina Nla ti China eyiti o ṣe iṣẹ afiwera ti sisẹ jade ọpọlọpọ awọn eniyan lati ri akoonu wẹẹbu (i.e. gbogbo eniyan ni Ilu China ati awọn eniyan ni ita).Lakoko ti o jẹ nigbakanna awọn ti ko ni fowo lati wo oju opo wẹẹbu ti o yatọ, oju opo wẹẹbu ti idanimọ bii aworan “eniyan ojò” ati itan-akọọlẹ ti awọn ifihan “Tiananmen Square”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare gba agbara nla.Ni ọna kan, wọn ṣakoso ohun ti olumulo opin yoo rii nikẹhin.O ṣe idiwọ fun lilọ kiri lori oju opo wẹẹbu nitori Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  A le lo Cloudflare fun ijuwe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Iwọ ko le wo oju opo wẹẹbu Cloudflared ti o ba nlo aṣawakiri kekere eyiti Cloudflare le ro pe o jẹ bot (nitori kii ṣe ọpọlọpọ eniyan lo o). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  O ko le ṣe afasilẹ afasiri “ṣayẹwo ẹrọ aṣiri” yii laisi muu JavaScript ṣiṣẹ.Eyi jẹ egbin ti awọn iṣẹju marun marun (tabi diẹ sii) ti igbesi aye ti o niyelori. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare tun ṣe idiwọ awọn roboti legit / crawlers bii Google, Yandex, Yacy, ati awọn alabara API.Cloudflare n ṣiṣẹ taara ni aabo “agbegbe Cloudflare” ti o ni ipinnu lati fọ awọn bot ni iwadii iwulo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare bakanna ṣe idiwọ fun ọpọlọpọ awọn eniyan ti o ni asopọ asopọ intanẹẹti ti ko dara lati wọle si awọn oju opo wẹẹbu lẹyin rẹ (fun apẹẹrẹ, wọn le wa ni ẹhin awọn ipele 7+ ti NAT tabi pinpin IP kanna, fun apẹẹrẹ Wifi gbangba) ayafi ti wọn ba yanju ọpọ CAPTCHAs ọpọ aworan.Ni awọn ọrọ kan, eyi yoo gba iṣẹju 10 si 30 lati ni itẹlọrun Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Ni ọdun 2020 Cloudflare yipada lati Google's Recaptcha si hCaptcha bi Google ṣe pinnu lati gba agbara fun lilo rẹ.Cloudflare sọ fun ọ pe wọn tọju aṣiri rẹ (“o ṣe iranlọwọ lati koju ibakcdun aṣiri kan”) ṣugbọn eyi han gedegbe.O jẹ gbogbo nipa owo.“HCaptcha n fun awọn aaye ayelujara laaye lati ṣe owo jijẹ ibeere yii lakoko didena awọn bot ati awọn iru ilokulo miiran” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Lati oju iwoye olumulo, eyi ko ni yi pada pupọ. O ti wa ni agadi lati lati yanju rẹ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Ọpọlọpọ eniyan ati sọfitiwia wa ni idilọwọ nipasẹ Cloudflare ni gbogbo ọjọ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare binu ọpọlọpọ awọn eniyan kakiri agbaye.Wo atokọ naa ki o ronu boya didi Cloudflare lori aaye rẹ dara fun iriri olumulo. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Kini idi ti intanẹẹti ti o ko ba le ṣe ohun ti o fẹ?Ọpọlọpọ eniyan ti o ṣabẹwo si oju opo wẹẹbu rẹ yoo kan wa fun awọn oju-iwe miiran ti wọn ko ba le ra oju opo wẹẹbu kan.O le ma ṣe fi opin si eyikeyi awọn alejo, ṣugbọn ogiriina aiyipada aiyipada Cloudflare jẹ to lati da eniyan pupọ duro. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Ko si ọna lati yanju captcha laisi ṣiṣiṣẹ Javascript ati Awọn Kukisi.Cloudflare nlo wọn lati ṣe ibuwọlu aṣàwákiri kan lati ṣe idanimọ rẹ.Cloudflare nilo lati mọ idanimọ rẹ lati pinnu boya o le to lati tẹsiwaju lilọ kiri ni aaye naa. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Awọn olumulo Tor ati awọn olumulo VPN tun jẹ olufaragba Cloudflare.Awọn solusan mejeeji ni lilo nipasẹ ọpọlọpọ awọn eniyan ti wọn ko le ṣowo intanẹẹti aiṣedede nitori orilẹ-ede wọn / ile-iṣẹ / eto imulo nẹtiwọọki tabi ti o fẹ lati ṣafikun Layer lati daabobo asiri wọnCloudflare n ṣe ikọlu fun awọn eniyan yẹn ni aibikita, fi ipa mu wọn lati pa ipinnu aṣoju wọn. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Ti o ko ba gbiyanju Tor titi di akoko yii, a gba ọ niyanju lati ṣe igbasilẹ Tor Browser ki o ṣabẹwo si awọn oju opo wẹẹbu ti o fẹran.A daba pe ki o ma ṣe wọle si oju opo wẹẹbu rẹ ti banki tabi oju opo wẹẹbu ti ijọba tabi wọn yoo ṣe ami akọọlẹ rẹ. Lo VPN fun awọn oju opo wẹẹbu wọnyẹn. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  O le fẹ lati sọ “Tor jẹ arufin! Awọn olumulo Tor jẹ ọdaràn! Tor buru! ”. Rara.O le kọ nipa Tor lati ori tẹlifisiọnu, sọ pe a le lo Tor lati lọ kiri okun dudu ati tita awọn ibon, awọn oogun tabi awọn ere onihoho chid.Lakoko ti alaye loke jẹ otitọ pe ọpọlọpọ oju opo wẹẹbu ọja wa nibi ti o ti le ra iru awọn ohun kan, awọn aaye yẹn nigbagbogbo han lori clearnet paapaa.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor ni idagbasoke nipasẹ Ẹgbẹ Amẹrika, ṣugbọn Tor lọwọlọwọ ni idagbasoke nipasẹ iṣẹ Tor.Ọpọlọpọ eniyan ati awọn agbari wa ti o lo Tor pẹlu awọn ọrẹ rẹ iwaju.Nitorinaa, ti o ba nlo Cloudflare lori oju opo wẹẹbu rẹ o n dena awọn eeyan gidi.Iwọ yoo padanu ọrẹ ti o pọju ati iṣowo iṣowo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Ati pe iṣẹ DNS wọn, 1.1.1.1, tun n ṣe atunyẹwo awọn olumulo lati ṣabẹwo si oju opo wẹẹbu nipa pada adirẹsi IP iro ti o ni ohun ini nipasẹ Cloudflare, IP localhost bii “127.0.0.x”, tabi ko pada nkankan. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS tun fọ software ori ayelujara lati ori ayelujara foonuiyara si ere kọmputa nitori idahun DNS iro wọn.Cloudflare DNS ko le ṣawari diẹ ninu awọn oju opo wẹẹbu banki. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Ati nibi o le ronu,<br>Emi ko nlo Tor tabi VPN, kilode ti MO fi ṣetọju?<br>Mo ni igbẹkẹle titaja Cloudflare, kilode ti MO yoo ṣe abojuto<br>Oju opo wẹẹbu mi jẹ https idi ti MO fi ni abojuto | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Ti o ba ṣabẹwo si oju opo wẹẹbu eyiti o lo Cloudflare, o n ṣe alabapin alaye rẹ kii ṣe fun oniwun aaye ayelujara nikan ṣugbọn Cloudflare.Eyi ni bi aṣoju ṣe n ṣiṣẹ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Ko ṣee ṣe lati ṣe itupalẹ laisi kọsẹ ijabọ TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare mọ gbogbo data rẹ gẹgẹbi ọrọ igbaniwọle alaise. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed le ṣẹlẹ nigbakugba. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare's https ko ni opin-si-opin. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Ṣe o fẹ looto lati pin data rẹ pẹlu Cloudflare, ati pe o tun jẹ ibẹwẹ 3-leta? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Profaili ori ayelujara ti olumulo olumulo jẹ “ọja” ti ijọba ati awọn ile-iṣẹ imọ-ẹrọ nla fẹ lati ra. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Ile-iṣẹ U.S ti Ile-Ile Aabo sọ:<br><br>Ṣe o ni imọran eyikeyi bi data ti o niyelori ṣe pọ si? Ṣe eyikeyi ọna ti o yoo ta wa data yẹn?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare tun nfunni ni iṣẹ VPN ọfẹ kan ti a pe ni "Cloudflare Warp".Ti o ba lo, gbogbo awọn asopọ foonu (tabi kọnputa rẹ) ni a firanṣẹ si awọn olupin Cloudflare.Cloudflare le mọ iru oju opo wẹẹbu ti o ti ka, ọrọ wo ti o firanṣẹ, tani o ti sọrọ si, ati bẹbẹ lọO ṣe atinuwa fifun gbogbo alaye rẹ si Cloudflare.Ti o ba ro “Ṣe o n ṣe ẹlẹfẹ? Cloudflare wa ni aabo. ” lẹhinna o nilo lati kọ ẹkọ bi VPN ṣe n ṣiṣẹ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare sọ pe iṣẹ VPN wọn jẹ ki intanẹẹti rẹ yara.Ṣugbọn VPN jẹ ki asopọ intanẹẹti rẹ lọra ju isopọ to wa lọwọ rẹ lọ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  O le ti mọ tẹlẹ nipa itanjẹ PRISM.Otitọ ni pe AT&T jẹ ki NSA ṣe ẹda gbogbo data intanẹẹti fun eto iwo-kakiri. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Jẹ ki a sọ pe o n ṣiṣẹ ni NSA, ati pe o fẹ profaili gbogbo ilu ti gbogbo ara ilu.O mọ pe pupọ ninu wọn ni igboya ni igbẹkẹle Cloudflare ati lilo rẹ - ẹnu-bode centralized kan nikan - lati ṣe aṣoju isopọmọ olupin ile-iṣẹ wọn (SSH / RDP), oju opo wẹẹbu ti ara ẹni, oju opo wẹẹbu iwiregbe, oju opo wẹẹbu aaye, banki aaye ayelujara, aaye ayelujara iṣeduro, ẹrọ wiwa, ọmọ ẹgbẹ aṣiri Oju opo wẹẹbu, oju opo wẹẹbu titaja, riraja, oju opo wẹẹbu fidio, oju opo wẹẹbu NSFW, ati oju opo wẹẹbu arufin.O tun mọ pe wọn lo iṣẹ DNS ti Cloudflare ("1.1.1.1") ati iṣẹ VPN ("Cloudflare Warp") fun “Aabo! Yara ju! Dara julọ! ” iriri ayelujara.Darapọ wọn pẹlu adiresi IP adiresi olumulo, itẹka aṣàwákiri, awọn kuki ati IDAY-ID yoo wulo lati kọ profaili ori ayelujara. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  O fẹ data wọn. Kini iwọ yoo ṣe? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare jẹ ibi ifun oyin.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Oyin didi fun gbogbo eniyan. Diẹ ninu awọn gbolohun ọrọ so.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Maṣe lo Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Decentralize ayelujara.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Jọwọ tẹsiwaju si oju-iwe atẹle:  "[Aṣa Cloudflare](yo.ethics.md)"

---

<details>
<summary>_tẹ mi_

## Data ati Alaye diẹ sii
</summary>


Ibi ipamọ yii jẹ atokọ ti awọn oju opo wẹẹbu ti o wa lẹhin "The Cloud Cloud" nla, n di awọn olumulo Tor ṣiṣẹ ati awọn CDN miiran.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Awọn olumulo Cloudflare](../cloudflare_users/)
* [Awọn ibugbe Cloudflare](../cloudflare_users/domains/)
* [Awọn olumulo CDN ti kii ṣe Cloudflare](../not_cloudflare/)
* [Awọn olumulo Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Alaye diẹ sii**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Ṣe igbasilẹ: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * EBook atilẹba (ePUB) ti paarẹ nipasẹ BookRix GmbH nitori irufin aṣẹ-aṣẹ ti ohun elo CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Tiketi ti bajẹ ni ọpọlọpọ awọn akoko.
  * [Ti paarẹ nipasẹ Tor Project.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Wo tiketi 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Tiketi ti idile to kẹhin 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_tẹ mi_

## Kini o le ṣe?
</summary>

* [Ka atokọ wa ti awọn iṣe iṣeduro niyanju ki o pin pẹlu awọn ọrẹ rẹ.](../ACTION.md)

* [Ka ohun olulo olumulo miiran ki o kọ awọn ero rẹ.](../PEOPLE.md)

* Wa nkan: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Ṣe imudojuiwọn akojọ ìkápá naa: [Awọn ilana atokọ](../INSTRUCTION.md).

* [Ṣafikun Cloudflare tabi iṣẹlẹ iṣẹlẹ ti o ni ibatan si itan.](../HISTORY.md)

* [Gbiyanju & kọ Ọpa / Akosile tuntun.](../tool/)

* [Eyi ni diẹ ninu PDF / ePUB lati ka.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Nipa awọn iroyin iro

Crimeflare mọ nipa aye ti awọn iroyin iro ti o ṣe afihan awọn ikanni osise wa, jẹ o jẹ Twitter, Facebook, Patreon, OpenCollective, Awọn abule ati be be lo.
**A ko beere imeeli rẹ rara.
A ko beere orukọ rẹ rara.
A ko beere idanimọ rẹ rara.
A ko beere ipo rẹ rara.
A ko beere ẹbun rẹ.
A ko beere atunyẹwo rẹ.
A ko ni beere lọwọ rẹ lati tẹle lori media media.
A ko beere fun media rẹ.**

# MAA ṢE ṢE Awọn iroyin Iṣeduro.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)