# Hodei Handia


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Gelditu Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "Hodei Handia" Cloudflare Inc., AEBetako enpresa da.CDN (edukiak bidaltzeko sarea) zerbitzuak eskaintzen ditu, DDoS arintzea, Interneteko segurtasuna eta banatutako DNS (domeinu izenen zerbitzaria) zerbitzuak eskaintzen ditu.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare munduko MITM proxy handiena da (alderantzizko proxy).Cloudflare CDN merkatuaren kuotaren% 80 baino gehiagoren jabe da eta hodei askoren erabiltzaile kopurua gero eta handiagoa da.Sarea 100 herrialde baino gehiagotara zabaldu dute.Cloudflarek Twitter, Amazon, Apple, Instagram, Bing eta Wikipedia konbinatuek baino web trafiko gehiago eskaintzen du.Cloudflare doako plana eskaintzen ari da eta jende asko erabiltzen ari da zerbitzariak behar bezala konfiguratu beharrean.Pribatutasuna erosotasunagatik negoziatzen zuten.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare zure eta jatorriaren webgune zerbitzariaren artean kokatzen da, mugako patruila-agente gisa jokatuz.Ezin zara zure aukeratutako helmugara konektatu.Cloudflare-ra konektatzen ari zara eta zure informazio guztia deskriptatu eta helarazten ari da. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Jatorrizko webgune zerbitzariaren administratzaileak agenteari - Cloudflare - agintari nor bere "web jabetza" sar dezakeen erabakitzeko eta "eremu mugatua" zehazteko baimena eman zion.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Begiratu eskuineko irudiari.Cloudflare blokeatzen duten gaiztoak soilik blokeatuko dituzula uste duzu.Cloudflare beti sarean dagoela pentsatuko duzu (inoiz ez behera).Botik legitimoek eta arakatzaileek zure webgunea indexatu dezaketela uste duzu.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Hala ere, horiek ez dira batere egia.Cloudflare-k jende errugabeak arrazoirik gabe blokeatzen ditu.Hodei-beherak behera egin dezake.Cloudflare blokeatzen du legez kanpoko botak.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Hostoetako edozein zerbitzu bezala, Cloudflare ez da perfektua.Pantaila hau ikusiko duzu jatorrizko zerbitzaria ondo funtzionatzen badu ere.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Benetan uste duzu Cloudflare-k% 100 uptime dituela?Ez dakizu Cloudflare zenbat aldiz jaisten den.Cloudflare jaisten bada zure bezeroak ezin du zure webgunera sartu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Txinako Suhiltzaile Handiari egiten zaio erreferentzia, eta, beraz, gizaki askok web edukiak ikustea (hau da, Txina penintsulako eta kanpoko jendeak) iragazteko antzeko lana egiten du.Aldi berean, kratikoki desberdina den web bat ikustean kaltetuta ez zegoen bitartean, zentsurarik gabeko web bat, hala nola "depositu gizonaren" irudia eta "Tiananmen plazako protesten historia". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare-k indar handia du.Nolabait esateko, azken erabiltzaileak azken finean ikusten duena kontrolatzen dute.Webgunea Cloudflare dela eta nabigatzea eragotzi duzu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare zentsurarako erabil daiteke. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Ezin duzu cloudflared webgunea ikusi Cloudflare bot-a dela pentsa dezakeen arakatzaile txikia erabiltzen ari bazara (jende askok ez duelako erabiltzen). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Ezin duzu "arakatzaileen egiaztapen" inbaditzaile hau Javascript gaitu gabe.Zure bizitza baliotsuaren bost segundo (edo gehiago) galtzea da. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare-k automatikoki ere blokeatzen ditu Google, Yandex, Yacy eta API bezero bezalako robot / arakatzaile legitimoak.Cloudflare aktiboki kontrolatzen ari da "saihestu cloudflare" komunitatea kontrolik gabeko ikerketa botak apurtzeko asmoz. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflarek era berean galarazten du Interneteko konektibitate eskasa duten pertsona asko atzean dauden webguneetara sartzea (adibidez, NAT 7+ geruzaren atzean egon litezke edo IP bera partekatzen dutenak, adibidez Wifi publikoak), irudi CAPTCHA anitz konpondu ezean.Zenbait kasutan, 10-30 minutu beharko dira Google asetzeko. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020. urtean Cloudflare Google-ren Recaptcha-tik hCaptcha-ra pasatu zen Google-k bere erabilera kobratzeko asmoa zuen moduan.Cloudflarek zure pribatutasuna zaintzen dutela esan du ("pribatutasun kezka zuzentzen laguntzen du") baina hori gezurra da.Diruari buruzkoa da."HCaptcha-k webguneek eskaerari erantzuteko dirua irabazteko aukera ematen du bot-ak eta bestelako tratu txarrak blokeatzen dituzten bitartean" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Erabiltzailearen ikuspegitik, horrek ez du asko aldatzen. Konpondu beharrean zaude. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Gizakiak eta software asko Cloudflare-k blokeatuta daude egunero. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflarek mundu osoko jende asko haserretzen du.Begiratu zerrenda eta pentsatu zure webgunean Cloudflare onartzea erabiltzaileentzako esperientzia ona den ala ez. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Zein da Interneten zer nahi duzun ezin baduzu egin?Zure webgunea bisitatzen duten gehienek beste orrialde batzuk besterik ez dituzte bilatzen web orria kargatzen ez badute.Baliteke bisitariak ez aktiboki blokeatzea, baina Cloudflareren suebaki lehenetsia jende asko blokeatzeko nahikoa zorrotza da. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Ez dago captcha konpontzeko modurik Javascript eta Cookieak gaitu gabe.Cloudflare erabiltzen ari da arakatzaileko sinadura zure identifikatzeko.Cloudflare-k zure identitatea ezagutu behar du webgunean arakatzen jarraitzeko gai zaren ala ez erabakitzeko. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor erabiltzaileak eta VPN erabiltzaileak ere Cloudflareren biktima dira.Bi konponbideak herrialdeko / korporazioko / sareko politika dela eta, zure pribatutasuna babesteko geruza gehigarririk nahi ez duten hainbat pertsona erabiltzen ari dira.Cloudflare lotsagabe ari da pertsona hauei erasotzen, proxy irtenbidea itzaltzera behartzen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Oraingoz Tor saiatu ez bazenuen, Tor Browser deskargatzera eta gogoko webguneetara joaten zaitugu animatzen zaituztegu.Zure bankuko webgunean edo gobernuaren web orrian ez sartzea gomendatzen dizugu edo zure kontua markatuko dute. Erabili VPN webgune horietarako. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Baliteke "Tor legez kanpokoa dela" esan nahi izatea. Tor erabiltzaileak kriminalak dira! Tor txarra da! ". Ez.Tor-i buruz telebistari buruz jakin zenuke, Tor-ek darknet-a arakatzeko eta pistolak, drogak edo porno porno saltzeko erabil daitekeela esanez.Aurreko adierazpena egia bada ere, merkatuan webgune ugari dago horrelako elementuak erosi ahal izateko, gune horiek clearnet-ean ere agertzen dira.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor AEBetako Armadak garatu zuen, baina Tor Tor-ek proiektuaren bidez garatzen da.Tor erabiltzen duten pertsona eta erakunde ugari daude zure etorkizuneko lagunak barne.Beraz, Cloudflare zure webgunean erabiltzen ari bazara, benetako gizakiak blokeatzen ari zara.Adiskidetasun eta negozio akordio potentziala galduko duzu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Eta euren DNS zerbitzua, 1.1.1.1, erabiltzaileek webgunera bisitatzen dituzten erabiltzaileak iragazten ari dira Cloudflare-ren jabetzakoa den IP helbide faltsua itzuliz, esaterako "127.0.0.x", edo, besterik gabe, ezer ez itzultzeko. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS-k ere lineako softwarea apurtzen du smartphone aplikaziotik ordenagailu jokoetara, DNS erantzun faltsua delako.Cloudflare DNS-k ezin ditu bankuko webgune batzuk kontsultatu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Eta hemen pentsa dezakezu<br>Ez naiz Tor edo VPN erabiltzen ari, zergatik zaindu behar dut?<br>Cloudflare marketinean konfiantza dut, zergatik zaindu behar dut<br>Nire webgunea https da zergatik zaindu behar dudan | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Cloudflare erabiltzen duten webguneak bisitatzen badituzu, informazioa zure webgunearen jabeaz gain Cloudflare ere partekatzen ari zara.Horrela funtzionatzen du alderantzizko proxyak. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Ezinezkoa da analizatzea TLS trafikoa deszifratu gabe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare-k zure datu guztiak ezagutzen ditu, hala nola, pasahitz gordinak. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed noiznahi gerta daiteke. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflareren https ez da inoiz amaitzen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Nahi al duzu zure datuak Cloudflare-rekin partekatu eta, gainera, 3 gutun-agentziarekin? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Interneteko erabiltzaileen lineako profila gobernuak eta teknologia konpainia handiek erosi nahi duten "produktua" da. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Esan zuen AEBetako Barne Segurtasun Sailak:<br><br>Baduzu ideiarik baliotsuak dituzun datuak? Ba al dago modurik datu horiek salduko zenizkigunean?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare-k, gainera, "Cloudflare Warp" izeneko DOAKO VPN zerbitzua eskaintzen du.Erabiliz gero, zure telefonoaren (edo ordenagailuaren) konexio guztiak Cloudflare zerbitzarietara bidaltzen dira.Cloudflare-k jakin dezake zein webgune irakurri duzun, zer iruzkin argitaratu dituzun, norekin hitz egin duzun, etab.Zure informazio guztia Cloudflare-ri ematen ari zara."Txantxetan ari zara?" Cloudflare segurua da. " orduan VPN funtzionatzen ikasi behar duzu. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflarek esan du bere VPN zerbitzuak zure internet azkar egiten duela.Baina VPN-k zure konexioa lehendik duzun konexioa baino motelagoa bihurtu du. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Baliteke dagoeneko ezagutu PRISM eskandalua.Egia da AT&T-k NSA-k Interneteko datu guztiak kopiatzeko aukera ematen duela zaintzarako. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Esan dezagun NSAn lan egiten duzula eta herritarren Interneteko profil bakoitza nahi duzula.Badakizu gehienok Cloudflare-n konfiantzaz erabiltzen dutela eta bakarrik erabiltzen dutela - atebide zentralizatu bakarra - beren enpresako zerbitzariaren konexioa proxy (SSH / RDP), webgune pertsonala, txat-webgunea, foroaren webgunea, bankuko webgunea, aseguruen webgunea, bilatzailea, kide sekretua -Egun webgunea, enkanteen webgunea, erosketak, bideoak, NSFW webgunea eta legez kanpoko webgunea.Badakizu Cloudflare-ren DNS zerbitzua ("1.1.1.1") eta VPN zerbitzua ("Cloudflare Warp") erabiltzen dituztela "Seguru!" Azkarrago! Hobe! " Interneteko esperientzia.Erabiltzailearen IP helbidea, arakatzailearen aztarna digitala, cookieak eta RAY-ID konbinatzea erabilgarria izango da helburuaren lineako profila sortzeko. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Haien datuak nahi dituzu. Zer egingo duzu? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare eztia da.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Doako eztia guztiontzat. Kate batzuk erantsita.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Ez erabili Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Internet deszentralizatu.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Mesedez, jarraitu hurrengo orrialdera:  "[Cloudflare Etika](eu.ethics.md)"

---

<details>
<summary>_egin klik nirekin_

## Datuak eta informazio gehiago
</summary>


Biltegi hau "The Great Cloudwall" atzean dauden webguneen zerrenda da, Tor erabiltzaileak eta beste CDNak blokeatuz.


**Datuak**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare erabiltzaileak](../cloudflare_users/)
* [Cloudflare domeinuak](../cloudflare_users/domains/)
* [Cloudflare CDN erabiltzaileak ez direnak](../not_cloudflare/)
* [Torrearen aurkako erabiltzaileak](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Informazio gehiago**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Deskargatu: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Liburu elektronikoa (ePUB) BookRix GmbH-k ezabatu zuen CC0 materialaren egile eskubideen urraketa dela eta
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Sarrerak hainbat aldiz bandalizatu ziren.
  * [Tor Proiektuak ezabatu du.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Ikusi 34175 txartela.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Azken artxiboko txartela 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_egin klik nirekin_

## Zer egin dezakezu?
</summary>

* [Irakurri gomendatutako ekintzen zerrenda eta partekatu zure lagunekin.](../ACTION.md)

* [Irakurri beste erabiltzailearen ahotsa eta idatzi zure pentsamenduak.](../PEOPLE.md)

* Zerbait bilatu: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Eguneratu domeinuen zerrenda: [Zerrendatu argibideak](../INSTRUCTION.md).

* [Gehitu Cloudflare edo proiektuarekin lotutako gertaera historiara.](../HISTORY.md)

* [Saiatu eta idatzi tresna / script berria.](../tool/)

* [Hona hemen PDF / ePUB batzuk irakurtzeko.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Kontu faltsuei buruz

Crimeflare-k gure kanal ofizialak ordezkatzen dituen kontu faltsuen existentziaren berri izango du, izan Twitter, Facebook, Patreon, OpenCollective, Villages eta abar.
**Ez dugu inoiz zure posta elektronikoa eskatzen.
Ez diogu inoiz eskatu zure izena.
Inoiz ez diogu zure identitateari galdetu.
Zure kokapena ez dugu inoiz galdetzen.
Ez dugu inoiz zure dohaintza eskatuko.
Ez dugu inoiz zure iritzia eskatzen.
Inoiz ez diogu eskatzen sare sozialetan jarraitzeko.
Inoiz ez diogu zure sare sozialetan galdetu.**

# EZ EGIN KONTABILITU KONTUAK.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)