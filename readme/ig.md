# Nnukwu igwe ojii


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Kwụsị Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  “Nnukwu igwe ojii” bụ Cloudflare Inc., ụlọ ọrụ U.S.Ọ na-enye ọrụ CDN (netwọta nnyefe ọdịnaya), mbelata DDoS, nchekwa ntanetị, yana nkesa ọrụ DNS (sava ​​aha ngalaba).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare bụ ihe nnọchiteanya MITM kasịnụ n'ụwa (zighachi akwụkwọ nnọchiteanya).Cloudflare nwere ihe karịrị 80% nke ahịa CDN na ọnụọgụ nke ndị ọrụ ojii na-eto kwa ụbọchị.Ha agbasawanye netwok ha na ihe karịrị otu narị mba.Cloudflare na-arụ ọrụ okporo ụzọ weebụ karịa Twitter, Amazon, Apple, Instagram, Bing & Wikipedia jikọtara.Cloudflare na-enye atụmatụ n'efu na ọtụtụ ndị mmadụ na-eji ya kama ịhazi sava ha nke ọma.Ha na-ere ahịa nzuzo maka ịdị mma.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare na-anọdụ n'etiti gị na onye na-amalite webserver, na-eme dị ka onye na-ahụ maka nchekwa ókè.Nweghị ike ijikọ na ebe ị họọrọ.Are na-ejikọ na Cloudflare a na-edegharị ma na-enyefe ozi gị niile na-aga. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Onye nchịkwa webserver sitere na nyere onye ọrụ ahụ - Cloudflare - ikpebi onye nwere ike ịbanye na "ihe onwunwe" ma kọwaa "mpaghara amachibidoro".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Legodị anya na foto dị mma.Will ga-eche Cloudflare igbochi naanị ụmụ ọjọọ.Will ga eche na Cloudflare dị n'ịntanetị mgbe niile (anaghị agbadata).Will ga eche na bots legit na crawlers nwere ike ịkọwa weebụsaịtị gị.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Agbanyeghị, ihe ndị ahụ abụghị eziokwu ma ọlị.Cloudflare na-egbochi ndị aka ha dị ọcha na-enweghị ihe kpatara ya.Cloudflare nwere ike gbadata.Cloudflare mgbochi legit bot.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Dịka ọrụ ọrụ nnabata ọ bụla, Cloudflare ezughị oke.Will ga-ahụ ihuenyo a ọ bụrụgodị na sava ahụ sitere na-arụ ọrụ nke ọma.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Really na eche n’ezie Cloudflare nwere 100% n’oge?I maghi oge ole Cloudflare na-agbada.Ọ bụrụ na Cloudflare gbadaa ahịa gị enweghị ike ịnweta weebụsaịtị gị. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  A na-akpọ nke a maka ya na Great Firewall nke China nke na-arụ ọrụ yikarịrị nke nhichapụ ọtụtụ mmadụ site na ịhụ ọdịnaya weebụ (ya bụ, onye ọ bụla nọ na China na ndị nọ n'èzí).Ezie na n'otu oge ahụ ndị ahụ emetụtaghị ihu igwe dị iche, web na-enweghị nyocha dị ka onyonyo nke “tan tank” na akụkọ ihe mere eme nke ngagharị iwe “Tiananmen Square”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare nwere nnukwu ikike.N’otu echiche, ha na-achịkwa ihe onye ọrụ n’ikpeazụ ga-eme.A na-egbochi gị ịgagharị na weebụsaịtị n'ihi Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Enwere ike iji Cloudflare mee ihe maka nyocha. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Cannotgaghị enwe ike ịlele weebụsaịtị webụsaịtị ma ọ bụrụ na ị na-eji obere ihe nchọgharị nke Cloudflare nwere ike iche na ọ bụ bot (n'ihi na ọ bụghị ọtụtụ ndị na-eji ya). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Cannotgaghị agagharị “nyocha ihe nchọgharị” a na - enyocha Javascript.Nke a bụ n'efu nke ise (ma ọ bụ karịa) nke ndụ gị bara uru. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare na-egbochi ndị roboti / crawlers nke iwu dị ka Google, Yandex, Yacy, na ndị ahịa API ozugbo.Cloudflare ji nlezianya na-enyocha obodo "na-agagharị ojii" nke nwere ebumnuche imebi bot na nyocha. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare N'otu aka ahụ gbochiri ọtụtụ ndị nwere njikọ ịntanetị na-adịghị mma ịbanye na ebe nrụọrụ weebụ n'azụ ya (dịka ọmụmaatụ, ha nwere ike ịnọ n'azụ 7+ akwa nke NAT ma ọ bụ na-ekerịta otu IP, dịka ọmụmaatụ Wifi ọha) ọ gwụla ma ha edozi otutu CAPTCHAs ọtụtụ.N'ụfọdụ, nke a ga-ewe nkeji iri ruo iri atọ iji mejuo Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  N’afọ 2020 Cloudflare gbanwere site na Recaptcha nke Google gaa hCaptcha ka Google bu n’obi ịkwụ ụgwọ maka ojiji ya.Cloudflare gwara gị na ha lebara nzuzo gị anya (“ọ na-enyere aka idozi nsogbu nzuzo)” mana nke a bụ ụgha.Ihe niile bụ ego."HCaptcha na-enye ohere webụsaịtị iji ego na-egbo mkpa a ebe igbochi bots na ụdị mmekpa ahụ ndị ọzọ" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Site n'echiche nke onye ọrụ, nke a anaghị agbanwe nke ukwuu. A na-amanye gị idozi ya. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare na-egbochi ọtụtụ ụmụ mmadụ na ngwanrọ kwa ụbọchị. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare na-akpasu ọtụtụ ndị mmadụ gburugburu ụwa.Lelee ndepụta ahụ ma chee ma iwere Cloudflare na saịtị gị dị mma maka ahụmịhe onye ọrụ. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Kedu ebumnuche ịntanetị ma ọ bụrụ na ịnweghị ike ime ihe ịchọrọ?Ọtụtụ ndị na-eleta weebụsaịtị gị ga-achọ naanị ibe ndị ọzọ ma ọ bụrụ na ha agaghị ebugharị webpage.May nwere ike ị gaghị egbochi ndị ọbịa ọ bụla na-arụ ọrụ, mana nchekwa ọkụ ndabere nke Cloudflare siri ike igbochi ọtụtụ mmadụ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Enweghị ụzọ isi dozie captcha na-ejighi ike Javascript na Kuki.Cloudflare na-eji ha eme ihe nchọgharị iji mata gị.Cloudflare kwesịrị ịma njirimara gị iji kpebie ma ị tozuru oke iji gaa n’ihu na saịtị ahụ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Ndị ọrụ Tor na ndị ọrụ VPN bụkwa ndị metụtara Cloudflare.Ndị mmadụ na-enweghị ike ịkwụ ụgwọ intanet na-akwadoghị n'ihi iwu obodo ha / ụlọ ọrụ / netwọk ma ọ bụ ndị chọrọ ịtinye akwa ọzọ iji kpuchido nzuzo ha.Cloudflare na-alụ ọgụ megide ndị ahụ na-eme ihere, na-amanye ha ịgbanwere ihe nnọchianya ha. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Ọ bụrụ na ịnwaleghị Tor ruo oge a, anyị na-agba gị ume ka ị budata Tor Browser ma gaa na weebụsaịtị ndị kachasị amasị gị.Anyị na-atụ aro ka ị ghara ịbanye na weebụsaịtị akụ gị ma ọ bụ websaịtị gọọmentị ma ọ bụ na ha ga-emepụta akaụntụ gị. Jiri VPN maka weebụsaịtị ndị ahụ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  I nwere ike ikwu “Tor bụ iwu akwadoghị! Ndị ọrụ Tor bụ ndị omekome! Tor dị njọ! ". Ee e.Television nwere ike isi na telivishọn mụta banyere Tor, na-ekwu na enwere ike iji Tor na-agagharị egbe na ire egbe, ọgwụ ma ọ bụ pid porn.Ọ bụ ezie na nkwupụta dị elu bụ eziokwu na enwere ọtụtụ ebe nrụọrụ weebụ ahịa ebe ị nwere ike ịzụta ụdị ihe ndị ahụ, saịtị ndị ahụ na-apụtakarị na clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Ndị agha US bụ Tor mepụtara Tor, mana ọ bụ Tor bụ onye nrụpụtara Tor ugbu a.Onwere otutu ndi mmadu na ndi otu eji Tor tinyere ndi enyi gi n’abia.Yabụ, ọ bụrụ na ị na-eji Cloudflare na weebụsaịtị gị, ị na-egbochi ezigbo mmadụ.You ga-efufu ọbụbụenyi na azụmaahịa gị. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Ọrụ DNS ha, 1.1.1.1, na-enyocha ndị ọrụ site na ịga na mkpokọta site na ịlaghachi adreesị IP adịgboroja nke Cloudflare, IP nke localhost dị ka “127.0.0.x”, ma ọ bụ na-alọghachi ihe ọ bụla. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS mebiri ngwanrọ na ntanetị site na ngwa smartphone gaa na egwuregwu kọmputa n'ihi azịza DNS adịgboroja ha.Cloudflare DNS enweghị ike ịchọpụta ebe nrụọrụ weebụ akụ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  N'ebe a, i nwere ike iche,<br>Anaghị m eji Tor ma ọ bụ VPN, gịnị kpatara m ga-eji lekọta ya?<br>Ekwere m ahịa Cloudflare, kedu ihe m ga-eji lekọta<br>Ebe nrụọrụ weebụ m bụ https gịnị kpatara m ga-eji lekọta | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Ọ bụrụ n’ị gaa na websaịtị nke na-eji Cloudflare, ị na - ekerịta ozi gị ọ bụghị naanị nye onye nwe ebe nrụọrụ weebụ kamakwa Cloudflare.Nke a bụ otu ihe nnọchite nnọchite ahụ si arụ ọrụ. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Ọ gaghị ekwe omume ịtụle n'ekwughị iju okporo ụzọ TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare maara data gị niile dịka paswọọdụ raw. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed nwere ike ime oge obula. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare si https adịghị agwụ agwụ na njedebe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Reallychọrọ n'ezie ịkekọrịta data gị na Cloudflare, yana ụlọ ọrụ 3-leta? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Profaịlụ ntanetị nke onye ntanetị bụ "ngwaahịa" nke gọọmentị na nnukwu ụlọ ọrụ eji achọ ịzụta. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  U.S. Department of Homeland Security kwuru:<br><br>You nwere echiche gbasara etu data ị siri bara uru? Enwere ụzọ ọ bụla ị ga - ere anyị data ahụ?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare na-enyekwa ọrụ VPN efu na-akpọ "Cloudflare Warp".Ọ bụrụ n’iji ya, a na-eziga ekwentị gị niile (ma ọ bụ kọmputa gị) na sava Cloudflare.Cloudflare nwere ike ịmara webusaịsaịtị ị gụrụ, ihe ị depụtara, onye ị gwara okwu, wdg.Na-eji afọ ofufo enye Cloudflare ozi gị niile.Ọ bụrụ n’uche “Are na-achị ọchị? Cloudflare dị nchebe. ” mgbe ahụ ikwesiri imuta otu VPN si aru oru. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare kwuru na ọrụ VPN ha na-eme ka ịntanetị gị dị ngwa.Mana VPN mee ka njikọ ịntanetị gị dị nwayọ karịa njikọ gị dị. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  O nwere ike ịbụ na ị marala gbasara asịrị PRISM.Ọ bụ eziokwu na AT&T na-ahapụ NSA i copyomi data ntanetị niile maka onyunyo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Ka anyị kwuo na ị na-arụ ọrụ na NSA, ịchọrọ profaịlụ ịntanetị nke nwa amaala ọ bụla.Knowmara na ọtụtụ n’ime ha na-atụkwasị obi Cloudflare na-eji ìsì na-eji ya eme ihe - naanị otu ọnụ ụzọ etiti - iji zighachi njikọ sava ụlọ ọrụ ha (SSH / RDP), webụsaịtị nke onwe ha, websaiti nkata, websaiti web, webụsaịtị web insurance, injin inyocha, onye otu nzuzo. webụsaịtị, websaịtị azụmaahịa, ịzụ ahịa, ebe nrụọrụ weebụ vidiyo, webụsaịtị NSFW, na weebụsaịtị na ezighi ezi.Also makwa na ha na-eji ọrụ DNS Cloudflare ("1.1.1.1") yana ọrụ VPN ("Cloudflare Warp") maka "urenọ na Nche! Ọsọ ọsọ! Nke ka mma! ” ahụmịhe intaneti.Jikọta ha na adreesị IP nke onye ọrụ, akara mkpịsị aka nyocha, kuki na RAY-ID ga-aba uru iji wuo profaịlụ nke ntanetị nke iche. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Chọrọ data ha. Kedu ihe ị ga-eme? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare bụ mmanụ a honeyụ.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Mmanụ a honeyụ na-akwụghị ụgwọ maka mmadụ niile. Stfọdụ eriri.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Ejila Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Chọpụta intaneti.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Biko gaa n’ihu na peeji na-esonụ:  "[Cloudflare ụkpụrụ ọma](ig.ethics.md)"

---

<details>
<summary>_pịa m_

## Data na Ihe omuma ndi ozo
</summary>


Ebe a bụ ndepụta nke weebụsaịtị dị n'azụ "The Great Cloudwall", na-egbochi ndị Tor na ndị CDN ndị ọzọ.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Ndị ọrụ Cloudflare](../cloudflare_users/)
* [Ngalaba Cloudflare](../cloudflare_users/domains/)
* [Ndị ọrụ CDN na-abụghị Cloudflare](../not_cloudflare/)
* [Ndị na-eme Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Ozi Ndị Ọzọ**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Budata: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * ERPix GmbH mebiri ihe mbụ eP (ePUB) site na ịda iwu nwebiisinka nke ihe CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Tiketi tiketi ahụ mebiri ọtụtụ oge.
  * [Ndi Tor Torhapụrụ ya.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Lee tiketi 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Tiketi ebe nde azụ 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_pịa m_

## Gịnị ka ị ga-eme?
</summary>

* [Gụọ ndepụta ntụrụ aka ndị edepụtara ma soro ndị enyi gị kerịta ya.](../ACTION.md)

* [Guo olu onye oru ndi ozo ma dee echiche gi.](../PEOPLE.md)

* Chọọ ihe: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Melite ngalaba ndepụta: [Ntuziaka ndepụta](../INSTRUCTION.md).

* [Tinye Cloudflare ma ọ bụ ihe omume metụtara akụkọ ihe mere eme.](../HISTORY.md)

* [Gbalịa & dee Ngwá Ọrụ / Ederede ọhụụ.](../tool/)

* [Nke a bụ ụfọdụ PDF / ePUB iji gụọ.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Banyere akụkọ adịgboroja

Crimeflare mara banyere ịdị adị nke akụkọ adịgboroja nke na-egosipụta ọwa ozi anyị, ka ọ bụrụ Twitter, Facebook, Patreon, OpenCollective, Village wdg.
**Anyị anaghị ajụ email gị.
Anyị anaghị ajụ gị aha gị.
Anyị anaghị ajụ njirimara gị.
Anyị anaghị ajụ ọnọdụ gị.
Anyị anaghị arịọ onyinye gị.
Anyị anaghị ajụ nyocha gị.
Anyị anaghị agwa gị ka ị soro na social media.
Anyị anaghị ajụ ndị mgbasa ozi na-elekọta gị.**

# EJIJE EGO EGO.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)