# Ilay Cloudwall Lehibe


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Ajanony ny Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Cloudwall" dia Cloudflare Inc., orinasa amerikanina.Manome serivisy CDN (serivisy fanaterana votoaty), fanalàna DDoS, fiarovana amin'ny Internet ary fizarana serivisy DNS (domain name server).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare no proxy MITM lehibe indrindra (proxy revers).Ny Cloudflare dia manana mihoatra ny 80% amin'ny tsena CDN ary mitombo ny isan'ireo mpampiasa Cloudflare isan'andro.Nampivelatra ny tambajotr'izy ireo tamin'ny firenena 100 mahery izy ireo.Cloudflare dia manompo fifamoivoizana betsaka kokoa noho ny Twitter, Amazon, Apple, Instagram, Bing & Wikipedia mitambatra.Cloudflare dia manome drafitra maimaimpoana ary maro ireo mampiasa azy fa tsy manamboatra ny mpizara azy araka ny tokony ho izy.Nivarotra tsiambaratelo izy ireo noho ny mora.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Mipetraka eo anelanelanelananao sy webserver avy eo ny Cloudflare, miasa toy ny mpiambina ny sisintany.Tsy afaka mifandray amin'ny alehanao ianao.Ianao dia mifandray amin'ny Cloudflare ary ny fampahalalana rehetra ataonao dia esorina ary atolotra azy ireo. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Ny tompon'andraikitra amin'ny tranokala niaviany dia namela ilay mpandraharaha - Cloudflare - hanapa-kevitra hanapa-kevitra hoe iza no afaka miditra ao amin'ny "fananany tranokala" ary mamaritra ny "faritra voaferana".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Topazo maso ilay sary mety.Hieritreritra ianao fa tsy misy olon-dratsy ratsy ny Cloudflare.Mihevitra ianao fa i Cloudflare dia ao anaty Internet foana (tsy midina).Hieritreritra ianao fa mety hanamarina ny tranokalanao ny bots ara-dalàna sy ny mpiady.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Tsy marina anefa izany.Ny Cloudflare dia manakana ny olona tsy manan-tsiny tsy misy antony.Afaka midina ny Cloudflare.Ny blockflare dia manakana bots ara-dalàna.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Tahaka ny serivisy fampiantranoana, tsy tonga lafatra ny Cloudflare.Ho hitanao ity fakana ity na dia miasa tsara aza ny mpizara voalohany.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Mieritreritra ve ianao fa manana 100% ambony i Cloudflare?Tsy fantatrao hoe impiry ny hidinan'i Cloudflare.Raha midina Cloudflare dia tsy afaka miditra amin'ny tranokalanao ny mpanjifanao. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Antsoina io amin'ny fironganana lehibe any Sina izay manao asa ampitahaina amin'ny fanivanana olona maro tsy hahita ny votoatin'ny tranonkala (izany hoe ny olona any amin'ny tanibe Shina sy ny olona any ivelany).Nandritra izany fotoana izany ireo izay tsy voakasik'izany nahita tranonkala tsy mitovy fijery, ny tranonkala tsy misy sivana toy ny sarin'ny «lehilahy tank» sy ny tantaran'ny "Tiananmen Square fanoherana". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Ny Cloudflare dia manana hery lehibe.Raha ny dikany dia mifehy izay hitan'ny mpampiasa farany izy amin'ny farany.Voasakana tsy hijery ny tranokala ianao noho ny Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Ny Cloudflare dia azo ampiasaina amin'ny sivana. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Tsy afaka mijery ny tranokala rahona ianao raha mampiasa navigateur kely izay mety hoheverin'ny Cloudflare fa botsika (satria tsy betsaka ny olona mampiasa azy). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Tsy afaka mandalo ity "cheque browser" manintona ity ianao raha tsy mahazo Javascript.Mandany dimy (na maromaro) segondra aza ny fiainanao sarobidy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Ny Cloudflare koa dia manakana ny robots / mpiady amin'ny alàlan'ny mpanjifanao toa an'i Google, Yandex, Yacy, ary API.Cloudflare dia manara-maso mavitrika ny vondrom-piarahamonina "bypass Cloudflare" miaraka amin'ny finiavana hamotehana ireo botsika fikarohana ara-dalàna. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Ny Cloudflare dia manakana ny olona maro izay manana fifandraisana an-tserasera marobe tsy hiditra amin'ny tranokala ao ambadiky ny tranonkala (ohatra, mety ho ao ambadiky ny 7+ layer amin'ny NAT izy ireo na mizara IP mitovy, ohatra an'ny Wifi ampahibemaso) raha tsy hoe mahavaha ireo sary CAPTCHA marobe.Amin'ny toe-javatra sasany, mandritra ny 10 ka hatramin'ny 30 minitra mba hanome fahafaham-po an'i Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Tamin'ny taona 2020, Cloudflare nivadika avy tao amin'ny Google Recaptcha mankany hCaptcha satria mikasa ny handoa ny fampiasana azy i Google.Cloudflare dia nilaza taminao fa miahy ny fiainanao manokana izy ireo ("manampy amin'ny famahana ny ahiahy momba ny fiainana manokana") fa izany dia mazava ho azy izany.Momba ny vola ny zavatra rehetra."Avelan'ny hCaptcha hividy vola hividianana an'io fangatahana io ny hCaptcha raha manakana ireo bots sy ny fanararaotana hafa" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Raha ny fomba fijerin'ny mpampiasa dia tsy miova be izany. Voatosika hamaha azy ianao. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Olombelona sy rindrambaiko maro no nosakanan'ny Cloudflare isan'andro. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Manelingelina olona maro eran-tany ny Cloudflare.Topazo maso ilay lisitra ary eritrereto raha tsara ny fahazoana ny Cloudflare ao amin'ny tranokalanao. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Inona ny tanjon'ny Internet raha tsy vitanao ny tianao?Ny ankamaroan'ny olona mitsidika ny tranokalanao dia hitady pejy hafa raha tsy mahavita pejin-tranonkala.Mety tsy manakana ny mpitsidika rehetra ianao, fa ny alàlan'ny afindrafindran'ny Cloudflare dia tena hentitra hanakanana olona maro. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Tsy misy fomba hamahana ny captcha tsy am-pahamarinana ny Javascript sy ny Cookies.Cloudflare dia mampiasa azy ireo hanao sonia navigateur ahafantarana anao.Ny Cloudflare dia mila mahafantatra ny mombamomba anao hanapa-kevitra raha mahafeno fepetra hitohy ilay tranokala ianao. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Ireo mpampiasa Tor sy mpampiasa VPN dia iharan'ny Cloudflare koa.Ireo vahaolana roa ireo dia ampiasain'ny olona maro izay tsy manam-bola amin'ny Internet tsy voatanisa noho ny politikam-pireneny / orinasa / tambajotra na te hanisy sosona fanampiny hiarovana ny tsiambaratelony.Nanafika ireo olona ireo ny Cloudflare, ka manery azy ireo hanafoana ny tetikasan'izy ireo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Raha toa ka tsy nanandrana Tor ianao tamin'ity indray mitoraka ity, dia manentana anao izahay hisintona Tor Browser ary hitsidika ireo tranonkala tianao.Manoro hevitra anao izahay mba tsy hisoratra anarana amin'ny tranokalan'ny bankinao na tranokalan'ny governemanta na hanasonia ny kaontinao. Mampiasà VPN amin'ireo tranokala ireo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Azonao atao ny milaza fa "tsy ara-dalàna ny Tor! Mpanara-dalàna ireo mpampiasa Tor! Ratsy ny Tor! ". Tsia.Mety ho nianatra momba ny Tor avy amin'ny fahitalavitra ianao, ka nilaza fa i Tor dia azo ampiasaina hijerena ny blacknet sy ny basy varotra, ny zava-mahadomelina na ny sary vetaveta.Raha marina ny fanambarana etsy ambony dia misy tranokala marobe izay ahafahanao mividy entana toy izany, matetika ireo fisian'ireo toerana ireo dia ao amin'ny clearnet koa.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Noforonin'ny tafika amerikana i Tor, fa ny Tor amin'izao fotoana izao dia novolavolain'ny tetikasa Tor.Misy olona sy fikambanana maro izay mampiasa Tor anisan'izany ny namanao amin'ny hoavy.Noho izany, raha mampiasa Cloudflare amin'ny tranokalanao ianao dia manakana ny tena olombelona.Ho very ny mety ho namana sy ny fifanarahana ara-barotra. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Ary ny serivisy DNS azy ireo, 1.1.1.1, dia manivana ireo mpampiasa tsy mitsidika ilay tranonkala amin'ny famerenany ny adiresy IP sandoka an'ny Cloudflare, IP localhost toy ny "127.0.0.x", na miverina fotsiny. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS dia manapaka ny rindrambaiko amin'ny Internet avy amin'ny fampiharana finday mankany amin'ny lalao amin'ny solosaina ihany koa noho ny valiny DNS sandoka.Cloudflare DNS dia tsy mahazo manontany tranonkala banky vitsivitsy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Ary eto angamba ianao no mieritreritra<br>Tsy mampiasa Tor na VPN aho, maninona no tokony hokarakaraiko?<br>Matoky ny marketing marketing Cloudflare aho, maninona no tokony hokarakaraiko<br>Ny tranokalako dia https nahoana no tokony hokarakaraiko | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Raha mitsidika tranokala mampiasa Cloudflare ianao, dia tsy ny tompona tranonkala ihany no mizara ny fampahalalana anao fa tsy i Cloudflare ihany koa.Toy izany ny fomba fiasan'ny proxy mifanohitra. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Tsy azo atao ny mamakafaka raha tsy manaparitaka ny fifamoivoizana TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Ny Cloudflare dia mahalala ny angon-drakitrao rehetra toy ny tenimiafina. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Misy rahona mety hitranga amin'ny fotoana rehetra. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Ny https Cloudflare dia tsy misy farany. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Tena te hizara ny data ao amin'ny Cloudflare ve ianao, ary koa ny masoivoho 3-taratasy? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Ny mombamomba ny mpampiasa Internet dia "vokatra" izay tian'ny governemanta sy ny orinasa teknolojia lehibe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Hoy ny sampana miadidy ny tanindrazana A.S.:<br><br>Misy hevitra anananao ve hoe sarobidy ny angon-drakinao anananao? Misy fomba hamidinao anay ve izany data izany?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare koa dia manolotra serivisy VPN maimaimpoana antsoina hoe "Cloudflare Warp".Raha mampiasa izany ianao, ny fifandraisana finday rehetra (na ny solosainao) dia alefa any amin'ny mpizara Cloudflare.Ny Cloudflare dia afaka mahalala hoe iza ilay tranonkala novakianao, inona ny hevitra narosanao, izay niresahanao, sns.Manolotsaka an-tsitrapo aminao ny fampahalalana rehetra anao amin'ny Cloudflare.Raha mieritreritra ianao hoe "Mivazivazy ve ianao? Azo antoka ny Cloudflare. ” dia mila mianatra ny fomba fiasan'ny VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Nilaza i Cloudflare fa ny serivisy VPN dia manamboatra haingana ny Internet.Saingy ny VPN dia mampihena ny fifandraisan'ny Internet noho ny fifandraisanao misy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Mety ho efa fantatrao ny momba ny tantara an-tsary PRISM.Marina fa ny AT&T dia mamela ny NSA handika ny angon-drakitra rehetra amin'ny Internet mba hanaraha-maso. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Andao lazaina hoe miasa ao amin'ny NSA ianao, ary maniry ny mombamomba ny olon-tsotra rehetra amin'ny Internet ianao.Fantatrao fa ny ankamaroan'izy ireo dia matoky ny Cloudflare an-jambany ary mampiasa izany - vavahady iray afovoany fotsiny - mba hanohana ny fifandraisan'izy ireo ny serivisin'ny mpizara (SSH / RDP), tranonkala manokana, tranokalan'ny chat, tranokala forum, tranokalan'ny banky, tranokalan'ny fiantohana, search engine, mpikambana miafina tranonkala -ly, tranokala lavanty, miantsena, tranonkala video, tranonkala NSFW, ary tranonkala tsy ara-dalàna.Fantatrao koa fa mampiasa ny serivisy DNS Cloudflare ("1.1.1.1") sy serivisy VPN ("Cloudflare Warp") ho an'ny "Secure! Haingankaingana! Better! " traikefa amin'ny Internet.Ny fampifangaroana azy ireo amin'ny adiresy IP an'ny mpampiasa, ny fanondro kaody, ny mofomamy ary ny RAY-ID dia ilaina amin'ny fananganana ny mombamomba an-tserasera kendrena kendrena. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Tianao ny angon-dry zareo. Inona no hataonao? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare dia honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Tantely maimaimpoana ho an'ny rehetra. Ny kofehy sasany miraikitra.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Aza mampiasa Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Araraoty ny Internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Azafady tohizo ny pejy manaraka:  "[Etika Cloudflare](mg.ethics.md)"

---

<details>
<summary>_tsindrio aho_

## Data sy fampahalalana bebe kokoa
</summary>


Ity rakitra ity dia lisitry ny tranonkala izay ao ambadiky ny "The Great Cloudwall", manakana ny mpampiasa Tor sy CDNs hafa.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Mpampiasa Cloudflare](../cloudflare_users/)
* [Domains Cloudflare](../cloudflare_users/domains/)
* [Mpampiasa CDN tsy Cloudflare](../not_cloudflare/)
* [Mpampiasa anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Fanazavana fanampiny**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Download: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Ny eBook (ePUB) tany am-boalohany dia nofafàn'ny BookRix GmbH noho ny fanitsakitsahana ny famoahana ny fananana CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Natezina imbetsaka ny tapakila.
  * [Nofafana noho ny Tor Pro.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Jereo ny tapakila 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Tapakila arsiva farany 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_tsindrio aho_

## Inona no azonao atao?
</summary>

* [Vakio ny lisitry ny hetsika naroso ary zarao amin'ny namanao.](../ACTION.md)

* [Vakio ny feon'ny mpampiasa hafa ary soraty ny hevitrao.](../PEOPLE.md)

* Mikaroka zavatra iray: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Hanavao ny lisitry ny sehatra: [Tanisao torolàlana](../INSTRUCTION.md).

* [Asio Cloudflare na hetsika mifandraika amin'ny tantara amin'ny tantara.](../HISTORY.md)

* [Manandrama & manoratra fitaovana / script vaovao.](../tool/)

* [Ity misy PDF / ePUB hamaky.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Momba ny kaonty sandoka

Fantatry ny Crimeflare momba ny fisian'ireo kaonty hosoka mampita ny fantsona ofisialy, dia ny Twitter, Facebook, Patreon, OpenCollective, Villages sns.
**Tsy manontany ny mailakao mihitsy izahay.
Tsy manontany ny anaranao mihitsy izahay.
Tsy manontany ny momba anao mihitsy izahay.
Tsy manontany ny toerana misy anao izahay.
Tsy mangataka velively ny fanomezanao izahay.
Tsy manontany ny famerenanao mihitsy izahay.
Tsy hangataka anao hanaraka fotsiny izahay amin'ny media sosialy.
Tsy manontany ny media sosialy mihitsy izahay.**

# TSY MANGALALA FANAVOTANA NY FANAVOTANA.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)