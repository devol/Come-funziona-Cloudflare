# Le Tele Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Taofi Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  O le “Great Cloudwall” o le Cloudflare Inc., le kamupani Amerika.E naʻo le faʻasoaina o le CDN (content delivery network), DDoS mitigation, Internet security, ma le tufatufaina DNS (domain name server) services.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare o le sui sili MITM sili a le lalolagi (sui sui).Cloudflare umia sili atu nai lo 80% o CDN maketi sea ma le numera o cloudflare tagata faʻateleina tuputupu aʻe i aso uma.Ua latou faʻalauteleina a latou fesoʻotaʻiga i le silia ma le 100 atunuʻu.Cloudflare tautua sili atu 'upega tafailagi nai lo Twitter, Amazon, Apple, Instagram, Bing & Wikipedia tuufaatasi.Cloudflare o loʻo tuʻuina mai fua fuafuaga ma e tele tagata o loʻo faʻaogaina nai lo le faʻatulagaina lelei o latou sulu.Latou fefaatauaiga le le faalauaiteleina luga faigofie.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare nofo i le va o oe ma le amataga webserver, galue e pei o se tuaoi patrol agent.E le mafai ona e fesoʻotaʻi i lau vaega filifilia.O loʻo e fesoʻotaʻi i le Cloudflare ma o au faʻamatalaga uma o loʻo faʻamalamalamaina ma tuʻuina atu i luga ole lele. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  O le mafuaʻaga webserver administrator na faʻatagaina le sooupu - Cloudflare - e filifili poʻo ai e mafai ona faʻaavanoa i latou "web meatotino" ma faʻamatala "nofoaga faʻatapulaʻa".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Vaʻavaʻai i le ata saʻo.O le ae manatu Cloudflare poloka na o tagata leaga.E te manatu o Cloudflare e masani lava i luga o le initaneti (aua le alu i lalo).O le ae manatu legit bots ma crawlers mafai ona faʻasino igoa lau '' upega tafaʻilagi.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Peitai e le moni na mea.Cloudflare polokaina tagata mama ma e leai se mafuaaga.E mafai ona alu i lalo le ao.Cloudflare poloka poloka poloka.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  E pei lava o soo se tautua talimalo, Cloudflare e le atoatoa.O le ae vaʻai i lenei laupepa tusa lava pe o loʻo manuia le amataga tautua.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  E te manatu o Cloudflare e iai 100% uptime?E te le iloa pe fia taimi Cloudflare e alu ifo i lalo.A faʻapea e alu i lalo le Cloudflare, e le mafai e lau tagata faʻatau ona mauaina lau 'upega tafaʻilagi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  E faʻaigoaina lea e faʻasino i le Great Firewall o Saina lea e faia se galuega faʻatusa o le faʻamamaina lea o le tele o tagata mai le vaʻaia o upega tafailagi (ie o tagata uma i le laueleele o Saina ma tagata i fafo).I le taimi lava e tasi oi latou na le aʻafia e vaʻai i se 'eseʻese web' upega tafaʻilagi, o se 'upega tafaʻilagi leai se faʻatagaina e pei o se ata o le "tane tane" ma le talaʻaga o le "Tiananmen Square tetee". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare maua tele mana.I se isi itu, latou te puleaina le mea e vaʻaia e le tagata faʻaaogaina.O loo e taofia le sailia o le upega tafailagi ona o le Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare mafai ona faʻaaogaina mo le pasiaina. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Oe le mafai vaʻaia cloudflared 'upega tafaʻilagi pe a fai o loʻo e faʻaaogaina nai browser e ono manatu Cloudflare o se bot (ona e le toʻatele tagata faʻaaogaina). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  E le mafai ona pasia lenei '' siaki siaki browser '' invasive aunoa ma mafai ai Javascript.Ole otaota lea ole lima (pe sili atu) sekone o lou taua ola. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare otometi foi poloka legit robots / crawlers pei o Google, Yandex, Yacy, ma API tagata faʻatau.Cloudflare o loʻo mataʻituina mataʻutia "aloese cloudflare" alaalafaga ma le faʻamoemoe e solia legit sailiga bot. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare faʻapena foi ona taofia le tele o tagata e matitiva fesoʻotaʻiga initaneti mai le ulufale atu i 'upega tafaʻilagi i tua atu (mo se faʻataʻitaʻiga, e ono i tua atu o le 7+ faaputuga o NAT poʻo le tutusa tutusa IP, mo se faʻataʻitaʻiga lautele Wifi) seivagana latou foia le tele o ata CAPTCHA.I nisi tulaga, o le a ave 10 i le 30 minute e faʻamalieina Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  I le tausaga 2020 Cloudflare na suia mai le Google's Recaptcha i le hCaptcha e pei ona manatu le Google e totogi mo lona faaaogaina.Cloudflare fai atu latou te tausia lo latou le faalauaiteleina ("e fesoasoani talanoaina se faalilolilo popolega") ae e mautinoa lava o se pepelo.E faatatau lava i tupe."HCaptcha faʻatagaina 'upega tafaʻilagi e fai tupe e tautua ai lenei manaʻoga a o poloka bot ma isi ituaiga o faʻaleagaina" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Mai le vaai a le tagata, e le tele se suiga. Ua fai faamalosi oe e fofo. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Tele tagata ma polokalama o loʻo poloka e Cloudflare i aso uma. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare faaita le tele o tagata i le lalolagi atoa.Vaʻavaʻai i le lisi ma mafaufau pe faʻaaogaina Cloudflare i luga o lau '' upega tafaʻilagi e lelei mo tagata faʻaaogaina le poto masani. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  O le a le mafuaʻaga o le initaneti pe a le mafai ona e faia le mea e te manaʻo ai?Ole toʻatele o tagata e asiasi lau upega tafailagi e naʻo le suʻe lava mo isi itulau pe a le mafai ona latou vaʻaia se upega tafailagi.Atonu e te le o polokaina soʻo se tagata asiasi, ae o Cloudflare le muamua firewall e matua faigata lava e poloka ai le tele o tagata. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  E leai se auala e foia ai le captcha ae le faʻatagaina le Javascript ma Kuki.Cloudflare o latou faʻaaogaina latou e fai ai le browser saini saini e faʻailoa oe.E manaʻomia e Cloudflare ona e iloa lou faʻasinomaga e filifili ai pe e te agavaʻa ona faʻaauau le suʻesuʻega i le upega tafailagi | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  O tagata Tor ma VPN tagata faʻaaogaina o latou foi na afaina i Cloudflare.Lua tali o loʻo faʻaaogaina e le toʻatele o tagata e le mafai ona gafatia le leai o ni laupepa i le initaneti ona o latou atunuu / faʻalapotopotoga / fesoʻotaʻiga fesoʻotaʻiga poʻo e manaʻo e faʻaopopo faʻaopopo vaega e puipuia ai lo latou le faalauaiteleina.Cloudflare e matamuli osofaia na tagata, faamalosia i latou e tape a latou sui sui. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Afai e te leʻi faataitaʻia Tor seia oo i lenei taimi, matou te fautuaina oe e download le Tor Browser ma asiasi i lau uepisaite e te fiafia iai.Matou te fautua atu ia aua neʻi e alu i totonu o lau faletupe i luga o le upega tafailagi poʻo le upega tafailagi a le malo, pe latou te faʻailogaina lau tala Faʻaoga le VPN mo na 'upega tafaʻilagi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Atonu e te manaʻo e fai mai “O le Tor e le tusa ai ma le tulafono! Tor tagata solitulafono e solitulafono! E leaga Tor! "Oe ono aʻoaʻoina e uiga i le Tor mai le televise, fai mai o Tor e mafai ona faʻaogaina e suʻesuʻeina i upega tafailagi ma fefaʻatauaʻi fana, vailaʻau poʻo ponokalafi.A o luga luga faʻamatalaga e moni o loʻo i ai le tele o maketi 'upega tafaʻilagi e mafai ona e faʻatau mai aitema, o na nofoaga e masani ona faʻaalia foi luga o clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  O le Tor na atiaʻe e le US Army, peitaʻi o le Tor o loʻo atiae e le Tor project.E tele tagata ma faʻalapotopotoga e faʻaaogaina Tor e aofia ai ma au uo i le lumanaʻi.O lea, afai oe faʻaoga Cloudflare i luga o lau 'upega tafaʻilagi e te punitia tagata moni.Ole ae le gafatia le faauo ma pisinisi pisinisi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Ma o la latou DNS auaunaga, 1.1.1.1, o loʻo latou faʻamamaina foi tagata e le asia le upega tafaʻilagi e ala i le toe faʻafoi atu o tuatusi IP tuʻufaʻatasi e Cloudflare, IP localhost pei o "127.0.0.x", pe naʻo le toe faʻafoʻi mai o se mea. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS solia foi le initoneti polokalama mai le smartphone app i komepiuta taʻaloga ona o latou pepelo DNS tali.Cloudflare DNS e le mafai ona fesiligia nisi uepisaite faletupe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Ma o iinei e te manatu,<br>Ou te le o faʻaaogaina Tor po o VPN, aisea e tatau ai ona ou popole?<br>Ou te faʻatuatuaina Cloudflare maketi, aisea e tatau ai ona ou popole<br>O laʻu 'upega tafaʻilagi o le https aisea e tatau ai ona ou tausia | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Afai e te asia luga o 'upega tafaʻilagi e faʻaogaina le Cloudflare, o loʻo e faʻasoaina lau faʻamatalaga e le gata i le pule o le upega tafailagi ae faapea foi le Cloudflare.Ole auala lea e galue ai le sui faʻafoi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  E le mafai ona faia iai ni auiliiliga e aunoa ma le faʻailoaina o auala i luga ole auala. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare iloa uma au faʻamatalaga pei o raw password. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed mafai ona tupu i soo se taimi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare's https e le uma-i-le-faʻaiuga. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  E te manaʻo e faʻasoa lau faʻamatalaga i le Cloudflare, faʻapea foʻi ma le 3-tusi sui? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  'Upega tafaʻilagi a le tagata faʻaaoga Initaneti o se' oloa 'e manaʻo e le malo ma kamupani tekonolosi tetele ona faʻatau. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Fai mai le US Department of Homeland Security:<br><br>E i ai sou manatu pe faʻatauaina le faʻamatalaga ua ia te oe? E i ai se auala e te faʻatau mai ai ia i matou faʻamaumauga?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare latou te ofoina atu foi le auaunaga VPN e taua ole "Cloudflare Warp".Afai e te faʻaaogaina, o au telefoni (po o lau komipiuta) fesoʻotaʻiga e lafoina i Cloudflare servers.E mafai e le Cloudflare ona ia iloa poo fea le upega tafaʻilagi na e faitauina, poʻo a ni manatu na e lafoina, pe o ai na e talanoa ai, ma isi.O oe e te ofoina atu lau lafo uma au faamatalaga ia Cloudflare.Afai e te manatu “E te tausua? Ua malu le Cloudflare. ” ona e manaʻomia ona aʻoaʻo le auala galue VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare fai mai latou VPN auaunaga faia lau initaneti vave.Ae VPN le faia o lau initaneti initaneti lemu nai lo lau i ai nei. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Atonu ua e iloa e uiga ile PRISM scandal.E moni e AT&T faʻataga NSA e kopi uma Initaneti faʻamaumauga mo mataituina. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Faʻapea o loʻo e galue i le NSA, ma e te manaʻo i le initaneti tagata uma a latou laiga.E te iloa o le toʻatele o loʻo faʻatuatuaina ma le faʻaua le Cloudflare ma faʻaaoga - naʻo le tasi o le faitotoʻa tutotonu - e sui ai le latou kamupani server Connection (SSH / RDP), luga o le upega tafailagi, talanoaga i luga o le upega tafailagi, fono i luga o le upega tafailagi, faletupe uepisaite, inisiua 'upega tafaʻilagi, sailiga afi, tagata lilo -o le laiga 'upega tafaʻilagi, faʻatautuʻi' upega tafaʻilagi, faʻatau, vitio o le 'upega tafaʻilagi, NSFW' upega tafaʻilagi, ma le tulaga faʻaletonu 'upega tafaʻilagi.E te iloa foʻi latou te faʻaaogaina le Cloudflare's DNS service ("1.1.1.1") ma VPN service ("Cloudflare Warp") mo le "Secure! Televave Sili atu! ” initaneti.O le tuʻufaʻatasia o latou ma le tuatusi IP a tagata e faʻaaogaina, sapalai tamaʻilima sipela, kuki ma le RAY-ID o le a aoga e fausia ai le faʻamoemoe o le initaneti. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Oe manaʻo i latou faʻamaumauga. O le a lau mea e fai? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare o le honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Talofa fua i tagata uma. O nisi manoa ua faapipii.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Aua le faʻaogaina Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Nofoaga le initaneti.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Faʻamolemole faʻaauau i le isi itulau:  "[Cloudflare Amio Taualoa](sm.ethics.md)"

---

<details>
<summary>_kiliki aʻu_

## Faʻamatalaga ma Faʻamatalaga
</summary>


O lenei fale teu oloa o se lisi o uepisaite o loʻo i tua o le "The Great Cloudwall", poloka Tor tagata faʻaaoga ma isi CDN.


**Faamatalaga**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare Tagata e Faʻaaogaina](../cloudflare_users/)
* [Cloudflare tuatusi](../cloudflare_users/domains/)
* [Tagata e le Cloudflare CDN tagata](../not_cloudflare/)
* [Aneti-Tor tagata faaaoga](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Nisi faʻamatalaga**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Sii mai: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * O le uluai eBook (ePUB) na tapeina e le BookRix GmbH ona o le puletaofia le solia o CC0 mea
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Sa faʻaleagaina le pepa i le tele o taimi.
  * [Aveesea e le Tor Project.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Silasila i le pepa ulufale 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Tiketi faʻamaumauga mulimuli 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_kiliki aʻu_

## O le a se mea e mafai ona e faia?
</summary>

* [Faitau la matou lisi o gaioiga fautuaina ma faasoa atu i au uo.](../ACTION.md)

* [Faitau le isi leo faʻaaoga ma tusi ou manatu.](../PEOPLE.md)

* Saili se mea: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Faafou le lisi lisi: [Lisi faatonuga](../INSTRUCTION.md).

* [Faʻaopopo Cloudflare poʻo poloketi e faʻatatau i le talaʻaga.](../HISTORY.md)

* [Taumafai & tusi fou Tool / Tusitusiga.](../tool/)

* [Lenei o nisi PDF / ePUB e faitau ai.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### E uiga i le pepelo tala

Crimeflare iloa e uiga i le i ai o pepelo tala pepelo faʻasaina o matou ala aloaia, ia Twitter, Facebook, Patreon, OpenCollective, Nuu etc.
**Matou te le fesiligia le imeli.
Matou te le fesiligia le igoa.
Matou te le fesiligia le ou igoa.
Matou te le fesiligia le mea ua iai.
Matou te le fesiligia le tou foai.
Matou te le fesiligia le matou iloiloga.
Matou te le fai atu lava ia te oe e te mulimuli i luga o faasalalauga lautele.
Matou te le fesiligia le ala o faasalalauga lautele.**

# AUA LE FAATALAINA FAʻAALIGA FAʻAALIGA.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)