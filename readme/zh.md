# 大云墙


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## 停止Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  “伟大的云墙”是美国公司Cloudflare Inc.。它提供CDN（内容交付网络）服务，DDoS缓解，Internet安全性和分布式DNS（域名服务器）服务。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare是世界上最大的MITM代理（反向代理）。Cloudflare拥有超过80％的CDN市场份额，并且Cloudflare用户的数量每天都在增长。他们已将其网络扩展到100多个国家。Cloudflare提供的网络访问量超过Twitter，Amazon，Apple，Instagram，Bing和Wikipedia的总和。Cloudflare提供免费计划，许多人正在使用它，而不是正确配置其服务器。他们用隐私代替便利。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare位于您和原始网络服务器之间，就像边境巡逻员一样。您无法连接到所选目的地。您正在连接到Cloudflare，所有信息都将被解密并即时传递。 |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  原始网络服务器管理员允许代理Cloudflare决定谁可以访问其“网络媒体资源”并定义“限制区域”。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  看一看正确的图像。您会认为Cloudflare只会阻止坏人。您会认为Cloudflare始终在线（永远不会宕机）。您会认为合法的漫游器和爬虫可以将您的网站编入索引。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  但是，这些都不是真的。Cloudflare无缘无故地封锁了无辜的人们。Cloudflare可能会崩溃。Cloudflare阻止合法机器人。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  就像任何托管服务一样，Cloudflare也并不完美。即使原始服务器运行良好，您也会看到此屏幕。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  您真的认为Cloudflare具有100％的正常运行时间吗？您不知道Cloudflare宕机了多少次。如果Cloudflare出现故障，您的客户将无法访问您的网站。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  所谓的“中国长城防火墙”就是这样，它在过滤许多人看不到Web内容（例如，中国大陆的每个人和外面的人）方面起到了类似的作用。同时，那些没有受到影响的人看到的网页完全不同，而没有审查的网页，例如“坦克手”的形象和“天安门广场抗议活动”的历史。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare具有强大的功能。从某种意义上说，它们控制最终用户最终看到的内容。由于Cloudflare，您无法浏览该网站。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare可用于审查制度。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  如果您使用的是次要的浏览器（Cloudflare可能认为它是机器人），则无法查看cloudflared网站（因为没有多少人使用它）。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  如果不启用Javascript，则无法通过这种侵入性的“浏览器检查”。这浪费了宝贵的生命五（或更多）秒。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare还会自动阻止合法的机器人/爬虫，例如Google，Yandex，Yacy和API客户端。Cloudflare正在积极监视“绕过cloudflare”社区，以打破合法的研究机器人。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare同样会阻止许多互联网连接较差的人访问其背后的网站（例如，他们可能位于NAT的7层以上或共享相同的IP，例如公共Wifi），除非他们解决了多个图像验证码。在某些情况下，这可能需要10到30分钟才能使Google满意。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020年，Cloudflare从Google的Recaptcha切换到hCaptcha，因为Google打算对其使用收费。Cloudflare告诉您他们关心您的隐私（“它有助于解决隐私问题”），但这显然是骗人的。这都是关于金钱的。“ hCaptcha使网站能够在满足这种需求的同时阻止机器人和其他形式的滥用来赚钱” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  从用户的角度来看，这并没有太大变化。您被迫解决。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  每天都有许多人和软件被Cloudflare阻止。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare困扰着全球许多人。查看列表，并考虑在您的站点上采用Cloudflare是否对用户体验有利。 |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  如果您无法做自己想做的事，互联网的目的是什么？如果大多数访问您网站的人无法加载网页，他们只会寻找其他页面。您可能没有积极地阻止任何访问者，但是Cloudflare的默认防火墙非常严格，足以阻止很多人。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  如果不启用Javascript和Cookies，就无法解决验证码。Cloudflare正在使用它们制作浏览器签名来标识您。Cloudflare需要知道您的身份，以决定您是否有资格继续浏览该网站。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor用户和VPN用户也是Cloudflare的受害者。这两种解决方案正被许多因国家/地区/公司/网络政策而无法提供未经审查的互联网服务的人使用，或者希望添加额外的层来保护其隐私的人使用这两种解决方案。Cloudflare正无耻地攻击那些人，迫使他们关闭其代理解决方案。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  如果您直到现在仍未尝试使用Tor，建议您下载Tor浏览器并访问您喜欢的网站。我们建议您不要登录银行网站或政府网页，否则它们会标记您的帐户。对那些网站使用VPN。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  您可能要说“ Tor是非法的！ Tor用户是犯罪分子！ Tor不好！”。您可能会从电视上了解到Tor，说Tor可用于浏览暗网并交易枪支，毒品或色情色情。尽管上面的陈述是正确的，有很多市场网站可以购买此类物品，但这些网站也经常出现在clearnet上。  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor由美国陆军开发，但当前的Tor由Tor项目开发。有很多使用Tor的人和组织，包括您未来的朋友。因此，如果您在网站上使用Cloudflare，则会阻止真实的人。您将失去潜在的友谊和生意往来。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  他们的DNS服务1.1.1.1还通过返回Cloudflare拥有的虚假IP地址，本地IP（例如“ 127.0.0.x”）来过滤掉用户访问该网站的行为，或者仅返回任何内容。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS还因为伪造的DNS答案而破坏了从智能手机应用程序到计算机游戏的在线软件。Cloudflare DNS无法查询某些银行网站。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  在这里您可能会想，<br>我没有使用Tor或VPN，为什么要在意呢？<br>我信任Cloudflare营销，为什么我应该关心<br>我的网站是https，为什么我应该关心 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  如果您访问使用Cloudflare的网站，则不仅会与网站所有者共享信息，还会与Cloudflare共享信息。这就是反向代理的工作方式。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  不解密TLS流量就无法进行分析。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare知道您的所有数据，例如原始密码。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed可以随时发生。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare的https从来都不是端对端的。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  您是否真的要与Cloudflare以及三字母代理商共享您的数据？ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  互联网用户的在线个人资料是政府和大型科技公司想要购买的“产品”。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  美国国土安全部说:<br><br>您是否知道所拥有数据的价值？您有什么办法可以向我们出售这些数据？  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare还提供称为“ Cloudflare Warp”的免费VPN服务。如果使用它，则所有智能手机（或计算机）连接都将发送到Cloudflare服务器。Cloudflare可以知道您阅读了哪个网站，发表了哪些评论，与谁进行了交谈等。您自愿将所有信息提供给Cloudflare。如果您认为“您在开玩笑吗？ Cloudflare是安全的。”那么您需要了解VPN的工作原理。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare表示，他们的VPN服务可让您快速上网。但是VPN使您的Internet连接速度比现有连接速度慢。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  您可能已经知道PRISM丑闻。AT＆T确实允许NSA复制所有互联网数据以进行监视。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  假设您在国家安全局（NSA）工作，并且想要每个公民的互联网个人资料。您知道他们中的大多数人都盲目地信任Cloudflare并使用它-仅一个集中式网关-代理其公司服务器连接（SSH / RDP），个人网站，聊天网站，论坛网站，银行网站，保险网站，搜索引擎，秘密成员仅限网站，拍卖网站，购物，视频网站，NSFW网站和非法网站。您还知道他们将Cloudflare的DNS服务（“ 1.1.1.1”）和VPN服务（“ Cloudflare Warp”）用于“安全！快点！更好！”互联网体验。将它们与用户的IP地址，浏览器指纹，Cookie和RAY-ID结合使用，将有助于建立目标的在线个人资料。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  您需要他们的数据。你会怎么做？ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare是一个蜜罐。** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **免费给大家蜂蜜。一些附加的字符串。** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **不要使用Cloudflare。** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **分散互联网。** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    请继续下一页:  "[Cloudflare伦理](zh.ethics.md)"

---

<details>
<summary>_点击我_

## 数据和更多信息
</summary>


该存储库是“ The Great Cloudwall”后面的网站的列表，这些网站阻止了Tor用户和其他CDN。


**数据**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare用户](../cloudflare_users/)
* [Cloudflare域](../cloudflare_users/domains/)
* [非Cloudflare CDN用户](../not_cloudflare/)
* [反Tor用户](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**更多信息**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * 下载: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * 由于侵犯CC0资料的版权，BookRix GmbH删除了原始电子书（ePUB）
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * 这张票被破坏了很多次。
  * [由Tor项目删除。](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [参见票号34175。](https://trac.torproject.org/projects/tor/ticket/34175)
  * [最后存档票证24351。](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_点击我_

## 你能做什么？
</summary>

* [阅读我们的建议措施清单，并与您的朋友分享。](../ACTION.md)

* [阅读其他用户的声音并写下您的想法。](../PEOPLE.md)

* 搜索东西: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* 更新域列表: [清单说明](../INSTRUCTION.md).

* [将Cloudflare或与项目相关的事件添加到历史记录。](../HISTORY.md)

* [尝试并编写新的工具/脚本。](../tool/)

* [这是一些PDF / ePUB可供阅读。](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### 关于假账户

Crimeflare了解假冒帐户的存在，这些假冒帐户可以假冒我们的官方频道，例如Twitter，Facebook，Patreon，OpenCollective，Villages等。
**我们从不询问您的电子邮件。
我们从不问您的名字。
我们从不询问您的身份。
我们从不询问您的位置。
我们从不要求您捐款。
我们从不要求您发表评论。
我们从不要求您关注社交媒体。
我们从不问您的社交媒体。**

# 不要信任虚假帐户。



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)