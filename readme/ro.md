# Marele Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Opriți Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  „Marele Cloudwall” este Cloudflare Inc., compania americană.Oferă servicii CDN (rețea de livrare de conținut), atenuare DDoS, securitate Internet și servicii DNS distribuite (server nume de domeniu).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare este cel mai mare proxy MITM din lume (proxy invers).Cloudflare deține mai mult de 80% din cota de piață CDN, iar numărul de utilizatori de cloudflare crește în fiecare zi.Și-au extins rețeaua în peste 100 de țări.Cloudflare servește mai mult trafic web decât Twitter, Amazon, Apple, Instagram, Bing și Wikipedia.Cloudflare oferă un plan gratuit și multe persoane îl folosesc în loc să își configureze serverele în mod corespunzător.Au tranzacționat confidențialitate prin comoditate.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare se află între dvs. și serverul de origine Web, acționând ca un agent de patrulare de frontieră.Nu vă puteți conecta la destinația aleasă.Vă conectați la Cloudflare și toate informațiile dvs. sunt decriptate și transmise din timp. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Administratorul webserver-ului originar a permis agentului - Cloudflare - să decidă cine poate accesa „proprietatea lor web” și să definească „zona restricționată”.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Aruncați o privire la imaginea potrivită.Vei crede că Cloudflare blochează doar băieții răi.Vei crede că Cloudflare este întotdeauna online (nu se stinge niciodată).Veți crede că roboții legitimi și crawler-urile vă pot indexa site-ul.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Totuși, acestea nu sunt deloc adevărate.Cloudflare blochează oameni nevinovați fără niciun motiv.Cloudflare poate coborî.Cloudflare blochează roboții legitimați.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  La fel ca orice serviciu de hosting, Cloudflare nu este perfect.Veți vedea acest ecran chiar dacă serverul de origine funcționează bine.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Chiar crezi că Cloudflare are timp de funcționare 100%?Nu ai idee de câte ori Cloudflare coboară.Dacă Cloudflare coboară, clientul dvs. nu vă poate accesa site-ul. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Se numește acest lucru în referire la Marele Firewall din China, care face o treabă comparabilă de a filtra mulți oameni de a vedea conținut web (adică toți din China continentală și oameni din afara).În același timp, cei neafectați să vadă un web diferit, un web lipsit de cenzură, cum ar fi o imagine a „omului de tanc” și istoria „protestelor din Piața Tiananmen”. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare are o putere mare.Într-un anumit sens, ei controlează ceea ce vede în final utilizatorul final.Sunteți împiedicat să navigați pe site din cauza Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare poate fi folosit pentru cenzură. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Nu puteți vizualiza site-ul cloudflared dacă utilizați un browser minor, care Cloudflare ar putea crede că este un bot (deoarece nu mulți îl folosesc). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Nu puteți trece această „verificare a browserului” invazivă fără a activa Javascript.Aceasta este o pierdere de cinci (sau mai multe) secunde din viața ta valoroasă. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  De asemenea, Cloudflare blochează automat roboți / crawlere legitime, precum clienții Google, Yandex, Yacy și API.Cloudflare monitorizează în mod activ comunitatea „bypass cloudflare”, cu intenția de a sparge robotii de cercetare legitimi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare împiedică în mod similar multe persoane care au o conectare slabă la internet să acceseze site-urile web din spatele acestuia (de exemplu, ar putea fi în spatele a 7+ straturi de NAT sau să partajeze același IP, de exemplu Wifi public), cu excepția cazului în care rezolvă mai multe CAPTCHA-uri de imagine.În unele cazuri, acest lucru va dura 10-30 de minute pentru a satisface Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  În 2020, Cloudflare a trecut de la Recaptcha Google la hCaptcha, deoarece Google intenționează să perceapă pentru utilizarea sa.Cloudflare v-a spus că le pasă confidențialitatea („ajută la abordarea unei probleme de confidențialitate”), dar aceasta este în mod evident o minciună.Este vorba despre bani.„HCaptcha permite site-urilor să câștige bani pentru a satisface această cerere în timp ce blochează roboți și alte forme de abuz” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Din perspectiva utilizatorului, acest lucru nu se schimbă prea mult. Ești obligat să o rezolvi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Mulți oameni și programe software sunt blocate de Cloudflare în fiecare zi. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare enervează mulți oameni din întreaga lume.Aruncați o privire la listă și gândiți-vă dacă adoptarea Cloudflare pe site-ul dvs. este bună pentru experiența utilizatorului. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Care este scopul internetului dacă nu puteți face ceea ce doriți?Majoritatea persoanelor care vizitează site-ul dvs. web vor căuta doar alte pagini dacă nu pot încărca o pagină web.Este posibil să nu blocați în mod activ niciun vizitator, dar firewall-ul implicit al Cloudflare este suficient de strict pentru a bloca multe persoane. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Nu există nicio modalitate de a rezolva captcha fără a activa Javascript și Cookie-uri.Cloudflare le folosește pentru a crea o semnătură a browserului pentru a vă identifica.Cloudflare trebuie să vă cunoască identitatea pentru a decide dacă sunteți eligibil pentru a continua navigarea pe site. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Utilizatorii Tor și utilizatorii VPN sunt, de asemenea, victima Cloudflare.Ambele soluții sunt utilizate de multe persoane care nu își pot permite internetul necenzurat din cauza politicii lor de țară / corporație / rețea sau care doresc să adauge un strat suplimentar pentru a-și proteja confidențialitatea.Cloudflare îi atacă fără rușine pe acești oameni, forțându-i să-și oprească soluția de procură. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Dacă nu ați încercat Tor până în acest moment, vă recomandăm să descărcați Tor Browser și să vizitați site-urile dvs. web favorite.Vă sugerăm să nu vă autentificați pe site-ul băncii sau pe pagina web a guvernului sau vă vor semnaliza contul. Utilizați VPN pentru acele site-uri web. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Poate doriți să spuneți „Tor este ilegal! Utilizatorii Tor sunt criminali! Tor este rău! ". Nu.S-ar putea să aflați despre Tor de la televizor, spunând că Tor poate fi folosit pentru a naviga cu darknet și pentru a comercializa arme, droguri sau porno cu copii.Deși afirmația de mai sus este adevărată că există multe site-uri de pe piață de unde puteți cumpăra astfel de articole, aceste site-uri sunt adesea apărute și pe clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor a fost dezvoltat de armata americană, dar Tor este actual dezvoltat de proiectul Tor.Există multe persoane și organizații care folosesc Tor, inclusiv viitorii tăi prieteni.Deci, dacă utilizați Cloudflare pe site-ul dvs., blocați oameni adevărați.Vei pierde potențialul acord de prietenie și de afaceri. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Și serviciul lor DNS, 1.1.1.1, filtrează de asemenea utilizatorii de la vizitarea site-ului web, returnând adresa IP falsă deținută de Cloudflare, IP localhost, cum ar fi „127.0.0.x”, sau doar returnează nimic. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS, de asemenea, rupe software-ul online de la aplicația smartphone la jocul pe computer, din cauza răspunsului lor fals DNS.Cloudflare DNS nu poate interoga unele site-uri web ale băncii. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Și aici s-ar putea să vă gândiți,<br>Nu folosesc Tor sau VPN, de ce să mă intereseze?<br>Am încredere în marketingul Cloudflare, de ce ar trebui să-mi pese<br>Site-ul meu este https de ce ar trebui să-mi pese | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Dacă vizitați site-ul web care utilizează Cloudflare, vă împărtășiți informațiile nu numai proprietarului site-ului, ci și Cloudflare.Așa funcționează proxy-ul invers. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Este imposibil de analizat fără a decripta traficul TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare cunoaște toate datele dvs., cum ar fi parola brută. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed se poate întâmpla oricând. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https-ul Cloudflare nu este niciodată la sfârșit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Doriți cu adevărat să vă împărtășiți datele dvs. cu Cloudflare și, de asemenea, agenția cu 3 scrisori? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Profilul online al utilizatorului de Internet este un „produs” pe care guvernul și marile companii de tehnologie doresc să îl cumpere. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  A declarat Departamentul de Securitate Interioară al SUA:<br><br>Aveți idee cât de valoroase sunt datele pe care le aveți? Există vreun fel în care ne-ai vinde aceste date?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare oferă, de asemenea, serviciul GRATUIT VPN numit „Cloudflare Warp”.Dacă îl utilizați, toate conexiunile smartphone-ului (sau computerului) sunt trimise către serverele Cloudflare.Cloudflare poate ști pe ce site web ați citit, cu ce comentarii ați postat, cu cine ați vorbit etc.Vă dați voluntar toate informațiile dvs. la Cloudflare.Dacă vă gândiți „glumiți? Cloudflare este sigur. " atunci trebuie să aflați cum funcționează VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare a spus că serviciul lor VPN vă face internetul rapid.Dar VPN face ca conexiunea dvs. la internet să fie mai lentă decât conexiunea dvs. existentă. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  S-ar putea să știți deja despre scandalul PRISM.Este adevărat că AT&T permite NSA să copieze toate datele de pe internet pentru supraveghere. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Să spunem că lucrați la ANS și doriți profilul de internet al fiecărui cetățean.Știți că majoritatea dintre ei au încredere orbă în Cloudflare și o folosesc - o singură poartă centralizată - pentru a-și oferi o legătură cu serverul companiei (SSH / RDP), site-ul personal, site-ul de chat, site-ul forumului, site-ul bancar, site-ul de asigurări, motorul de căutare, membru secret -un singur site, site-ul de licitație, cumpărături, site-uri video, site-ul NSFW și site-ul ilegal.Știți, de asemenea, că folosesc serviciul DNS al Cloudflare („1.1.1.1”) și serviciul VPN („Cloudflare Warp”) pentru „Secure! Mai repede! Mai bine!" experiență pe internet.Combinarea acestora cu adresa IP a utilizatorului, amprenta digitală a browserului, cookie-urile și RAY-ID va fi utilă pentru a crea profilul online al țintei. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Vrei datele lor. Ce vei face? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare este o miere.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Miere gratuită pentru toată lumea. Câteva șiruri atașate.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Nu folosiți Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Descentralizați internetul.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Vă rugăm să continuați la pagina următoare:  "[Etică la nori](ro.ethics.md)"

---

<details>
<summary>_faceți clic pe mine_

## Date și mai multe informații
</summary>


Acest depozit este o listă de site-uri web care se află în spatele „Marelui Cloudwall”, care blochează utilizatorii Tor și alte CDN-uri.


**Date**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Utilizatori Cloudflare](../cloudflare_users/)
* [Domenii Cloudflare](../cloudflare_users/domains/)
* [Utilizatori CDN non-Cloudflare](../not_cloudflare/)
* [Utilizatori anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Mai multe informatii**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Descarca: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * EBook original (ePUB) a fost șters de BookRix GmbH din cauza încălcării drepturilor de autor a materialelor CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Biletul a fost vandalizat de atâtea ori.
  * [Șters de Proiectul Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Vezi biletul 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Ultimul bilet de arhivă 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_faceți clic pe mine_

## Ce poti face?
</summary>

* [Citiți lista noastră de acțiuni recomandate și împărtășiți-o prietenilor.](../ACTION.md)

* [Citiți vocea altui utilizator și scrieți-vă gândurile.](../PEOPLE.md)

* Căutați ceva: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Actualizați lista de domenii: [Enumerați instrucțiunile](../INSTRUCTION.md).

* [Adăugați Cloudflare sau eveniment legat de proiect în istoric.](../HISTORY.md)

* [Încercați și scrieți un nou instrument / script.](../tool/)

* [Iată câteva documente PDF / ePUB pentru citit.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Despre conturi false

Crimeflare știe despre existența unor conturi false care să impună canalele noastre oficiale, fie că e vorba de Twitter, Facebook, Patreon, OpenCollective, Villages etc.
**Nu vă cerem niciodată e-mailul.
Nu vă întrebăm niciodată numele.
Nu vă întrebăm niciodată identitatea.
Nu vă întrebăm niciodată locația.
Nu vă cerem niciodată donația.
Nu vă cerem niciodată recenzia.
Nu vă rugăm niciodată să urmați pe social media.
Nu vă întrebăm niciodată media socială.**

# NU FIDERIȚI FACURILE DE CONTURI.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)