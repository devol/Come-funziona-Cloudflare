# گریٹ کلاؤڈ وال


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## کلاؤڈ فلایر کو روکیں


|  🖹  |  🖼 |
| --- | --- |
|  "گریٹ کلاؤڈ وال" امریکی کمپنی ، کلاؤڈ فلائر انکارپوریشن ہے۔یہ CDN (مواد کی فراہمی کے نیٹ ورک) خدمات ، DDoS تخفیف ، انٹرنیٹ سیکیورٹی ، اور تقسیم DNS (ڈومین نام سرور) خدمات مہیا کررہا ہے۔  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  کلاؤڈ فلایر دنیا کا سب سے بڑا MITM پراکسی ہے (الٹا پراکسی)کلاؤڈ فلایر سی ڈی این مارکیٹ شیئر کا 80٪ سے زیادہ کا مالک ہے اور ہر دن کلاؤڈ فلائر صارفین کی تعداد بڑھ رہی ہے۔انہوں نے اپنے نیٹ ورک کو 100 سے زیادہ ممالک تک بڑھایا ہے۔کلاؤڈ فلایر ٹویٹر ، ایمیزون ، ایپل ، انسٹاگرام ، بنگ اور ویکیپیڈیا کے مشترکہ سے زیادہ ویب ٹریفک کی خدمت کرتا ہے۔کلاؤڈ فلایر مفت منصوبہ پیش کررہا ہے اور بہت سے لوگ اپنے سرورز کو صحیح طریقے سے تشکیل دینے کے بجائے اسے استعمال کررہے ہیں۔وہ سہولت سے زیادہ پرائیویسی کا کاروبار کرتے تھے۔  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  کلاؤڈ فلایر بارڈر گشت ایجنٹ کی طرح کام کرتے ہوئے ، آپ اور اورجنسی سرور کے بیچ بیٹھتا ہے۔آپ اپنی منتخب منزل سے رابطہ قائم کرنے کے اہل نہیں ہیں۔آپ کلاؤڈ فلایر سے منسلک ہو رہے ہیں اور آپ کی ساری معلومات ڈکرپٹ اور اڑان کے حوالے کردی جارہی ہے۔ |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  اوریجنسی کے ویب ایڈورسٹ ایڈمنسٹریٹر نے ایجنٹ - کلاؤڈ فلایر - کو یہ فیصلہ کرنے کی اجازت دی کہ ان کی "ویب پراپرٹی" تک کون رسائی حاصل کرسکتا ہے اور "محدود علاقے" کی وضاحت کرسکتا ہے۔  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  صحیح تصویر پر ایک نظر ڈالیں۔آپ سوچیں گے کہ کلاؤڈ فلایر صرف برے لوگوں کو روکتا ہے۔آپ سوچیں گے کہ کلاؤڈ فلایر ہمیشہ آن لائن ہوتا ہے (کبھی نیچے نہیں جاتا)۔آپ سوچیں گے کہ لیٹ بوٹس اور کرالر آپ کی ویب سائٹ کو انڈیکس کرسکتے ہیں۔  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  تاہم ، یہ بالکل بھی درست نہیں ہیں۔کلاؤڈ فلایر بے وجہ لوگوں کو بلا وجہ روک رہا ہے۔کلاؤڈ فلایر نیچے جاسکتا ہے۔کلاؤڈ فلایر نے قانونی چالوں کو روک دیا ہے۔  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  کسی بھی ہوسٹنگ سروس کی طرح ، کلاؤڈ فلایر بھی کامل نہیں ہے۔آپ کو یہ اسکرین نظر آئے گی یہاں تک کہ اگر اصل سرور ٹھیک کام کررہا ہے۔  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  کیا آپ واقعی میں سوچتے ہیں کہ کلاؤڈ فلایر میں 100٪ اپ ٹائم ہے؟آپ کو اندازہ نہیں ہے کہ کلاؤڈ فلایر کتنی بار نیچے جاتا ہے۔اگر کلاؤڈ فلایر نیچے جاتا ہے تو آپ کا صارف آپ کی ویب سائٹ تک رسائی حاصل نہیں کرسکتا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  اسے چین کے عظیم فائروال کے حوالے سے کہا جاتا ہے جو ویب انسانوں (یعنی سرزمین چین میں ہر شخص اور باہر کے لوگوں) کو دیکھنے سے بہت سے انسانوں کو فلٹر کرنے کا موازنہ کام کرتا ہے۔جب کہ ایک ہی وقت میں ، متاثر کن لوگوں کو خوفناک حد تک مختلف ویب دیکھنے کی ضرورت نہیں ہے ، لیکن ایسے سینسرشپ سے پاک ویب جیسے "ٹینک مین" کی شبیہہ اور "تیان مین اسکوائر احتجاج" کی تاریخ۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  کلاؤڈ فلایر میں بڑی طاقت ہے۔ایک لحاظ سے ، وہ اس پر قابو رکھتے ہیں کہ آخر کار آخر کار کیا دیکھتا ہے۔کلاؤڈ فلایر کی وجہ سے آپ کو ویب سائٹ براؤز کرنے سے روکا گیا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  کلاؤڈ فلایر سنسرشپ کے لئے استعمال کیا جاسکتا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  اگر آپ معمولی براؤزر استعمال کررہے ہیں تو آپ کلاؤڈ فلاڈ ویب سائٹ نہیں دیکھ سکتے ہیں جو کلاؤڈ فلائر سوچ سکتا ہے کہ یہ ایک بوٹ ہے (کیونکہ بہت سے لوگ اسے استعمال نہیں کرتے ہیں)۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  آپ جاوا اسکرپٹ کو چالو کیے بغیر اس جارحانہ "براؤزر چیک" کو پاس نہیں کرسکتے ہیں۔یہ آپ کی قیمتی زندگی کے پانچ (یا اس سے زیادہ) سیکنڈ کا ضیاع ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  کلاؤڈ فلایر خود بخود قانونی روبوٹ / کرولرز جیسے گوگل ، یینڈیکس ، یسی ، اور API کلائنٹس کو بھی روکتا ہے۔کلاؤڈ فلایر جائز تحقیقات کو توڑنے کے ارادے سے "بائی پاس کلاؤڈ فلایر" کمیونٹی کی سرگرمی سے نگرانی کر رہا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  کلاؤڈ فلایر اسی طرح بہت سارے لوگوں کو روکتا ہے جن کے پاس ناقص انٹرنیٹ کنیکٹیویٹی موجود ہے اس کے پیچھے ویب سائٹ تک رسائی حاصل کرنے سے روکتا ہے (مثال کے طور پر ، وہ NAT کی 7+ تہوں کے پیچھے ہوسکتے ہیں یا اسی IP کا اشتراک کر سکتے ہیں ، مثال کے طور پر عوامی وائی فائی) جب تک وہ متعدد تصویری CAPTCHA کو حل نہ کریں۔کچھ معاملات میں ، گوگل کو مطمئن کرنے میں 10 سے 30 منٹ لگیں گے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  سال 2020 میں کلاؤڈ فلایر نے گوگل کے ریپیٹا سے ایچ سی کیپچا میں تبدیل کردیا کیونکہ گوگل اپنے استعمال کے لئے معاوضہ لینا چاہتا ہے۔کلاؤڈ فلایر نے آپ کو بتایا کہ وہ آپ کی رازداری کی دیکھ بھال کرتے ہیں ("اس سے رازداری کی تشویش کو دور کرنے میں مدد ملتی ہے") لیکن یہ ظاہر ہے کہ یہ جھوٹ ہے۔یہ سب رقم کا ہے۔"ایچ سی کیپچا ویب سائٹ کو اس مطالبے کے مطابق رقم کمانے کی اجازت دیتی ہے جبکہ بوٹس اور دیگر قسم کی زیادتیوں کو روکتی ہے۔" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  صارف کے نقطہ نظر سے ، اس میں زیادہ تبدیلی نہیں آتی ہے۔ آپ کو اس کو حل کرنے پر مجبور کیا جارہا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  ہر روز کلاؤڈ فلایر کے ذریعہ بہت سارے انسانوں اور سافٹ ویروں کو مسدود کردیا جاتا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  کلاؤڈ فلایر نے دنیا بھر کے بہت سارے لوگوں کو ناراض کیا ہے۔فہرست پر ایک نظر ڈالیں اور سوچیں کہ آیا آپ کی سائٹ پر کلاؤڈ فلایر کو اپنانا صارف کے تجربے کے ل. اچھا ہے۔ |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  اگر آپ اپنی مرضی سے نہیں کر سکتے تو انٹرنیٹ کا مقصد کیا ہے؟زیادہ تر لوگ جو آپ کی ویب سائٹ پر جاتے ہیں وہ دوسرے صفحات کی تلاش کریں گے اگر وہ ویب صفحہ لوڈ نہیں کرسکتے ہیں۔ہوسکتا ہے کہ آپ کسی بھی ملاقاتی کو فعال طور پر روک نہیں رہے ہیں ، لیکن کلاؤڈ فلایر کا ڈیفالٹ فائر وال بہت سارے لوگوں کو روکنے کے لئے کافی سخت ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  جاوا اسکرپٹ اور کوکیز کو چالو کیے بغیر کیپچا کو حل کرنے کا کوئی طریقہ نہیں ہے۔کلاؤڈ فلایر آپ کی شناخت کیلئے براؤزر کے دستخط بنانے کیلئے ان کا استعمال کررہا ہے۔کلاؤڈ فلایر کو یہ فیصلہ کرنے کے لئے اپنی شناخت جاننے کی ضرورت ہے کہ آیا آپ سائٹ کو براؤز کرنا جاری رکھنے کے اہل ہیں یا نہیں۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  ٹور استعمال کنندہ اور وی پی این صارفین بھی کلاؤڈ فلایر کا شکار ہیں۔یہ دونوں حل بہت سارے افراد استعمال کر رہے ہیں جو اپنے ملک / کارپوریشن / نیٹ ورک کی پالیسی کی وجہ سے غیر سنسر انٹرنیٹ کے متحمل نہیں ہوسکتے ہیں یا جو اپنی رازداری کے تحفظ کے ل extra اضافی پرت شامل کرنا چاہتے ہیں۔کلاؤڈ فلایر بے شرمی سے ان لوگوں پر حملہ کر رہا ہے ، اور انہیں اپنے پراکسی حل کو بند کرنے پر مجبور کررہا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  اگر آپ نے اس لمحے تک ٹور کی کوشش نہیں کی تو ہم آپ کو حوصلہ افزائی کرتے ہیں کہ ٹور براؤزر ڈاؤن لوڈ کریں اور اپنی پسندیدہ ویب سائٹ دیکھیں۔ہم آپ کو مشورہ دیتے ہیں کہ آپ اپنی بینک ویب سائٹ یا سرکاری ویب پیج پر لاگ ان نہ ہوں یا وہ آپ کے اکاؤنٹ کو پرچم لگائیں گے۔ ان ویب سائٹوں کے لئے وی پی این کا استعمال کریں۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  آپ یہ کہنا چاہیں گے کہ “ٹور غیر قانونی ہے! ٹور استعمال کرنے والے مجرم ہیں! ٹور خراب ہے! ". نہیں۔آپ نے ٹور کے بارے میں ٹیلی ویژن سے یہ جان لیا ہو گا کہ ٹور کو ڈارک نیٹ اور ٹریڈ گن ، منشیات یا چڈ فحش براؤز کرنے کے لئے استعمال کیا جاسکتا ہے۔اگرچہ مذکورہ بالا بیان درست ہے کہ بہت ساری مارکیٹ کی ویب سائٹ موجود ہے جہاں آپ ایسی چیزیں خرید سکتے ہیں ، وہ سائٹیں اکثر کلارنیٹ پر بھی دکھائی دیتی ہیں۔  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  ٹور کو امریکی فوج نے تیار کیا تھا ، لیکن موجودہ ٹور کو ٹور پروجیکٹ نے تیار کیا ہے۔بہت سارے لوگ اور تنظیمیں ہیں جو آپ کے مستقبل کے دوستوں سمیت ٹور استعمال کرتے ہیں۔لہذا ، اگر آپ اپنی ویب سائٹ پر کلاؤڈ فلایر استعمال کررہے ہیں تو آپ حقیقی انسانوں کو مسدود کررہے ہیں۔آپ ممکنہ دوستی اور کاروباری معاہدے سے محروم ہوجائیں گے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  اور ان کی ڈی این ایس سروس ، 1.1.1.1 ، کلاؤڈ فلایر ، لوکل ہومسٹ IP جیسے "127.0.0.x" کی ملکیت میں جعلی آئی پی ایڈریس ، یا کچھ بھی نہیں لوٹانے کے ذریعے صارفین کو ویب سائٹ پر جانے سے روک رہی ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  کلاؤڈ فلائر ڈی این ایس اپنے جعلی ڈی این ایس جواب کی وجہ سے اسمارٹ فون ایپ سے کمپیوٹر گیم میں آن لائن سافٹ ویئر کو بھی توڑ دیتا ہے۔کلاؤڈفلیئر DNS کچھ بینک ویب سائٹوں سے استفسار نہیں کرسکتا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  اور یہاں آپ سوچ سکتے ہیں ،<br>میں ٹور یا وی پی این استعمال نہیں کر رہا ہوں ، مجھے کیوں پرواہ کرنا چاہئے؟<br>مجھے کلاؤڈ فلایر مارکیٹنگ پر اعتماد ہے ، مجھے کیوں پرواہ کرنی چاہئے<br>میری ویب سائٹ https ہے کیوں مجھے پرواہ کرنا چاہئے | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  اگر آپ ویب سائٹ ملاحظہ کرتے ہیں جو کلاؤڈ فلایر استعمال کرتی ہے تو ، آپ اپنی معلومات نہ صرف ویب سائٹ کے مالک بلکہ کلاؤڈ فلایر کو بھی بانٹ رہے ہیں۔اس طرح ریورس پراکسی کام کرتی ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  TLS ٹریفک کو ڈیریکٹ کیے بغیر تجزیہ کرنا ناممکن ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  کلاؤڈ فلایر آپ کے تمام ڈیٹا کو جانتا ہے جیسے خام پاس ورڈ۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  کلاؤڈ بیڈ کبھی بھی ہو سکتی ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  کلاؤڈ فلائرز کے https کبھی بھی ختم نہیں ہوتے ہیں۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  کیا آپ واقعی اپنے ڈیٹا کو کلاؤڈ فلایر کے ساتھ ، اور 3-خط والی ایجنسی کے ساتھ بھی بانٹنا چاہتے ہیں؟ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  انٹرنیٹ صارف کا آن لائن پروفائل ایک "پروڈکٹ" ہے جسے حکومت اور بڑی ٹیک کمپنیاں خریدنا چاہتی ہیں۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  امریکی محکمہ ہوم لینڈ سیکیورٹی نے کہا:<br><br>کیا آپ کو اندازہ ہے کہ آپ کے پاس موجود ڈیٹا کتنا قیمتی ہے؟ کیا آپ کو وہ ڈیٹا بیچنے کا کوئی طریقہ ہے؟  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  کلاؤڈ فلایر مفت کلاؤڈ فلایر سروس بھی پیش کرتا ہے جسے "کلاؤڈ فلائر وارپ" کہا جاتا ہے۔اگر آپ اسے استعمال کرتے ہیں تو ، آپ کے تمام اسمارٹ فون (یا آپ کا کمپیوٹر) کنکشن کلاؤڈ فلاla سرورز پر بھیجے جاتے ہیں۔کلاؤڈ فلایر جان سکتا ہے کہ آپ نے کس ویب سائٹ کو پڑھا ہے ، آپ نے کیا تبصرہ کیا ہے ، کس سے بات کی ہے وغیرہ۔آپ اپنی تمام معلومات کلاؤڈ فلایر کو رضاکارانہ طور پر دے رہے ہیں۔اگر آپ کو لگتا ہے کہ "کیا آپ مذاق کر رہے ہیں؟ کلاؤڈ فلایر محفوظ ہے۔ تب آپ کو یہ سیکھنے کی ضرورت ہوگی کہ وی پی این کیسے کام کرتا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  کلاؤڈ فلائر نے کہا کہ ان کی وی پی این سروس آپ کے انٹرنیٹ کو تیز تر بناتی ہے۔لیکن وی پی این آپ کے موجودہ کنیکشن سے آپ کا انٹرنیٹ کنیکشن سست بناتا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  آپ شاید PRISM اسکینڈل کے بارے میں جان چکے ہوں گے۔یہ سچ ہے کہ اے ٹی اینڈ ٹی این ایس اے کو نگرانی کے لئے تمام انٹرنیٹ ڈیٹا کاپی کرنے کی اجازت دیتا ہے۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  ہم کہتے ہیں کہ آپ NSA پر کام کر رہے ہیں ، اور آپ ہر شہری کا انٹرنیٹ پروفائل چاہتے ہیں۔آپ جانتے ہیں کہ ان میں سے بیشتر آنکھیں بند کرکے کلاؤڈ فلایر پر بھروسہ کررہے ہیں اور اسے استعمال کررہے ہیں - صرف ایک مرکزی گیٹ وے - اپنی کمپنی سرور کنیکشن (ایس ایس ایچ / آر ڈی پی) ، ذاتی ویب سائٹ ، چیٹ ویب سائٹ ، فورم ویب سائٹ ، بینک ویب سائٹ ، انشورنس ویب سائٹ ، سرچ انجن ، خفیہ ممبر صرف ویب سائٹ ، نیلامی کی ویب سائٹ ، خریداری ، ویڈیو ویب سائٹ ، NSFW ویب سائٹ ، اور غیر قانونی ویب سائٹ۔آپ یہ بھی جانتے ہیں کہ وہ کلاؤڈ فلایر کی ڈی این ایس سروس ("1.1.1.1") اور VPN سروس ("کلاؤڈ فلائر وارپ") کو "سیکیور!" کے لئے استعمال کرتے ہیں۔ تیز! بہتر! " انٹرنیٹ کا تجربہ۔ان کا صارف کے IP ایڈریس ، براؤزر فنگر پرنٹ ، کوکیز اور RAY-ID کے ساتھ جوڑنا ہدف کے آن لائن پروفائل کی تعمیر کے ل useful مفید ہوگا۔ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  آپ ان کا ڈیٹا چاہتے ہیں۔ آپ کیا کریں گے؟ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **کلاؤڈ فلایر ایک ہنی پاٹ ہے۔** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **سب کے لئے مفت شہد۔ کچھ ڈور منسلک۔** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **کلاؤڈ فلایر کا استعمال نہ کریں۔** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **انٹرنیٹ کو وکندریقرت بنائیں۔** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    براہ کرم اگلے صفحے پر جاری رکھیں:  "[کلاؤڈ فلایر اخلاقیات](ur.ethics.md)"

---

<details>
<summary>_مجھے کلک کیجیے_

## ڈیٹا اور مزید معلومات
</summary>


یہ ذخیر ویب سائٹوں کی ایک فہرست ہے جو "دی گریٹ کلاؤڈ وال" کے پیچھے ہے ، جس سے ٹور صارفین اور دیگر سی ڈی این مسدود ہوتے ہیں۔


**ڈیٹا**
* [کلاؤڈ فلائر انکارپوریٹڈ](../cloudflare_inc/)
* [کلاؤڈ فلا Usersر صارفین](../cloudflare_users/)
* [کلاؤڈ فلایر ڈومینز](../cloudflare_users/domains/)
* [نان-کلاؤڈفلیئر سی ڈی این صارفین](../not_cloudflare/)
* [اینٹی ٹور استعمال کرنے والے](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**مزید معلومات**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * ڈاؤن لوڈ کریں: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * اصل ای بک (ePUB) کو BookRix GmbH نے CC0 مواد کی حق اشاعت کی خلاف ورزی کی وجہ سے حذف کردیا تھا۔
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * اتنے بار ٹکٹ کی توڑ پھوڑ کی گئی۔
  * [ٹور پروجیکٹ کے ذریعہ حذف ہوگیا۔](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [ٹکٹ 34175 دیکھیں۔](https://trac.torproject.org/projects/tor/ticket/34175)
  * [آخری محفوظ شدہ دستاویزات کا ٹکٹ 24351۔](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_مجھے کلک کیجیے_

## تم کیا کر سکتے ہو؟
</summary>

* [ہماری تجویز کردہ کارروائیوں کی فہرست پڑھیں اور اسے اپنے دوستوں کے ساتھ شیئر کریں۔](../ACTION.md)

* [دوسرے صارف کی آواز پڑھیں اور اپنے خیالات لکھیں۔](../PEOPLE.md)

* کچھ تلاش کریں: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* ڈومین لسٹ کو اپ ڈیٹ کریں: [ہدایات کی فہرست](../INSTRUCTION.md).

* [کلاؤڈ فلایر یا پروجیکٹ سے متعلق ایونٹ کو تاریخ میں شامل کریں۔](../HISTORY.md)

* [نیا ٹول / اسکرپٹ آزمائیں اور لکھیں۔](../tool/)

* [پڑھنے کے لئے کچھ پی ڈی ایف / ای پی یو بی یہ ہے۔](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### جعلی اکاؤنٹس کے بارے میں

کرائم فلایر ہمارے سرکاری چینلز کی نقالی کرنے والے جعلی کھاتوں کے وجود کے بارے میں جانتے ہیں ، چاہے وہ ٹویٹر ، فیس بک ، پیٹریون ، اوپن کلیکٹو ، دیہات وغیرہ ہوں۔
**ہم آپ کا ای میل کبھی نہیں پوچھتے ہیں۔
ہم کبھی آپ کا نام نہیں پوچھتے۔
ہم آپ کی شناخت کبھی نہیں پوچھتے ہیں۔
ہم کبھی بھی آپ کا مقام نہیں پوچھتے۔
ہم کبھی بھی آپ کا چندہ نہیں مانگتے ہیں۔
ہم آپ کا جائزہ کبھی نہیں پوچھتے۔
ہم کبھی بھی آپ کو سوشل میڈیا پر عمل کرنے کو نہیں کہتے ہیں۔
ہم آپ کے سوشل میڈیا سے کبھی نہیں پوچھتے۔**

# جعلی اکائونٹس پر اعتبار نہ کریں۔



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)