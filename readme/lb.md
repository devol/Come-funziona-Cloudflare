# De Grousse Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Stop Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "The Great Cloudwall" ass Cloudflare Inc., d'US Firma.Et bitt CDN (Inhalt Liwwerung Reseau) Servicer, DDoS mitigatioun, Internet Sécherheet, a verdeelt DNS (Domain Name Server) Servicer.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare ass dee weltgréisste MITM Proxy (Reverse Proxy).Cloudflare Besëtz méi wéi 80% vum CDN Maartundeel an d'Zuel vu Cloudflare Benotzer wuessen all Dag.Si hunn hire Reseau op méi wéi 100 Länner ausgebaut.Cloudflare servéiert méi Webverkéier wéi Twitter, Amazon, Apple, Instagram, Bing & Wikipedia kombinéiert.Cloudflare bitt e gratis Plang a vill Leit benotze se anstatt hir Serveren richteg ze konfiguréieren.Si hunn Privatsphär iwwer Komfort verhandelt.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare sëtzt tëscht Iech an Hierkonft Webserver, wierkt wéi e Grenzpatruléierende Agent.Dir kënnt net mat Ärer gewielter Destinatioun verbannen.Dir verbonne mat Cloudflare an all Är Informatioune ginn entschlësselt an iwwerweist. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Den Originaler Webserver Administrator huet den Agent erlaabt - Cloudflare - fir ze entscheeden wien op hir "Web Property" Zougang kann an "limitéiert Regioun definéieren".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Kuckt Iech dat richtegt Bild.Dir mengt Cloudflare blockéiere nëmme schlecht Männer.Dir mengt Cloudflare ass ëmmer online (geet ni erof).Dir mengt legit Bots a Crawler kënnen Är Websäit indexéieren.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Déi sinn awer guer net wouer.Cloudflare blockéiert onschëlleg Leit ouni Grond.Cloudflare kann erof goen.Cloudflare blockéiert legit Bots.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Just wéi all Hosting Service ass Cloudflare net perfekt.Dir gesitt dësen Écran och wann den Originalserver gutt funktionnéiert.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Mengt Dir wierklech datt Cloudflare 100% Iwwerstonnen huet?Dir hutt keng Ahnung wéi oft Mol Cloudflare erof geet.Wann Cloudflare erof geet, da kënnt Äre Client net op Är Websäit. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Et gëtt dëst als Referenz zu der Great Firewall vu China genannt, wat e vergläichbare Job mécht fir vill Mënschen ze filteren fir de Webinhalt ze gesinn (dh all an de Festland China a Leit dobausse).Iwwerdeems gläichzäiteg déi net betraff sinn eng dratesch aner Web ze gesinn, e Web fräi vu Zensur wéi e Bild vum "Tankman" an d'Geschicht vum "Tiananmen Square Protester". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare besëtzt grouss Kraaft.An engem Sënn kontrolléieren se wat den Endbenutzer schlussendlech gesäit.Dir sidd verhënnert d'Websäit ze surfen wéinst Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare kann fir Zensur benotzt ginn. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Dir kënnt net Cloudflared Websäit kucken wann Dir e klenge Browser benotzt, deen Cloudflare kéint mengen et ass e Bot (well net vill Leit et benotzen). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Dir kënnt dës invasiv "Browsercheck" net duerchgoe loossen ouni Javascript z'aktivéieren.Dëst ass eng Offall vu fënnef (oder méi) Sekonnen vun Ärem wäertvollen Liewen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare blockéieren och automatesch legit Roboteren / Crawler wéi Google, Yandex, Yacy, an API Clienten.Cloudflare ass iwwerwaacht aktiv "bypass Cloudflare" Gemeinschaft mat enger Absicht legit Fuerschungsbots ze briechen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare ähnlech verhënnert datt vill Leit, déi aarm Internetkonnektivitéit hunn, op d'Websäiten hannendrun zougräifen (zum Beispill, se kéinten hannert 7+ Schichten vun der NAT sinn oder déiselwecht IP deelen, zum Beispill ëffentlech Wifi) ausser si léisen multiple Image CAPTCHAs.An e puer Fäll dauert dat 10 bis 30 Minutten fir Google zefridden ze stellen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Am Joer 2020 ass de Cloudflare vum Google Recaptcha op hCaptcha gewiesselt well Google virgesi war fir säi Gebrauch ze charge.Cloudflare huet Iech gesot datt si Är Privatsphär këmmeren ("et hëlleft eng Privatsphär betrëfft") awer dëst ass offensichtlech eng Ligen.Et ass alles ëm Suen."HCaptcha erlaabt Websäite Suen ze maachen fir dës Nofro ze servéieren wärend Bots an aner Forme vu Mëssbrauch blockéiert ginn" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Aus der Perspektiv vum Benotzer ännert dat net vill. Dir sidd gezwongen et ze léisen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Vill Mënschen a Software ginn all Dag vum Cloudflare blockéiert. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare irritéiert vill Leit weltwäit.Kuckt Iech d'Lëscht an denkt ob d'Collflare op Ärem Site adoptéiere gutt ass fir d'Benotzererfarung. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Wat ass den Zweck vum Internet wann Dir net maache kënnt wat Dir wëllt?Déi meescht Leit, déi Är Websäit besichen, kucken just op aner Säiten, wa se eng Websäit net kënne lueden.Dir blockéiert vläicht net aktiv Besucher, awer de Standard Firewall vum Cloudflare ass streng genuch fir vill Leit ze blockéieren. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Et gëtt kee Wee fir de Captcha ze léisen ouni Javascript a Cookien z'aktivéieren.Cloudflare benotzt se fir e Browser Ënnerschrëft ze maachen fir Iech z'identifizéieren.Cloudflare muss Är Identitéit wëssen fir ze entscheeden ob Dir berechtegt sidd weider op de Site ze surfen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor Benotzer a VPN Benotzer sinn och Affer vu Cloudflare.Béid Léisunge gi vu ville Leit benotzt déi net onsensuréiert Internet leeschte kënnen duerch hir Land / Firma / Netzpolitik oder déi extra Schicht addéiere fir hir Privatsphär ze schützen.Cloudflare attackéiert dës Leit ouni Schimmt, forcéiert se hir Proxy-Léisung auszeschalten. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Wann Dir Tor bis dëse Moment net probéiert hutt, encouragéiere mir Iech de Tor Browser erofzelueden an Är Liiblingswebsäiten ze besichen.Mir proposéieren Iech net op Ärer Bank Websäit oder der Regierungswebsäit ze loggen oder datt se Äre Kont flagge loossen. Benotzt VPN fir dës Websäiten. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Dir wëllt soen "Tor ass illegal! Tor Benotzer si kriminell! Tor ass schlecht! ". Nee.Dir hutt iwwer de Fernseh iwwer Tor geléiert, a gesot datt den Tor ka benotzt ginn fir donkelnetz an Handelsgewënn, Drogen oder Chid Porn ze surfen.Während déi Ausso uewen richteg ass datt et vill Maartwebsäit ass, wou Dir sou Artikele kënnt kafen, ginn dës Site dacks och op Clearnet gewisen.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor gouf vun der US Army entwéckelt, awer den aktuellen Tor gëtt vum Tor Projet entwéckelt.Et gi vill Leit an Organisatiounen déi Tor benotzen inklusiv Är zukünfteg Frënn.Also, wann Dir Cloudflare op Ärer Websäit benotzt, blockéiert Dir wierklech Mënschen.Dir verléiert potenziell Frëndschaft a Geschäftsgeschäft. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  An hiren DNS Service, 1.1.1.1, filtert och d'Benotzer vun der Websäit ze besichen andeems se falsch IP Adresse vum Cloudflare, localhost IP wéi "127.0.0.x" zréckginn oder just näischt zréckginn. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS briechen och online Software vu Smartphone App bis Computerspill wéinst hirer falscher DNS Äntwert.Cloudflare DNS kann net op e puer Bankwebsäite froen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  An hei kënnt Dir denkt,<br>Ech benotze keng Tor oder VPN, firwat soll ech oppassen?<br>Ech vertrauen Cloudflare Marketing, firwat soll ech oppassen<br>Meng Websäit ass https firwat soll ech oppassen | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Wann Dir Websäit besicht, déi Cloudflare benotzt, deelt Dir Är Informatioun net nëmmen un de Besëtzer vum Site, awer och Cloudflare.Dëst ass wéi de reverse Proxy funktionnéiert. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Et ass onméiglech ze analyséieren ouni den TLS Traffic ze dekryptere. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare kennt all Är Donnéeën wéi rau Passwuert. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed kann zu all Moment geschéien. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Den https vum Cloudflare ass ni end-to-end. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Wëllt Dir wierklech Är Daten mat Cloudflare deelen, an och 3-Bréif Agence? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Den Online Benotzer Online Profil ass e "Produkt" dat d'Regierung a grouss Tech Firmen kafen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  U.S.Departement fir Homelands Sécherheet sot:<br><br>Hutt Dir eng Iddi wéi wertvoll d'Daten déi Dir hutt ass? Gëtt et e Wee wéi Dir eis dës Donnéeën géif verkafen?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare bitt och GRATIS VPN Service mam Numm "Cloudflare Warp".Wann Dir et benotzt, ginn all Ärem Smartphone (oder Äre Computer) Verbindungen op Cloudflare Server geschéckt.Cloudflare ka wësse wéi eng Websäit Dir gelies hutt, wat fir eng Kommentar Dir hutt gepost, mat wiem Dir geschwat hutt etc.Dir sidd fräiwëlleg all Är Informatioun op Cloudflare ze ginn.Wann Dir denkt "Witzt Dir? Cloudflare ass sécher. " da musst Dir léieren wéi VPN funktionnéiert. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare sot datt hire VPN Service Ären Internet séier mécht.Awer VPN maacht Är Internetverbindung méi lues wéi Är existent Verbindung. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Dir wësst vläicht scho iwwer de PRISM Skandal.Et ass richteg datt AT&T NSA erlaabt all Internet Daten fir d'Iwwerwaachung ze kopéieren. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Loosst eis soen datt Dir op der NSA schafft, an Dir wëllt all Bierger säin Internetprofil hunn.Dir wësst datt déi meescht vun hinnen blann op Cloudflare trauen an et benotze - nëmmen eng zentraliséiert Paart - fir hir Firmesserververbindung (SSH / RDP) ze proxy, perséinlech Websäit, Chat Websäit, Forum Websäit, Bank Websäit, Versécherungs Websäit, Sichmotor, Geheim Member -eng eenzeg Websäit, Auktioun Websäit, Shopping, Video Websäit, NSFW Websäit, an illegal Websäit.Dir wësst och datt se Cloudflare's DNS Service ("1.1.1.1") an VPN Service ("Cloudflare Warp") benotze fir "Séchert! Méi séier! Besser! “ Internet Erfahrung.D'Kombinatioun vun hinnen mat der IP-Adress vum Benotzer, Browser Fangerofdrock, Cookien an RAY-ID wier nëtzlech fir den Online-Profil vum Zil ze bauen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Dir wëllt hir Donnéeën. Waat wëlls du maachen? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare ass en Honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Gratis Schatz fir all. E puer Saiten ugeschloss.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Benotzt net Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Dezentraliséiert den Internet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Fuert weider op déi nächst Säit:  "[Cloudflare Ethik](lb.ethics.md)"

---

<details>
<summary>_klickt mir_

## Daten a méi Informatioun
</summary>


Dëst Repository ass eng Lëscht vu Websäiten, déi hannert "The Great Cloudwall" stinn, blockéiere Tor Benotzer an aner CDNs.


**Donnéeën**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare Benotzer](../cloudflare_users/)
* [Cloudflare Domains](../cloudflare_users/domains/)
* [Net-Cloudflare CDN Benotzer](../not_cloudflare/)
* [Anti-Tor Benotzer](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Méi Informatioun**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Eroflueden: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * D'Original eBook (ePUB) gouf vu BookRix GmbH geläscht wéinst Copyrightverletzung vu CC0 Material
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Den Ticket gouf esou vill Mol vandaliséiert.
  * [Geläscht vum Tor Project.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Kuckt Ticket 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Leschten Archivbilljee 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_klickt mir_

## Wat kënnt Dir maachen?
</summary>

* [Liest eis Lëscht vun empfohlenen Aktiounen an deelt se mat Äre Frënn.](../ACTION.md)

* [Liest aner Benotzer Stëmm a schreift Är Gedanken.](../PEOPLE.md)

* Sich no eppes: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Update d'Domain Lëscht: [Lëscht Instruktiounen](../INSTRUCTION.md).

* [Füügt Cloudflare oder Projetsrelatéiert Event zu der Geschicht.](../HISTORY.md)

* [Probéiert & schreift neit Tool / Skript.](../tool/)

* [Hei e puer PDF / ePUB fir ze liesen.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Iwwer gefälschte Konten

Crimeflare wëssen iwwer d'Existenz vu falschen Konten, déi eis offiziell Channels verzeechnen, sief et Twitter, Facebook, Patreon, OpenCollective, Villages etc.
**Mir froen ni Är E-Mail.
Mir froen ni Ären Numm.
Mir froen ni Är Identitéit.
Mir froen ni Är Standuert.
Mir froen nie Ären Don.
Mir froen ni Är Bewäertung.
Mir bieden Iech ni op de soziale Medien ze verfollegen.
Mir froen ni Är Social Media.**

# VERSTÄNNT NET FAKKE.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)