# Մեծ Cloudwall- ը


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Դադարեցրեք Cloudflare- ը


|  🖹  |  🖼 |
| --- | --- |
|  «Մեծ Cloudwall» - ը Cloudflare Inc., ամերիկյան ընկերություն է:Այն մատուցում է CDN (բովանդակության առաքման ցանց) ծառայություններ, DDoS մեղմացում, ինտերնետ անվտանգություն և բաշխված DNS (դոմենի անուն սերվեր) ծառայություններ:  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare- ը աշխարհի ամենամեծ MITM վստահված անձն է (հակադարձ վստահված անձ):Cloudflare- ը պատկանում է CDN շուկայի մասնաբաժնի ավելի քան 80% -ին, և ամպամածուկ օգտագործողների թիվը ամեն օր աճում է:Նրանք ընդլայնել են իրենց ցանցը ավելի քան 100 երկրներում:Cloudflare- ը ավելի շատ վեբ տրաֆիկ է մատուցում, քան համակցված Twitter- ի, Amazon- ի, Apple- ի, Instagram- ի, Bing- ի և Wikipedia- ի:Cloudflare- ն առաջարկում է անվճար պլան, և շատ մարդիկ օգտագործում են այն ՝ փոխարենը իրենց սերվերները պատշաճ կերպով կազմաձևելու փոխարեն:Նրանք հարմարության համար առևտուր էին անում գաղտնիությունը:  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare- ը նստում է ձեր և ծագման ցանցի միջև, որը գործում է որպես սահմանային պարեկային գործակալ:Դուք չեք կարող միանալ ձեր ընտրած նպատակակետին:Դուք կապվում եք Cloudflare- ին և ձեր բոլոր տեղեկությունները գաղտնագրվում և փոխանցվում են թռիչքի ժամանակ: |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Ծագման ցանցի ադմինիստրատորը թույլ է տվել գործակալին - Cloudflare- ին որոշել, թե ով կարող է մուտք ունենալ իրենց «վեբ գույք» և սահմանել «սահմանափակ տարածք»:  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Նայեք ճիշտ պատկերին:Դուք կարծում եք, որ Cloudflare- ը արգելափակում է միայն վատ տղաներին:Դուք կմտածեք, որ Cloudflare- ը միշտ առցանց է (երբեք մի իջիր):Դուք կարծում եք, որ օրինական բոտերը և սողացողները կարող են ինդեքսավորել ձեր վեբ կայքը:  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Այնուամենայնիվ, դրանք բոլորովին էլ ճիշտ չեն:Cloudflare- ն առանց պատճառի արգելափակում է անմեղ մարդկանց:Cloudflare- ն կարող է իջնել:Cloudflare- ն արգելափակում է լեգիտիմ բոտերը:  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Likeիշտ ինչպես ցանկացած հոստինգի ծառայություններ, Cloudflare- ը կատարյալ չէ:Դուք կտեսնեք այս էկրանը, նույնիսկ եթե ծագման սերվերը լավ է աշխատում:  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Դուք իսկապես կարծում եք, որ Cloudflare- ը 100% թարմացում ունի:Դուք գաղափար չունեք, թե քանի անգամ Cloudflare- ն իջնում ​​է:Եթե ​​Cloudflare- ն իջնում ​​է, ձեր հաճախորդը չի կարող մուտք գործել ձեր վեբ կայք: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Դա կոչվում է սա ՝ նկատի ունենալով Չինաստանի Մեծ Firewall- ը, որը համեմատաբար աշխատանք է կատարում ՝ զտելով շատ մարդկանց վեբ բովանդակությունը տեսնելու համար (այսինքն ՝ բոլորը Չինաստանում և դրսում գտնվող մարդիկ):Միևնույն ժամանակ, տուժածները կտրուկ տարբերվող վեբ կայք չտեսնելու համար, գրաքննությունից զերծ ցանց, ինչպիսիք են «տանկ մարդու» կերպարը և «Տյանանմեն հրապարակի բողոքի ցույցերի» պատմությունը: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare- ն ունի մեծ ուժ:Ինչ-որ իմաստով նրանք վերահսկում են այն, ինչ ի վերջո տեսնում է վերջնական օգտագործողը:Ձեզ խանգարվում է զննել կայքը `Cloudflare- ի պատճառով: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare- ը կարող է օգտագործվել գրաքննության համար: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Դուք չեք կարող դիտել ամպամած կայքը, եթե դուք օգտագործում եք փոքր բրաուզեր, որը Cloudflare- ը կարծում է, որ դա բոտ է (քանի որ այն շատ մարդիկ չեն օգտագործում): | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Դուք չեք կարող անցնել այս ինվազիվ «զննարկչի ստուգում» ՝ առանց Javascript- ին հնարավորություն տալու:Սա ձեր արժեքավոր կյանքի հինգ (կամ ավելի) վայրկյանների վատնում է: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare- ը նաև ավտոմատ կերպով արգելափակում է օրինական ռոբոտներին / սողացողներին, ինչպիսիք են Google- ը, Yandex- ը, Yacy- ը և API- ի հաճախորդները:Cloudflare- ն ակտիվորեն վերահսկում է «շրջանցելով ամպամածոցը» համայնքը `նպատակ ունենալով խախտել օրինական հետազոտական ​​բոտերը: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare- ը նման կերպ խանգարում է նաև այն մարդկանց, ովքեր ունեն ինտերնետի անբավարար կապ, որի հետևում կարող են մուտք գործել կայքեր (օրինակ, նրանք կարող են լինել NAT- ի 7+ շերտերի ետևում կամ նույն IP- ի, օրինակ ՝ հանրային Wifi- ի փոխանակում):Որոշ դեպքերում Google- ը բավարարելու համար դա տևելու է 10-30 րոպե: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020-ին Cloudflare- ը Google- ի Recaptcha- ից անցավ hCaptcha- ին, քանի որ Google- ը մտադիր է գանձել դրա օգտագործման համար:Cloudflare- ն ասաց, որ իրենք հոգ են տանում ձեր գաղտնիության մասին («դա օգնում է դիմել գաղտնիության խնդրին»), բայց դա ակնհայտորեն սուտ է:Ամեն ինչ փողի մասին է:«HCaptcha- ն թույլ է տալիս վեբ-կայքերին գումար վաստակել այս պահանջը սպասարկելու համար` միաժամանակ արգելափակելով բոտերը և չարաշահման այլ ձևեր »: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Օգտագործողի տեսանկյունից սա շատ բան չի փոխվում: Դուք ստիպված եք լինում լուծել այն: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Շատ մարդիկ և ծրագրաշարերը ամեն օր արգելափակվում են Cloudflare- ի կողմից: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare- ը վրդովեցնում է աշխարհի շատ մարդկանց:Նայեք ցանկին և մտածեք, արդյոք Cloudflare- ը ձեր կայքում ընդունելը օգտակար է օգտվողի փորձի համար: |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Ո՞րն է ինտերնետի նպատակը, եթե չես կարող անել այն ինչ ուզում ես:Ձեր կայք այցելած մարդկանց մեծամասնությունը պարզապես կփնտրի այլ էջեր, եթե նրանք չկարողանան բեռնել կայք:Հնարավոր է, որ դուք ակտիվորեն չեք արգելափակում որևէ այցելու, բայց Cloudflare- ի կանխադրված պատնեշը բավականաչափ խստ է ՝ շատ մարդկանց արգելափակելու համար: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Հնարավոր չէ լուծել captcha- ն ՝ առանց Javascript- ի և Cookie- ի միացման:Cloudflare- ն օգտագործում է դրանք զննարկչի զննման համար `ձեզ նույնականացնելու համար:Cloudflare- ը պետք է իմանա ձեր ինքնությունը `որոշելու` արդյո՞ք Դուք հեշտ եք շարունակել զննել կայքը: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor- ի օգտվողները և VPN օգտագործողները նույնպես Cloudflare- ի զոհ են:Երկու լուծումներն օգտագործվում են շատ մարդկանց կողմից, ովքեր իրենց երկրի / կորպորացիայի / ցանցի քաղաքականության պատճառով չեն կարողանա թույլ տալ անպատասխանատու ինտերնետ, կամ ովքեր ցանկանում են լրացուցիչ շերտ ավելացնել իրենց գաղտնիությունը պաշտպանելու համար:Cloudflare- ը անամոթորեն հարձակվում է այդ մարդկանց վրա ՝ ստիպելով նրանց անջատել իրենց վստահված լուծումը: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Եթե ​​մինչ այս պահը չեք փորձել Tor- ը, մենք խրախուսում ենք ձեզ ներբեռնել Tor Browser- ը և այցելել ձեր նախընտրած կայքերը:Առաջարկում ենք մուտք գործել ձեր բանկի կայք կամ կառավարության կայքէջ, կամ նրանք դրոշակ կդնեն ձեր հաշիվը: Օգտագործեք VPN այդ կայքերի համար: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Կարող եք ասել ՝ «Tor- ը ապօրինի է: Tor օգտագործողները հանցավոր են: Tor- ը վատ է »:Դուք Թոռի մասին կարող եք իմանալ հեռուստատեսությունից ՝ ասելով, որ Թոռը կարող է օգտագործվել մութ ցանց և առևտրի զենքեր, թմրանյութեր կամ զվարճալի պոռնո:Թեև վերը նշված հայտարարությունը ճիշտ է, որ շուկայի շատ կայքեր կան, որտեղ կարելի է գնել այդպիսի իրեր, այդ կայքերը հաճախ հայտնվում են նաև clearnet- ում:  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor- ը մշակվել է ԱՄՆ-ի բանակի կողմից, սակայն ներկայիս Tor- ը մշակված է Tor նախագծով:Կան բազմաթիվ մարդիկ և կազմակերպություններ, որոնք օգտագործում են Tor- ը ՝ ներառյալ ձեր ապագա ընկերները:Այսպիսով, եթե դուք օգտագործում եք Cloudflare- ը ձեր կայքում, ապա արգելակում եք իրական մարդկանց:Դուք կկորցնեք հավանական բարեկամությունն ու գործարքը: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Նրանց DNS ծառայությունը ՝ 1.1.1.1, նաև զտում է օգտվողներին կայք այցելելու միջոցով ՝ վերադարձնելով կեղծ IP հասցեն, որը պատկանում է Cloudflare- ին, տեղական IP- ում, ինչպիսիք են «127.0.0.x», կամ պարզապես ոչինչ չեն վերադարձնում: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS- ը կոտրում է առցանց ծրագրակազմը `սմարթֆոնի հավելվածից համակարգչային խաղ, նրանց կեղծ DNS պատասխանի պատճառով:Cloudflare DNS- ը չի կարող հարցնել բանկի որոշ կայքեր: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Եվ այստեղ կարող եք մտածել,<br>Ես Tor կամ VPN չեմ օգտագործում, ինչու՞ պետք է հոգ տանել:<br>Ես վստահում եմ Cloudflare- ի շուկայավարմանը, ինչու եմ պետք հոգ տանել<br>Իմ կայքը https է, թե ինչու պետք է հոգ տանել | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Եթե ​​այցելում եք Cloudflare- ով օգտագործող վեբ կայք, ձեր տեղեկությունները կիսում եք ոչ միայն կայքի սեփականատիրոջ, այլև Cloudflare- ի միջոցով:Այսպես է գործում հակադարձ վստահված անձը: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Անհնար է վերլուծել ՝ առանց գաղտնազերծելու TLS երթևեկությունը: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare- ը գիտի ձեր բոլոր տվյալները, ինչպիսիք են հում գաղտնաբառը: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed- ը կարող է պատահել ցանկացած պահի: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflare- ի https- ը երբեք ավարտ չունի: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Դուք իսկապես ցանկանում եք ձեր տվյալները կիսել Cloudflare- ի, ինչպես նաև 3-նամական գործակալության հետ: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Ինտերնետից օգտվողների առցանց պրոֆիլը «արտադրանք» է, որը կառավարությունը և խոշոր տեխնոլոգիական ընկերությունները ցանկանում են գնել: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Այդ մասին հայտնել են ԱՄՆ-ի ներքին անվտանգության վարչությունում:<br><br>Դուք պատկերացում ունե՞ք, թե որքան արժեքավոր են ձեր ունեցած տվյալները: Կա՞ որևէ կերպ, որ մեզ վաճառեք այդ տվյալները:  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare- ն առաջարկում է նաև ԱՆՎԱՐ VPN ծառայություն, որը կոչվում է «Cloudflare Warp»:Եթե ​​այն օգտագործում եք, ձեր բոլոր սմարթֆոնների (կամ ձեր համակարգչի) կապերը ուղարկվում են Cloudflare սերվերներին:Cloudflare- ը կարող է իմանալ, թե որ կայք եք կարդացել, ինչ մեկնաբանություն եք տեղադրել, ում հետ եք խոսել և այլն:Դուք կամավոր եք տրամադրել ձեր բոլոր տեղեկությունները Cloudflare- ին:Եթե ​​կարծում եք. «Կատակո՞ւմ եք: Cloudflare- ն ապահով է »: ապա դուք պետք է սովորեք, թե ինչպես է աշխատում VPN- ն: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare- ն ասաց, որ իրենց VPN ծառայությունը արագացնում է ձեր ինտերնետը:Բայց VPN- ը ձեր ինտերնետային կապը դանդաղեցնում է, քան առկա կապը: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Դուք արդեն կարող եք իմանալ PRISM սկանդալի մասին:Իշտ է, AT&T- ն ԱԱԾ-ին թույլ է տալիս բոլոր ինտերնետ տվյալների պատճենել հսկողության համար: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Եկեք ասենք, որ աշխատում եք ԱԱԾ-ում, և ցանկանում եք յուրաքանչյուր քաղաքացու ինտերնետային պրոֆիլ:Դուք գիտեք, որ նրանց մեծ մասը կուրորեն վստահում է Cloudflare- ին և օգտագործում է այն ՝ միայն մեկ կենտրոնացված դարպաս, որպեսզի վստահի իրենց ընկերության սերվերի կապը (SSH / RDP), անձնական կայքը, չաթ կայքը, ֆորումի կայքը, բանկի կայքը, ապահովագրության կայքը, որոնիչը, գաղտնի անդամը մեկական կայք, աճուրդների կայք, գնումներ, վիդեո կայք, NSFW կայք և ապօրինի կայք:Դուք նաև գիտեք, որ նրանք օգտագործում են Cloudflare- ի DNS ծառայությունը («1.1.1.1») և VPN ծառայությունը («Cloudflare Warp») «Ապահով! Ավելի արագ: Ավելի լավ է »: ինտերնետի փորձ:Դրանք համադրելով օգտագործողի IP հասցեի, զննարկչի մատնահետքի, cookie- ների և RAY-ID- ի հետ, օգտակար կլինեն նպատակային առցանց պրոֆիլը կառուցելու համար: | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Դուք ուզում եք նրանց տվյալները: Ի՞նչ կանես | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare- ը մեղր է:** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Բոլորի համար անվճար մեղր: Մի քանի տող կցված է:** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Մի օգտագործեք Cloudflare:** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Ապակենտրոնացնել ինտերնետը:** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Խնդրում ենք շարունակել հաջորդ էջը:  "[Cloudflare- ի էթիկան](hy.ethics.md)"

---

<details>
<summary>_կտտացրեք ինձ_

## Տվյալներ և այլ տեղեկություններ
</summary>


Այս պահեստը «Մեծ Cloudwall» - ի հետևում կանգնած կայքերի ցուցակ է, որոնք արգելափակում են Tor- ի օգտագործողներին և այլ CDN- ներ:


**Տվյալներ**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare օգտվողները](../cloudflare_users/)
* [Cloudflare տիրույթները](../cloudflare_users/domains/)
* [Ոչ Cloudflare CDN օգտագործողներ](../not_cloudflare/)
* [Anti-Tor օգտագործողներ](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Լրացուցիչ տեղեկություններ**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Ներբեռնեք: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Բուն eBook- ը (ePUB) ջնջվել է BookRix GmbH- ի կողմից CC0- ի նյութերի հեղինակային իրավունքի խախտման պատճառով:
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Տոմսը շատ անգամ վանդալիզացվել է:
  * [Delնջվել է Tor Project- ի կողմից:](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Տե՛ս 34175 տոմսը:](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Արխիվի վերջին տոմս 24351:](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_կտտացրեք ինձ_

## Ինչ կարող ես դու անել?
</summary>

* [Կարդացեք առաջարկվող գործողությունների մեր ցանկը և այն կիսեք ձեր ընկերների հետ:](../ACTION.md)

* [Կարդացեք այլ օգտվողի ձայնը և գրեք ձեր մտքերը:](../PEOPLE.md)

* Ինչ-որ բան փնտրեք: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Թարմացրեք տիրույթի ցուցակը: [Listուցադրել հրահանգները](../INSTRUCTION.md).

* [Ավելացնել Cloudflare կամ ծրագրի հետ կապված իրադարձություն պատմության մեջ:](../HISTORY.md)

* [Փորձեք և գրեք նոր գործիք / սցենար:](../tool/)

* [Ահա PDF / ePUB- ը կարդալու համար:](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Կեղծ հաշիվների մասին

Crimeflare- ը իմացեք մեր պաշտոնական ալիքները կեղծող հաշիվների առկայության մասին, լինի դա Twitter, Facebook, Patreon, OpenCollective, Գյուղեր և այլն:
**Մենք երբեք չենք հարցնում ձեր էլ.
Մենք երբեք ձեր անունը չենք հարցնում:
Մենք երբեք չենք հարցնում ձեր ինքնությունը:
Մենք երբեք չենք հարցնում ձեր գտնվելու վայրը:
Մենք երբեք չենք խնդրում ձեր նվիրատվությունը:
Մենք երբեք չենք հարցնում ձեր կարծիքը:
Մենք երբեք չենք խնդրում, որ հետևեք սոցիալական լրատվամիջոցներին:
Մենք երբեք չենք հարցնում ձեր սոցիալական մեդիան:**

# ՉԻ ՎՏԱՆՈՒՄ ՀԱՇՎԻ ՀԱՇՎԵՏՎՈՒԹՅՈՒՆՆԵՐ:



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)