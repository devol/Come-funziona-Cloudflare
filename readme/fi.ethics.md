# Eettiset ongelmat

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

"Älä tue tätä yritystä, jolla ei ole etiikkaa"

"Yrityksesi ei ole luotettava. Väität noudattavansa DMCA: ta, mutta sinulla on monia oikeusjuttuja siitä, että et tee niin."

"He sensuroivat vain niitä, jotka kyseenalaistavat heidän etiikansa."

"Luulen, että totuus on hankalaa ja se on paremmin piilotettu yleisön näkökulmasta."  -- [phyzonloop](https://twitter.com/phyzonloop)


---


<details>
<summary>napsauta minua

## CloudFlare spamme ihmisiä
</summary>


Cloudflare lähettää roskapostia muille kuin Cloudflare-käyttäjille.

- Lähetä sähköpostia vain valitsemille tilaajille
- Kun käyttäjä sanoo "lopettaa", lopeta sähköpostien lähettäminen

Se on niin yksinkertaista. Mutta Cloudflare ei välitä.
Cloudflare sanoi, että heidän palvelunsa avulla voidaan estää kaikki roskapostittajat tai hyökkääjät.
Kuinka voimme pysäyttää Cloudflaren aktivoimatta Cloudflare-ohjelmaa?


| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspam01.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspam03.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspam02.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspambrittany.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfspamtwtr.jpg) |

</details>

---

<details>
<summary>napsauta minua

## Poista käyttäjän arvostelu
</summary>


Cloudflare sensuroi negatiivisia arvosteluja.
Jos lähetät Cloudflaren vastaisen tekstin Twitterissä, sinulla on mahdollisuus saada vastaus Cloudflaren työntekijältä "Ei, se ei ole" -viestillä.
Jos lähetät kielteisen arvostelun mihin tahansa tarkistussivustoon, he yrittävät sensuroida sen.


| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfcenrev_01.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfcenrev_02.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfcenrev_03.jpg) |

</details>

---

<details>
<summary>napsauta minua

## Jaa käyttäjän yksityisiä tietoja
</summary>


Pilvipalolla on valtava häirintäongelma.
Cloudflare jakaa henkilökohtaisia ​​tietoja niistä, jotka valittavat isännöimistä sivustoista.
Joskus he pyytävät sinua antamaan todellisen henkilöllisyytesi.
Jos et halua häiritä, pahoinpidelä, hieroa tai tappaa, sinun on parasta pysyä poissa Cloudflared-verkkosivustoista.


| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_what.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_swat.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_kill.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_threat.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_dox.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_ex1.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdox_ex2.jpg) |

</details>

---

<details>
<summary>napsauta minua

## Hyväntekeväisyysmaksun yrityshaku
</summary>


CloudFlare pyytää hyväntekeväisyyttä.
On melko kauhistuttavaa, että amerikkalainen yritys pyytää hyväntekeväisyyttä voittoa tavoittelemattomien organisaatioiden rinnalla, joilla on hyvät syyt.
Jos haluat estää ihmisiä tai tuhlata muiden ihmisten aikaa, kannattaa tilata pizzat Cloudflaren työntekijöille.


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdonate.jpg)

</details>

---

<details>
<summary>napsauta minua

## Päättyvät sivustot
</summary>


Mitä teet, jos sivustosi sammuu yhtäkkiä?
On ilmoitettu, että Cloudflare poistaa käyttäjän määritykset tai lopettaa palvelun ilman varoitusta, hiljaa.
Ehdotamme sinun löytää parempi tarjoaja.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftmnt.jpg)

</details>

---

<details>
<summary>napsauta minua

## Selaimen toimittajien syrjintä
</summary>


CloudFlare antaa etusijan kohtelussa Firefoxia käyttäville, kun taas Tor-selaimen käyttäjille, jotka eivät ole Tor-selainta, tapahtuu vihamielistä kohtelua.
Tor-käyttäjät, jotka kieltäytyvät perustellusti kieltäytymästä ilmaisen javascriptin toteuttamisesta, saavat myös vihamielisen kohtelun.
Tämä käyttöoikeuksien epätasa-arvo on verkon neutraalisuuden väärinkäyttö ja vallan väärinkäyttö.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/browdifftbcx.gif)

- Vasen: Tor-selain, oikea: Chrome. Sama IP-osoite.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/browserdiff.jpg)

- Vasen: Tor-selaimen JavaScripti poistettu käytöstä, eväste käytössä
- Oikealla: Chrome Javascript käytössä, eväste poistettu käytöstä

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfsiryoublocked.jpg)

- QuteBrowser (alaselain) ilman Tor (Clearnet IP)

| ***Selain*** | ***Hoito*** |
| --- | --- |
| Tor Browser (Javascript käytössä) | pääsy sallittu |
| Firefox (Javascript käytössä) | pääsy heikentynyt |
| Chromium (Javascript käytössä) | pääsy heikentynyt |
| Chromium or Firefox (Javascript poistettu käytöstä) | pääsy evätty |
| Chromium or Firefox (Eväste on poistettu käytöstä) | pääsy evätty |
| QuteBrowser | pääsy evätty |
| lynx | pääsy evätty |
| w3m | pääsy evätty |
| wget | pääsy evätty |


Miksi et käytä äänipainiketta helpon haasteen ratkaisemiseksi?

Kyllä, äänipainike on, mutta se ei aina toimi Torilla.
Saat tämän viestin, kun napsautat sitä:

```
Yritä myöhemmin uudelleen
Tietokoneesi tai verkkoasi voi lähettää automatisoituja kyselyjä.
Käyttäjien suojelemiseksi emme voi käsitellä pyyntöäsi nyt.
Lisätietoja on ohjepalvelumme
```

</details>

---

<details>
<summary>napsauta minua

## Äänestäjien tukahduttaminen
</summary>


Yhdysvaltain osavaltioiden äänestäjät ilmoittavat äänestävänsä viime kädessä asuinvaltionsa valtiosihteerin verkkosivuston kautta.
Republikaanien kontrolloimat valtiosihteeristön toimistot harjoittavat äänestäjien tukahduttamista asettamalla valtiosihteerin verkkosivuston pilvipalvelun kautta.
Cloudflaren vihamielinen kohtelu Tor-käyttäjille, sen MITM-asema keskitetynä globaalina valvontapisteenä ja sen vahingollinen rooli tekevät mahdollisille äänestäjille vastahakoisen rekisteröinnin.
Varsinkin liberaalit omaksuvat yksityisyyden.
Äänestäjien rekisteröintilomakkeet keräävät arkaluontoisia tietoja äänestäjän poliittisesta taipumuksesta, henkilökohtaisesta fyysisestä osoitteesta, sosiaaliturvatunnuksesta ja syntymäajasta.
Suurin osa valtioista asettaa vain osan näistä tiedoista julkisesti saataville, mutta Cloudflare näkee kaikki nämä tiedot, kun joku ilmoittautuu äänestämään.

Huomaa, että paperirekisteröinti ei kiertä Cloudflare-ohjelmaa, koska valtiosihteeristön työntekijöiden työntekijät käyttävät todennäköisesti Cloudflare-verkkosivustoa tietojen syöttämiseen.

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfvotm_01.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfvotm_02.jpg) |

- Change.org on kuuluisa verkkosivusto keräämään ääniä ja ryhtymään toimiin.
“kaikkialla ihmiset käynnistävät kampanjoita, saavat tukijoita ja työskentelevät päätöksentekijöiden kanssa ratkaisujen löytämiseksi.”
Valitettavasti monet ihmiset eivät voi nähdä muutosta.org ollenkaan Cloudflaren aggressiivisen suodattimen takia.
Heitä estetään allekirjoittamasta vetoomusta, jolloin heidät poistetaan demokraattisesta prosessista.
Muiden ei pilvitetyn alustan, kuten OpenPetition, käyttö auttaa korjaamaan ongelman.

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/changeorgasn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/changeorgtor.jpg) |

- Cloudflaren "Ateenan projekti" tarjoaa ilmaisen yritystason suojauksen valtion ja paikallisten vaalien verkkosivustoille.
He sanoivat, että "heidän vaalipiirinsä voivat käyttää vaalia koskevia tietoja ja äänestäjien rekisteröintiä", mutta tämä on valhe, koska monet ihmiset eivät vain pysty selaamaan sivustoa ollenkaan.

</details>

---

<details>
<summary>napsauta minua

## Ohitetaan käyttäjän mieltymykset
</summary>


Jos hylkäät jotain, luulet, että et saa siitä sähköpostia.
Cloudflare ohittaa käyttäjän mieltymykset ja jakaa tietoja ulkopuolisten yritysten kanssa ilman asiakkaan suostumusta.
Jos käytät heidän ilmaista pakettiaan, he lähettävät joskus sähköpostia sinulle, jossa he pyytävät ostamaan kuukausitilauksen.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfviopl_tp.jpg)

</details>

---

<details>
<summary>napsauta minua

## Makaa käyttäjän tietojen poistamisesta
</summary>


Tämän entisen pilvipalveluasiakkaan blogin mukaan Cloudflare valehtelee tilien poistamisesta.
Nykyään monet yritykset pitävät tietosi sen jälkeen, kun olet sulkenut tai poistanut tilisi.
Suurin osa hyvistä yrityksistä mainitsee sen tietosuojakäytännössään.
Pilvipallo? Ei.

```
2019-08-05 CloudFlare lähetti minulle vahvistuksen siitä, että he olivat poistaneet tilini.
2019-10-02 Sain sähköpostin CloudFlarelta "koska olen asiakas"
```

Cloudflare ei tiennyt sanasta "poista".
Jos se todella poistetaan, miksi entinen asiakas sai sähköpostin?
Hän mainitsi myös, että Cloudflaren tietosuojakäytännössä ei mainita sitä.

```
Heidän uudessa tietosuojakäytännössään ei mainita tietojen säilyttämistä vuodeksi.
```

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfviopl_notdel.jpg)

Kuinka voit luottaa Cloudflareen, jos heidän tietosuojakäytäntönsä on LIE?

</details>

---

<details>
<summary>napsauta minua

## Säilytä henkilökohtaiset tietosi
</summary>


Cloudflare-tilin poistaminen on vaikeaa tasoa.

```
Lähetä tukilippu "Tili" -luokan avulla,
ja pyydä tilin poistamista viestin rungosta.
Tilissäsi ei saa olla verkkotunnuksia tai luottokortteja, ennen kuin pyydät poistoa.
```

Saat tämän vahvistusviestin.

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cf_deleteandkeep.jpg)

"Olemme alkaneet käsitellä poistopyyntöäsi", mutta "Jatkamme henkilökohtaisten tietojen tallentamista".

Voitko "luottaa" tähän?

</details>

---

## Aliaj informoj

- Joseph Sullivan (Joe Sullivan) ([Cloudflare CSO](https://twitter.com/eastdakota/status/1296522269313785862))
  - [Ex-Uber security head charged in connection with the cover-up of a 2016 hack that affected 57 million customers](https://www.businessinsider.com/uber-data-hack-security-head-joe-sullivan-charged-cover-up-2020-8)
  - [Former Chief Security Officer For Uber Charged With Obstruction Of Justice](https://www.justice.gov/usao-ndca/pr/former-chief-security-officer-uber-charged-obstruction-justice)


---

## Jatka seuraavalle sivulle:   [Cloudflare Voices](../PEOPLE.md)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/freemoldybread.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg)
