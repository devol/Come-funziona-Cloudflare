# The Great Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Stop Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  “The Great Cloudwall” er Cloudflare Inc., det amerikanske firma.Det leverer CDN-tjenester (indholdsleveringsnetværk), DDoS-begrænsning, internetsikkerhed og distribueret DNS (domænenavnserver) -tjenester.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare er verdens største MITM-proxy (reverse proxy).Cloudflare ejer mere end 80% af CDN-markedsandelen, og antallet af cloudflare-brugere vokser hver dag.De har udvidet deres netværk til mere end 100 lande.Cloudflare tjener mere webtrafik end Twitter, Amazon, Apple, Instagram, Bing & Wikipedia tilsammen.Cloudflare tilbyder gratis plan, og mange mennesker bruger det i stedet for at konfigurere deres servere korrekt.De handlede privatliv ud over bekvemmelighed.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare sidder mellem dig og oprindelseswebserver og fungerer som en grænsepatruljeagent.Du kan ikke oprette forbindelse til din valgte destination.Du opretter forbindelse til Cloudflare, og alle dine oplysninger bliver dekrypteret og overdraget undervejs. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Originalserveren administrator tilladt agenten - Cloudflare - at beslutte, hvem der kan få adgang til deres "webejendom" og definere "begrænset område".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Se på det rigtige billede.Du vil tro, at Cloudflare kun blokerer dårlige fyre.Du vil tro, at Cloudflare altid er online (gå aldrig ned).Du vil tro, at legitime bots og crawlere kan indeksere dit websted.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Disse er dog slet ikke sande.Cloudflare blokerer for uskyldige mennesker uden grund.Cloudflare kan gå ned.Cloudflare blokerer legit bots.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Ligesom enhver hosting service er Cloudflare ikke perfekt.Du vil se denne skærm, selvom originalserveren fungerer godt.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Tror du virkelig Cloudflare har 100% oppetid?Du har ingen idé om, hvor mange gange Cloudflare går ned.Hvis Cloudflare går ned, kan din kunde ikke få adgang til dit websted. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Det kaldes dette med henvisning til Great Firewall of China, der gør et sammenligneligt job med at filtrere mange mennesker fra at se webindhold (dvs. alle i det kinesiske fastland og folk uden for).Mens de på samme tid ikke er berørt af at se en dratisk anderledes web, er et web frit for censur såsom et billede af "tankmand" og historien om "Den Himmelske freds plads protester". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare besidder stor kraft.På en måde styrer de, hvad slutbrugeren i sidste ende ser.Du er forhindret i at gennemse webstedet på grund af Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare kan bruges til censur. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Du kan ikke se cloudflared-websted, hvis du bruger mindre browser, som Cloudflare måske synes, det er en bot (fordi ikke mange mennesker bruger det). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Du kan ikke videregive denne invasive “browser check” uden at aktivere Javascript.Dette er spild af fem (eller flere) sekunder af dit værdifulde liv. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare blokerer også automatisk legit robotter / crawlere som Google, Yandex, Yacy og API klienter.Cloudflare overvåger aktivt “bypass cloudflare” -samfundet med det formål at bryde legitime forskningsbots. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare forhindrer ligeledes mange mennesker, der har dårlig internetforbindelse i at få adgang til websiderne bag det (for eksempel kunne de være bag 7+ lag NAT eller dele samme IP, for eksempel offentlig Wifi), medmindre de løser flere billed CAPTCHA'er.I nogle tilfælde tager dette 10 til 30 minutter at tilfredsstille Google. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  I 2020 skiftede Cloudflare fra Googles Recaptcha til hCaptcha, da Google har til hensigt at opkræve gebyr for dets brug.Cloudflare fortalte dig, at de er interesserede i dit privatliv ("det hjælper med at tackle et privatlivsproblem"), men dette er tydeligvis en løgn.Det handler om penge.“HCaptcha giver websteder mulighed for at tjene penge, der imødekommer dette krav, mens de blokerer for bots og andre former for misbrug” | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Fra brugerens perspektiv ændrer dette ikke meget. Du bliver tvunget til at løse det. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Mange mennesker og software blokeres af Cloudflare hver dag. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare irriterer mange mennesker over hele verden.Se på listen og tænk, om det at bruge Cloudflare på dit websted er godt for brugeroplevelsen. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Hvad er formålet med internettet, hvis du ikke kan gøre, hvad du vil?De fleste mennesker, der besøger dit websted, vil bare kigge efter andre sider, hvis de ikke kan indlæse en webside.Du blokerer muligvis ikke aktivt nogen besøgende, men Cloudflares standard firewall er streng nok til at blokere mange mennesker. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Der er ingen måde at løse captcha uden at aktivere Javascript og Cookies.Cloudflare bruger dem til at oprette en browsersignatur til at identificere dig.Cloudflare skal vide din identitet for at beslutte, om du er berettiget til at fortsætte med at gennemse stedet. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor-brugere og VPN-brugere er også et offer for Cloudflare.Begge løsninger bruges af mange mennesker, der ikke har råd til usensureret internet på grund af deres land / virksomhed / netværkspolitik, eller som ønsker at tilføje ekstra lag for at beskytte deres privatliv.Cloudflare angriber skamløst disse mennesker og tvinger dem til at slå deres proxy-løsning fra. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Hvis du ikke prøvede Tor før dette øjeblik, opfordrer vi dig til at downloade Tor Browser og besøge dine foretrukne websteder.Vi foreslår, at du ikke logger ind på dit bankwebsted eller den offentlige webside, eller de vil markere din konto. Brug VPN til disse websteder. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Måske vil du sige “Tor er ulovligt! Tor-brugere er kriminelle! Tor er dårlig! ". Nej.Du lærte måske om Tor fra fjernsynet ved at sige, at Tor kan bruges til at gennemsøge mørknet og handle pistoler, stoffer eller chid-porno.Selvom ovenstående udsagn er sandt, at der er mange markedswebsteder, hvor du kan købe sådanne varer, vises disse sider ofte også på clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor blev udviklet af den amerikanske hær, men den nuværende Tor er udviklet af Tor-projektet.Der er mange mennesker og organisationer, der bruger Tor inklusive dine fremtidige venner.Så hvis du bruger Cloudflare på dit websted, blokerer du for rigtige mennesker.Du mister potentielt venskab og forretningsforhold. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  Og deres DNS-service, 1.1.1.1, filtrerer også brugere fra at besøge webstedet ved at returnere falske IP-adresser, der ejes af Cloudflare, localhost IP, såsom “127.0.0.x”, eller bare returnere intet. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS bryder også online software fra smartphone-app til computerspil på grund af deres falske DNS-svar.Cloudflare-DNS kan ikke forespørge nogle bankwebsteder. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  Og her kan du tænke,<br>Jeg bruger ikke Tor eller VPN, hvorfor skal jeg passe på?<br>Jeg stoler på Cloudflare-marketing, hvorfor skulle jeg være interesseret<br>Min hjemmeside er https, hvorfor skulle jeg passe på | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Hvis du besøger et websted, der bruger Cloudflare, deler du dine oplysninger ikke kun til webstedsejer, men også Cloudflare.Sådan fungerer den omvendte proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Det er umuligt at analysere uden at dekryptere TLS-trafik. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare kender alle dine data såsom rå adgangskode. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed kan ske når som helst. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflares https er aldrig ende til ende. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Ønsker du virkelig at dele dine data med Cloudflare og også agentur med 3 bogstaver? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Internetbrugerens online profil er et "produkt", som regeringen og big tech-virksomheder ønsker at købe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  U.S. Department of Homeland Security sagde:<br><br>Har du nogen idé om, hvor værdifulde de data, du har, er? Er der nogen måde, du sælger os disse data på?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare tilbyder også GRATIS VPN-service kaldet “Cloudflare Warp”.Hvis du bruger den, sendes alle din smartphone (eller din computer) forbindelser til Cloudflare-servere.Cloudflare kan vide, hvilket websted du har læst, hvilken kommentar du har sendt, hvem du har talt med osv.Du giver frivilligt alle dine oplysninger til Cloudflare.Hvis du tænker “Gør du spøg? Cloudflare er sikkert. ” så skal du lære, hvordan VPN fungerer. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare sagde, at deres VPN-service gør dit internet hurtigt.Men VPN gør din internetforbindelse langsommere end din eksisterende forbindelse. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Du ved muligvis allerede om PRISM-skandalen.Det er rigtigt, at AT&T lader NSA kopiere alle internetdata til overvågning. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Lad os sige, at du arbejder på NSA, og at du ønsker enhver borgeres internetprofil.Du ved, at de fleste af dem stoler blindt på Cloudflare og bruger det - kun en centraliseret gateway - til at proxy deres firmageserverforbindelse (SSH / RDP), personlig webside, chatwebsted, forumwebsted, bankwebsted, forsikringswebsted, søgemaskine, hemmeligt medlem - kun websted, auktionswebsted, shopping, videowebsite, NSFW-websted og ulovligt websted.Du ved også, at de bruger Cloudflares DNS-service ("1.1.1.1") og VPN-service ("Cloudflare Warp") til "Secure! Hurtigere! Bedre!" internetoplevelse.Kombination af dem med brugerens IP-adresse, browserfingeraftryk, cookies og RAY-ID vil være nyttigt til at oprette måls online-profil. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Du vil have deres data. Hvad vil du gøre? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare er en honningplade.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Gratis skat til alle. Nogle strenge er knyttet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Brug ikke Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Decentraliser internettet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Fortsæt til næste side:  "[Cloudflare-etik](da.ethics.md)"

---

<details>
<summary>_klik på mig_

## Data og mere information
</summary>


Dette depot er en liste over websteder, der ligger bag "The Great Cloudwall", der blokerer for Tor-brugere og andre CDN'er.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare-brugere](../cloudflare_users/)
* [Cloudflare-domæner](../cloudflare_users/domains/)
* [Ikke-Cloudflare CDN-brugere](../not_cloudflare/)
* [Anti-Tor brugere](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Mere information**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Hent: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Den originale e-bog (ePUB) blev slettet af BookRix GmbH på grund af krænkelse af copyright af CC0-materiale
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Billetten blev vandaliseret så mange gange.
  * [Slet af Tor-projektet.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Se billet 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Sidste arkivbillet 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_klik på mig_

## Hvad kan du gøre?
</summary>

* [Læs vores liste over anbefalede handlinger, og del den med dine venner.](../ACTION.md)

* [Læs den anden brugers stemme, og skriv dine tanker.](../PEOPLE.md)

* Søg efter noget: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Opdater domænelisten: [Liste instruktioner](../INSTRUCTION.md).

* [Føj Cloudflare eller projektrelateret begivenhed til historien.](../HISTORY.md)

* [Prøv & skriv nyt værktøj / script.](../tool/)

* [Her er nogle PDF / ePUB at læse.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Om falske konti

Crimeflare kender til eksistensen af ​​falske konti, der efterligger vores officielle kanaler, det være sig Twitter, Facebook, Patreon, OpenCollective, Landsbyer osv.
**Vi beder aldrig din e-mail.
Vi beder aldrig dit navn.
Vi beder aldrig om din identitet.
Vi spørger aldrig om din placering.
Vi beder aldrig om din donation.
Vi beder aldrig om din anmeldelse.
Vi beder dig aldrig følge på sociale medier.
Vi spørger aldrig dine sociale medier.**

# TRYG IKKE FAKE REGNSKABER.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)