# グレートクラウドウォール


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Cloudflareを停止


|  🖹  |  🖼 |
| --- | --- |
|  「The Great Cloudwall」は、米国の会社であるCloudflare Inc.です。CDN（コンテンツ配信ネットワーク）サービス、DDoS緩和、インターネットセキュリティ、分散DNS（ドメインネームサーバー）サービスを提供しています。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflareは、世界最大のMITMプロキシ（リバースプロキシ）です。CloudflareはCDN市場シェアの80％以上を所有しており、cloudflareユーザーの数は毎日増加しています。彼らはネットワークを100か国以上に拡大しています。Cloudflareは、Twitter、Amazon、Apple、Instagram、Bing、Wikipediaの合計よりも多くのWebトラフィックを処理します。Cloudflareは無料のプランを提供しており、多くの人々がサーバーを適切に構成する代わりにそれを使用しています。彼らは利便性よりプライバシーを交換した。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflareは、ユーザーと起点Webサーバーの間に位置し、国境警備隊のように機能します。選択した宛先に接続できません。Cloudflareに接続しており、すべての情報が解読されてその場で引き渡されています。 |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  元のWebサーバー管理者は、エージェント-Cloudflare-が「ウェブプロパティ」にアクセスできるユーザーを決定し、「制限された領域」を定義できるようにしました。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  右の画像をご覧ください。Cloudflareは悪者のみをブロックすると思います。Cloudflareは常にオンラインであると思います（ダウンしないでください）。正当なボットやクローラーがWebサイトをインデックスに登録できると思うでしょう。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  しかし、それらはまったく真実ではありません。Cloudflareは無罪の人々を理由もなくブロックしています。Cloudflareがダウンする可能性があります。Cloudflareは正当なボットをブロックします。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  他のホスティングサービスと同様に、Cloudflareは完璧ではありません。オリジンサーバーが正常に機能している場合でも、この画面が表示されます。  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Cloudflareの稼働時間は100％だと本当に思いますか？Cloudflareが何回ダウンするか分からない。Cloudflareがダウンした場合、顧客はWebサイトにアクセスできません。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  これは、中国の大ファイアウォールに関連してこれと呼ばれ、Webコンテンツを見ないように多くの人間（つまり、中国本土のすべての人と外の人々）を除外するという同等の役割を果たします。同時に、影響を受けていない人々は劇的に異なるウェブを見る一方で、「戦車兵」の画像や「天安門広場抗議」の歴史など、検閲のないウェブ。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflareには大きなパワーがあります。ある意味では、エンドユーザーが最終的に見るものを制御します。Cloudflareのため、ウェブサイトを閲覧できません。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflareは検閲に使用できます。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Cloudflareがボットであると見なす可能性のあるマイナーブラウザーを使用している場合、cloudflared Webサイトを表示することはできません（多くのユーザーが使用していないため）。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  JavaScriptを有効にしないと、この侵襲的な「ブラウザチェック」に合格できません。これは、あなたの貴重な人生の5秒以上の無駄です。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflareは、Google、Yandex、Yacy、APIクライアントなどの正当なロボット/クローラーも自動的にブロックします。Cloudflareは、合法的な研究ボットを解読する目的で、「バイパスcloudflare」コミュニティを積極的に監視しています。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflareは、複数の画像のCAPTCHAを解決しない限り、同様にインターネット接続の悪い多くの人々が背後のWebサイトにアクセスすることを防ぎます（たとえば、7層以上のNATの背後にあるか、同じIPを共有している可能性があります）。場合によっては、Googleが満足するまでに10〜30分かかることがあります。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  2020年に、GoogleはCloudflareの使用料金を請求する予定であったため、CloudflareはGoogleのRecaptchaからhCaptchaに切り替わりました。Cloudflareはあなたのプライバシーを気にかけている（「プライバシーの問題に対処するのに役立ちます」）と言いましたが、これは明らかに嘘です。それはすべてお金についてです。「hCaptchaにより、ボットやその他の形の不正行為をブロックしながら、ウェブサイトがこの要求に応えてお金を稼ぐことができます。」 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  ユーザーの観点からは、これはほとんど変わりません。あなたはそれを解決することを余儀なくされています。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  多くの人間とソフトウェアがCloudflareによって毎日ブロックされています。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflareは世界中の多くの人々を悩ませています。リストを見て、サイトでCloudflareを採用することがユーザーエクスペリエンスに役立つかどうかを検討してください。 |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  あなたがやりたいことができないのなら、インターネットの目的は何ですか？ウェブサイトにアクセスするほとんどの人は、ウェブページを読み込めない場合、他のページを探すだけです。訪問者を積極的にブロックしていない可能性がありますが、Cloudflareのデフォルトのファイアウォールは、多くの人々をブロックするのに十分なほど厳格です。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  JavascriptとCookieを有効にしないと、キャプチャを解決する方法はありません。Cloudflareはそれらを使用して、ユーザーを識別するブラウザー署名を作成しています。Cloudflareは、サイトを閲覧し続けることができるかどうかを判断するために、自分の身元を知る必要があります。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  TorユーザーとVPNユーザーもCloudflareの犠牲者です。どちらのソリューションも、国/企業/ネットワークポリシーのために無検閲のインターネットを買う余裕がないか、プライバシーを保護するために追加のレイヤーを追加したい多くの人々によって使用されています。Cloudflareは恥知らずにそれらの人々を攻撃しており、プロキシソリューションをオフにすることを強制しています。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  この時点までTorを試していない場合は、Tor Browserをダウンロードして、お気に入りのWebサイトにアクセスすることをお勧めします。銀行のウェブサイトや政府のウェブページにログインしないことをお勧めします。そうしないと、アカウントにフラグが立てられます。それらのウェブサイトにはVPNを使用してください。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  「トールは違法です！ Torユーザーは犯罪者です！ Torは悪い！」テレビからTorについて学んだかもしれませんが、Torを使用してダークネットを閲覧したり、銃、麻薬、ポルノを交換したりできます。上記のステートメントは、そのようなアイテムを購入できる多くの市場Webサイトがあることは事実ですが、それらのサイトは多くの場合、クリアネットにも表示されます。  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Torは米軍によって開発されましたが、現在のTorはTorプロジェクトによって開発されました。あなたの将来の友達を含め、Torを使用する人や組織はたくさんあります。したがって、WebサイトでCloudflareを使用している場合は、実際の人間をブロックしています。あなたは潜在的な友情と商取引を失うでしょう。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  また、DNSサービス1.1.1.1は、Cloudflareが所有する偽のIPアドレス、「127.0.0.x」などのlocalhost IPを返すか、何も返さないことで、ユーザーがWebサイトにアクセスできないようにします。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNSはまた、偽のDNS回答のため、スマートフォンアプリからコンピューターゲームへのオンラインソフトウェアを壊します。Cloudflare DNSは一部の銀行のWebサイトにクエリを実行できません。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  そしてここであなたは考えるかもしれません、<br>TorやVPNを使用していませんが、なぜ気にする必要があるのですか？<br>私はCloudflareマーケティングを信頼していますが、なぜ気にする必要がありますか<br>私のウェブサイトはhttpsです | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Cloudflareを使用しているWebサイトにアクセスすると、Webサイトの所有者だけでなくCloudflareにも情報が共有されます。これがリバースプロキシのしくみです。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  TLSトラフィックを復号化せずに分析することは不可能です。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflareは生のパスワードなどのすべてのデータを認識しています。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeedはいつでも発生する可能性があります。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Cloudflareのhttpsがエンドツーエンドになることはありません。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Cloudflareと3文字の代理店とデータを本当に共有しますか？ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  インターネットユーザーのオンラインプロファイルは、政府や大手テクノロジー企業が購入したい「製品」です。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  米国国土安全保障省は言った:<br><br>あなたが持っているデータがどれほど価値があるかについて何か考えがありますか？そのデータを販売する方法はありますか？  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflareは、「Cloudflare Warp」と呼ばれる無料のVPNサービスも提供しています。これを使用すると、スマートフォン（またはコンピューター）のすべての接続がCloudflareサーバーに送信されます。Cloudflareは、あなたが読んだウェブサイト、あなたが投稿したコメント、あなたが誰と話し合ったかなどを知ることができます。Cloudflareにすべての情報を提供することは自発的です。「冗談ですか？ Cloudflareは安全です。」次に、VPNの仕組みを学ぶ必要があります。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflareは、彼らのVPNサービスはあなたのインターネットを高速化すると述べた。ただし、VPNを使用すると、インターネット接続が既存の接続よりも遅くなります。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  あなたはすでにPRISMスキャンダルについて知っているかもしれません。AT＆TがNSAにすべてのインターネットデータを監視のためにコピーさせることは事実です。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  NSAで働いていて、すべての市民のインターネットプロファイルが必要だとします。あなたは彼らのほとんどがCloudflareを盲目的に信頼し、それを使用して-中央ゲートウェイ1つだけ-会社のサーバー接続（SSH / RDP）、個人Webサイト、チャットWebサイト、フォーラムWebサイト、銀行Webサイト、保険Webサイト、検索エンジン、秘密メンバーをプロキシすることを知っています。 -のみのWebサイト、オークションWebサイト、ショッピング、ビデオWebサイト、NSFW Webサイト、および違法Webサイト。また、彼らはCloudflareのDNSサービス（「1.1.1.1」）とVPNサービス（「Cloudflare Warp」）を使用して「セキュア！もっと早く！良い！」インターネット体験。それらをユーザーのIPアドレス、ブラウザのフィンガープリント、Cookie、RAY-IDと組み合わせることで、ターゲットのオンラインプロファイルを作成するのに役立ちます。 | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  あなたは彼らのデータを求めています。あなたは何をしますか？ | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflareはハニーポットです。** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **皆のための無料の蜂蜜。紐がついています。** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Cloudflareは使用しないでください。** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **インターネットを分散化します。** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    次のページに進んでください:  "[Cloudflare倫理](ja.ethics.md)"

---

<details>
<summary>_私をクリックして_

## データと詳細情報
</summary>


このリポジトリは、「The Great Cloudwall」の背後にあるWebサイトのリストであり、Torユーザーと他のCDNをブロックしています。


**データ**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflareユーザー](../cloudflare_users/)
* [Cloudflareドメイン](../cloudflare_users/domains/)
* [Cloudflare以外のCDNユーザー](../not_cloudflare/)
* [アンチトールユーザー](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**詳しくは**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * ダウンロード: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * オリジナルのeBook（ePUB）は、CC0素材の著作権侵害によりBookRix GmbHによって削除されました
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * チケットは何度も破壊されました。
  * [Torプロジェクトにより削除されました。](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [チケット34175を参照してください。](https://trac.torproject.org/projects/tor/ticket/34175)
  * [最終アーカイブチケット24351。](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_私をクリックして_

## あなたは何ができますか？
</summary>

* [推奨されるアクションのリストを読んで、友達と共有してください。](../ACTION.md)

* [他のユーザーの声を読んで、考えを書いてください。](../PEOPLE.md)

* 何かを検索: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* ドメインリストを更新する: [手順の一覧](../INSTRUCTION.md).

* [Cloudflareまたはプロジェクト関連のイベントを履歴に追加します。](../HISTORY.md)

* [新しいツール/スクリプトを試してみてください。](../tool/)

* [これが読むべきPDF / ePUBです。](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### 偽のアカウントについて

Crimeflareは、Twitter、Facebook、Patreon、OpenCollective、Villagesなど、公式チャンネルになりすましている偽のアカウントの存在を知っています。
**私たちはあなたのメールを決して求めません。
お名前はお伺いしません。
私たちはあなたの身元を尋ねることはありません。
私たちはあなたの場所を尋ねることはありません。
寄付をお願いすることはありません。
私たちはあなたのレビューを求めることはありません。
私たちはあなたにソーシャルメディアでフォローすることを決して求めません。
私たちはあなたのソーシャルメディアに尋ねることはありません。**

# 偽のアカウントを信用しないでください。



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)