# The Great Cloudwall


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Stopje Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "De grutte Cloudwall" is Cloudflare Inc., it Amerikaanske bedriuw.It leveret tsjinsten foar CDN (ynhâldferlieningsnetwurk), DDoS-mitigaasje, ynternetfeiligens, en ferspraat DNS (domeinnamme-server) tsjinsten.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare is de grutste MITM-proxy fan 'e wrâld (reverse-proxy).Cloudflare hat mear as 80% fan it CDN-merkdiel en it oantal brûkers fan wolkfloed groeit elke dei.Se hawwe har netwurk útwreide nei mear as 100 lannen.Cloudflare tsjinnet mear webferkear dan kombineare Twitter, Amazon, Apple, Instagram, Bing & Wikipedia.Cloudflare biedt fergees plan oan en in soad minsken brûke it yn plak fan har servers goed te konfigurearjen.Se ferhannele privacy oer gemak.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare sit tusken dy en oarsprong-webserver, en fungeart as in grinspatrolagint.Jo kinne gjin ferbining meitsje mei jo keazen bestimming.Jo ferbine mei Cloudflare en al jo ynformaasje wurdt ûntsifere en oerlevere yn 'e flecht. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  De behearder fan de webserver fan oarsprong liet de agent - Cloudflare - beslute wa't tagong kin ta har "webeigenskip" en "beheind gebiet" definiearje.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Sjoch efkes nei de juste ôfbylding.Jo sille tinke dat Cloudflare allinich minne jonges blokkeart.Jo sille tinke dat Cloudflare altyd online is (gean noait del).Jo sille tinke dat legit bots en crawlers jo webside kinne yndeksearje.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Dat binne lykwols hielendal net wier.Cloudflare blokkeart ûnskuldige minsken sûnder reden.Cloudflare kin nei ûnderen gean.Cloudflare blokkeart legit bots.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Krekt as elke hostingtsjinst is Cloudflare net perfekt.Jo sille dit skerm sjen, sels as de oarsprongsserver goed wurket.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Tinke jo wirklik dat Cloudflare 100% uptime hat?Jo hawwe gjin idee hoefolle kearen Cloudflare delkomt.As Cloudflare delgiet, kin jo klant gjin tagong krije ta jo webside. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  It wurdt dit neamd yn ferwizing nei de Grutte Firewall fan Sina, dy't in fergelykbere taak docht om in soad minsken te filterjen fan it sjen fan webynhâld (dat wol sizze elkenien yn it fêstelân fan Sina en minsken bûten).Wylst tagelyk degenen dy't net oantaast binne in dratysk oars web te sjen, in web frij fan sensuer lykas in ôfbylding fan "tankmens" en de skiednis fan "protesten fan it Tiananmenplein". | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare hat grutte krêft.Yn in sin kontrolearje se wat de ein brûker úteinlik sjocht.Jo wurde foarkommen fan it blêdzjen fan 'e webside fanwegen Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare kin brûkt wurde foar sensuer. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Jo kinne gjin cloudflared-webside besjen as jo lytse browser brûke dy't Cloudflare tinkt dat it in bot is (om't net in soad minsken it brûke). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Jo kinne dizze invasive "browserkontrôle" net trochjaan sûnder Javascript yn te skeakeljen.Dit is in fergriemen fan fiif (of mear) sekonden fan jo weardefolle libben. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare blokkearje ek automatysk legit robots / crawlers lykas Google, Yandex, Yacy en API kliïnten.Cloudflare kontroleart aktyf "bypass cloudflare" -mienskip mei in bedoeling om legit ûndersyksbots te brekken. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare foarkomt op deselde manier in protte minsken dy't in minne ynternetferbining hawwe fan tagong ta de websides der efter (se koenen bygelyks efter 7+ lagen fan NAT wêze of deselde IP diele, bygelyks iepenbiere Wifi), útsein as se meardere ôfbyldings CAPTCHA's oplosse.Yn guon gefallen sil dit 10 oant 30 minuten nimme om Google te befredigjen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  Yn it jier 2020 skeat Cloudflare fan Google's Recaptcha nei hCaptcha, om't Google fan doel is te beteljen foar har gebrûk.Cloudflare fertelde jo dat se jo privacy behannelje ("it helpt om in privacybelied oan te pakken") mar dit is fansels in leagen.It giet allegear oer jild."HCaptcha stelt websides yn steat om jild te meitsjen dy't dizze fraach tsjinje, wylst bots en oare foarmen fan misbrûk blokkearje" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  Fanút it perspektyf fan de brûker feroaret dit net folle. Jo wurde twongen om it op te lossen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  In protte minsken en software wurde elke dei blokkearre troch Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare ferneatiget in soad minsken oer de hiele wrâld.Sjoch efkes nei de list en tink of it oannimmen fan Cloudflare op jo side goed is foar brûkersûnderfining. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Wat is it doel fan it ynternet as jo net kinne dwaan wat jo wolle?De measte minsken dy't jo webside besykje sille gewoan nei oare siden sykje as se gjin webside kinne lade.Jo kinne miskien gjin besikers blokkearje, mar de standert firewall fan Cloudflare is strang genôch om in soad minsken te blokkearjen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  D'r is gjin manier om de captcha op te lossen sûnder Javascript en Cookies yn te skeakeljen.Cloudflare brûkt se om in browsertekening te meitsjen om jo te identifisearjen.Cloudflare moat jo identiteit witte om te besluten of jo yn oanmerking komme om troch te gean mei blêdzjen op de side. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Tor-brûkers en VPN-brûkers binne ek in slachtoffer fan Cloudflare.Beide oplossingen wurde brûkt troch in protte minsken dy't gjin censureare ynternet kinne betelje fanwege har lân / korporaasje / netwurkbelied of dy't ekstra laach wolle tafoegje om har privacy te beskermjen.Cloudflare slacht dizze minsken skamteleas oan, en twingt har om har proxy-oplossing út te skeakeljen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  As jo ​​Tor oant dit momint net hawwe besocht, moedigje wy jo oan om Tor Browser te downloaden en jo favorite websiden te besykjen.Wy riede oan dat jo net oanmelde op jo bankwebside of webside fan 'e regearing, of se sille jo akkount flagje. Brûk VPN foar dy websides. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Jo wolle miskien sizze: Tor is yllegaal! Tor-brûkers binne kriminele! Tor is min! ". Nee.Jo hawwe miskien oer televyzje oer Tor leard, en sei dat Tor kin brûkt wurde om te bladeren yn darknet en hannelsgewearen, drugs of chidporno.Wylst de ferklearring hjirboppe wier is dat d'r in protte merkenwebside binne wêr't jo sokke items kinne keapje, ferskine dizze siden ek faak op clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor waard ûntwikkele troch US Army, mar hjoeddeistige Tor wurdt ûntwikkele troch it Tor-projekt.D'r binne in soad minsken en organisaasjes dy't Tor brûke, ynklusyf jo takomstige freonen.Dus, as jo Cloudflare brûke op jo webside blokkearje jo echte minsken.Jo sille potensjele freonskip en saaklike deal ferlieze. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  En har DNS-tsjinst, 1.1.1.1, filtert brûkers ek fan it besykjen fan 'e webside troch falsk IP-adres werom te jaan yn eigendom fan Cloudflare, localhost IP lykas "127.0.0.x", of gewoan neat werombringe. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS brekke ek online software fan smartphone-app nei kompjûterspultsje fanwegen har falske DNS-antwurd.Cloudflare DNS kin net freegje oer guon bankwebsides. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  En hjir soene jo miskien tinke,<br>Ik brûk gjin Tor of VPN, wêrom soe ik soarchje moatte?<br>Ik fertrou marketing op Cloudflare, wêrom soe ik soarchje moatte<br>Myn webside is https, wêrom soe ik soarchje moatte | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  As jo ​​webside besykje dy't Cloudflare brûke, diele jo jo ynformaasje net allinich oan webside-eigner, mar ek Cloudflare.Dit wurket de reverse proxy. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  It is ûnmooglik om te analysearjen sûnder TLS-ferkear te ûntsiferjen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare wit al jo gegevens lykas rau wachtwurd. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Cloudbeed kin elk momint barre. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  De https fan Cloudflare is nea ein-oan-ein. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Wolle jo jo gegevens wirklik diele mei Cloudflare, en ek agintskip mei 3 letters? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  It online profyl fan ynternetbrûkers is in 'produkt' dat de oerheid en grutte techbedriuwen wolle keapje. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Amerikaanske ôfdieling Homeland Security sei:<br><br>Hawwe jo idee hoe weardefol de gegevens dy't jo hawwe is? Is d'r ien manier hoe't jo ús dy gegevens ferkeapje?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare biedt ek FERGESE VPN-tsjinst neamd "Cloudflare Warp".As jo ​​it brûke, wurde al jo smartphone (as jo kompjûter) ferbinings stjoerd nei Cloudflare-servers.Cloudflare kin witte hokker webside jo hawwe lêzen, hokker kommentaar jo hawwe pleatst, mei wa't jo hawwe praat, ensfh.Jo binne frijwillich it jaan fan al jo ynformaasje oan Cloudflare.As jo ​​tinke "Binne jo grapke? Cloudflare is feilich. ” dan moatte jo leare hoe't VPN wurket. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare sei dat har VPN-tsjinst jo ynternet snel makket.Mar VPN meitsje jo ynternetferbining stadiger dan jo besteande ferbining. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Jo kinne miskien al witte oer it PRISM-skandaal.It is wier dat AT&T NSA alle ynternetgegevens kopieart foar tafersjoch. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Litte we sizze dat jo wurkje by de NSA, en jo wolle it ynternetprofyl fan elke boarger hawwe.Jo wite dat de measte fan harren blyn Cloudflare fertrouwe en it brûke - mar ien sintralisearre gateway - om har bedriuwsserverferbining (SSH / RDP) te proxy, persoanlike webside, petearwebside, forumwebsite, bankwebside, fersekeringswebside, sykmasjine, geheime lid -only website, Auction website, shopping, video website, NSFW website, en yllegale webside.Jo wite ek dat se de DNS-tsjinst fan Cloudflare ("1.1.1.1") en VPN-tsjinst ("Cloudflare Warp") brûke foar "Feilich! Faster! Better!" ynternetûnderfining.Kombinearje se mei it IP-adres fan 'e brûker, fingeravdruk fan' e browser, cookies en RAY-ID sil nuttich wêze om it online profyl fan it doel te bouwen. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Jo wolle har gegevens. Wat silst dwaan? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare is in honeypot.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Fergees huning foar elkenien. Guon snaren taheakke.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Brûk gjin Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Desintralisearje it ynternet.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Trochgean asjebleaft nei folgjende pagina:  "[Cloudflare Etyk](fy.ethics.md)"

---

<details>
<summary>_klik my_

## Gegevens en mear ynformaasje
</summary>


Dit repository is in list fan websides dy't efter "The Great Cloudwall" steane, dy't Tor-brûkers en oare CDN's blokkearje.


**Data**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Cloudflare-brûkers](../cloudflare_users/)
* [Cloudflare-domeinen](../cloudflare_users/domains/)
* [Net-Cloudflare CDN-brûkers](../not_cloudflare/)
* [Anti-Tor brûkers](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Mear ynformaasje**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Download: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * It orizjinele eBook (ePUB) waard troch BookRix GmbH ferwidere fanwegen ynbreuk op auteursrjocht op CC0-materiaal
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * It kaartsje waard safolle kearen fandaliseare.
  * [Ferwidere troch it Tor-projekt.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Sjoch ticket 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Lêste argyfkaart 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_klik my_

## Wat kinst dwaan?
</summary>

* [Lês ús list mei oanbefellende aksjes en diel it mei jo freonen.](../ACTION.md)

* [Lês de stim fan oare brûker en skriuw jo gedachten.](../PEOPLE.md)

* Sykje wat: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Update de domeinnamme: [List ynstruksjes](../INSTRUCTION.md).

* [Foegje Cloudflare as projekt relatearre barrens ta oan skiednis.](../HISTORY.md)

* [Besykje en skriuw in nij ark / skript.](../tool/)

* [Hjir is wat PDF / ePUB om te lêzen.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Oer falske akkounts

Crimeflare wite oer it bestean fan falske akkounts dy't ús offisjele kanalen ferpersoanlikje, of it no Twitter, Facebook, Patreon, OpenCollective, Doarpen ensfh.
**Wy freegje jo e-post noait.
Wy freegje jo namme noait.
Wy freegje jo identiteit noait.
Wy freegje jo lokaasje noait.
Wy freegje jo donaasje noait.
Wy freegje jo resinsje noait.
Wy freegje jo noait om te folgjen op sosjale media.
Wy freegje jo sosjale media noait.**

# BETROKKE FAKKE REKEN NET.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)