# Вялікая Воблака


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/itsreallythatbad.jpg)
![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/telegram/c81238387627b4bfd3dcd60f56d41626.jpg)

---


## Спыніце Cloudflare


|  🖹  |  🖼 |
| --- | --- |
|  "Вялікай воблачнай сцяной" з'яўляецца кампанія Cloudflare Inc., ЗША.Ён прадастаўляе паслугі CDN (сетка дастаўкі кантэнту), змякчэнне DDoS, бяспеку ў Інтэрнэце і размеркаваны DNS (сервер даменных імёнаў).  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflaredearuser.jpg) |
|  Cloudflare - найбуйнейшы ў свеце проксі-сервер MITM (зваротны проксі).Cloudflare належыць больш за 80% долі рынку CDN, і колькасць карыстальнікаў cloudflare расце з кожным днём.Яны пашырылі сваю сетку больш чым у 100 краінах.Cloudflare абслугоўвае больш трафіку ў Інтэрнэце, чым Twitter, Amazon, Apple, Instagram, Bing і Wikipedia.Cloudflare прапануе бясплатны план, і многія людзі выкарыстоўваюць яго, замест таго, каб правільна наладзіць свае серверы.Яны гандлявалі канфідэнцыяльнасцю з-за зручнасці.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfmarketshare.jpg)  |
|  Cloudflare сядзіць паміж вамі і вытворцам вэб-сервера, дзейнічаючы як агент памежнага патруля.Вы не можаце падключыцца да абранага пункта прызначэння.Вы падключаецеся да Cloudflare, і ўся ваша інфармацыя расшыфроўваецца і перадаецца на хаду. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/border_patrol.jpg)  |
|  Адміністратар вэб-сервера паходжання дазволіў агенту - Cloudflare - вырашыць, хто можа атрымаць доступ да сваёй "вэб-уласнасці" і вызначыць "зону абмежаванага доступу".  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/usershoulddecide.jpg)  |
|  Зірніце на правільнае малюнак.Вы думаеце, што Cloudflare блакуе толькі дрэнных хлопцаў.Вы думаеце, што Cloudflare заўсёды ў сеціве (ніколі не спускайцеся).Вы будзеце думаць, што законныя боты і сканеры могуць індэксаваць ваш сайт.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howcfwork.jpg)  |
|  Аднак гэта зусім не так.Cloudflare без прычыны блакуе нявінных людзей.Воблачная хваля можа спусціцца.Cloudflare блакуе законныя боты.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdowncfcom.jpg)  |
|  Як і любы хостынг, Cloudflare не з'яўляецца дасканалым.Вы ўбачыце гэты экран, нават калі сервер паходжання працуе добра.  |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdown2019.jpg) |
|  Вы сапраўды думаеце, што Cloudflare мае 100% часу працы?Вы нават не ўяўляеце, колькі разоў аблокі апускаюцца.Калі Cloudflare зніжаецца, ваш кліент не можа атрымаць доступ да вашага сайта. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareinternalerror.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflareoutage-2020.jpg) |
|  Гэта называецца гэта ў спасылцы на Вялікі брандмаўэр Кітая, які выконвае супастаўную працу па фільтрацыі шматлікіх людзей ад прагляду вэб-кантэнту (г.зн. усіх у Кітаі і людзей па-за межамі).У той жа час тыя, хто не пацярпеў ад бачання дратычна іншай сеткі, сеткі без цэнзуры, напрыклад, вобраз "танкіста" і гісторыя пратэстаў на плошчы Цяньаньмэнь. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cloudflarechina.jpg)  |
|  Cloudflare валодае вялікай сілай.У пэўным сэнсе яны кантралююць тое, што бачыць канчатковы карыстальнік.Вам забаронена праглядаць вэб-сайт з-за Cloudflare. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/onemorestep.jpg) |
|  Cloudflare можна выкарыстоўваць для цэнзуры. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/accdenied.jpg) |
|  Вы не можаце праглядаць воблачны сайт, калі вы карыстаецеся нязначным браўзэрам, для якога Cloudflare можа думаць, што гэта бот (таму што не шмат людзей карыстаецца ім). | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfublock.jpg) |
|  Вы не можаце прайсці гэтую інвазівную "праверку браўзэра", не ўключыўшы Javascript.Гэта марнаванне пяці (і больш) секунд вашага каштоўнага жыцця. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsjsck.jpg) |
|  Cloudflare таксама аўтаматычна блакуе законныя робаты / сканеры, такія як Google, Yandex, Yacy і кліенты API.Cloudflare актыўна адсочвае супольнасць "абыход воблака" з мэтай зламаць законныя робаты даследаванняў. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cftestgoogle.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/htmlalertcloudflare2.jpg) |
|  Cloudflare аналагічна перашкаджае шматлікім людзям з дрэннай падключэннем да Інтэрнэту атрымліваць доступ да вэб-сайтаў, якія знаходзяцца за ім (напрыклад, яны могуць быць за 7+ слаёў NAT або абменьвацца тым жа IP, напрыклад, грамадскім Wi-Fi), калі яны не вырашаюць некалькі малюнкаў CAPTCHA.У некаторых выпадках для задавальнення Google спатрэбіцца ад 10 да 30 хвілін. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/googlerecaptcha.jpg) |
|  У 2020 годзе Cloudflare пераключылася з Google Recaptcha на hCaptcha, паколькі Google мае намер спаганяць плату за яго выкарыстанне.Cloudflare сказаў вам, што яны клапоцяцца пра вашу прыватнасць ("гэта дапамагае вырашаць праблемы прыватнасці"), але гэта, відавочна, хлусня.Гаворка ідзе пра грошы."HCaptcha дазваляе веб-сайтам зарабляць грошы, якія абслугоўваюць гэты попыт, блакуючы боты і іншыя формы злоўжывання" | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fedup_fucking_hcaptcha.jpg) |
|  З пункту гледжання карыстальніка, гэта не моцна змянілася. Вас прымушаюць вырашаць. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_abrv.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/hcaptcha_chrome.jpg) |
|  Cloudflare штодня блакуе шмат людзей і праграмнае забеспячэнне. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsnote.jpg) |
|  Cloudflare раздражняе многіх людзей па ўсім свеце.Паглядзіце на спіс і падумайце, ці добра прыняцце Cloudflare на вашым сайце для карыстацкага досведу. |  ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsstream.jpg) |
|  Якая мэта Інтэрнэту, калі вы не можаце рабіць тое, што хочаце?Большасць людзей, якія наведваюць ваш сайт, проста будуць шукаць іншыя старонкі, калі яны не могуць загрузіць вэб-старонку.Магчыма, вы не актыўна блакуеце наведвальнікаў, але брандмаўэр па змаўчанні Cloudflare дастаткова строгі, каб заблакаваць многіх людзей. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsdroid.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsappl.jpg) |
|  Няма магчымасці вырашыць капчу без ўключэння Javascript і Cookies.Cloudflare выкарыстоўвае іх, каб зрабіць подпіс браўзэра, каб ідэнтыфікаваць вас.Cloudflare павінна ведаць вашу асобу, каб вырашыць, ці можна вам працягваць праглядаць сайт. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1010bsig.jpg) |
|  Карыстальнікі Tor і VPN таксама сталі ахвярай Cloudflare.Абодва рашэнні выкарыстоўваюцца многімі людзьмі, якія не могуць дазволіць сабе цэнзуру ў Інтэрнэце з-за сваёй краіны / карпарацыі / сеткавай палітыкі альбо хочуць дадаць дадатковы пласт для абароны сваёй прыватнасці.Cloudflare бессаромна атакуе гэтых людзей, прымушаючы іх адключыць рашэнне проксі. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn2.jpg) |
|  Калі вы да гэтага часу не спрабавалі Tor, рэкамендуем загрузіць Brow Browser і наведаць любімыя сайты.Мы рэкамендуем вам не ўваходзіць на сайт вашага банка або на ўрадавую старонку, інакш яны будуць пазначаць ваш рахунак. Выкарыстоўвайце VPN для гэтых сайтаў. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/banvpn.jpg) |
|  Вы можаце сказаць: "Тор незаконны! Карыстальнікі Tor злачынствы! Тор дрэнна! ". Не.Вы можаце даведацца пра Tor з тэлебачання, кажучы, што Tor можна выкарыстоўваць для прагляду цёмных сетак і гандлявання зброяй, наркотыкамі або чыд-порна.Хоць вышэй сцвярджаецца, што існуе мноства рынкавых вэб-сайтаў, на якіх можна набыць такія прадметы, але і гэтыя сайты часта з'яўляюцца на clearnet.  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whousetor.jpg) |
|  Tor быў распрацаваны арміяй ЗША, але цяперашні Tor распрацаваны праектам Tor.Ёсць шмат людзей і арганізацый, якія выкарыстоўваюць Tor, уключаючы вашых будучых сяброў.Такім чынам, калі вы выкарыстоўваеце Cloudflare на сваім сайце, вы блакуеце сапраўдных людзей.Вы страціце патэнцыйную дружбу і дзелавыя здзелкі. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iusetor_alith.jpg) |
|  І іх служба DNS 1.1.1.1 таксама фільтруе карыстальнікаў ад наведвання вэб-сайта, вяртаючы падроблены IP-адрас, які належыць Cloudflare, localhost IP, напрыклад "127.0.0.x", альбо проста нічога не вяртае. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cferr1016sp.jpg) |
|  Cloudflare DNS таксама пераадольвае праграмнае забеспячэнне ў Інтэрнэце ад прыкладання для смартфонаў да камп'ютэрнай гульні з-за фальшывага адказу DNS.Cloudflare DNS не можа запытаць некаторыя вэб-сайты банкаў. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfdnsprob.jpg)<br>![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dnsfailtest.jpg) |
|  І тут вы можаце падумаць,<br>Я не выкарыстоўваю Tor ці VPN, чаму я павінен клапаціцца?<br>Я давяраю маркетынгу Cloudflare, чаму мне ўсё роўна<br>Мой сайт https, чаму я павінен клапаціцца | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/annoyed.jpg) |
|  Калі вы наведваеце вэб-сайт, які выкарыстоўвае Cloudflare, вы абменьваецеся сваёй інфармацыяй не толькі ўладальніку сайта, але і Cloudflare.Вось як працуе зваротны проксі. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prism_gfe.jpg) |
|  Немагчыма прааналізаваць без расшыфроўкі трафіку TLS. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelp204144518.jpg) |
|  Cloudflare ведае ўсе вашы дадзеныя, напрыклад, неапрацаваны пароль. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfhelpforum.jpg) |
|  Воблачнае поле можа здарыцца ў любы час. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfbloghtmledit.jpg) |
|  Https Cloudflare ніколі не бывае канца ў канец. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/sniff2.gif) |
|  Вы сапраўды хочаце падзяліцца сваімі дадзенымі з Cloudflare, а таксама 3-лістным агенцтвам? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfstrengthdata.jpg) |
|  Інтэрнэт-профіль карыстальніка Інтэрнэту - гэта "прадукт", які ўрад і буйныя тэхналагічныя кампаніі хочуць набыць. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/federalinterest.jpg) |
|  Аб гэтым паведаміў Міністэрства ўнутранай бяспекі ЗША:<br><br>Ці ведаеце вы, як каштоўныя вашы дадзеныя? Ці ёсць у нас спосаб прадаваць гэтыя дадзеныя?  | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/dhssaid.jpg) |
|  Cloudflare таксама прапануе бясплатную паслугу VPN пад назвай "Cloudflare Warp".Калі вы выкарыстоўваеце яго, усе злучэнні вашага смартфона (ці кампутара) адпраўляюцца на серверы Cloudflare.Cloudflare можа ведаць, які вэб-сайт вы чыталі, які каментарый вы размясцілі, з кім вы размаўлялі і г.д.Вы добраахвотна перадайце ўсю сваю інфармацыю Cloudflare.Калі вы думаеце, "Вы жартуеце? Cloudflare - гэта бяспека. " тады вам трэба даведацца, як працуе VPN. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/howvpnwork.jpg) |
|  Cloudflare сказаў, што іх паслуга VPN хутка зробіць ваш Інтэрнэт.Але VPN робіць ваш інтэрнэт-сувязь павольней, чым наяўнае ў вас. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/notfastervpn.jpg) |
|  Вы ўжо можаце ведаць пра скандал з ПРЫЗАМ.Гэта праўда, што AT&T дазваляе NSA капіяваць усе дадзеныя ў Інтэрнэце для назірання. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/prismattnsa.jpg) |
|  Скажам, вы працуеце ў АНБ і хочаце атрымаць інтэрнэт-профіль кожнага грамадзяніна.Вы ведаеце, што большасць з іх даверліва давяраюць Cloudflare і выкарыстоўваюць яго - толькі адзін цэнтралізаваны шлюз - праксіміраваць падключэнне да сервера сваёй кампаніі (SSH / RDP), асабісты сайт, чат-сайт, вэб-сайт форума, вэб-сайт банка, страхавы сайт, пошукавік, сакрэтны член -толькі сайт, аўкцыённы сайт, крамы, відэа-сайт, сайт NSFW і нелегальны сайт.Вы таксама ведаеце, што яны выкарыстоўваюць службу DNS Cloudflare ("1.1.1.1") і VPN ("Cloudflare Warp") для "Бяспечнай! Хутчэй! Лепш! " Інтэрнэт-досвед.Спалучэнне іх з IP-адрасамі карыстальніка, адбіткам пальцаў, кукі і RAY-ідэнтыфікатарам будзе карысна для стварэння анлайн-профілю мэтавай. | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/edw_snow.jpg) |
|  Вы хочаце, каб іх дадзеныя. Што ты будзеш рабіць? | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/nsaslide_prismcorp.gif) |
|  **Cloudflare - гэта мядовы кашаль.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/honeypot.gif) |
|  **Бясплатны мёд для ўсіх. Некаторыя радкі далучаны.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/iminurtls.jpg) |
|  **Не выкарыстоўвайце Cloudflare.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/shadycloudflare.jpg) |
|  **Дэцэнтралізаваць Інтэрнэт.** | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/cfisnotanoption.jpg) |


---


##    Калі ласка, працягвайце наступную старонку:  "[Воблачная этыка](be.ethics.md)"

---

<details>
<summary>_націсніце на мяне_

## Дадзеныя і дадатковая інфармацыя
</summary>


У гэтым сховішчы знаходзіцца спіс сайтаў, якія стаяць за "Вялікай воблачнай сцяной", якія блакуюць карыстальнікаў Tor і іншых CDN.


**Дадзеныя**
* [Cloudflare Inc.](../cloudflare_inc/)
* [Карыстальнікі Cloudflare](../cloudflare_users/)
* [Дамены Cloudflare](../cloudflare_users/domains/)
* [Карыстальнікі CDN, якія не з'яўляюцца Cloudflare](../not_cloudflare/)
* [Карыстальнікі Anti-Tor](../anti-tor_users/)


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/goodorbad.jpg)


**Больш інфармацыі**
* [Myth Catalog](../subfiles/myth_catalog.md)
* [The Great Cloudwall](../pdf/2019-Jeff_Cliff_Book1.txt), [Mr. Jeff Cliff](https://shitposter.club/users/jeffcliff)
  * Спампаваць: [PDF](../pdf/2019-The_Great_Cloudwall.pdf), [ePUB](../pdf/2019-Jeff_Cliff_The_Great_Cloudwall.epub)
  * Арыгінальную электронную кнігу (ePUB) выдалілі BookRix GmbH з-за парушэння аўтарскіх правоў на матэрыял CC0
* [Padlock icon indicates a secure SSL connection established w MITM-ed](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=831835), Anonymous
* [Block Global Active Adversary Cloudflare](https://trac.torproject.org/projects/tor/ticket/24351), nym-zone
  * Квіток шмат разоў прапагандыраваўся.
  * [Выдалены праектам Tor.](https://lists.torproject.org/pipermail/anti-censorship-team/2020-May/000098.html) [Глядзіце білет 34175.](https://trac.torproject.org/projects/tor/ticket/34175)
  * [Апошні архіўны білет 24351.](https://web.archive.org/web/20200301013104/https://trac.torproject.org/projects/tor/ticket/24351)
* [The problem with Cloudflare](https://neoreddit.horobets.me/post/43), stopCloudflare
* [Cloudflare Watch](http://www.crimeflare.org:82/)
* [Criticism and controversies](https://en.wikipedia.org/wiki/Cloudflare#Criticism_and_controversies), Wikipedia
* [Another landmark day in the war to control, centralize and censor the internet.](https://www.reddit.com/r/privacy/comments/b8dptl/another_landmark_day_in_the_war_to_control/), TheGoldenGoose8888
* [Disadvantage of relying on only one service](https://twitter.com/w3Nicolas/status/1134529316904153089) ([DO is CF](https://www.digwebinterface.com/?hostnames=ns1.digitalocean.com%0D%0Ans2.digitalocean.com%0D%0Ans3.digitalocean.com%0D%0Awww.digitalocean.com&type=A&ns=resolver&useresolver=8.8.4.4&nameservers=))

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/watcloudflare.jpg)


</details>

---

<details>
<summary>_націсніце на мяне_

## Што зробіш?
</summary>

* [Прачытайце наш спіс рэкамендаваных дзеянняў і падзяліцеся ім з сябрамі.](../ACTION.md)

* [Чытайце голас іншага карыстальніка і пішыце свае думкі.](../PEOPLE.md)

* Шукаць што-небудзь: [Ansero](https://ansero.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://ansero.eu.org/)), [Crimeflare \#Search](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/) ([clearnet](https://crimeflare.eu.org/))

* Абнавіце спіс даменаў: [Пералічыце інструкцыі](../INSTRUCTION.md).

* [Дадайце Cloudflare альбо падзею, звязаную з праектам, у гісторыю.](../HISTORY.md)

* [Паспрабуйце і напішыце новы інструмент / сцэнар.](../tool/)

* [Вось некаторыя PDF / ePUB для чытання.](../pdf/)

* [Help translate cloudflare-tor](translateData/instructions.md)


---

### Пра падробленыя рахункі

Крымфляры ведаюць пра наяўнасць фальшывых уліковых запісаў, якія ўвасабляюць нашы афіцыйныя каналы, няхай гэта будзе Twitter, Facebook, Patreon, OpenCollective, Villageges і г.д.
**Мы ніколі не просім вашу электронную пошту.
Мы ніколі не пытаемся ў вашага імя.
Мы ніколі не пытаемся ў вашай асобы.
Мы ніколі не пытаемся пра ваша месцазнаходжанне.
Мы ніколі не просім вашу ахвяраванне.
Мы ніколі не просім вашага агляду.
Мы ніколі не просім вас сачыць у сацыяльных медыя.
Мы ніколі не пытаемся ў вашых сацыяльных медыя.**

# Не давярайце фальшывым рахункам.



---

| 🖼 | 🖼 |
| --- | --- |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/wtfcf.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl2.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/omsirl.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/whydoihavetosolveacaptcha.jpg) |
| ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/fixthedamn.jpg) | ![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/imnotarobot.jpg) |

</details>

---


![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_lb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_dz.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_jb.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_ial.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/twe_eptg.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/eastdakota_1273277839102656515.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/stopcf.jpg)

![](https://codeberg.org/crimeflare/cloudflare-tor/media/branch/master/image/peopledonotthink.jpg)