# FQDN-listo de CDN-uzantoj [ℹ](https://crimeflare.wodferndripvpe6ib4uz4rtngrnzichnirgn7t5x64gxcyroopbhsuqd.onion/ss/pct_zc.php)


[//]: # (do not edit me; start)

## *5,736,237* FQDN

[//]: # (do not edit me; end)


- Dateno pri Cloudflare (kodo "Z3"): [/cloudflare_users/domains](../../cloudflare_users/domains)
- Bonvolu vidi [INSTRUCTION.md](../../INSTRUCTION.md) por dosiera celo kaj formato specifoj.


# Malamikeco kontraŭ Tor (Tor Browser)

- Kombinita rezulto de _Kontraŭ-Tor uzantoj_ kaj _CDN uzantoj_.
- Ni ne povas provizi rezulton Z1 kaj Z5 pro memora limo.

[//]: # (start; table / do not edit me; If necessary please create an issue first)

| CDN-Kodo<br>CDN Code | Nomo<br>Name | % de bazaj domajnoj estas blokitaj<br>% of base domains blocked |
| -------- | -------- | -------- |
| Z2 | Akamai | 39.373 % |
| Z4 | Imperva | 3.563 % |
| Z6 | Microsoft | 2.949 % |
| Z7 | INAP | 1.904 % |
| Z8 | Sucuri | 3.612 % |
| Z9 | Fastly | 2.453 % |

[//]: # (end; table)


-----


# CDN users FQDN list

- Cloudflare data (code "Z3"): [/cloudflare_users/domains](../../cloudflare_users/domains)
- See [INSTRUCTION.md](../../INSTRUCTION.md) for file purpose and format specifications.

# Hostility against Tor (Tor Browser)

- Combined result of _Anti-Tor users_ and _CDN users_.
- We cannot provide Z1/Z5 result due to memory limit.
